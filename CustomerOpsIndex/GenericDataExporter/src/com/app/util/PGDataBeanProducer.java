package com.app.util;

import com.app.base.Constants;
import com.app.base.ModuleInfo;
import com.app.helper.DBConnectionProvider;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.apache.camel.Endpoint;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.beanutils.DynaClass;

public class PGDataBeanProducer implements Processor {
  private String clientName;
  
  private String codeValue;
  
  private String dbName; 
  private String tomcatServer;
  ByteArrayOutputStream bos;
	ObjectOutputStream oos;
	byte[] byteArray;
  
  private HashMap<String, ModuleInfo> moduleQueriesMap;
  
  public PGDataBeanProducer(String clientName, String codeValue, HashMap<String, ModuleInfo> moduleQueriesMap, String dbName, String tomcatServer) {
    this.clientName = clientName;
    this.codeValue = codeValue;
    this.moduleQueriesMap = moduleQueriesMap;
    this.dbName = dbName;
    this.tomcatServer=tomcatServer;
  }
  
  public void process(Exchange exchange) throws Exception {
    Set<String> moduleIds = new HashSet<>();
    ArrayList<String> columns = new ArrayList<>();
    columns.add("CLIENT_NAME");
    columns.add("CODE_VALUE");
    columns.add("DATE_FROM");
    columns.add("DATE_TO");
    String tableName = "";
    moduleIds = this.moduleQueriesMap.keySet();
    Iterator<String> mods = moduleIds.iterator();
    while (mods.hasNext()) {
      String modId = mods.next();
      columns.addAll(((ModuleInfo)this.moduleQueriesMap.get(modId)).getColumns());
      tableName = ((ModuleInfo)this.moduleQueriesMap.get(modId)).getTableName();
    } 
    ArrayList<DynaBean> data = new ArrayList<>();
    DynaClass dClass = BeanUtil.createBeanGenericProperty(columns,Object.class);
    DynaBean clientData = null;
    if (!Constants.isMultiRowData) {
      clientData = dClass.newInstance();
      clientData.set("CLIENT_NAME", this.clientName);
      clientData.set("CODE_VALUE", this.codeValue);
      clientData.set("DATE_FROM", Constants.dateFrom);
      clientData.set("DATE_TO", Constants.dateTo);
    } 
    Iterator<String> modulesIt = moduleIds.iterator();
    while (modulesIt.hasNext()) {
      String moduleId = modulesIt.next();
      Map<String, Map<String, String>> queriesMap = ((ModuleInfo)this.moduleQueriesMap.get(moduleId)).getQueriesMap();
      Iterator<String> attributesIt = queriesMap.keySet().iterator();
      while (attributesIt.hasNext()) {
        String attributeId = attributesIt.next();
        Map<String, String> qMap = queriesMap.get(attributeId);
        String query = qMap.get("QUERY");
        query = prepareQuery(query, this.dbName);
        Endpoint dbConnection = null;
        		
        if("1958991521".equals(tomcatServer))
        	if(DBConnectionProvider.getInstance().getDBConnection("PostGresApac")!=null)
        		dbConnection=DBConnectionProvider.getInstance().getDBConnection("PostGresApac");
        	else
        	{
        		dbConnection=DBConnectionProvider.getInstance().getDBConnection("PostGres");
        	}
        else
        	dbConnection=DBConnectionProvider.getInstance().getDBConnection("PostGres");
        try {
          HashMap<String, Object> results = null;
          System.out.println(" executing primary query :  " + query);
          results = QueryUtil.executeQuery(dbConnection, query);
          boolean isFailed = ((Boolean)results.get("IS_FAILED")).booleanValue();
          if (isFailed)
            if (qMap.containsKey("SECONDARY_QUERY") && qMap.get("SECONDARY_QUERY") != null) {
              String secondaryQuery = qMap.get("SECONDARY_QUERY");
              secondaryQuery = prepareQuery(secondaryQuery, this.dbName);
              System.out.println(" executing secondary query :  " + secondaryQuery);
              results = QueryUtil.executeQuery(dbConnection, secondaryQuery);
            }
          if("EAVDATA".equals(qMap.get("TYPE")))
			{
            /******/
          if (results != null && !((Boolean)results.get("IS_FAILED")).booleanValue()) {
            Set<String> cols = (Set<String>)results.get("COLUMN_NAMES");
            ArrayList<DynaBean> dataBeans = (ArrayList<DynaBean>)results.get("DATA");
            if (dataBeans != null && dataBeans.size() > 0 && cols != null) {
              Iterator<DynaBean> it = dataBeans.iterator();
              while (it.hasNext()) {
                DynaBean bean = it.next();
                if (Constants.isMultiRowData) {
                  clientData = dClass.newInstance();
                  clientData.set("CLIENT_NAME", this.clientName);
                  clientData.set("CODE_VALUE", this.codeValue);
                  clientData.set("DATE_FROM", Constants.dateFrom);
                  clientData.set("DATE_TO", Constants.dateTo);
                } 
                Iterator<String> colsIt = cols.iterator();
                while (colsIt.hasNext()) {
                  String col = colsIt.next();
                  clientData.set(col, bean.get(col));
                } 
                if (Constants.isMultiRowData)
                  data.add(clientData); 
              } 
            } 
          } 
          /*********/
			}
          else // if EAVHLDATA
          {


				
				{
					System.out.println("Is Failed for high level data "+results.get("IS_FAILED")+", for column name "+qMap.get("COLUMN_NAMES"));
					
					if(results!= null && !(boolean)results.get("IS_FAILED"))
					{
						Set<String> cols = (Set<String>)results.get("COLUMN_NAMES");
						ArrayList<DynaBean> dataBeans = (ArrayList<DynaBean>) results.get("DATA");
				    	if(dataBeans != null && dataBeans.size() > 0 && cols !=null)
				    	{	

					    		//DynaBean bean = it.next();

					    		
					    		//Iterator<String> colsIt = cols.iterator();

					    			//String col = colsIt.next();
					    			ArrayList<HashMap<String,Object>> beanHM = beanToHashMapConverter(dataBeans,cols);
					    			
					    			//ObjectOutputStream oos=new ObjectOutputStream();
//					    			bos = new ByteArrayOutputStream();
//					    	        oos = new ObjectOutputStream(bos);
//					    	        oos.writeObject(beanHM);
//					    	        byteArray = bos.toByteArray();
					    			System.out.println("column name ::::::::"+qMap);
					    			

					    			clientData.set(qMap.get("COLUMN_NAMES"), beanHM);

					    		
					    		
					    		if(Constants.isMultiRowData)
					    		{
					    			data.add(clientData);
					    		}
					    		
				    		
				    	}
				    	
					}
					
				}
				
			
        	  
          }
        } catch (Exception ex) {
          ex.printStackTrace();
          System.out.println(" ex occured for " + query + " client :" + this.clientName);
        } 
      } 
    } 
    Message msg = exchange.getIn();
    if (Constants.isMultiRowData) {
      msg.setBody(data);
    } else {
      msg.setBody(clientData);
    } 
    msg.setHeader("COLUMNS", columns);
    msg.setHeader("TABLE_NAME", tableName);
    exchange.setOut(msg);
  }
  
  public static String prepareQuery(String query, String dbName) {
    if (query.contains("@@dateFrom"))
      query = query.replace("@@dateFrom", Constants.dateFrom); 
    if (query.contains("@@dateTo"))
      query = query.replace("@@dateTo", Constants.dateTo); 
    if(query.contains("@@dbName"))
    	query = query.replace("@@dbName", "\"" + dbName + "\"");
    /*
    if (query.contains("entities "))
      query = query.replace("entities ", "\"" + dbName + "\".entities "); 
    if (query.contains("entities_relations "))
      query = query.replace("entities_relations ", "\"" + dbName + "\".entities_relations "); 
    if (query.contains("entity_attribute_values_admin "))
      query = query.replace("entity_attribute_values_admin ", "\"" + dbName + "\".entity_attribute_values_admin "); 
    if (query.contains("entity_attribute_values_audit "))
      query = query.replace("entity_attribute_values_audit ", "\"" + dbName + "\".entity_attribute_values_audit "); 
    if (query.contains("entity_attribute_values_audit_logs "))
      query = query.replace("entity_attribute_values_audit_logs ", "\"" + dbName + "\".entity_attribute_values_audit_logs "); 
    if (query.contains("entity_attribute_values_audit_views "))
      query = query.replace("entity_attribute_values_audit_views ", "\"" + dbName + "\".entity_attribute_values_audit_views "); 
    if (query.contains("entity_attribute_values_calendar "))
      query = query.replace("entity_attribute_values_calendar ", "\"" + dbName + "\".entity_attribute_values_calendar "); 
    if (query.contains("entity_attribute_values_forms "))
      query = query.replace("entity_attribute_values_forms ", "\"" + dbName + "\".entity_attribute_values_forms "); 
    if (query.contains("entity_attribute_values_tasks "))
      query = query.replace("entity_attribute_values_tasks ", "\"" + dbName + "\".entity_attribute_values_tasks "); 
    if (query.contains("entity_attributes "))
      query = query.replace("entity_attributes ", "\"" + dbName + "\".entity_attributes "); 
    if (query.contains("entity_attribute_values "))
      query = query.replace("entity_attribute_values ", "\"" + dbName + "\".entity_attribute_values "); */
    return query;
  }
  
  
	private ArrayList<HashMap<String, Object>> beanToHashMapConverter(ArrayList dynaBeans, Set<String> cols)  {
		ArrayList<HashMap<String, Object>> list = new ArrayList();
		HashMap<String, Object> hm;
		Iterator<DynaBean> it= dynaBeans.iterator();
		while (it.hasNext())
		{
			hm=new HashMap<String, Object>();
			DynaBean bean= it.next();
			for(String col:cols)
			{
				//String col = colsIt.next();
				hm.put(col, bean.get(col));
				
			}
			
			list.add( hm);
			
		}
		// TODO Auto-generated method stub
		return list;
	}
  
}
