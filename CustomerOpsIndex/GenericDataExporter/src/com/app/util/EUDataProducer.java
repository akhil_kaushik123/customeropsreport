package com.app.util;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.camel.CamelContext;
import org.apache.camel.Endpoint;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.camel.ProducerTemplate;

import com.app.base.BillingReportBean;
import com.app.base.ClientInfo;


/*
 * EU Countries Data Fetch | FCSKYS-17758
 */
public class EUDataProducer implements Processor
{
	private String clientName ;
	//private String codeValue ;
	//private String billableLevel;
	private ClientInfo dbProps;
	
	public EUDataProducer(String clientName,ClientInfo dbProps) 
	{
		this.clientName = clientName;
		this.dbProps= dbProps;
	}
	
	@Override
	public void process(Exchange exchange) throws Exception 
	{	
		String codeValue = dbProps.getCodeValue();
		String billableLevel = dbProps.getBillableLevel();
		String clientName = dbProps.getClientName();
		HashMap<String,HashMap<String,HashMap<String,String>>> EUMap=null;
		
		try
		{	
			CamelContext context = exchange.getContext();
			Endpoint endpoint = context.getEndpoint("direct:"+clientName);
			ProducerTemplate template = context.createProducerTemplate();
			
			//EUMap =UserBillingSystemUsageUtility.getEUData(template, endpoint, exchange,billableLevel,codeValue,clientName);
			//System.out.println(EUMap);
			BillingReportBean report = new BillingReportBean();
			
			report.setDbName(UserBillingSystemUsageUtility.getDbName(template, endpoint, exchange));
			report.setDateFrom(UserBillingSystemUsageUtility.dateFrom);
			report.setDateTo(UserBillingSystemUsageUtility.dateTo);
			
			if(EUMap!=null)
			{
				report.setClientName(clientName);
				report.setCodeValue(codeValue);
				report.setEUMap(EUMap);	
			}
			Message msg = exchange.getIn();
			msg.setBody(report);
			exchange.setOut(msg);
			//System.out.println(clientName+"**************"+EUMap);
		}
		catch(Exception ex)
		{
			System.out.println(" exception in Report Bean Generator fot endpoint : "+clientName);
			ex.printStackTrace();
		}
		finally
		{
			EUMap=null;
		}
		
		
	}
}