/*
 *  FCSKYS-18302 | Users Fetching based on their Roles 
 */
package com.app.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.camel.CamelContext;
import org.apache.camel.Endpoint;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.camel.ProducerTemplate;

import com.app.base.BillingReportBean;
import com.app.base.ClientInfo;
import com.app.base.Constants;
import com.app.helper.DBConnectionProvider;


public class SystemUsageUserBillingDataProducer implements Processor
{
	private String clientName ;
	//private String codeValue ;
	//private String billableLevel;
	private ClientInfo dbProps;
	
	public SystemUsageUserBillingDataProducer(String clientName,ClientInfo dbProps) 
	{
		this.clientName = clientName;
		this.dbProps= dbProps;
	}
	
	@Override
	synchronized public void process(Exchange exchange) throws Exception 
	{	
		String codeValue = dbProps.getCodeValue();
		String billableLevel = dbProps.getBillableLevel();
		String clientName = dbProps.getClientName();
		String hostURL=dbProps.getHostURL();
		String fromdate=Constants.dateFrom;
		String todate=Constants.dateTo;
		String specialReport=Constants.specialReport;
		HashMap<String,HashMap<String,HashMap<String,String>>> locationMap=null;
		Map<String, Object> brightStarCarReportMap=null;
		synchronized(this)
		{
		try
		{	
			CamelContext context = exchange.getContext();
			/*Endpoint endpoint = context.getEndpoint("direct:"+clientName);*/
			Endpoint endpoint =DBConnectionProvider.getInstance().getDBConnection(clientName); 
			ProducerTemplate template = context.createProducerTemplate();
			synchronized(exchange)
			{
			if ("brightstar".equals(codeValue) && "Y".equals(specialReport)) {
			brightStarCarReportMap  = UserBillingSystemUsageUtility.getBillingReport(fromdate,todate,endpoint,exchange,template);
			}
			}
			locationMap =UserBillingSystemUsageUtility.getLocationData(template, endpoint, exchange,billableLevel,codeValue,clientName,hostURL);
			BillingReportBean report = new BillingReportBean();
			
			report.setDbName(UserBillingSystemUsageUtility.getDbName(template, endpoint, exchange));
			report.setDateFrom(UserBillingSystemUsageUtility.dateFrom);
			report.setDateTo(UserBillingSystemUsageUtility.dateTo);
			report.setHostURL(dbProps.getHostURL());
			if(locationMap!=null)
			{
				report.setClientName(clientName);
				report.setCodeValue(codeValue);
				report.setLocationMap(locationMap);	
				if("brightstar".equals(codeValue) && "Y".equals(specialReport)){
					report.setBrightStarCarReportMap(brightStarCarReportMap);	
			}
			}
			
			
			
			
			Message msg = exchange.getIn();
			msg.setBody(report);
			exchange.setOut(msg);
			System.out.println("report : "+clientName );
		}
		catch(Exception ex)
		{
			System.out.println(" exception in Report Bean Generator fot endpoint : "+clientName);
			ex.printStackTrace();
		}
		finally
		{
			locationMap=null;
		}
		}
		
	}
}