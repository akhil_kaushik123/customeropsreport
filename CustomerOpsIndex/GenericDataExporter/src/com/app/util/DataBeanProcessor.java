package com.app.util;

import java.util.ArrayList;
import java.util.Iterator;

import org.apache.camel.Endpoint;
import org.apache.camel.Exchange;
import org.apache.commons.beanutils.DynaBean;

import com.app.base.Constants;
import com.app.helper.DBConnectionProvider;

public class DataBeanProcessor {

	public static void processBean(Exchange exchange) 
	{	
		Object data = exchange.getOut().getBody();
		if(data instanceof DynaBean)
		{
			DynaBean usageBean = (DynaBean)data;
			System.out.println(" usaage bean "+usageBean);
			Endpoint connection = DBConnectionProvider.getInstance().getDBConnection("InHouse");
			ArrayList<String> columns = (ArrayList<String>) exchange.getOut().getHeader("COLUMNS");
			String tableName = (String)exchange.getOut().getHeader("TABLE_NAME");
			if(Constants.isUpdateThread)
			{
				QueryUtil.updateRecord(connection, tableName, columns, usageBean);
			}
			else
			{
				System.out.println("Come here for "+tableName);
				QueryUtil.executeInsert(connection,tableName,columns,usageBean);
			}
		}
		else if(data instanceof ArrayList)
		{
			Iterator<DynaBean> beansIt = ((ArrayList) data).iterator();
			while(beansIt.hasNext())
			{

				DynaBean usageBean = (DynaBean)beansIt.next();
				System.out.println(" usaage bean "+usageBean);
				Endpoint connection = DBConnectionProvider.getInstance().getDBConnection("InHouse");
				ArrayList<String> columns = (ArrayList<String>) exchange.getOut().getHeader("COLUMNS");
				String tableName = (String)exchange.getOut().getHeader("TABLE_NAME");
				System.out.println(" upd dddd  : "+ Constants.isUpdateThread);
				if(Constants.isUpdateThread)
				{
					QueryUtil.updateRecord(connection, tableName, columns, usageBean);
				}
				else
				{
					//System.out.println("Come here for "+tableName);
					QueryUtil.executeInsert(connection,tableName,columns,usageBean);
				}
			}
		}
	}
}
