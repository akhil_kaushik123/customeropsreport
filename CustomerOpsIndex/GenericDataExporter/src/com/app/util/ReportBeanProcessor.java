package com.app.util;

import org.apache.camel.CamelContext;
import org.apache.camel.Endpoint;
import org.apache.camel.Exchange;
import org.apache.camel.ProducerTemplate;

import com.app.base.BillingReportBean;
import com.app.helper.DBConnectionProvider;

public class ReportBeanProcessor {
	
	
	public static void processBean(Exchange exchange)
	{	
		System.out.println(" inserting ");
		BillingReportBean report = null;
		try
		{	Object obj = exchange.getOut().getBody();
			if(obj instanceof BillingReportBean)
			{
				report = (BillingReportBean)obj;
				CamelContext context = exchange.getContext();
				
				Endpoint endpoint =DBConnectionProvider.getInstance().getDBConnection("InHouse"); 
				
				
				//Endpoint endpoint = context.getEndpoint("direct:fss");
				Exchange doExchange = endpoint.createExchange();
				ProducerTemplate template = context.createProducerTemplate();
				
				StringBuffer insertQuery = new StringBuffer("INSERT INTO USER_BILLING_REPORT(BUILD_NAME,CODE_VALUE,DB_NAME,DATE_FROM,DATE_TO,ACTIVE_CU,TEST_CU,ACTIVE_RU,TEST_RU,ACTIVE_FU,TEST_FU,ACTIVE_DU,TEST_DU,ACTIVE_LOCATIONS,UNIQUE_CALS,EXTRA_CALS,TOTAL_CALS_UNIQUE,FDD_SENT,TOTAL_SYSTEM_TIME,ADMIN_TIME,P_TIME,ACTIVE_SU,TEST_SU,ACTIVE_VU,TEST_VU,VENDOR_LOGIN_LOGS , LOGGED_IN_CU,LOGGED_IN_RU,LOGGED_IN_FU,LOGGED_IN_DU,LOGGED_IN_HRS_CU,LOGGED_IN_HRS_RU,LOGGED_IN_HRS_FU,LOGGED_IN_HRS_DU) VALUES(");
				insertQuery.append("\""+report.getClientName()+"\" , ");
				insertQuery.append("\""+report.getCodeValue()+"\" , ");										
				
				insertQuery.append("\""+report.getDbName()+"\" , ");
				
				insertQuery.append("\""+report.getDateFrom()+"\" , ");
				insertQuery.append("\""+report.getDateTo()+"\" , ");
				
				insertQuery.append("\""+report.getActiveCU()+"\" , ");
				insertQuery.append("\""+report.getTestCU()+"\" , ");
				insertQuery.append("\""+report.getActiveRU()+"\" , ");
				insertQuery.append("\""+report.getTestRU()+"\" , ");
				insertQuery.append("\""+report.getActiveFU()+"\" , ");
				insertQuery.append("\""+report.getTestFU()+"\" , ");
				
				insertQuery.append("\""+report.getActiveDU()+"\" , ");
				insertQuery.append("\""+report.getTestDU()+"\" , ");
				
				insertQuery.append("\""+report.getActiveCals()+"\" , ");
				insertQuery.append("\""+report.getUniqueCals()+"\" , ");
				insertQuery.append("\""+report.getExtraCals()+"\" , ");
				insertQuery.append("\""+report.getTotalCals()+"\" , ");
				
				insertQuery.append("\""+report.getFddCount()+"\" , ");
				insertQuery.append("\""+report.getTotalSystemTime()+"\" , ");
				
				insertQuery.append("\""+report.getAdminSystemTime()+"\" , ");
				insertQuery.append("\""+report.getNetSystemTime()+"\"");
				
				insertQuery.append(",'0','0','0','0','0' , ");
				insertQuery.append("\""+report.getLoggedInCU()+"\" , ");
				insertQuery.append("\""+report.getLoggedInRU()+"\" , ");
				insertQuery.append("\""+report.getLoggedInFU()+"\" , ");
				insertQuery.append("\""+report.getLoggedInDU()+"\" , ");
				insertQuery.append("\""+report.getLoggedInHrsCU()+"\" , ");
				insertQuery.append("\""+report.getLoggedInHrsRU()+"\" , ");
				insertQuery.append("\""+report.getLoggedInHrsFU()+"\" , ");
				insertQuery.append("\""+report.getLoggedInHrsDU()+"\" ) ");
				
				
				doExchange.getIn().setBody(insertQuery);
				
				Exception excpetion = template.send(endpoint, doExchange).getException();
				if(excpetion != null)
				{
					System.out.println("client Name : "+report.getClientName()+" query for insert isFailed "+excpetion.getMessage());
					excpetion.printStackTrace();
				}
				System.out.println("client Name : "+report.getClientName()+" query for insert "+insertQuery);
				
			}
			else
			{
				//System.out.println(" not inserted");
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			report = null;
		}
	}
}
