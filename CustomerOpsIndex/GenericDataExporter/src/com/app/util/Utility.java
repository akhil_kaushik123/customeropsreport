package com.app.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Properties;

import com.app.base.Constants;

public class Utility {


	public static boolean isValidDate(String inDate) 
	{
	    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	    dateFormat.setLenient(false);
	    try {
	      dateFormat.parse(inDate.trim());
	      System.out.println(dateFormat.parse(inDate.trim()));
	    } catch (ParseException pe) {
	      return false;
	    }
	    return true;
	}
	
	public static PrintStream setOutputToLogFile()
	{
		PrintStream out = null;
		try 
		{
			out = new PrintStream(new FileOutputStream(Constants.filePath+Constants.fileName));
			System.setOut(out);
		}
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		}
		return out;
		
	}
	
	public static Properties readProperties(String file)
	{
		Properties props = new Properties();
		try
		{ 
			
			InputStream ip = new FileInputStream(file);
			System.out.println(ip);
			props.load(ip);
		} 
		catch (Exception ex) {
		    ex.printStackTrace();
		}
		return props;
	}
	
	
}

