/*
 * (FCSKYS-15522) : Bhavna Chugh : Inhouse Billing : //Changes in query For UFG For Multiple Regions
 * (FCSKYS-15522) : Bhavna Chugh : Inhouse Billing : //Changes in query For UFG For Multiple Regions
 * (FCSKYS-15522) : Bhavna Chugh : Inhouse Billing : //Checks For Test Locations
 * FCSKYS-16290 | Add new Column 'Logged Insight Users' | Fetch data not for 'franconnect or Test' locations
 *  FCSKYS-15522  | Implemented Billing Changes
 *  FCSKYS-18245 | Automate MUID Reports
 *	FCSKYS-18302 | Users Fetching based on their Roles 
 *  	FCSKYS-18301 | Merging of users
 *  (FCSKYS-18304) Special Report in the BrightStar for Cars Report
 *  (FCSKYS-18765) Automated report of LightBridge Academy
 *  (FCSKYS-19457) SMCZcubator billing getting stuck
 */

package com.app.util;

import java.io.File;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import javax.sql.DataSource;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.camel.CamelContext;
import org.apache.camel.Endpoint;
import org.apache.camel.Exchange;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.app.base.ClientInfo;
import com.app.base.Constants;
import com.app.helper.DBConnectionProvider;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

public class UserBillingSystemUsageUtility {
	public static String dateFrom;
	public static String dateTo;

	public static String getDbName(ProducerTemplate template, Endpoint endpoint, Exchange exchange) {
		String dbName = "";
		String query2 = "SELECT DATA_VALUE FROM MASTER_DATA WHERE DATA_TYPE = '111311' limit 1";
		String query = "SELECT DATA_VALUE FROM MASTER_DATA_FOR_SCHEDULER WHERE DATA_TYPE=10009";
		try {
			exchange = endpoint.createExchange();
			exchange.getIn().setBody(query);
			Exchange out = template.send(endpoint, exchange);
			List<Map<String, Object>> result = out.getOut().getBody(List.class);
			if (result != null) {
				int i = result.size();
				for (int j = 0; j < i; j++) {
					Map<String, Object> row = result.get(j);
					if (row.get("DATA_VALUE") != null && !"".equals((String) row.get("DATA_VALUE"))) {
						dbName = (String) row.get("DATA_VALUE");
					}
				}
			}
			if (dbName == null || "".equals(dbName) || "null".equals(dbName)) {
				exchange = endpoint.createExchange();
				exchange.getIn().setBody(query2);
				Exchange out2 = template.send(endpoint, exchange);
				List<Map<String, Object>> result2 = out2.getOut().getBody(List.class);
				if (result2 != null) {
					int i2 = result2.size();
					for (int j2 = 0; j2 < i2; j2++) {
						Map<String, Object> row2 = result2.get(j2);
						dbName = (String) row2.get("DATA_VALUE");
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		/*if (dbName.contains(" ")) {
			dbName = dbName.replaceAll(" ", "_");
		}
		if (dbName.contains("'")) {
			dbName = dbName.replaceAll("'", "");
		}*/
		return dbName;
	}

	public static String searchDisplayGMTString(String sDBStyleValue) {
		String[] timeZoneDisplayStrings = getTimeZoneDisplayStrings();
		String[] timeZoneIds = getTimeZoneIds();
		int n = timeZoneIds.length;
		for (int i = 0; i < n; i++) {
			if (timeZoneIds[i].equals(sDBStyleValue)) {
				return timeZoneDisplayStrings[i]; // Bothe the string arrays are
			}
		}
		return "";
	}

	public static String[] getTimeZoneIds() {
		String[] timeZoneIds = new String[] {
				"GMT", "GMT+00:30", "GMT+01:00", "GMT+01:30", "GMT+02:00", "GMT+02:30", "GMT+03:00", "GMT+03:30",
				"GMT+04:00", "GMT+04:30", "GMT+05:00", "Asia/Kolkata", "GMT+06:00", "GMT+06:30", "GMT+07:00",
				"GMT+07:30", "GMT+08:00", "GMT+08:30", "GMT+09:00", "GMT+09:30", "GMT+10:00", "GMT+10:30", "GMT+11:00",
				"GMT+11:30", "GMT+12:00", "GMT-12:00", "GMT-11:30", "GMT-11:00", "GMT-10:30", "GMT-10:00", "GMT-09:30",
				"GMT-09:00", "GMT-08:30", "US/Pacific", // P_INT_B_52986 by
																					// by
				"GMT-07:30", "US/Mountain", "GMT-06:30", "US/Central", "GMT-05:30", "US/Eastern", // P_INT_B_52986
				"GMT-04:30", "GMT-04:00", "GMT-03:30", "GMT-03:00", "GMT-02:30", "GMT-02:00", "GMT-01:30", "GMT-01:00",
				"GMT-00:30" };
		return timeZoneIds;
	}

	public static String[] getTimeZoneDisplayStrings() {
		String[] timeZoneDisplayStrings = new String[] {
				"GMT +00:00 Britain, Ireland, Portugal, Western Africa", "GMT +00:30 ",
				"GMT +01:00 Western Europe, Central Africa", "GMT +01:30 ", "GMT +02:00 Eastern Europe, Eastern Africa",
				"GMT +02:30 ", "GMT +03:00 Russia, Saudi Arabia", "GMT +03:30 ", "GMT +04:00 Arabian", "GMT +04:30 ",
				"GMT +05:00 West Asia, Pakistan", "GMT +05:30 India", "GMT +06:00 Central Asia", "GMT +06:30 ",
				"GMT +07:00 Bangkok, Hanoi, Jakarta", "GMT +07:30 ", "GMT +08:00 China, Singapore, Taiwan",
				"GMT +08:30 ", "GMT +09:00 Korea, Japan", "GMT +09:30 Central Australia",
				"GMT +10:00 Eastern Australia", "GMT +10:30 ", "GMT +11:00 Central Pacific", "GMT +11:30 ",
				"GMT +12:00 Fiji, New Zealand", "GMT -12:00 DATELINE ", "GMT -11:30 ", "GMT -11:00 Samoa",
				"GMT -10:30 ", "GMT -10:00 Hawaiian", "GMT -09:30 ", "GMT -09:00 Alaska/Pitcairn Islands", // P_INT_B_52986
																											// by
				"GMT -08:30 ", "GMT -08:00 US/Canada/Pacific", // 8:00
				"GMT -07:30 ", "GMT -07:00 US/Canada/Mountain/Arizona", // P_E_CAL_TIMEZONE
				"GMT -06:30 ", "GMT -06:00 US/Canada/Central", // 6:00
				"GMT -05:30 ", "GMT -05:00 US/Canada/Eastern", // 5:00
				"GMT -04:30 ", // P_INT_B_52986 by Ankit Saini on 30/11/2009 end
				"GMT -04:00 Bolivia, Western Brazil, Chile, Atlantic", "GMT -03:30 Newfoundland",
				"GMT -03:00 Argentina, Eastern Brazil, Greenland", "GMT -02:30 ", "GMT -02:00 Mid-Atlantic",
				"GMT -01:30 ", "GMT -01:00 Azores/Eastern Atlantic", "GMT -00:30 " }; // P_B_ADMIN_7458
		return timeZoneDisplayStrings;
	}

	private static String getChildElementContent(Element e, String childName) {
		NodeList children = e.getElementsByTagName(childName);
		if (children.getLength() > 0) {
			return children.item(0).getTextContent();
		}
		return "";
	}

	synchronized public static java.util.Map<String, Object> getBillingReport(String fromDate, String toDate,
			Endpoint endpoint, Exchange exchange, ProducerTemplate template) {
		java.util.Map<String, Object> map = new java.util.HashMap<String, Object>();
		try {
			int parentLocationCount = 0;
			int childLocationCount = 0;
			int totalLocations = 0;
			int orphanLocations = 0;
			String franchiseeNo = "";
			int childUsers = 0;
			int parentUsers = 0;
			int commonUserCount = 0;
			String franIdsNos = "";
			Map<String, Object> row = null;
			Map<String, Object> row1 = null;
			toDate = toDate + " 00:00:00";
			fromDate = fromDate + " 23:59:59";
			System.out.println("Bright Start" + fromDate + "*********88" + toDate);
			java.util.HashSet<String> parentSet = new java.util.HashSet<String>();
			HashMap<String, Object> results = new HashMap<String, Object>();
			List<Map<String, Object>> result;
			List<Map<String, Object>> result1;
			Exchange out;
			java.util.HashSet<String> franchiseeNoSetChild = new java.util.HashSet<String>();
			java.util.HashSet<String> franchiseeNoSetParent = new java.util.HashSet<String>();
			java.util.HashSet<String> franchiseeNoSetOrphan = new java.util.HashSet<String>();
			java.util.HashSet<String> parentLocationNameSet = new java.util.HashSet<String>();
			java.util.HashSet<String> childLocationNameSet = new java.util.HashSet<String>();
			java.util.HashSet<String> orphaonLocationNameSet = new java.util.HashSet<String>();
			java.util.Map<String, String> childPerParent = new java.util.HashMap<String, String>();
			java.util.Map<String, Integer> childLocationWiseUser = new java.util.HashMap<String, Integer>();
			java.util.Map<String, Integer> parentLocationWiseUser = new java.util.HashMap<String, Integer>();
			java.util.Map<String, Integer> orphanLocationWiseUser = new java.util.HashMap<String, Integer>();
			java.util.Map<String, java.util.Set<String>> childParentFranchiseeNames = new java.util.HashMap<String, java.util.Set<String>>();
			java.util.Set<String> tempSet = null;
			StringBuilder location = new StringBuilder(
					"SELECT FRANCHISEE.FRANCHISEE_NO AS FRANCHISEE_NO ,FRANCHISEE_NAME, PARENT_ID, ABS_FRANCHISEE_NO ");
			StringBuilder locationNos = new StringBuilder("SELECT GROUP_CONCAT(FRANCHISEE_NO) AS FRANCHISEE_NO ");
			StringBuilder whereQuery = new StringBuilder(
					" FROM FRANCHISEE WHERE (IS_FRANCHISEE='Y' AND STATUS IN (0,4) " + "AND DEACTIVATION_DATE >= '")
							.append(fromDate).append("' AND DEACTIVATION_DATE <= '").append(toDate)
							.append("') OR (IS_FRANCHISEE='Y' " + "AND STATUS IN (1,3) AND CREATION_DATE <= '")
							.append(toDate)
							.append("') OR (IS_FRANCHISEE='Y' AND STATUS IN (0,4) " + "AND DEACTIVATION_DATE > '")
							.append(toDate).append("' AND CREATION_DATE <= '").append(toDate).append("') ");
			location.append(whereQuery);
			locationNos.append(whereQuery);
			result = QueryUtil.executeQuerySelect(endpoint, location.toString());
			int locationQueryi = result.size();
			HashMap<String, String> activeLocationMap = null;
			String parentId = "";
			for (int j = 0; j < locationQueryi; j++) {
				row = result.get(j);
				totalLocations++;
				parentId = row.get("PARENT_ID") + "";
				franchiseeNo = row.get("FRANCHISEE_NO") + "";
				if (!"0".equals(parentId) && !"7777".equals(parentId) && !"NULL".equalsIgnoreCase(parentId)
						&& !parentId.equals("")) {
					parentSet.add(parentId);
				}
				franchiseeNoSetChild.add(franchiseeNo);
				childLocationNameSet.add(row.get("FRANCHISEE_NAME") + "");
			}
			result1 = QueryUtil.executeQuerySelect(endpoint, locationNos.toString());
			locationQueryi = result1.size();
			for (int j = 0; j < locationQueryi; j++) {
				row = result1.get(0);
				franIdsNos = row.get("FRANCHISEE_NO") + "";
			}
			if (parentSet != null && !parentSet.isEmpty()) {
				parentLocationCount = parentSet.size();
			}
			childLocationCount = totalLocations - parentLocationCount;
			for (String parentAbsNo : parentSet) {
				String franchiseeName = "";
				String query = "SELECT FRANCHISEE_NAME, FRANCHISEE_NO FROM FRANCHISEE WHERE ABS_FRANCHISEE_NO='"
						+ parentAbsNo + "'";
				String query1 = "SELECT COUNT(*) AS COUNT FROM FRANCHISEE WHERE PARENT_ID='" + parentAbsNo + "'";
				String query12 = "SELECT FRANCHISEE_NAME FROM FRANCHISEE WHERE PARENT_ID='" + parentAbsNo + "'";
				result = QueryUtil.executeQuerySelect(endpoint, query);
				locationQueryi = result.size();
				List<Map<String, Object>> result12 = QueryUtil.executeQuerySelect(endpoint, query12);
				int size = result12.size();
				for (int j = 0; j < locationQueryi; j++) {
					row = result.get(j);
					franchiseeNoSetChild.remove(row.get("FRANCHISEE_NO") + "");
					childLocationNameSet.remove(row.get("FRANCHISEE_NAME") + "");
					franchiseeName = row.get("FRANCHISEE_NAME") + "";
					franchiseeNoSetParent.add(row.get("FRANCHISEE_NO") + "");
					parentLocationNameSet.add(row.get("FRANCHISEE_NAME") + "");
					tempSet = new java.util.LinkedHashSet<String>();
					for (int i = 0; i < size; i++) {
						row1 = result12.get(i);
						tempSet.add(row1.get("FRANCHISEE_NAME") + "");
					}
					if (franchiseeName.length() > 0) {
						childParentFranchiseeNames.put(row.get("FRANCHISEE_NAME") + "", tempSet);
					}
				}
				result12 = QueryUtil.executeQuerySelect(endpoint, query1);
				row1 = result12.get(0);
				int size1 = result12.size();
				for (int k = 0; k < size1; k++) {
					childPerParent.put(franchiseeName, row1.get("COUNT") + "");
				}
			}
			for (String childFranNos : franchiseeNoSetChild) {
				String query2 = "SELECT FRANCHISEE_NO, FRANCHISEE_NAME FROM FRANCHISEE WHERE FRANCHISEE_NO='"
						+ childFranNos + "' AND (PARENT_ID=0 OR PARENT_ID=7777 OR PARENT_ID IS NULL) ";
				result = QueryUtil.executeQuerySelect(endpoint, query2);
				locationQueryi = result.size();
				if (locationQueryi > 0) {
					for (int j = 0; j < locationQueryi; j++) {
						row.clear();
						row = result.get(j);
						orphanLocations++;
						childLocationCount--;
						orphaonLocationNameSet.add(row.get("FRANCHISEE_NAME") + "");
						franchiseeNoSetOrphan.add(row.get("FRANCHISEE_NO") + "");
					}
				}
			}
			StringBuilder userQuery = new StringBuilder(
					"SELECT U.USER_IDENTITY_NO AS FRANCHISEE_USER_NO, F.FRANCHISEE_NAME, U.ABS_USER_NO AS ABS_USER_NO, U.ABS_FRANCHISEE_NO, U.FRANCHISEE_NO AS FRANCHISEE_NO "
							+ "FROM FRANCHISEE F,USERS U  LEFT JOIN ( SELECT MAX(CHANGE_DATE) AS CHANGE_DATE,USER_NO,REMARK FROM USER_STATUS_CHANGED WHERE (REMARK='Deactivated' "
							+ "OR REMARK='Deleted' OR REMARK='Deactivated From Franchisor' ) GROUP BY USER_NO) AS USC ON U.USER_NO=USC.USER_NO LEFT JOIN  USER_STATUS_CHANGED USC1 ON U.USER_NO=USC1.USER_NO "
							+ "AND (USC1.REMARK='Activated' OR USC1.REMARK='Activated From Franchisor') WHERE U.USER_ID!='adm' AND "
							+ "U.CREATION_DATE<='" + toDate + "' AND ( (USC.CHANGE_DATE IS NULL OR USC.CHANGE_DATE>='"
							+ fromDate + "') OR ( USC1.CHANGE_DATE IS NOT NULL " + "AND  USC1.CHANGE_DATE<='" + toDate
							+ "' AND USC.CHANGE_DATE<=USC1.CHANGE_DATE)  OR (USC.CHANGE_DATE IS NULL OR USC.CHANGE_DATE > '"
							+ toDate + "'))"
							+ "  AND U.USER_NO!=1 AND ((U.USER_LEVEL=1 AND F.IS_FRANCHISEE='Y'  AND F.FRANCHISEE_NO IN ("
							+ franIdsNos + " ))) AND F.FRANCHISEE_NO = U.FRANCHISEE_NO ORDER BY F.FRANCHISEE_NAME");
			List<Map<String, Object>> resultuser = QueryUtil.executeQuerySelect(endpoint, userQuery.toString());
			locationQueryi = resultuser.size();
			Map<String, Object> userRow;
			String franNo = "";
			for (int j = 0; j < locationQueryi; j++) {
				userRow = resultuser.get(j);
				franNo = userRow.get("FRANCHISEE_NO") + "";
				if (franchiseeNoSetOrphan.contains(franNo)) {
					franchiseeNoSetChild.remove(franNo);
					if (orphanLocationWiseUser.containsKey(userRow.get("FRANCHISEE_NAME") + "")) {
						orphanLocationWiseUser.put(userRow.get("FRANCHISEE_NAME") + "",
								orphanLocationWiseUser.get(userRow.get("FRANCHISEE_NAME") + "") + 1);
					} else {
						orphanLocationWiseUser.put(userRow.get("FRANCHISEE_NAME") + "", 1);
					}
				} else if (franchiseeNoSetChild.contains(franNo) && franNo != "null" && franNo != "") {
					childUsers++;
					if (childLocationWiseUser.containsKey(userRow.get("FRANCHISEE_NAME") + "")) {
						childLocationWiseUser.put(userRow.get("FRANCHISEE_NAME") + "",
								childLocationWiseUser.get(userRow.get("FRANCHISEE_NAME") + "") + 1);
					} else {
						childLocationWiseUser.put(userRow.get("FRANCHISEE_NAME") + "", 1);
					}
					String commonUser = "SELECT COUNT(*) AS COUNT FROM USERS WHERE ABS_USER_NO='"
							+ userRow.get("ABS_USER_NO") + ""
							+ "' AND ABS_FRANCHISEE_NO IN(SELECT PARENT_ID FROM FRANCHISEE WHERE FRANCHISEE_NO='"
							+ franNo + "')";
					result = QueryUtil.executeQuerySelect(endpoint, commonUser);
					for (int k = 0; k < result.size(); k++) {
						row = result.get(k);
						commonUserCount += Integer.parseInt(row.get("COUNT") + "");
					}
				}
				else if (franchiseeNoSetParent.contains(franNo)) {
					parentUsers++;
					if (parentLocationWiseUser.containsKey(userRow.get("FRANCHISEE_NAME"))) {
						parentLocationWiseUser.put(userRow.get("FRANCHISEE_NAME") + "",
								parentLocationWiseUser.get(userRow.get("FRANCHISEE_NAME")) + 1);
					} else {
						parentLocationWiseUser.put(userRow.get("FRANCHISEE_NAME") + "", 1);
					}
				}
			}
			map.put("childLocation", String.valueOf(childLocationCount)); // total
			map.put("childLocationName", childLocationNameSet); // total child
			map.put("childParentFranchiseeNames", childParentFranchiseeNames); // parent
			map.put("parentLocation", String.valueOf(parentLocationCount));// total
			map.put("childPerParent", childPerParent);// Parent with child Count
			map.put("parentLocationName", parentLocationNameSet); // total child
			map.put("orphaonLocationNameSet", orphaonLocationNameSet); // total
			map.put("totalLocation", String.valueOf(totalLocations));// total
			map.put("orphanLocations", String.valueOf(orphanLocations));// niether
			map.put("childLocationWiseUser", childLocationWiseUser);
			map.put("parentLocationWiseUser", parentLocationWiseUser);
			map.put("orphanLocationWiseUser", orphanLocationWiseUser);
			map.put("childUsers", String.valueOf(childUsers));
			map.put("commonUsers", String.valueOf(childUsers - commonUserCount));
			map.put("parentUsers", String.valueOf(parentUsers));
			map.put("totalUsers", String.valueOf(parentUsers + childUsers));
		} catch (Exception e) {
			System.out.println("Exception in the Bright Star Map");
			e.printStackTrace();
		}
		return map;
	}
	public static HashMap<String, HashMap<String, HashMap<String, String>>> getLocationData(ProducerTemplate template,
			Endpoint endpoint, Exchange exchange, String billableLevel, String codeValue, String clientName,
			String hostURL) {

		HashMap<String, HashMap<String, HashMap<String, String>>> finaldata = new HashMap<String, HashMap<String, HashMap<String, String>>>();

		try {
			System.out.println("**********61333**************8" + clientName);
			String contextPath = "";

			if ("frannet".equals(codeValue)) {
				contextPath = "/frannet";
			}

			if ("aceSushi".equals(codeValue)) {
				contextPath = "/ace";
			}
			if ("theUpsStore".equals(codeValue)) {
				contextPath = "/ups";
			}
			if ("ufgCorp".equals(codeValue)) {
				// Multiple Regions
				contextPath = "/ufgCorp";
			}
			if ("g6Hospitality(Motel6)".equals(codeValue)) {
				contextPath = "/g6Hospitality";
			}
			if ("clockworkdirectEnergy".equals(codeValue) && "ClockWork".equals(clientName)) {
				contextPath = "/ClockWork";
			}
			if ("rightAtHomeInc".equals(codeValue)) {
				contextPath = "/RightAtHome";
			}
			if ("mosquitoJoe".equals(codeValue)) {
				contextPath = "/MosquitoJoe";
			}
			if ("brightstar".equals(codeValue)) {
				contextPath = "/brightstar";
			}
			if ("lightbridgeAcademy".equals(codeValue)) {
				contextPath = "/lightbridgeAcademy";
			}
			HashMap<String, String> calslocationhmap = new HashMap<String, String>();
			HashMap<String, HashMap<String, String>> calslocationhmapdata = new HashMap<String, HashMap<String, String>>();
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = null;
			boolean isBliable = false;
			boolean deactiveDeleted = false;
			boolean isFimUser = false;
			boolean isNewUserLevel = false;
			boolean isuserFirstNameAvailable = false;
			boolean isDeletedAvailable = false;
			boolean isStoreAvailable = false;
			boolean isStoreArchiveAvailable = false;
			boolean isOpeningDate = false;
			boolean isReportPeriodStatDate = false;
			boolean isAreaIDAvailable = false;
			String hiddenvalueNumber = "-1";
			String divUSerName = "";
			String fromWhere = "";
			if (clientName.contains("Zcubator") || clientName.contains("zcubator")) {
				fromWhere = "updateCals";
				if (clientName.contains("deconetzcubator")) {
					fromWhere = "";
				}
			}
			String franchiseeNo = "-1";
			String OpenerNo="-1";
			try {

				exchange = endpoint.createExchange();
				exchange.getIn().setBody("SELECT DIVISION_USER_LABEL FROM USER_LEVEL_CONFIGURATION LIMIT 1");
				//// System.out.println(codeValue+"=======SELECT
				//// DIVISION_USER_LABEL FROM USER_LEVEL_CONFIGURATION LIMIT
				//// 1==========>");
				Exchange out = template.send(endpoint, exchange);
				List<Map<String, Object>> result = out.getOut().getBody(List.class);

				int i = result.size();
				isNewUserLevel = true;

				for (int j = 0; j < i; j++) {
					Map<String, Object> row = result.get(j);
					if (row.get("DIVISION_USER_LABEL") != null) {
						divUSerName = row.get("DIVISION_USER_LABEL") + "";
					} else {
						divUSerName = "";
					}
				}

			} catch (Exception e1) {

				// System.out.println("=Exception Found for
				// Client====================>"+codeValue+"\n"+stackTraceToString(e1));
				// e1.printStackTrace();

				isNewUserLevel = false;
			}

			try {

				exchange = endpoint.createExchange();
				exchange.getIn().setBody("SELECT USER_FIRST_NAME FROM USERS LIMIT 1");
				//// System.out.println(codeValue+"=======SELECT USER_FIRST_NAME
				//// FROM USERS LIMIT 1==========>");
				Exchange out = template.send(endpoint, exchange);
				List<Map<String, Object>> result = out.getOut().getBody(List.class);

				int i = result.size();
				isuserFirstNameAvailable = true;

			} catch (Exception e1) {

				// System.out.println("=Exception Found for
				// Client====================>"+codeValue+"\n"+stackTraceToString(e1));
				// e1.printStackTrace();
				isuserFirstNameAvailable = false;
			}

			try {

				exchange = endpoint.createExchange();
				exchange.getIn().setBody("SELECT IS_DELETED FROM USERS LIMIT 1");
				Exchange out = template.send(endpoint, exchange);
				List<Map<String, Object>> result = out.getOut().getBody(List.class);
				int i = result.size();
				isDeletedAvailable = true;
			} catch (Exception e1) {
				isDeletedAvailable = false;
			}
			try {

				exchange = endpoint.createExchange();
				exchange.getIn().setBody("SELECT IS_STORE FROM FRANCHISEE LIMIT 1");
				Exchange out = template.send(endpoint, exchange);
				List<Map<String, Object>> result = out.getOut().getBody(List.class);
				int i = result.size();
				isStoreAvailable = true;
			} catch (Exception e1) {
				isStoreAvailable = false;
			}
			try {

				exchange = endpoint.createExchange();
				exchange.getIn().setBody("SELECT IS_STORE_ARCHIVED FROM FRANCHISEE LIMIT 1");
				Exchange out = template.send(endpoint, exchange);
				List<Map<String, Object>> result = out.getOut().getBody(List.class);
				int i = result.size();
				isStoreArchiveAvailable = true;
			} catch (Exception e1) {
				isStoreArchiveAvailable = false;
			}
			try {
				exchange = endpoint.createExchange();
				exchange.getIn().setBody("SELECT OPENING_DATE FROM FRANCHISEE LIMIT 1");
				Exchange out = template.send(endpoint, exchange);
				List<Map<String, Object>> result = out.getOut().getBody(List.class);
				int i = result.size();
				isOpeningDate = true;
			} catch (Exception e1) {
				isOpeningDate = false;
			}
			try {
				exchange = endpoint.createExchange();
				exchange.getIn().setBody("SELECT REPORT_PERIOD_START_DATE FROM FRANCHISEE LIMIT 1");
				Exchange out = template.send(endpoint, exchange);
				List<Map<String, Object>> result = out.getOut().getBody(List.class);
				int i = result.size();
				isReportPeriodStatDate = true;
			} catch (Exception e1) {
				isReportPeriodStatDate = false;
			}
			try {
				exchange = endpoint.createExchange();
				exchange.getIn().setBody("SELECT AREA_ID FROM FRANCHISEE LIMIT 1");
				//// System.out.println(codeValue+"=======SELECT AREA_ID FROM
				//// FRANCHISEE LIMIT 1==========>");
				Exchange out = template.send(endpoint, exchange);
				List<Map<String, Object>> result = out.getOut().getBody(List.class);

				int i = result.size();
				isAreaIDAvailable = true;

			} catch (Exception e1) { // System.out.println("=Exception Found for
										// Client====================>"+codeValue+"\n"+stackTraceToString(e1));
										// e1.printStackTrace();

				isAreaIDAvailable = false;
			}
			try {

				exchange = endpoint.createExchange();
				exchange.getIn().setBody("SELECT IS_BILLABLE FROM USERS LIMIT 1");
				//// System.out.println(codeValue+"=======SELECT IS_BILLABLE
				//// FROM USERS LIMIT 1==========>");
				Exchange out = template.send(endpoint, exchange);
				List<Map<String, Object>> result = out.getOut().getBody(List.class);
				int i = result.size();
				isBliable = true;

			} catch (Exception e1) { // System.out.println("=Exception Found for
										// Client====================>"+codeValue+"\n"+stackTraceToString(e1));
										// e1.printStackTrace();
				isBliable = false;
			}

			try {

				exchange = endpoint.createExchange();
				exchange.getIn().setBody("SELECT DEACTIVATED_DELETED FROM USER_STATUS_CHANGED LIMIT 1");
				//// System.out.println(codeValue+"=======SELECT
				//// DEACTIVATED_DELETED FROM USER_STATUS_CHANGED LIMIT
				//// 1==========>");
				Exchange out = template.send(endpoint, exchange);
				List<Map<String, Object>> result = out.getOut().getBody(List.class);
				int i = result.size();
				deactiveDeleted = true;

			} catch (Exception e1) { // System.out.println("=Exception Found for
										// Client====================>"+codeValue+"\n"+stackTraceToString(e1));
										// e1.printStackTrace();
				deactiveDeleted = false;
			}
			try {

				exchange = endpoint.createExchange();
				exchange.getIn().setBody("SELECT * FROM FIM_USERS LIMIT 1");
				//// System.out.println(codeValue+"=======SELECT * FROM
				//// FIM_USERS LIMIT 1==========>");
				Exchange out = template.send(endpoint, exchange);
				List<Map<String, Object>> result = out.getOut().getBody(List.class);
				int i = result.size();
				isFimUser = true;

			} catch (Exception e1) { // System.out.println("=Exception Found for
										// Client====================>"+codeValue+"\n"+stackTraceToString(e1));
										// e1.printStackTrace();
				isFimUser = false;
			}

			try {
				StringBuilder insightQuery = null;// For InSight User Counts
				List data = null;
				long insightCount1 = 0;
				String insightCount = "0";
				Map<String, Object> row1 = null;
				if (Constants.version.equals("SaaS")) {
					insightQuery = new StringBuilder(
							"SELECT count(distinct USER_NO) AS COUNT FROM MODULE_ACCESS_LOGS  WHERE MODULE_NAME='intelligence' AND ACCESS_DATE>='"
									+ dateFrom + " 00:00:00' AND ACCESS_DATE <='" + dateTo
									+ " 23:59:59' AND USER_NO!='1'");
					exchange = endpoint.createExchange();
					exchange.getIn().setBody(insightQuery.toString());
					Exchange out = template.send(endpoint, exchange);
					if (!exchange.isFailed()) {
						data = (List) exchange.getOut().getBody();
					}
					if (data != null) {
						int i = data.size();
						row1 = (Map<String, Object>) data.get(0);
						insightCount1 = (long) row1.get("COUNT");
						insightCount = "" + insightCount1;
						calslocationhmap.put("insightCount", insightCount);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			if ("/g6Hospitality".equals(contextPath)) {
				try {
					Endpoint endpointInHouse = DBConnectionProvider.getInstance().getDBConnection("InHouse");
					String priviledgeQuery = "SELECT GROUP_CONCAT(PRIVILEGE_ID) AS PRIVILEGE_ID ,MODULE FROM FC_PRIVILEGES  WHERE MODULE='Sales' GROUP BY MODULE";
					Map<String, Object> row = null;
					List privilegeMap = QueryUtil.executeQuerySelect(endpointInHouse, priviledgeQuery);
					int privilegeSize = privilegeMap.size();
					row = (Map<String, Object>) privilegeMap.get(0);
					String salesprivilege = row.get("PRIVILEGE_ID") + "";
					List data = null;
					Map<String, String> row1 = null;
					HashMap<String, String> rmap = new HashMap<>();
					HashMap<String, HashMap<String, String>> rolemap = new HashMap<>();
					exchange = endpoint.createExchange();
					exchange.getIn().setBody(
							"SELECT NAME,ROLE_ID FROM ROLE WHERE ROLE_ID IN (SELECT DISTINCT ROLE_ID  FROM ROLE_PRIVILEGES WHERE PRIVILEGE_ID IN ("
									+ salesprivilege + "))");
					Exchange out = template.send(endpoint, exchange);
					List<Map<String, Object>> result = out.getOut().getBody(List.class);
					if (!exchange.isFailed()) {
						data = (List) exchange.getOut().getBody();
					}
					if (data != null) {
						int i = data.size();
						for (int j = 0; j < i; j++) {
							row1 = (Map<String, String>) data.get(j);
							rmap.put(row1.get("ROLE_ID"), row1.get("NAME"));
						}
						rolemap.put("rmap", rmap);
						finaldata.put("roleMap", rolemap);
					}
				} catch (Exception e1) {
					System.out.println("=Exception Found forClient====================>" + codeValue + "\n"
							+ stackTraceToString(e1));
					e1.printStackTrace();
				}
			}
			try {
				StringBuilder moduleQuery = null;// For InSight User Counts
				List data = null;
				int moduleCount = 0;
				String modules = "";
				Map<String, Object> row1 = null;
				if (Constants.version.equals("SaaS")) {
					moduleQuery = new StringBuilder(
							"SELECT KEY_VALUE FROM APPLICATION_CONFIGURATION_DATA WHERE KEY_TYPE='keyVal.xml' AND KEY_NAME='modules'");
					exchange = endpoint.createExchange();
					exchange.getIn().setBody(moduleQuery.toString());
					Exchange out = template.send(endpoint, exchange);
					if (!exchange.isFailed()) {
						data = (List) exchange.getOut().getBody();
					}
					if (data != null) {
						int i = data.size();
						row1 = (Map<String, Object>) data.get(0);
						modules = row1.get("KEY_VALUE") + "";
						String moduleSplit[] = modules.split(",");
						moduleCount = moduleSplit.length;
						calslocationhmap.put("modules", modules);
						calslocationhmap.put("moduleCount", "" + moduleCount);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				int moduleCount = 0;
				String modules = "";
				List data = null;
				String path = "";
				File file;
				Map<String, Object> row1 = null;
				if (Constants.version.equals("Legacy")) {
					path = hostURL + "/tenants/Default/xml/keyval.xml";
					file = new File(path);
					try {
						doc = dBuilder.parse(path);
						doc.getDocumentElement().normalize();
						Element eElement = (Element) doc.getDocumentElement();
						modules = getChildElementContent(eElement, "modules");
					} catch (Exception e) {
					}
					if (modules == "") {
						path = hostURL + "/WEB-INF/xml/keyval.xml";
						file = new File(path);
						try {
							doc = dBuilder.parse(path);
							doc.getDocumentElement().normalize();
							Element eElement1 = (Element) doc.getDocumentElement();
							modules = getChildElementContent(eElement1, "modules");
						} catch (Exception e) {
						}
					}
					String moduleSplit[] = modules.split(",");
					moduleCount = moduleSplit.length;
					calslocationhmap.put("modules", modules);
					calslocationhmap.put("moduleCount", "" + moduleCount);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				StringBuilder systemTimeQuery = null;// For System Login Time
				StringBuilder admsystemTimeQuery = null;
				StringBuilder clientSystemTimeQuery = null;
				List data = null;
				long i1=0;
				BigDecimal systemTime1 = null;
				String systemTime = "0";
				Map<String, Object> row1 = null;
				systemTimeQuery = new StringBuilder(
						"SELECT SUM(UNIX_TIMESTAMP(LOGOUT_DATE))-SUM(UNIX_TIMESTAMP(LOGIN_DATE)) TIME FROM LOGIN_DETAILS WHERE (LOGIN_DATE>='"
								+ dateFrom + " 00:00:00' AND LOGIN_DATE<='" + dateTo
								+ " 23:59:59') and USER_ID not in(1000) ");
				admsystemTimeQuery = new StringBuilder(
						"SELECT SUM(UNIX_TIMESTAMP(LOGOUT_DATE))-SUM(UNIX_TIMESTAMP(LOGIN_DATE)) TIME FROM LOGIN_DETAILS WHERE (LOGIN_DATE>='"
								+ dateFrom + " 00:00:00' AND LOGIN_DATE<='" + dateTo
								+ " 23:59:59') and USER_ID in (1)");
				clientSystemTimeQuery = new StringBuilder(
						"SELECT SUM(UNIX_TIMESTAMP(LOGOUT_DATE))-SUM(UNIX_TIMESTAMP(LOGIN_DATE)) TIME FROM LOGIN_DETAILS WHERE (LOGIN_DATE>='"
								+ dateFrom + " 00:00:00' AND LOGIN_DATE<='" + dateTo
								+ " 23:59:59') and USER_ID not in(1,1000)");
			//	 System.out.println(clientSystemTimeQuery+"clientSystemTimeQuery");
				 data=QueryUtil.executeQuerySelect(endpoint,systemTimeQuery.toString());
				
				if (data != null) {
					int i = data.size();
					row1 = (Map<String, Object>) data.get(0);
					if (row1 != null && row1.get("TIME") != null) {
						systemTime = "" + row1.get("TIME");
							BigDecimal bd = new BigDecimal(systemTime);
							i1=bd.longValue();  
						/*}else{
						StringTokenizer st = new StringTokenizer(systemTime, ".");
						systemTime = st.nextToken(); 
						 i1 = Long.parseLong(systemTime);
						}*/
					//	 System.out.println("time"+i1);
						String timeDis = "00:00:00";
						// int i1 = Integer.parseInt(systemTime);
						String hour = "" + (i1 / 3600);
						i1 = i1 % 3600;
						String min = "" + (i1 / 60);
						String sec = "" + (i1 % 60);
						if (hour.length() == 1)
							hour = "0" + hour;
						if (min.length() == 1)
							min = "0" + min;
						if (sec.length() == 1)
							sec = "0" + sec;

						timeDis = hour + ":" + min + ":" + sec;
						calslocationhmap.put("systemTime", timeDis);
					} else {
						calslocationhmap.put("systemTime", "00:00:00");
					}
				}
				/*exchange.getIn().setBody(admsystemTimeQuery.toString());
				Exchange out1 = template.send(endpoint, exchange);
				if (!exchange.isFailed()) {
					data = (List) exchange.getOut().getBody();
				}*/
				 data=QueryUtil.executeQuerySelect(endpoint,admsystemTimeQuery.toString());
				if (data != null) {
					int i = data.size();
					row1 = (Map<String, Object>) data.get(0);
					if (row1 != null && row1.get("TIME") != null) {
						systemTime = "" + row1.get("TIME");
							BigDecimal bd = new BigDecimal(systemTime);
							i1=bd.longValue();  
						/*}else{
						StringTokenizer st = new StringTokenizer(systemTime, ".");
						systemTime = st.nextToken(); 
						}*/
						String timeDis = "00:00:00";
						//long i1 = Long.parseLong(systemTime);
						// int i1 = Integer.parseInt(systemTime);
						String hour = "" + (i1 / 3600);
						i1 = i1 % 3600;
						String min = "" + (i1 / 60);
						String sec = "" + (i1 % 60);
						if (hour.length() == 1)
							hour = "0" + hour;
						if (min.length() == 1)
							min = "0" + min;
						if (sec.length() == 1)
							sec = "0" + sec;
						timeDis = hour + ":" + min + ":" + sec;
						calslocationhmap.put("admsystemTime", timeDis);
					} else {
						calslocationhmap.put("admsystemTime", "00:00:00");
					}
				}
				/*exchange.getIn().setBody(clientSystemTimeQuery.toString());
				Exchange out2 = template.send(endpoint, exchange);
				if (!exchange.isFailed()) {
					data = (List) exchange.getOut().getBody();
				}*/
				 data=QueryUtil.executeQuerySelect(endpoint,clientSystemTimeQuery.toString());
				if (data != null) {
					int i = data.size();
					row1 = (Map<String, Object>) data.get(0);
					if (row1 != null && row1.get("TIME") != null) {
						systemTime = "" + row1.get("TIME");
						/*StringTokenizer st = new StringTokenizer(systemTime, ".");
						systemTime = st.nextToken();*/
						String timeDis = "00:00:00";
							BigDecimal bd = new BigDecimal(systemTime);
							i1=bd.longValue();  
						//long i1 = Long.parseLong(systemTime);
						// int i1 = Integer.parseInt(systemTime);
						String hour = "" + (i1 / 3600);
						i1 = i1 % 3600;
						String min = "" + (i1 / 60);
						String sec = "" + (i1 % 60);
						if (hour.length() == 1)
							hour = "0" + hour;
						if (min.length() == 1)
							min = "0" + min;
						if (sec.length() == 1)
							sec = "0" + sec;
						timeDis = hour + ":" + min + ":" + sec;
						calslocationhmap.put("clientSystemTime", timeDis);
					} else {
						calslocationhmap.put("clientSystemTime", "00:00:00");
					}
					// System.out.println("******************349"+calslocationhmap);
				}
			} catch (Exception e) {
				System.out.println(
						"=Exception Found for Client====================>" + codeValue + "\n" + stackTraceToString(e));
				e.printStackTrace();
			}
			try {
				StringBuilder mainQuery = null;
				List<Map<String, Object>> result = null;
				mainQuery = new StringBuilder(
						" SELECT COUNT(FRANCHISEE_NO) AS COUNT, IFNULL(GROUP_CONCAT(FRANCHISEE_NO),'-1') AS FRANCHISEE_NO FROM FRANCHISEE WHERE ((IS_FRANCHISEE='Y' AND STATUS IN (0,4) ");
				mainQuery.append("AND DEACTIVATION_DATE >= '" + dateFrom + " 00:00:00' AND DEACTIVATION_DATE <= '"
						+ dateTo + " 23:59:59') OR (IS_FRANCHISEE='Y' AND STATUS IN (1,3) ");
				mainQuery.append("AND CREATION_DATE <= '" + dateTo
						+ " 23:59:59') OR (IS_FRANCHISEE='Y' AND STATUS IN (0,4) AND DEACTIVATION_DATE > '" + dateTo
						+ " 23:59:59' AND CREATION_DATE <= '" + dateTo + " 23:59:59'))");

				mainQuery.append(
						" AND lower(FRANCHISEE_NAME) NOT LIKE '%franconnect%' AND lower(FRANCHISEE_NAME) NOT LIKE '%test%'");

				String activeLoc = "0";
				exchange = endpoint.createExchange();
				exchange.getIn().setBody(mainQuery.toString());
				//System.out.println(clientName + "\n======codeValue==mainQuery=======>" + mainQuery.toString());
				Exchange out = template.send(endpoint, exchange);
				if (!exchange.isFailed()) {
					result = exchange.getOut().getBody(List.class);
				}
				int i = result.size();
				for (int j = 0; j < i; j++) {
					Map<String, Object> row = result.get(j);
					if (row.get("FRANCHISEE_NO") != null) {
						if (row.get("FRANCHISEE_NO") instanceof byte[]) {
							final String pass = new String((byte[]) row.get("FRANCHISEE_NO"));
							franchiseeNo = pass;
						} else {
							franchiseeNo = (String) row.get("FRANCHISEE_NO");
						}

					} else {
						franchiseeNo = "-1";
					}

					if (row.get("COUNT") != null) {
						if (row.get("COUNT") instanceof byte[]) {
							final String pass = new String((byte[]) row.get("COUNT"));
							activeLoc = pass;
						} else {
							activeLoc = row.get("COUNT") + "";
						}

					} else {
						activeLoc = "0";
					}
				}
				calslocationhmap.put("activeLoc", activeLoc);

			} catch (Exception e) {
				System.out.println(
						"=Exception Found for Client====================>" + codeValue + "\n" + stackTraceToString(e));
				e.printStackTrace();

			}

			/*
			 * 
			 * Opener Count 
			 * 
			 * LEGACYS-16261
			 * 
			 */
			try {
				StringBuilder mainQuery = null;
				List<Map<String, Object>> result = null;
				mainQuery = new StringBuilder(
						" SELECT COUNT(FRANCHISEE_NO) AS COUNT, IFNULL(GROUP_CONCAT(FRANCHISEE_NO),'-1') AS FRANCHISEE_NO FROM FRANCHISEE WHERE ((IS_FRANCHISEE!='Y' AND DEACTIVATION_DATE IS NULL) AND (");
				if(isStoreAvailable) {
					mainQuery.append("IS_STORE ='Y' AND ");
				}
				if(isStoreArchiveAvailable) {
					mainQuery.append("IS_STORE_ARCHIVED='N' AND ");
				}
				mainQuery.append(" DEACTIVATION_DATE IS NULL) OR ( DEACTIVATION_DATE >= '" + dateFrom + " 00:00:00' AND DEACTIVATION_DATE <= '"
						+ dateTo + " 23:59:59'))") ;
				mainQuery.append(
						" AND lower(FRANCHISEE_NAME) NOT LIKE '%franconnect%' AND lower(FRANCHISEE_NAME) NOT LIKE '%test%'");

				String activeOpnerLoc = "0";
				exchange = endpoint.createExchange();
				exchange.getIn().setBody(mainQuery.toString());
				System.out.println(clientName + "\n======codeValue==Opnener=======>" + mainQuery.toString());
				Exchange out = template.send(endpoint, exchange);
				if (!exchange.isFailed()) {
					result = exchange.getOut().getBody(List.class);
				}
				int i = result.size();
				for (int j = 0; j < i; j++) {
					Map<String, Object> row = result.get(j);
					if (row.get("FRANCHISEE_NO") != null) {
						if (row.get("FRANCHISEE_NO") instanceof byte[]) {
							final String pass = new String((byte[]) row.get("FRANCHISEE_NO"));
							OpenerNo = pass;
						} else {
							OpenerNo = (String) row.get("FRANCHISEE_NO");
						}

					} else {
						OpenerNo = "-1";
					}

					if (row.get("COUNT") != null) {
						if (row.get("COUNT") instanceof byte[]) {
							final String pass = new String((byte[]) row.get("COUNT"));
							activeOpnerLoc = pass;
						} else {
							activeOpnerLoc = row.get("COUNT") + "";
						}

					} else {
						activeOpnerLoc = "0";
					}
				}
				calslocationhmap.put("activeOpnerLoc", activeOpnerLoc);

			} catch (Exception e) {
				System.out.println(
						"=Exception Found for Client======in activeOpnerLoc ==============>" + codeValue + "\n" + stackTraceToString(e));
				e.printStackTrace();

			}
			
			/*
			 * 
			 * Franchisee USer Count 
			 * 
			 */
			HashMap<String,String> countMap=new HashMap();
			
			try{
				StringBuffer userCountQuery=new StringBuffer();
				if(isFimUser){
					userCountQuery = userCountQuery
							.append("SELECT COUNT(DISTINCT FU.FRANCHISEE_USER_NO) AS COUNT,FU.FRANCHISEE_NO FROM FIM_USERS FU , USERS U LEFT JOIN ( SELECT MAX(CHANGE_DATE) AS CHANGE_DATE,USER_NO,REMARK FROM USER_STATUS_CHANGED WHERE (REMARK='Deactivated' OR REMARK='Deleted' OR REMARK='Deactivated From Franchisor' ) GROUP BY USER_NO) AS USC ON U.USER_NO=USC.USER_NO LEFT JOIN  USER_STATUS_CHANGED USC1 ON U.USER_NO=USC1.USER_NO AND (USC1.REMARK='Activated' OR USC1.REMARK='Activated From Franchisor')  WHERE U.USER_ID!='adm' AND U.CREATION_DATE<='"
						+ dateTo + " 23:59:59' AND ( (USC.CHANGE_DATE IS NULL OR USC.CHANGE_DATE>='"
						+ dateFrom
									+ " 00:00:00') OR ( USC1.CHANGE_DATE IS NOT NULL AND  USC1.CHANGE_DATE<='" + dateTo
						+ " 23:59:59' AND USC.CHANGE_DATE<=USC1.CHANGE_DATE) OR (USC.CHANGE_DATE IS NULL OR USC.CHANGE_DATE > '"
						+ dateTo + " 23:59:59'))  AND FU.FRANCHISEE_USER_NO = U.USER_IDENTITY_NO");
				} else {
					userCountQuery = userCountQuery
							.append("SELECT COUNT(DISTINCT FU.FRANCHISEE_USER_NO) AS COUNT,FU.FRANCHISEE_NO FROM FRANCHISEE_USERS FU , USERS U LEFT JOIN ( SELECT MAX(CHANGE_DATE) AS CHANGE_DATE,USER_NO,REMARK FROM USER_STATUS_CHANGED WHERE (REMARK='Deactivated' OR REMARK='Deleted' OR REMARK='Deactivated From Franchisor' ) GROUP BY USER_NO) AS USC ON U.USER_NO=USC.USER_NO LEFT JOIN  USER_STATUS_CHANGED USC1 ON U.USER_NO=USC1.USER_NO AND (USC1.REMARK='Activated' OR USC1.REMARK='Activated From Franchisor')  WHERE U.USER_ID!='adm' AND U.CREATION_DATE<='"
							+ dateTo + " 23:59:59' AND ( (USC.CHANGE_DATE IS NULL OR USC.CHANGE_DATE>='"
							+ dateFrom
									+ " 00:00:00') OR ( USC1.CHANGE_DATE IS NOT NULL AND  USC1.CHANGE_DATE<='" + dateTo
							+ " 23:59:59' AND USC.CHANGE_DATE<=USC1.CHANGE_DATE) OR (USC.CHANGE_DATE IS NULL OR USC.CHANGE_DATE > '"
							+ dateTo + " 23:59:59'))   ");  
				}
				if (isBliable) {
					userCountQuery = userCountQuery.append(
							" AND U.IS_BILLABLE='Y' AND FU.FRANCHISEE_USER_NO = U.USER_IDENTITY_NO GROUP BY FU.FRANCHISEE_NO");
				}else{
					userCountQuery = userCountQuery
							.append("  AND FU.FRANCHISEE_USER_NO = U.USER_IDENTITY_NO GROUP BY FU.FRANCHISEE_NO ");
				}
				//System.out.println(clientName + "userCountQuery" + userCountQuery);
				List data=QueryUtil.executeQuerySelect(endpoint, userCountQuery.toString());
			 	Map<String, Object> row=null;
				if (data != null && data.size() > 0) {
			 		for(int i=0;i<data.size();i++){
			        row=(Map<String, Object>) data.get(i);
			        countMap.put(row.get("FRANCHISEE_NO")+"", row.get("COUNT")+"");
			 		}
			 	}
			}catch(Exception e){
				System.out.println(
						"=Exception Found for Client====================>" + codeValue + "\n" + stackTraceToString(e));
				e.printStackTrace();
			}
			HashMap<String, List<String>> uscactiveMap = new HashMap<>();
			HashMap<String, List<String>> uscdeactiveMap = new HashMap<>();
			
				try{
				StringBuffer activateUserQuery = new StringBuffer(
						"SELECT IF(MAX(CHANGE_DATE) IS NULL || MAX(CHANGE_DATE) ='0000-00-00 00:00:00', (SELECT MAX(LOGOUT_DATE) FROM LOGIN_DETAILS WHERE USER_ID=U.USER_NO),MAX(CHANGE_DATE)) AS CHANGE_DATE,USER_NO,REMARK FROM USER_STATUS_CHANGED WHERE (REMARK='Activated' OR REMARK='Activated From Franchisor') AND CHANGE_DATE IS NOT NULL AND CHANGE_DATE!='0000-00-00 00:00:00' GROUP BY USER_NO ORDER BY USER_NO");
				List activatedata = QueryUtil.executeQuerySelect(endpoint, activateUserQuery.toString());
				Map<String, Object> row = null;
				List<String> ls = null;
				if (activatedata != null && activatedata.size() > 0) {
					for (int i = 0; i < activatedata.size(); i++) {
						ls = new ArrayList<>();
						row = (Map<String, Object>) activatedata.get(i);
						ls.add(row.get("CHANGE_DATE") + "");
						ls.add(row.get("REMARK") + "");
						uscactiveMap.put(row.get("USER_NO") + "", ls);
					}
				}
			} catch (Exception e) {
				System.out.println("=Exception Found for Client========USER_STATUS_CHANGED============>" + codeValue
						+ "\n" + stackTraceToString(e));
				e.printStackTrace();
			}
				try {
					StringBuffer deactivateUserQuery = new StringBuffer("SELECT IF(MAX(CHANGE_DATE) IS NULL || MAX(CHANGE_DATE) ='0000-00-00 00:00:00', (SELECT MAX(LOGOUT_DATE) FROM LOGIN_DETAILS WHERE USER_ID=U.USER_NO),MAX(CHANGE_DATE)) AS CHANGE_DATE,USER_NO,REMARK FROM USER_STATUS_CHANGED WHERE (REMARK='Deactivated' OR REMARK='Deleted' OR REMARK='Deactivated From Franchisor' ) AND CHANGE_DATE IS NOT NULL AND CHANGE_DATE!='0000-00-00 00:00:00' GROUP BY USER_NO ORDER BY USER_NO");
					List deactivatedata = QueryUtil.executeQuerySelect(endpoint, deactivateUserQuery.toString());
					Map<String, Object> row = null;
					List<String> ls = null;
					if (deactivatedata != null && deactivatedata.size() > 0) {
						for (int i = 0; i < deactivatedata.size(); i++) {
							ls = new ArrayList<>();
							row = (Map<String, Object>) deactivatedata.get(i);
							ls.add(row.get("CHANGE_DATE") + "");
							ls.add(row.get("REMARK") + "");
							uscdeactiveMap.put(row.get("USER_NO") + "", ls);
						}
					}
				}catch (Exception e) {
					System.out.println("=Exception Found for Client========USER_STATUS_CHANGED============>" + codeValue
							+ "\n" + stackTraceToString(e));
					e.printStackTrace();
				}	
			/*
			 * 
			 * Active location
			 * 
			 */

			try {
				hiddenvalueNumber = "-1";

				if (franchiseeNo != null && franchiseeNo.trim().length() != 0
						&& !franchiseeNo.trim().equalsIgnoreCase("null") && !"-1".equals(franchiseeNo)) {
					hiddenvalueNumber = franchiseeNo;
				} else {
					hiddenvalueNumber = "-1";
				}

				// System.out.println("=====codeValue====="+codeValue);

				if (hiddenvalueNumber != null && hiddenvalueNumber.trim().length() != 0
						&& !hiddenvalueNumber.trim().equalsIgnoreCase("null") && !"-1".equals(hiddenvalueNumber)) {
					StringBuilder activelocationQuery = new StringBuilder();
					if (isOpeningDate) {
						activelocationQuery.append(
								"SELECT CASE WHEN FRANCHISEE_NAME IS NULL OR FRANCHISEE_NAME='' THEN CONCAT(FIRST_NAME,' ',LAST_NAME) ELSE FRANCHISEE_NAME END AS FRANCHISEE_NAME, F.FRANCHISEE_NO,IF(F.OPENING_DATE='0000-00-00','',F.OPENING_DATE) AS OPENING_DATE ,CITY,STATE");
					} else {
						if (isReportPeriodStatDate) {
							activelocationQuery.append(
									"SELECT CASE WHEN FRANCHISEE_NAME IS NULL OR FRANCHISEE_NAME='' THEN CONCAT(FIRST_NAME,' ',LAST_NAME) ELSE FRANCHISEE_NAME END AS FRANCHISEE_NAME, F.FRANCHISEE_NO,IF(F.REPORT_PERIOD_START_DATE='0000-00-00','',F.REPORT_PERIOD_START_DATE) AS OPENING_DATE ,CITY,STATE");
						} else {
							activelocationQuery.append(
									"SELECT CASE WHEN FRANCHISEE_NAME IS NULL OR FRANCHISEE_NAME='' THEN CONCAT(FIRST_NAME,' ',LAST_NAME) ELSE FRANCHISEE_NAME END AS FRANCHISEE_NAME, F.FRANCHISEE_NO,'' AS OPENING_DATE ,CITY,STATE");
						}
					}
					activelocationQuery.append(
							",GROUP_CONCAT(' ',CONCAT(OWNER_FIRST_NAME,' ',OWNER_LAST_NAME)) AS OWNER_NAME, AREA_NAME, STORE_PHONE, COUNTRY, F.STATUS");
					/*if (isFimUser) {
						if(!clientName.equals("SMCZcubator")){
						activelocationQuery
								.append(",(SELECT COUNT(DISTINCT FU.FRANCHISEE_USER_NO) FROM FIM_USERS FU , USERS U LEFT JOIN ( SELECT MAX(CHANGE_DATE) AS CHANGE_DATE,USER_NO,REMARK FROM USER_STATUS_CHANGED WHERE (REMARK='Deactivated' OR REMARK='Deleted' OR REMARK='Deactivated From Franchisor' ) GROUP BY USER_NO) AS USC ON U.USER_NO=USC.USER_NO LEFT JOIN  USER_STATUS_CHANGED USC1 ON U.USER_NO=USC1.USER_NO AND (USC1.REMARK='Activated' OR USC1.REMARK='Activated From Franchisor')  WHERE U.USER_ID!='adm' AND U.CREATION_DATE<='"
										+ dateTo + " 23:59:59' AND ( (USC.CHANGE_DATE IS NULL OR USC.CHANGE_DATE>='"
										+ dateFrom
										+ " 00:00:00') OR ( USC1.CHANGE_DATE IS NOT NULL AND  USC1.CHANGE_DATE<='"
										+ dateTo
										+ " 23:59:59' AND USC.CHANGE_DATE<=USC1.CHANGE_DATE) OR (USC.CHANGE_DATE IS NULL OR USC.CHANGE_DATE > '"
										+ dateTo + " 23:59:59'))   AND F.FRANCHISEE_NO=FU.FRANCHISEE_NO");
						}
					} else {
						activelocationQuery
								.append(",(SELECT COUNT(DISTINCT FU.FRANCHISEE_USER_NO) FROM FRANCHISEE_USERS FU , USERS U LEFT JOIN ( SELECT MAX(CHANGE_DATE) AS CHANGE_DATE,USER_NO,REMARK FROM USER_STATUS_CHANGED WHERE (REMARK='Deactivated' OR REMARK='Deleted' OR REMARK='Deactivated From Franchisor' ) GROUP BY USER_NO) AS USC ON U.USER_NO=USC.USER_NO LEFT JOIN  USER_STATUS_CHANGED USC1 ON U.USER_NO=USC1.USER_NO AND (USC1.REMARK='Activated' OR USC1.REMARK='Activated From Franchisor')  WHERE U.USER_ID!='adm' AND U.CREATION_DATE<='"
										+ dateTo + " 23:59:59' AND ( (USC.CHANGE_DATE IS NULL OR USC.CHANGE_DATE>='"
										+ dateFrom
										+ " 00:00:00') OR ( USC1.CHANGE_DATE IS NOT NULL AND  USC1.CHANGE_DATE<='"
										+ dateTo
										+ " 23:59:59' AND USC.CHANGE_DATE<=USC1.CHANGE_DATE) OR (USC.CHANGE_DATE IS NULL OR USC.CHANGE_DATE > '"
										+ dateTo + " 23:59:59'))   AND F.FRANCHISEE_NO=FU.FRANCHISEE_NO   ");
					}
					if (isBliable) {
						activelocationQuery.append(
								" AND U.IS_BILLABLE='Y' AND FU.FRANCHISEE_USER_NO = U.USER_IDENTITY_NO ) AS USER_COUNT ");
					} else {
						if(!clientName.equals("SMCZcubator")){
						activelocationQuery.append("  AND FU.FRANCHISEE_USER_NO = U.USER_IDENTITY_NO ) AS USER_COUNT ");
						}
					}*/
					activelocationQuery.append(
							" FROM FRANCHISEE F LEFT JOIN OWNERS O ON F.FRANCHISEE_NO=O.FRANCHISEE_NO LEFT JOIN FIM_OWNERS FO ON O.OWNER_ID=FO.FRANCHISE_OWNER_ID LEFT JOIN AREAS A ON F.AREA_ID=A.AREA_ID");
					activelocationQuery.append(" WHERE F.FRANCHISEE_NO IN (").append(hiddenvalueNumber).append(")");
					activelocationQuery.append(
							"  AND lower(FRANCHISEE_NAME) NOT LIKE lower('%franconnect%') AND lower(FRANCHISEE_NAME) NOT LIKE lower('%test%') GROUP BY F.FRANCHISEE_NO ORDER BY FRANCHISEE_NAME ASC");

					exchange = endpoint.createExchange();
					exchange.getIn().setBody(activelocationQuery.toString());
					// System.out.println(clientName + "\n=======activelocationQuery=========>" +activelocationQuery.toString());
					Exchange locationQueryout = template.send(endpoint, exchange);
					List<Map<String, Object>> locationQueryresult = locationQueryout.getOut().getBody(List.class);
					int locationQueryi = locationQueryresult.size();
					HashMap<String, String> activeLocationMap = null;
					HashMap<String, HashMap<String, String>> activeLocationMapdata = new HashMap<String, HashMap<String, String>>();
					for (int j = 0; j < locationQueryi; j++) {
						Map<String, Object> row = locationQueryresult.get(j);
						activeLocationMap = new HashMap<String, String>();

						if (row.get("FRANCHISEE_NO") != null) {
							activeLocationMap.put("FRANCHISEE_NO", row.get("FRANCHISEE_NO") + "");
						} else {
							activeLocationMap.put("FRANCHISEE_NO", "-1");
						}

						if (row.get("FRANCHISEE_NAME") != null) {
							activeLocationMap.put("FRANCHISEE_NAME", row.get("FRANCHISEE_NAME") + "");
						} else {
							activeLocationMap.put("FRANCHISEE_NAME", "");
						}

						if (row.get("OPENING_DATE") != null) {
							activeLocationMap.put("OPENING_DATE", row.get("OPENING_DATE") + "");
						} else {
							activeLocationMap.put("OPENING_DATE", "");
						}
						/*if(!clientName.equals("SMCZcubator")){
						if (row.get("USER_COUNT") != null) {

							activeLocationMap.put("USER_COUNT", row.get("USER_COUNT") + "");
						}*/
						    if(countMap.get(row.get("FRANCHISEE_NO")+"")!="0"){
							activeLocationMap.put("USER_COUNT",countMap.get(row.get("FRANCHISEE_NO")+""));
						    }else{
						    	activeLocationMap.put("USER_COUNT", "0");
						    }
						    

						activeLocationMapdata.put(j + "", activeLocationMap);
					}
					finaldata.put("activeLocationMap", activeLocationMapdata);
				}
			} catch (Exception e) {
				System.out.println(
						"=Exception Found for Client====================>" + codeValue + "\n" + stackTraceToString(e));
				e.printStackTrace();
			}
			
			/*
			 * 
			 * Indevelopment  location
			 * 
			 */

			try {
				hiddenvalueNumber = "-1";

				if (OpenerNo != null && OpenerNo.trim().length() != 0
						&& !OpenerNo.trim().equalsIgnoreCase("null") && !"-1".equals(OpenerNo)) {
					hiddenvalueNumber = OpenerNo;
				} else {
					hiddenvalueNumber = "-1";
				}

				// System.out.println("=====codeValue====="+codeValue);

				if (hiddenvalueNumber != null && hiddenvalueNumber.trim().length() != 0
						&& !hiddenvalueNumber.trim().equalsIgnoreCase("null") && !"-1".equals(hiddenvalueNumber)) {
					StringBuilder activeindevelopmentlocationQuery = new StringBuilder();
					if (isOpeningDate) {
						activeindevelopmentlocationQuery.append(
								"SELECT CASE WHEN FRANCHISEE_NAME IS NULL OR FRANCHISEE_NAME='' THEN CONCAT(FIRST_NAME,' ',LAST_NAME) ELSE FRANCHISEE_NAME END AS FRANCHISEE_NAME, F.FRANCHISEE_NO,IF(F.OPENING_DATE='0000-00-00','',F.OPENING_DATE) AS OPENING_DATE ,CITY,STATE");
					} else {
						if (isReportPeriodStatDate) {
							activeindevelopmentlocationQuery.append(
									"SELECT CASE WHEN FRANCHISEE_NAME IS NULL OR FRANCHISEE_NAME='' THEN CONCAT(FIRST_NAME,' ',LAST_NAME) ELSE FRANCHISEE_NAME END AS FRANCHISEE_NAME, F.FRANCHISEE_NO,IF(F.REPORT_PERIOD_START_DATE='0000-00-00','',F.REPORT_PERIOD_START_DATE) AS OPENING_DATE ,CITY,STATE");
						} else {
							activeindevelopmentlocationQuery.append(
									"SELECT CASE WHEN FRANCHISEE_NAME IS NULL OR FRANCHISEE_NAME='' THEN CONCAT(FIRST_NAME,' ',LAST_NAME) ELSE FRANCHISEE_NAME END AS FRANCHISEE_NAME, F.FRANCHISEE_NO,'' AS OPENING_DATE ,CITY,STATE");
						}
					}
					activeindevelopmentlocationQuery.append(
							",GROUP_CONCAT(' ',CONCAT(OWNER_FIRST_NAME,' ',OWNER_LAST_NAME)) AS OWNER_NAME, AREA_NAME, STORE_PHONE, COUNTRY, F.STATUS");
					/*if (isFimUser) {
						if(!clientName.equals("SMCZcubator")){
						activelocationQuery
								.append(",(SELECT COUNT(DISTINCT FU.FRANCHISEE_USER_NO) FROM FIM_USERS FU , USERS U LEFT JOIN ( SELECT MAX(CHANGE_DATE) AS CHANGE_DATE,USER_NO,REMARK FROM USER_STATUS_CHANGED WHERE (REMARK='Deactivated' OR REMARK='Deleted' OR REMARK='Deactivated From Franchisor' ) GROUP BY USER_NO) AS USC ON U.USER_NO=USC.USER_NO LEFT JOIN  USER_STATUS_CHANGED USC1 ON U.USER_NO=USC1.USER_NO AND (USC1.REMARK='Activated' OR USC1.REMARK='Activated From Franchisor')  WHERE U.USER_ID!='adm' AND U.CREATION_DATE<='"
										+ dateTo + " 23:59:59' AND ( (USC.CHANGE_DATE IS NULL OR USC.CHANGE_DATE>='"
										+ dateFrom
										+ " 00:00:00') OR ( USC1.CHANGE_DATE IS NOT NULL AND  USC1.CHANGE_DATE<='"
										+ dateTo
										+ " 23:59:59' AND USC.CHANGE_DATE<=USC1.CHANGE_DATE) OR (USC.CHANGE_DATE IS NULL OR USC.CHANGE_DATE > '"
										+ dateTo + " 23:59:59'))   AND F.FRANCHISEE_NO=FU.FRANCHISEE_NO");
						}
					} else {
						activelocationQuery
								.append(",(SELECT COUNT(DISTINCT FU.FRANCHISEE_USER_NO) FROM FRANCHISEE_USERS FU , USERS U LEFT JOIN ( SELECT MAX(CHANGE_DATE) AS CHANGE_DATE,USER_NO,REMARK FROM USER_STATUS_CHANGED WHERE (REMARK='Deactivated' OR REMARK='Deleted' OR REMARK='Deactivated From Franchisor' ) GROUP BY USER_NO) AS USC ON U.USER_NO=USC.USER_NO LEFT JOIN  USER_STATUS_CHANGED USC1 ON U.USER_NO=USC1.USER_NO AND (USC1.REMARK='Activated' OR USC1.REMARK='Activated From Franchisor')  WHERE U.USER_ID!='adm' AND U.CREATION_DATE<='"
										+ dateTo + " 23:59:59' AND ( (USC.CHANGE_DATE IS NULL OR USC.CHANGE_DATE>='"
										+ dateFrom
										+ " 00:00:00') OR ( USC1.CHANGE_DATE IS NOT NULL AND  USC1.CHANGE_DATE<='"
										+ dateTo
										+ " 23:59:59' AND USC.CHANGE_DATE<=USC1.CHANGE_DATE) OR (USC.CHANGE_DATE IS NULL OR USC.CHANGE_DATE > '"
										+ dateTo + " 23:59:59'))   AND F.FRANCHISEE_NO=FU.FRANCHISEE_NO   ");
					}
					if (isBliable) {
						activelocationQuery.append(
								" AND U.IS_BILLABLE='Y' AND FU.FRANCHISEE_USER_NO = U.USER_IDENTITY_NO ) AS USER_COUNT ");
					} else {
						if(!clientName.equals("SMCZcubator")){
						activelocationQuery.append("  AND FU.FRANCHISEE_USER_NO = U.USER_IDENTITY_NO ) AS USER_COUNT ");
						}
					}*/
					activeindevelopmentlocationQuery.append(
							" FROM FRANCHISEE F LEFT JOIN OWNERS O ON F.FRANCHISEE_NO=O.FRANCHISEE_NO LEFT JOIN FIM_OWNERS FO ON O.OWNER_ID=FO.FRANCHISE_OWNER_ID LEFT JOIN AREAS A ON F.AREA_ID=A.AREA_ID");
					activeindevelopmentlocationQuery.append(" WHERE F.FRANCHISEE_NO IN (").append(hiddenvalueNumber).append(")");
					activeindevelopmentlocationQuery.append(
							"  AND lower(FRANCHISEE_NAME) NOT LIKE lower('%franconnect%') AND lower(FRANCHISEE_NAME) NOT LIKE lower('%test%') GROUP BY F.FRANCHISEE_NO ORDER BY FRANCHISEE_NAME ASC");

					exchange = endpoint.createExchange();
					exchange.getIn().setBody(activeindevelopmentlocationQuery.toString());
					 //System.out.println(clientName + "\n=======activeindevelopmentlocationQuery=========>" +activeindevelopmentlocationQuery.toString());
					Exchange locationQueryout = template.send(endpoint, exchange);
					List<Map<String, Object>> locationQueryresult = locationQueryout.getOut().getBody(List.class);
					int locationQueryi = locationQueryresult.size();
					HashMap<String, String> activeInDevelopmentLocationMap = null;
					HashMap<String, HashMap<String, String>> activeInDevelopmentLocationMapdata = new HashMap<String, HashMap<String, String>>();
					for (int j = 0; j < locationQueryi; j++) {
						Map<String, Object> row = locationQueryresult.get(j);
						activeInDevelopmentLocationMap = new HashMap<String, String>();

						if (row.get("FRANCHISEE_NO") != null) {
							activeInDevelopmentLocationMap.put("FRANCHISEE_NO", row.get("FRANCHISEE_NO") + "");
						} else {
							activeInDevelopmentLocationMap.put("FRANCHISEE_NO", "-1");
						}

						if (row.get("FRANCHISEE_NAME") != null) {
							activeInDevelopmentLocationMap.put("FRANCHISEE_NAME", row.get("FRANCHISEE_NAME") + "");
						} else {
							activeInDevelopmentLocationMap.put("FRANCHISEE_NAME", "");
						}

						if (row.get("OPENING_DATE") != null) {
							activeInDevelopmentLocationMap.put("OPENING_DATE", row.get("OPENING_DATE") + "");
						} else {
							activeInDevelopmentLocationMap.put("OPENING_DATE", "");
						}
						/*if(!clientName.equals("SMCZcubator")){
						if (row.get("USER_COUNT") != null) {

							activeLocationMap.put("USER_COUNT", row.get("USER_COUNT") + "");
						}*/
						    if(countMap.get(row.get("FRANCHISEE_NO")+"")!="0"){
						    	activeInDevelopmentLocationMap.put("USER_COUNT",countMap.get(row.get("FRANCHISEE_NO")+""));
						    }else{
						    	activeInDevelopmentLocationMap.put("USER_COUNT", "0");
						    }
						    

						    activeInDevelopmentLocationMapdata.put(j + "", activeInDevelopmentLocationMap);
					}
					finaldata.put("activeInDevelopmentLocationMap", activeInDevelopmentLocationMapdata);
				}
			} catch (Exception e) {
				System.out.println(
						"=Exception Found for Client====================>" + codeValue + "\n" + stackTraceToString(e));
				e.printStackTrace();
			}
			
			/*
			 * 
			 * Unique Cal
			 * 
			 * 
			 */

			int count = 0;
			int atleastOne = 0;
			String atleastOneUsers = "";
			String moreThanOrFiveUsers = "";
			String noUsers = "";
			int moreThanOrFive = 0;
			int cals = 0;
			int extraCals = 0;

			try {

				StringBuilder queryForLocation = new StringBuilder();

				if (isFimUser) {
					queryForLocation.append(
							"SELECT COUNT(DISTINCT FU.FRANCHISEE_USER_NO) AS COUNT,IFNULL(F.FRANCHISEE_NO,'-1') AS FRANCHISEE_NO FROM FRANCHISEE F,FIM_USERS FU ,USERS U  LEFT JOIN ( SELECT MAX(CHANGE_DATE) AS CHANGE_DATE,USER_NO,REMARK FROM USER_STATUS_CHANGED WHERE (REMARK='Deactivated' OR REMARK='Deleted' OR REMARK='Deactivated From Franchisor' ) GROUP BY USER_NO) AS USC ON U.USER_NO=USC.USER_NO LEFT JOIN  USER_STATUS_CHANGED USC1 ON U.USER_NO=USC1.USER_NO AND (USC1.REMARK='Activated' OR USC1.REMARK='Activated From Franchisor')");
				} else {
					queryForLocation.append(
							"SELECT COUNT(DISTINCT FU.FRANCHISEE_USER_NO) AS COUNT,IFNULL(F.FRANCHISEE_NO,'-1') AS FRANCHISEE_NO FROM FRANCHISEE F,FRANCHISEE_USERS FU ,USERS U  LEFT JOIN ( SELECT MAX(CHANGE_DATE) AS CHANGE_DATE,USER_NO,REMARK FROM USER_STATUS_CHANGED WHERE (REMARK='Deactivated' OR REMARK='Deleted' OR REMARK='Deactivated From Franchisor' ) GROUP BY USER_NO) AS USC ON U.USER_NO=USC.USER_NO LEFT JOIN  USER_STATUS_CHANGED USC1 ON U.USER_NO=USC1.USER_NO AND (USC1.REMARK='Activated' OR USC1.REMARK='Activated From Franchisor')");
				}
				// P_ADMIN_BUG_49441 ends
				queryForLocation.append(" WHERE U.USER_ID!='adm' AND U.CREATION_DATE<='" + dateTo
						+ " 23:59:59' AND ( (USC.CHANGE_DATE IS NULL OR USC.CHANGE_DATE>='" + dateFrom
						+ " 00:00:00') OR ( USC1.CHANGE_DATE IS NOT NULL AND  USC1.CHANGE_DATE<='" + dateTo
						+ " 23:59:59' AND USC.CHANGE_DATE<=USC1.CHANGE_DATE) OR (USC.CHANGE_DATE IS NULL OR USC.CHANGE_DATE > '"
						+ dateTo + " 23:59:59')) AND U.USER_NO!=1 AND U.USER_LEVEL = '1'  AND F.IS_FRANCHISEE='Y' ");// BB-20141006-154

				if (isBliable) {
					queryForLocation.append(" AND U.IS_BILLABLE='Y' ");
				}

				if (franchiseeNo != null && franchiseeNo.trim().length() != 0
						&& !franchiseeNo.trim().equalsIgnoreCase("null") && !"-1".equals(franchiseeNo)) {
					queryForLocation.append(" AND F.FRANCHISEE_NO IN (").append(franchiseeNo).append(")");
				}

				if ("updateCals".equals(fromWhere))
					// query.append(" AND U.USER_NO NOT IN (SELECT USER_NO FROM
					// USERS WHERE USER_NO IN (SELECT USER_NO FROM USER_ROLES
					// WHERE ROLE_ID IN (SELECT ROLE_ID FROM ROLE_CLIENT_MAPPING
					// WHERE ROLE_ID NOT IN (1,2,3,4)))) ");
					queryForLocation.append(
							" AND U.USER_NO IN (SELECT USER_NO FROM USER_ROLES WHERE ROLE_ID IN (SELECT R.ROLE_ID FROM ROLE R LEFT JOIN ROLE_CLIENT_MAPPING RCM ON R.ROLE_ID = RCM.ROLE_ID WHERE R.ROLE_ID NOT IN(1,2,3,4) AND RCM.ROLE_ID IS NULL) OR ROLE_ID IN (1,2,3,4)  ) ");

				// P_ADMIN_BUG_49441 starts
				// query.append(" AND F.FRANCHISEE_NO = U.FRANCHISEE_NO ");
				queryForLocation.append(
						" AND F.FRANCHISEE_NO = FU.FRANCHISEE_NO AND FU.FRANCHISEE_USER_NO=U.USER_IDENTITY_NO ");

				queryForLocation.append(
						" AND F.FRANCHISEE_NAME NOT LIKE '%franconnect%' AND F.FRANCHISEE_NAME NOT LIKE '%test%' AND F.FRANCHISEE_NAME NOT LIKE '%Test%' AND  F.FRANCHISEE_NAME NOT LIKE '%TEST%' ");
				// P_ADMIN_BUG_49441 ends
				queryForLocation.append("GROUP BY FRANCHISEE_NO");

				//System.out.println(clientName + "\n=======queryForLocation=========>" + queryForLocation.toString());
				exchange = endpoint.createExchange();
				exchange.getIn().setBody(queryForLocation.toString());
				// System.out.println(codeValue+"\nqueryForLocation===>"+queryForLocation.toString());
				Exchange queryForLocationout = template.send(endpoint, exchange);
				List<Map<String, Object>> queryForLocationresult = queryForLocationout.getOut().getBody(List.class);
				int queryForLocationi = queryForLocationresult.size();

				for (int j = 0; j < queryForLocationi; j++) {
					Map<String, Object> row = queryForLocationresult.get(j);

					count = Integer.parseInt(row.get("COUNT") + "");
					if (count > 0) {
						atleastOne++;
						atleastOneUsers += "," + row.get("FRANCHISEE_NO") + "";
						if (count > 5) {
							moreThanOrFive++;
							moreThanOrFiveUsers += "," + row.get("FRANCHISEE_NO") + "";
						}
						if (count % 5 != 0) {
							extraCals += count / 5;
						} else if (count % 5 == 0) {
							extraCals += count / 5 - 1;
						}
						if (count % 5 == 0)
							cals += count / 5;
						else
							cals += count / 5 + 1;
					}

				}

				calslocationhmap.put("atleastOne", atleastOne + "");
				calslocationhmap.put("moreThanOrFive", moreThanOrFive + "");
				calslocationhmap.put("extraCals", extraCals + "");
				calslocationhmap.put("cals", cals + "");

			} catch (Exception e) {
				System.out.println(
						"=Exception Found for Client====================>" + codeValue + "\n" + stackTraceToString(e));
				e.printStackTrace();
			}

			try {
				String fddcount = "0";
				exchange = endpoint.createExchange();
				StringBuilder fddQuery = null;
				fddQuery = new StringBuilder(
						"  SELECT COUNT(*) AS UFOC_COUNT FROM UFOC_SEND_SCHEDULE WHERE SENT_DATE >='" + dateFrom
								+ "' AND SENT_DATE<='" + dateTo + "' ");

				exchange.getIn().setBody(fddQuery.toString());
				// System.out.println(codeValue+"\nfddQuery===>"+fddQuery.toString());
				Exchange fddout = template.send(endpoint, exchange);
				List<Map<String, Object>> fddresult = fddout.getOut().getBody(List.class);
				int fddi = fddresult.size();

				for (int j = 0; j < fddi; j++) {
					Map<String, Object> row = fddresult.get(j);

					if (row.get("UFOC_COUNT") != null) {
						calslocationhmap.put("fddCount", row.get("UFOC_COUNT") + "");
					} else {
						calslocationhmap.put("fddCount", "0");
					}

				}

			} catch (Exception e) {

				// System.out.println("=Exception Found for
				// Client====================>"+codeValue+"\n"+stackTraceToString(e));
				// e.printStackTrace();

			}

			try {
				hiddenvalueNumber = "-1";
				if (atleastOne != 0) {
					hiddenvalueNumber = atleastOneUsers.substring(1);
				} else {
					hiddenvalueNumber = "-1";
				}

				if (hiddenvalueNumber != null && hiddenvalueNumber.trim().length() != 0
						&& !hiddenvalueNumber.trim().equalsIgnoreCase("null") && !"-1".equals(hiddenvalueNumber)) {
					StringBuilder uniquelocationQuery = new StringBuilder();
					uniquelocationQuery.append(
							"SELECT CASE WHEN FRANCHISEE_NAME IS NULL OR FRANCHISEE_NAME='' THEN CONCAT(FIRST_NAME,' ',LAST_NAME) ELSE FRANCHISEE_NAME END AS FRANCHISEE_NAME, F.FRANCHISEE_NO,CITY,STATE");
					uniquelocationQuery.append(
							",GROUP_CONCAT(' ',CONCAT(OWNER_FIRST_NAME,' ',OWNER_LAST_NAME)) AS OWNER_NAME, AREA_NAME, STORE_PHONE, COUNTRY, F.STATUS");
					/*if (isFimUser) {
						if(!clientName.equals("SMCZcubator")){
						uniquelocationQuery
								.append(",(SELECT COUNT(DISTINCT FU.FRANCHISEE_USER_NO) FROM FIM_USERS FU , USERS U LEFT JOIN ( SELECT MAX(CHANGE_DATE) AS CHANGE_DATE,USER_NO,REMARK FROM USER_STATUS_CHANGED WHERE (REMARK='Deactivated' OR REMARK='Deleted' OR REMARK='Deactivated From Franchisor' ) GROUP BY USER_NO) AS USC ON U.USER_NO=USC.USER_NO LEFT JOIN  USER_STATUS_CHANGED USC1 ON U.USER_NO=USC1.USER_NO AND (USC1.REMARK='Activated' OR USC1.REMARK='Activated From Franchisor')  WHERE U.USER_ID!='adm' AND U.CREATION_DATE<='"
										+ dateTo + " 23:59:59' AND ( (USC.CHANGE_DATE IS NULL OR USC.CHANGE_DATE>='"
										+ dateFrom
										+ " 00:00:00') OR ( USC1.CHANGE_DATE IS NOT NULL AND  USC1.CHANGE_DATE<='"
										+ dateTo
										+ " 23:59:59' AND USC.CHANGE_DATE<=USC1.CHANGE_DATE) OR (USC.CHANGE_DATE IS NULL OR USC.CHANGE_DATE > '"
										+ dateTo + " 23:59:59'))   AND F.FRANCHISEE_NO=FU.FRANCHISEE_NO");
						}
					} else {
						uniquelocationQuery
								.append(",(SELECT COUNT(DISTINCT FU.FRANCHISEE_USER_NO) FROM FRANCHISEE_USERS FU , USERS U LEFT JOIN ( SELECT MAX(CHANGE_DATE) AS CHANGE_DATE,USER_NO,REMARK FROM USER_STATUS_CHANGED WHERE (REMARK='Deactivated' OR REMARK='Deleted' OR REMARK='Deactivated From Franchisor' ) GROUP BY USER_NO) AS USC ON U.USER_NO=USC.USER_NO LEFT JOIN  USER_STATUS_CHANGED USC1 ON U.USER_NO=USC1.USER_NO AND (USC1.REMARK='Activated' OR USC1.REMARK='Activated From Franchisor')  WHERE U.USER_ID!='adm' AND U.CREATION_DATE<='"
										+ dateTo + " 23:59:59' AND ( (USC.CHANGE_DATE IS NULL OR USC.CHANGE_DATE>='"
										+ dateFrom
										+ " 00:00:00') OR ( USC1.CHANGE_DATE IS NOT NULL AND  USC1.CHANGE_DATE<='"
										+ dateTo
										+ " 23:59:59' AND USC.CHANGE_DATE<=USC1.CHANGE_DATE) OR (USC.CHANGE_DATE IS NULL OR USC.CHANGE_DATE > '"
										+ dateTo + " 23:59:59'))   AND F.FRANCHISEE_NO=FU.FRANCHISEE_NO   ");
					}
					if (isBliable) {
						uniquelocationQuery.append(
								" AND U.IS_BILLABLE='Y' AND FU.FRANCHISEE_USER_NO = U.USER_IDENTITY_NO ) AS USER_COUNT ");
					} else {
						if(!clientName.equals("SMCZcubator")){
						uniquelocationQuery.append("  AND FU.FRANCHISEE_USER_NO = U.USER_IDENTITY_NO ) AS USER_COUNT ");
						}
					}*/
					uniquelocationQuery.append(
							" FROM FRANCHISEE F LEFT JOIN OWNERS O ON F.FRANCHISEE_NO=O.FRANCHISEE_NO LEFT JOIN FIM_OWNERS FO ON O.OWNER_ID=FO.FRANCHISE_OWNER_ID LEFT JOIN AREAS A ON F.AREA_ID=A.AREA_ID");
					uniquelocationQuery.append(" WHERE F.FRANCHISEE_NO IN (").append(hiddenvalueNumber).append(")");
					uniquelocationQuery.append(
							"  AND lower(FRANCHISEE_NAME) NOT LIKE lower('%franconnect%') AND lower(FRANCHISEE_NAME) NOT LIKE lower('%test%') GROUP BY F.FRANCHISEE_NO ORDER BY FRANCHISEE_NAME ASC");

					exchange = endpoint.createExchange();
					exchange.getIn().setBody(uniquelocationQuery.toString());
					 //System.out.println(codeValue+"\n=======uniquelocationQuery=========>"+uniquelocationQuery.toString());
					Exchange activelocationQueryout = template.send(endpoint, exchange);
					List<Map<String, Object>> activelocationQueryresult = activelocationQueryout.getOut()
							.getBody(List.class);
					int activelocationQueryi = activelocationQueryresult.size();
					HashMap<String, String> uniquelocationhmap = null;
					HashMap<String, HashMap<String, String>> uniquelocationhmapdata = new HashMap<String, HashMap<String, String>>();
					for (int j = 0; j < activelocationQueryi; j++) {
						Map<String, Object> row = activelocationQueryresult.get(j);
						uniquelocationhmap = new HashMap<String, String>();

						if (row.get("FRANCHISEE_NO") != null) {
							uniquelocationhmap.put("FRANCHISEE_NO", row.get("FRANCHISEE_NO") + "");
						} else {
							uniquelocationhmap.put("FRANCHISEE_NO", "-1");
						}

						if (row.get("FRANCHISEE_NAME") != null) {
							uniquelocationhmap.put("FRANCHISEE_NAME", row.get("FRANCHISEE_NAME") + "");
						} else {
							uniquelocationhmap.put("FRANCHISEE_NAME", "");
						}
						/*if(!clientName.equals("SMCZcubator")){
						if (row.get("USER_COUNT") != null) {

							uniquelocationhmap.put("USER_COUNT", row.get("USER_COUNT") + "");
						} else {
							uniquelocationhmap.put("USER_COUNT", "0");
						}
						}*/
							uniquelocationhmap.put("USER_COUNT",countMap.get(row.get("FRANCHISEE_NO")+""));
						
						uniquelocationhmapdata.put(j + "", uniquelocationhmap);
					}

					finaldata.put("atleastOneUsersLocationMap", uniquelocationhmapdata);
				}
			} catch (Exception e) {
				System.out.println(
						"=Exception Found for Client====================>" + codeValue + "\n" + stackTraceToString(e));
				e.printStackTrace();
			}

			/*
			 * moreThanOrFivelocationMap
			 * 
			 */

			try {
				hiddenvalueNumber = "-1";
				if (moreThanOrFive != 0) {
					hiddenvalueNumber = moreThanOrFiveUsers.substring(1);
				} else {
					hiddenvalueNumber = "-1";
				}
				if (hiddenvalueNumber != null && hiddenvalueNumber.trim().length() != 0
						&& !hiddenvalueNumber.trim().equalsIgnoreCase("null") && !"-1".equals(hiddenvalueNumber)) {
					StringBuilder moreThanOrFiveUserslocationQuery = new StringBuilder();
					moreThanOrFiveUserslocationQuery.append(
							"SELECT CASE WHEN FRANCHISEE_NAME IS NULL OR FRANCHISEE_NAME='' THEN CONCAT(FIRST_NAME,' ',LAST_NAME) ELSE FRANCHISEE_NAME END AS FRANCHISEE_NAME, F.FRANCHISEE_NO,CITY,STATE");
					moreThanOrFiveUserslocationQuery.append(
							",GROUP_CONCAT(' ',CONCAT(OWNER_FIRST_NAME,' ',OWNER_LAST_NAME)) AS OWNER_NAME, AREA_NAME, STORE_PHONE, COUNTRY, F.STATUS");
					/*if (isFimUser) {
						if(!clientName.equals("SMCZcubator")){
						moreThanOrFiveUserslocationQuery
								.append(",(SELECT COUNT(DISTINCT FU.FRANCHISEE_USER_NO) FROM FIM_USERS FU , USERS U LEFT JOIN ( SELECT MAX(CHANGE_DATE) AS CHANGE_DATE,USER_NO,REMARK FROM USER_STATUS_CHANGED WHERE (REMARK='Deactivated' OR REMARK='Deleted' OR REMARK='Deactivated From Franchisor' ) GROUP BY USER_NO) AS USC ON U.USER_NO=USC.USER_NO LEFT JOIN  USER_STATUS_CHANGED USC1 ON U.USER_NO=USC1.USER_NO AND (USC1.REMARK='Activated' OR USC1.REMARK='Activated From Franchisor')  WHERE U.USER_ID!='adm' AND U.CREATION_DATE<='"
										+ dateTo + " 23:59:59' AND ( (USC.CHANGE_DATE IS NULL OR USC.CHANGE_DATE>='"
										+ dateFrom
										+ " 00:00:00') OR ( USC1.CHANGE_DATE IS NOT NULL AND  USC1.CHANGE_DATE<='"
										+ dateTo
										+ " 23:59:59' AND USC.CHANGE_DATE<=USC1.CHANGE_DATE) OR (USC.CHANGE_DATE IS NULL OR USC.CHANGE_DATE > '"
										+ dateTo + " 23:59:59'))   AND F.FRANCHISEE_NO=FU.FRANCHISEE_NO");
						}
					} else {
						moreThanOrFiveUserslocationQuery
								.append(",(SELECT COUNT(DISTINCT FU.FRANCHISEE_USER_NO) FROM FRANCHISEE_USERS FU , USERS U LEFT JOIN ( SELECT MAX(CHANGE_DATE) AS CHANGE_DATE,USER_NO,REMARK FROM USER_STATUS_CHANGED WHERE (REMARK='Deactivated' OR REMARK='Deleted' OR REMARK='Deactivated From Franchisor' ) GROUP BY USER_NO) AS USC ON U.USER_NO=USC.USER_NO LEFT JOIN  USER_STATUS_CHANGED USC1 ON U.USER_NO=USC1.USER_NO AND (USC1.REMARK='Activated' OR USC1.REMARK='Activated From Franchisor')  WHERE U.USER_ID!='adm' AND U.CREATION_DATE<='"
										+ dateTo + " 23:59:59' AND ( (USC.CHANGE_DATE IS NULL OR USC.CHANGE_DATE>='"
										+ dateFrom
										+ " 00:00:00') OR ( USC1.CHANGE_DATE IS NOT NULL AND  USC1.CHANGE_DATE<='"
										+ dateTo
										+ " 23:59:59' AND USC.CHANGE_DATE<=USC1.CHANGE_DATE) OR (USC.CHANGE_DATE IS NULL OR USC.CHANGE_DATE > '"
										+ dateTo + " 23:59:59'))   AND F.FRANCHISEE_NO=FU.FRANCHISEE_NO   ");
					}
					if (isBliable) {
						moreThanOrFiveUserslocationQuery.append(
								" AND U.IS_BILLABLE='Y' AND FU.FRANCHISEE_USER_NO = U.USER_IDENTITY_NO ) AS USER_COUNT ");
					} else {
						
						if(!clientName.equals("SMCZcubator")){
						moreThanOrFiveUserslocationQuery
								.append("  AND FU.FRANCHISEE_USER_NO = U.USER_IDENTITY_NO ) AS USER_COUNT ");
						}
					}*/
					moreThanOrFiveUserslocationQuery.append(
							" FROM FRANCHISEE F LEFT JOIN OWNERS O ON F.FRANCHISEE_NO=O.FRANCHISEE_NO LEFT JOIN FIM_OWNERS FO ON O.OWNER_ID=FO.FRANCHISE_OWNER_ID LEFT JOIN AREAS A ON F.AREA_ID=A.AREA_ID");
					moreThanOrFiveUserslocationQuery.append(" WHERE F.FRANCHISEE_NO IN (").append(hiddenvalueNumber)
							.append(")");
					moreThanOrFiveUserslocationQuery.append(
							" AND lower(FRANCHISEE_NAME) NOT LIKE lower('%franconnect%') AND lower(FRANCHISEE_NAME) NOT LIKE lower('%test%')  GROUP BY F.FRANCHISEE_NO ORDER BY FRANCHISEE_NAME ASC");

					exchange = endpoint.createExchange();
					exchange.getIn().setBody(moreThanOrFiveUserslocationQuery.toString());
					// System.out.println(codeValue+"\n====moreThanOrFiveUserslocationQuery=========>"+moreThanOrFiveUserslocationQuery.toString());
					Exchange moreThanOrFiveUserslocationQueryout = template.send(endpoint, exchange);
					List<Map<String, Object>> moreThanOrFiveUserslocationQueryresult = moreThanOrFiveUserslocationQueryout
							.getOut().getBody(List.class);
					int moreThanOrFiveUserslocationQueryi = moreThanOrFiveUserslocationQueryresult.size();
					HashMap<String, String> moreThanOrFiveUserslocationQueryhmap = null;
					HashMap<String, HashMap<String, String>> moreThanOrFiveUserslocationQueryhmapdata = new HashMap<String, HashMap<String, String>>();
					for (int j = 0; j < moreThanOrFiveUserslocationQueryi; j++) {
						Map<String, Object> row = moreThanOrFiveUserslocationQueryresult.get(j);
						moreThanOrFiveUserslocationQueryhmap = new HashMap<String, String>();

						if (row.get("FRANCHISEE_NO") != null) {
							moreThanOrFiveUserslocationQueryhmap.put("FRANCHISEE_NO", row.get("FRANCHISEE_NO") + "");
						} else {
							moreThanOrFiveUserslocationQueryhmap.put("FRANCHISEE_NO", "-1");
						}

						if (row.get("FRANCHISEE_NAME") != null) {
							moreThanOrFiveUserslocationQueryhmap.put("FRANCHISEE_NAME",
									row.get("FRANCHISEE_NAME") + "");
						} else {
							moreThanOrFiveUserslocationQueryhmap.put("FRANCHISEE_NAME", "");
						}
						/*if(!clientName.equals("SMCZcubator")){
						if (row.get("USER_COUNT") != null) {

							moreThanOrFiveUserslocationQueryhmap.put("USER_COUNT", row.get("USER_COUNT") + "");
						} else {
							moreThanOrFiveUserslocationQueryhmap.put("USER_COUNT", "0");
						}
						}*/
						moreThanOrFiveUserslocationQueryhmap.put("USER_COUNT",
								countMap.get(row.get("FRANCHISEE_NO") + ""));
						
						moreThanOrFiveUserslocationQueryhmapdata.put(j + "", moreThanOrFiveUserslocationQueryhmap);
					}

					finaldata.put("moreThanOrFiveUserslocationMap", moreThanOrFiveUserslocationQueryhmapdata);
				}
			} catch (Exception e) {
				System.out.println(
						"=Exception Found for Client====================>" + codeValue + "\n" + stackTraceToString(e));
				e.printStackTrace();
			}
			/*
			 * 
			 * Extra cal
			 * 
			 */
			try {
				hiddenvalueNumber = "-1";
				if (extraCals != 0) {
					hiddenvalueNumber = moreThanOrFiveUsers.substring(1);
				} else {
					hiddenvalueNumber = "-1";
				}
				if (hiddenvalueNumber != null && hiddenvalueNumber.trim().length() != 0
						&& !hiddenvalueNumber.trim().equalsIgnoreCase("null") && !"-1".equals(hiddenvalueNumber)) {
					StringBuilder extraCalslocationQuery = new StringBuilder();
					extraCalslocationQuery.append(
							"SELECT CASE WHEN FRANCHISEE_NAME IS NULL OR FRANCHISEE_NAME='' THEN CONCAT(FIRST_NAME,' ',LAST_NAME) ELSE FRANCHISEE_NAME END AS FRANCHISEE_NAME, F.FRANCHISEE_NO,CITY,STATE");
					extraCalslocationQuery.append(
							",GROUP_CONCAT(' ',CONCAT(OWNER_FIRST_NAME,' ',OWNER_LAST_NAME)) AS OWNER_NAME, AREA_NAME, STORE_PHONE, COUNTRY, F.STATUS");
					/*if (isFimUser) {
						if(!clientName.equals("SMCZcubator")){
						extraCalslocationQuery
								.append(",(SELECT COUNT(DISTINCT FU.FRANCHISEE_USER_NO) FROM FIM_USERS FU , USERS U LEFT JOIN ( SELECT MAX(CHANGE_DATE) AS CHANGE_DATE,USER_NO,REMARK FROM USER_STATUS_CHANGED WHERE (REMARK='Deactivated' OR REMARK='Deleted' OR REMARK='Deactivated From Franchisor' ) GROUP BY USER_NO) AS USC ON U.USER_NO=USC.USER_NO LEFT JOIN  USER_STATUS_CHANGED USC1 ON U.USER_NO=USC1.USER_NO AND (USC1.REMARK='Activated' OR USC1.REMARK='Activated From Franchisor')  WHERE U.USER_ID!='adm' AND U.CREATION_DATE<='"
										+ dateTo + " 23:59:59' AND ( (USC.CHANGE_DATE IS NULL OR USC.CHANGE_DATE>='"
										+ dateFrom
										+ " 00:00:00') OR ( USC1.CHANGE_DATE IS NOT NULL AND  USC1.CHANGE_DATE<='"
										+ dateTo
										+ " 23:59:59' AND USC.CHANGE_DATE<=USC1.CHANGE_DATE) OR (USC.CHANGE_DATE IS NULL OR USC.CHANGE_DATE > '"
										+ dateTo + " 23:59:59'))   AND F.FRANCHISEE_NO=FU.FRANCHISEE_NO");
						}
					} else {
						extraCalslocationQuery
								.append(",(SELECT COUNT(DISTINCT FU.FRANCHISEE_USER_NO) FROM FRANCHISEE_USERS FU , USERS U LEFT JOIN ( SELECT MAX(CHANGE_DATE) AS CHANGE_DATE,USER_NO,REMARK FROM USER_STATUS_CHANGED WHERE (REMARK='Deactivated' OR REMARK='Deleted' OR REMARK='Deactivated From Franchisor' ) GROUP BY USER_NO) AS USC ON U.USER_NO=USC.USER_NO LEFT JOIN  USER_STATUS_CHANGED USC1 ON U.USER_NO=USC1.USER_NO AND (USC1.REMARK='Activated' OR USC1.REMARK='Activated From Franchisor')  WHERE U.USER_ID!='adm' AND U.CREATION_DATE<='"
										+ dateTo + " 23:59:59' AND ( (USC.CHANGE_DATE IS NULL OR USC.CHANGE_DATE>='"
										+ dateFrom
										+ " 00:00:00') OR ( USC1.CHANGE_DATE IS NOT NULL AND  USC1.CHANGE_DATE<='"
										+ dateTo
										+ " 23:59:59' AND USC.CHANGE_DATE<=USC1.CHANGE_DATE) OR (USC.CHANGE_DATE IS NULL OR USC.CHANGE_DATE > '"
										+ dateTo + " 23:59:59'))   AND F.FRANCHISEE_NO=FU.FRANCHISEE_NO   ");
					}
					if (isBliable) {
						extraCalslocationQuery.append(
								" AND U.IS_BILLABLE='Y' AND FU.FRANCHISEE_USER_NO = U.USER_IDENTITY_NO ) AS USER_COUNT ");
					} else {
						if(!clientName.equals("SMCZcubator")){
						extraCalslocationQuery
								.append("  AND FU.FRANCHISEE_USER_NO = U.USER_IDENTITY_NO ) AS USER_COUNT ");
						}
					}*/
					extraCalslocationQuery.append(
							" FROM FRANCHISEE F LEFT JOIN OWNERS O ON F.FRANCHISEE_NO=O.FRANCHISEE_NO LEFT JOIN FIM_OWNERS FO ON O.OWNER_ID=FO.FRANCHISE_OWNER_ID LEFT JOIN AREAS A ON F.AREA_ID=A.AREA_ID");
					extraCalslocationQuery.append(" WHERE F.FRANCHISEE_NO IN (").append(hiddenvalueNumber).append(")");
					extraCalslocationQuery.append(
							" AND lower(FRANCHISEE_NAME) NOT LIKE lower('%franconnect%') AND lower(FRANCHISEE_NAME) NOT LIKE lower('%test%')  GROUP BY F.FRANCHISEE_NO ORDER BY FRANCHISEE_NAME ASC");

					exchange = endpoint.createExchange();
					exchange.getIn().setBody(extraCalslocationQuery.toString());
					// System.out.println(codeValue+"\n=======extraCalslocationQuery======>"+extraCalslocationQuery.toString());
					Exchange extraCalslocationQueryout = template.send(endpoint, exchange);
					List<Map<String, Object>> extraCalslocationQueryresult = extraCalslocationQueryout.getOut()
							.getBody(List.class);
					int extraCalslocationQueryi = extraCalslocationQueryresult.size();
					HashMap<String, String> extraCalslocationQueryhmap = null;
					HashMap<String, HashMap<String, String>> extraCalslocationQueryhmapdata = new HashMap<String, HashMap<String, String>>();
					for (int j = 0; j < extraCalslocationQueryi; j++) {
						Map<String, Object> row = extraCalslocationQueryresult.get(j);
						extraCalslocationQueryhmap = new HashMap<String, String>();

						if (row.get("FRANCHISEE_NO") != null) {
							extraCalslocationQueryhmap.put("FRANCHISEE_NO", row.get("FRANCHISEE_NO") + "");
						} else {
							extraCalslocationQueryhmap.put("FRANCHISEE_NO", "-1");
						}

						if (row.get("FRANCHISEE_NAME") != null) {
							extraCalslocationQueryhmap.put("FRANCHISEE_NAME", row.get("FRANCHISEE_NAME") + "");
						} else {
							extraCalslocationQueryhmap.put("FRANCHISEE_NAME", "");
						}
						/*if(!clientName.equals("SMCZcubator")){
						if (row.get("USER_COUNT") != null) {

							extraCalslocationQueryhmap.put("USER_COUNT", row.get("USER_COUNT") + "");
						} else {
							extraCalslocationQueryhmap.put("USER_COUNT", "0");
						}
						}*/
							extraCalslocationQueryhmap.put("USER_COUNT",countMap.get(row.get("FRANCHISEE_NO")+""));
						
						extraCalslocationQueryhmapdata.put(j + "", extraCalslocationQueryhmap);
					}
					finaldata.put("extraCalslocationMap", extraCalslocationQueryhmapdata);
				}
			} catch (Exception e) {
				System.out.println(
						"=Exception Found for Client====================>" + codeValue + "\n" + stackTraceToString(e));
				e.printStackTrace();
			}
			/*
			 * Total CAL
			 * 
			 * 
			 */

			try {
				hiddenvalueNumber = "-1";
				if (cals != 0) {
					hiddenvalueNumber = franchiseeNo;
				} else {
					hiddenvalueNumber = "-1";
				}
				if (hiddenvalueNumber != null && hiddenvalueNumber.trim().length() != 0
						&& !hiddenvalueNumber.trim().equalsIgnoreCase("null") && !"-1".equals(hiddenvalueNumber)) {
					StringBuilder CalslocationQuery = new StringBuilder();
					CalslocationQuery.append(
							"SELECT CASE WHEN FRANCHISEE_NAME IS NULL OR FRANCHISEE_NAME='' THEN CONCAT(FIRST_NAME,' ',LAST_NAME) ELSE FRANCHISEE_NAME END AS FRANCHISEE_NAME, F.FRANCHISEE_NO,CITY,STATE");
					CalslocationQuery.append(
							",GROUP_CONCAT(' ',CONCAT(OWNER_FIRST_NAME,' ',OWNER_LAST_NAME)) AS OWNER_NAME, AREA_NAME, STORE_PHONE, COUNTRY, F.STATUS");
					/*if (isFimUser) {
						if(!clientName.equals("SMCZcubator")){
						CalslocationQuery
								.append(",(SELECT COUNT(DISTINCT FU.FRANCHISEE_USER_NO) FROM FIM_USERS FU , USERS U LEFT JOIN ( SELECT MAX(CHANGE_DATE) AS CHANGE_DATE,USER_NO,REMARK FROM USER_STATUS_CHANGED WHERE (REMARK='Deactivated' OR REMARK='Deleted' OR REMARK='Deactivated From Franchisor' ) GROUP BY USER_NO) AS USC ON U.USER_NO=USC.USER_NO LEFT JOIN  USER_STATUS_CHANGED USC1 ON U.USER_NO=USC1.USER_NO AND (USC1.REMARK='Activated' OR USC1.REMARK='Activated From Franchisor')  WHERE U.USER_ID!='adm' AND U.CREATION_DATE<='"
										+ dateTo + " 23:59:59' AND ( (USC.CHANGE_DATE IS NULL OR USC.CHANGE_DATE>='"
										+ dateFrom
										+ " 00:00:00') OR ( USC1.CHANGE_DATE IS NOT NULL AND  USC1.CHANGE_DATE<='"
										+ dateTo
										+ " 23:59:59' AND USC.CHANGE_DATE<=USC1.CHANGE_DATE) OR (USC.CHANGE_DATE IS NULL OR USC.CHANGE_DATE > '"
										+ dateTo + " 23:59:59'))   AND F.FRANCHISEE_NO=FU.FRANCHISEE_NO");
						}
					} else {
						CalslocationQuery
								.append(",(SELECT COUNT(DISTINCT FU.FRANCHISEE_USER_NO) FROM FRANCHISEE_USERS FU , USERS U LEFT JOIN ( SELECT MAX(CHANGE_DATE) AS CHANGE_DATE,USER_NO,REMARK FROM USER_STATUS_CHANGED WHERE (REMARK='Deactivated' OR REMARK='Deleted' OR REMARK='Deactivated From Franchisor' ) GROUP BY USER_NO) AS USC ON U.USER_NO=USC.USER_NO LEFT JOIN  USER_STATUS_CHANGED USC1 ON U.USER_NO=USC1.USER_NO AND (USC1.REMARK='Activated' OR USC1.REMARK='Activated From Franchisor')  WHERE U.USER_ID!='adm' AND U.CREATION_DATE<='"
										+ dateTo + " 23:59:59' AND ( (USC.CHANGE_DATE IS NULL OR USC.CHANGE_DATE>='"
										+ dateFrom
										+ " 00:00:00') OR ( USC1.CHANGE_DATE IS NOT NULL AND  USC1.CHANGE_DATE<='"
										+ dateTo
										+ " 23:59:59' AND USC.CHANGE_DATE<=USC1.CHANGE_DATE) OR (USC.CHANGE_DATE IS NULL OR USC.CHANGE_DATE > '"
										+ dateTo + " 23:59:59'))   AND F.FRANCHISEE_NO=FU.FRANCHISEE_NO   ");
					}
					if (isBliable) {
						CalslocationQuery.append(
								" AND U.IS_BILLABLE='Y' AND FU.FRANCHISEE_USER_NO = U.USER_IDENTITY_NO ) AS USER_COUNT ");
					} else {
						if(!clientName.equals("SMCZcubator")){
						CalslocationQuery.append("  AND FU.FRANCHISEE_USER_NO = U.USER_IDENTITY_NO ) AS USER_COUNT ");
						}
					}*/
					CalslocationQuery.append(
							" FROM FRANCHISEE F LEFT JOIN OWNERS O ON F.FRANCHISEE_NO=O.FRANCHISEE_NO LEFT JOIN FIM_OWNERS FO ON O.OWNER_ID=FO.FRANCHISE_OWNER_ID LEFT JOIN AREAS A ON F.AREA_ID=A.AREA_ID");
					CalslocationQuery.append(" WHERE F.FRANCHISEE_NO IN (").append(hiddenvalueNumber).append(")");
					CalslocationQuery.append(
							" AND lower(FRANCHISEE_NAME) NOT LIKE lower('%franconnect%') AND lower(FRANCHISEE_NAME) NOT LIKE lower('%test%') GROUP BY F.FRANCHISEE_NO ORDER BY FRANCHISEE_NAME ASC");

					exchange = endpoint.createExchange();
					exchange.getIn().setBody(CalslocationQuery.toString());
				//	 System.out.println(codeValue+"\n=====CalslocationQuery========>"+CalslocationQuery.toString());
					Exchange CalslocationQueryout = template.send(endpoint, exchange);
					List<Map<String, Object>> CalslocationQueryresult = CalslocationQueryout.getOut()
							.getBody(List.class);
					int CalslocationQueryi = CalslocationQueryresult.size();
					HashMap<String, String> CalslocationQueryhmap = null;
					HashMap<String, HashMap<String, String>> CalslocationQueryhmapdata = new HashMap<String, HashMap<String, String>>();
					for (int j = 0; j < CalslocationQueryi; j++) {
						Map<String, Object> row = CalslocationQueryresult.get(j);
						CalslocationQueryhmap = new HashMap<String, String>();
						if (row.get("FRANCHISEE_NO") != null) {
							CalslocationQueryhmap.put("FRANCHISEE_NO", row.get("FRANCHISEE_NO") + "");
						} else {
							CalslocationQueryhmap.put("FRANCHISEE_NO", "-1");
						}

						if (row.get("FRANCHISEE_NAME") != null) {
							CalslocationQueryhmap.put("FRANCHISEE_NAME", row.get("FRANCHISEE_NAME") + "");
						} else {
							CalslocationQueryhmap.put("FRANCHISEE_NAME", "");
						}
						/*if(!clientName.equals("SMCZcubator")){
						if (row.get("USER_COUNT") != null) {

							CalslocationQueryhmap.put("USER_COUNT", row.get("USER_COUNT") + "");
						} else {
							CalslocationQueryhmap.put("USER_COUNT", "0");
						}
						}else{*/
							CalslocationQueryhmap.put("USER_COUNT",countMap.get(row.get("FRANCHISEE_NO")+""));
						CalslocationQueryhmapdata.put(j + "", CalslocationQueryhmap);
					}
					finaldata.put("TotalCalslocationMap", CalslocationQueryhmapdata);
				}
			} catch (Exception e) {
				System.out.println(
						"=Exception Found for Client====================>" + codeValue + "\n" + stackTraceToString(e));
				e.printStackTrace();
			}
			/*
			 * 
			 * Locations without any User
			 * 
			 * 
			 */

			try {
				String franchiseIdString = "-1";

				String query = "";

				if (isFimUser) {
					query = "SELECT IFNULL(GROUP_CONCAT(FU.FRANCHISEE_NO),'-1') AS FRANCHISEE_NO FROM FIM_USERS FU , USERS U  LEFT JOIN ( SELECT MAX(CHANGE_DATE) AS CHANGE_DATE,USER_NO,REMARK FROM USER_STATUS_CHANGED WHERE (REMARK='Deactivated' OR REMARK='Deleted' OR REMARK='Deactivated From Franchisor' ) GROUP BY USER_NO) AS USC ON U.USER_NO=USC.USER_NO LEFT JOIN  USER_STATUS_CHANGED USC1 ON U.USER_NO=USC1.USER_NO AND (USC1.REMARK='Activated' OR USC1.REMARK='Activated From Franchisor') ";// P_ADMIN_BUG_49441
				} else {
					query = "SELECT IFNULL(GROUP_CONCAT(FU.FRANCHISEE_NO),'-1') AS FRANCHISEE_NO FROM FRANCHISEE_USERS FU , USERS U  LEFT JOIN ( SELECT MAX(CHANGE_DATE) AS CHANGE_DATE,USER_NO,REMARK FROM USER_STATUS_CHANGED WHERE (REMARK='Deactivated' OR REMARK='Deleted' OR REMARK='Deactivated From Franchisor' ) GROUP BY USER_NO) AS USC ON U.USER_NO=USC.USER_NO LEFT JOIN  USER_STATUS_CHANGED USC1 ON U.USER_NO=USC1.USER_NO AND (USC1.REMARK='Activated' OR USC1.REMARK='Activated From Franchisor') ";// P_ADMIN_BUG_49441
				}

				query += " WHERE U.USER_ID!='adm' AND U.CREATION_DATE<='" + dateTo
						+ " 23:59:59' AND ( (USC.CHANGE_DATE IS NULL OR USC.CHANGE_DATE>='" + dateFrom
						+ " 00:00:00') OR ( USC1.CHANGE_DATE IS NOT NULL AND  USC1.CHANGE_DATE<='" + dateTo
						+ " 23:59:59' AND USC.CHANGE_DATE<=USC1.CHANGE_DATE)  OR (USC.CHANGE_DATE IS NULL OR USC.CHANGE_DATE > '"
						+ dateTo
						+ " 23:59:59')) AND U.USER_NO!=1 AND U.USER_LEVEL = '1'  AND FU.FRANCHISEE_USER_NO = U.USER_IDENTITY_NO  ";// BB-20141006-154
																																	// //P_ADMIN_BUG_49441

				if (isBliable) {
					query += " AND U.IS_BILLABLE='Y' ";
				}

				if ("updateCals".equals(fromWhere))
					query += " AND U.USER_NO IN (SELECT USER_NO FROM USER_ROLES WHERE ROLE_ID IN (SELECT R.ROLE_ID FROM ROLE R LEFT JOIN ROLE_CLIENT_MAPPING RCM ON R.ROLE_ID = RCM.ROLE_ID WHERE R.ROLE_ID NOT IN(1,2,3,4) AND RCM.ROLE_ID IS NULL) OR ROLE_ID IN (1,2,3,4)  ) ";
				// query+=" AND U.USER_NO NOT IN (SELECT USER_NO FROM USERS
				// WHERE USER_NO IN (SELECT USER_NO FROM USER_ROLES WHERE
				// ROLE_ID IN (SELECT ROLE_ID FROM ROLE_CLIENT_MAPPING WHERE
				// ROLE_ID NOT IN (1,2,3,4)))) ";

				// P_B_FCC-78 starts

				exchange = endpoint.createExchange();
				exchange.getIn().setBody(query.toString());
				//System.out.println(codeValue + "\n==QUERY  Locations without any User============>" + query.toString());
				Exchange queryout = template.send(endpoint, exchange);
				List<Map<String, Object>> queryresult = queryout.getOut().getBody(List.class);
				int queryi = queryresult.size();
				for (int j = 0; j < queryi; j++) {
					Map<String, Object> row = queryresult.get(j);
					if (row.get("FRANCHISEE_NO") != null) {
						if (row.get("FRANCHISEE_NO") instanceof byte[]) {
							final String pass = new String((byte[]) row.get("FRANCHISEE_NO"));
							franchiseIdString = pass;
						} else {
							franchiseIdString = (String) row.get("FRANCHISEE_NO");
						}

					} else {
						franchiseIdString = "-1";
					}
				}

				StringBuilder queryForNoUsers = new StringBuilder(
						"SELECT COUNT(FRANCHISEE_NO) AS COUNT, IFNULL(GROUP_CONCAT(FRANCHISEE_NO),'-1') AS FRANCHISEE_NO FROM FRANCHISEE WHERE");
				queryForNoUsers
						.append(" (FRANCHISEE_NO NOT IN (" + franchiseIdString
								+ ") AND IS_FRANCHISEE='Y' AND STATUS IN (0,4) AND DEACTIVATION_DATE >= '")
						.append(dateFrom).append(" 00:00:00' AND DEACTIVATION_DATE <= '").append(dateTo)
						.append(" 23:59:59' AND lower(FRANCHISEE_NAME) NOT LIKE '%franconnect%' AND lower(FRANCHISEE_NAME) NOT LIKE '%test%') ");
				queryForNoUsers
						.append(" OR (FRANCHISEE_NO NOT IN (" + franchiseIdString
								+ ") AND IS_FRANCHISEE='Y' AND STATUS IN (1,3) AND CREATION_DATE <= '")
						.append(dateTo)
						.append(" 23:59:59' AND lower(FRANCHISEE_NAME) NOT LIKE '%franconnect%' AND lower(FRANCHISEE_NAME) NOT LIKE '%test%')");
				queryForNoUsers
						.append(" OR (FRANCHISEE_NO NOT IN (" + franchiseIdString
								+ ") AND IS_FRANCHISEE='Y' AND STATUS IN (0,4) AND DEACTIVATION_DATE > '")
						.append(dateTo).append(" 23:59:59' AND CREATION_DATE <= '" + dateTo
								+ " 23:59:59' AND lower(FRANCHISEE_NAME) NOT LIKE '%franconnect%' AND lower(FRANCHISEE_NAME) NOT LIKE '%test%')");

				// queryForNoUsers.append(" AND lower(FRANCHISEE_NAME) NOT LIKE
				// '%franconnect%' AND lower(FRANCHISEE_NAME) NOT LIKE
				// '%test%')");

				String franchiseeNOforNoUser = "-1";
				String countforUser = "0";
				exchange = endpoint.createExchange();
				exchange.getIn().setBody(queryForNoUsers.toString());
			//	 System.out.println(codeValue+"\n=====queryForNoUsers===========>"+queryForNoUsers.toString());
				Exchange queryForNoUsersout = template.send(endpoint, exchange);
				List<Map<String, Object>> queryForNoUsersresult = queryForNoUsersout.getOut().getBody(List.class);
				int queryForNoUsersi = queryForNoUsersresult.size();
				for (int j = 0; j < queryForNoUsersi; j++) {
					Map<String, Object> row = queryForNoUsersresult.get(j);
					if (row.get("FRANCHISEE_NO") != null) {
						if (row.get("FRANCHISEE_NO") instanceof byte[]) {
							final String pass = new String((byte[]) row.get("FRANCHISEE_NO"));
							franchiseeNOforNoUser = pass;
						} else {
							franchiseeNOforNoUser = (String) row.get("FRANCHISEE_NO");
						}

					} else {
						franchiseeNOforNoUser = "-1";
					}

					if (row.get("COUNT") != null) {
						if (row.get("COUNT") instanceof byte[]) {
							final String pass = new String((byte[]) row.get("COUNT"));
							countforUser = pass;
						} else {
							countforUser = row.get("COUNT") + "";
						}

					} else {
						countforUser = "0";
					}
				}

				calslocationhmap.put("noUserlocation", countforUser);
				hiddenvalueNumber = "-1";
				if (!countforUser.equals("0")) {
					hiddenvalueNumber = franchiseeNOforNoUser;
				} else {
					hiddenvalueNumber = "-1";
				}
				if (hiddenvalueNumber != null && hiddenvalueNumber.trim().length() != 0
						&& !hiddenvalueNumber.trim().equalsIgnoreCase("null") && !"-1".equals(hiddenvalueNumber)) {
					StringBuilder noUserlocationQuery = new StringBuilder();
					noUserlocationQuery.append(
							"SELECT CASE WHEN FRANCHISEE_NAME IS NULL OR FRANCHISEE_NAME='' THEN CONCAT(FIRST_NAME,' ',LAST_NAME) ELSE FRANCHISEE_NAME END AS FRANCHISEE_NAME, F.FRANCHISEE_NO,CITY,STATE");
					noUserlocationQuery.append(
							",GROUP_CONCAT(' ',CONCAT(OWNER_FIRST_NAME,' ',OWNER_LAST_NAME)) AS OWNER_NAME, AREA_NAME, STORE_PHONE, COUNTRY, F.STATUS");
					/*if (isFimUser) {
						if(!clientName.equals("SMCZcubator")){
						noUserlocationQuery
								.append(",(SELECT COUNT(DISTINCT FU.FRANCHISEE_USER_NO) FROM FIM_USERS FU , USERS U LEFT JOIN ( SELECT MAX(CHANGE_DATE) AS CHANGE_DATE,USER_NO,REMARK FROM USER_STATUS_CHANGED WHERE (REMARK='Deactivated' OR REMARK='Deleted' OR REMARK='Deactivated From Franchisor' ) GROUP BY USER_NO) AS USC ON U.USER_NO=USC.USER_NO LEFT JOIN  USER_STATUS_CHANGED USC1 ON U.USER_NO=USC1.USER_NO AND (USC1.REMARK='Activated' OR USC1.REMARK='Activated From Franchisor')  WHERE U.USER_ID!='adm' AND U.CREATION_DATE<='"
										+ dateTo + " 23:59:59' AND ( (USC.CHANGE_DATE IS NULL OR USC.CHANGE_DATE>='"
										+ dateFrom
										+ " 00:00:00') OR ( USC1.CHANGE_DATE IS NOT NULL AND  USC1.CHANGE_DATE<='"
										+ dateTo
										+ " 23:59:59' AND USC.CHANGE_DATE<=USC1.CHANGE_DATE) OR (USC.CHANGE_DATE IS NULL OR USC.CHANGE_DATE > '"
										+ dateTo + " 23:59:59'))   AND F.FRANCHISEE_NO=FU.FRANCHISEE_NO");
						}
					} else {
						noUserlocationQuery
								.append(",(SELECT COUNT(DISTINCT FU.FRANCHISEE_USER_NO) FROM FRANCHISEE_USERS FU , USERS U LEFT JOIN ( SELECT MAX(CHANGE_DATE) AS CHANGE_DATE,USER_NO,REMARK FROM USER_STATUS_CHANGED WHERE (REMARK='Deactivated' OR REMARK='Deleted' OR REMARK='Deactivated From Franchisor' ) GROUP BY USER_NO) AS USC ON U.USER_NO=USC.USER_NO LEFT JOIN  USER_STATUS_CHANGED USC1 ON U.USER_NO=USC1.USER_NO AND (USC1.REMARK='Activated' OR USC1.REMARK='Activated From Franchisor')  WHERE U.USER_ID!='adm' AND U.CREATION_DATE<='"
										+ dateTo + " 23:59:59' AND ( (USC.CHANGE_DATE IS NULL OR USC.CHANGE_DATE>='"
										+ dateFrom
										+ " 00:00:00') OR ( USC1.CHANGE_DATE IS NOT NULL AND  USC1.CHANGE_DATE<='"
										+ dateTo
										+ " 23:59:59' AND USC.CHANGE_DATE<=USC1.CHANGE_DATE) OR (USC.CHANGE_DATE IS NULL OR USC.CHANGE_DATE > '"
										+ dateTo + " 23:59:59'))   AND F.FRANCHISEE_NO=FU.FRANCHISEE_NO   ");
					}
					if (isBliable) {
						noUserlocationQuery.append(
								" AND U.IS_BILLABLE='Y' AND FU.FRANCHISEE_USER_NO = U.USER_IDENTITY_NO ) AS USER_COUNT ");
					} else {
						if(!clientName.equals("SMCZcubator")){
						noUserlocationQuery.append("  AND FU.FRANCHISEE_USER_NO = U.USER_IDENTITY_NO ) AS USER_COUNT ");
						}
					}*/
					noUserlocationQuery.append(
							" FROM FRANCHISEE F LEFT JOIN OWNERS O ON F.FRANCHISEE_NO=O.FRANCHISEE_NO LEFT JOIN FIM_OWNERS FO ON O.OWNER_ID=FO.FRANCHISE_OWNER_ID LEFT JOIN AREAS A ON F.AREA_ID=A.AREA_ID");
					noUserlocationQuery.append(" WHERE F.FRANCHISEE_NO IN (").append(hiddenvalueNumber).append(")");
					noUserlocationQuery.append(
							"  AND lower(FRANCHISEE_NAME) NOT LIKE lower('%franconnect%') AND lower(FRANCHISEE_NAME) NOT LIKE lower('%test%') GROUP BY F.FRANCHISEE_NO ORDER BY FRANCHISEE_NAME ASC");

					exchange = endpoint.createExchange();
					exchange.getIn().setBody(noUserlocationQuery.toString());
					// System.out.println(codeValue+"\n======noUserlocationQuery=========>"+noUserlocationQuery.toString());
					Exchange noUserlocationQueryout = template.send(endpoint, exchange);
					List<Map<String, Object>> noUserlocationQueryresult = noUserlocationQueryout.getOut()
							.getBody(List.class);
					int noUserlocationQueryi = noUserlocationQueryresult.size();
					HashMap<String, String> noUserlocationQueryhmap = null;
					HashMap<String, HashMap<String, String>> noUserlocationQueryhmapdata = new HashMap<String, HashMap<String, String>>();
					for (int j = 0; j < noUserlocationQueryi; j++) {
						Map<String, Object> row = noUserlocationQueryresult.get(j);
						noUserlocationQueryhmap = new HashMap<String, String>();

						if (row.get("FRANCHISEE_NO") != null) {
							noUserlocationQueryhmap.put("FRANCHISEE_NO", row.get("FRANCHISEE_NO") + "");
						} else {
							noUserlocationQueryhmap.put("FRANCHISEE_NO", "-1");
						}

						if (row.get("FRANCHISEE_NAME") != null) {
							noUserlocationQueryhmap.put("FRANCHISEE_NAME", row.get("FRANCHISEE_NAME") + "");
						} else {
							noUserlocationQueryhmap.put("FRANCHISEE_NAME", "");
						}
						/*if(!clientName.equals("SMCZcubator")){
						if (row.get("USER_COUNT") != null) {

							noUserlocationQueryhmap.put("USER_COUNT", row.get("USER_COUNT") + "");
						} else {
							noUserlocationQueryhmap.put("USER_COUNT", "0");
						}
						}else{*/
							noUserlocationQueryhmap.put("USER_COUNT",countMap.get(row.get("FRANCHISEE_NO")+""));
						
						noUserlocationQueryhmapdata.put(j + "", noUserlocationQueryhmap);
					}
					finaldata.put("noUserlocationMap", noUserlocationQueryhmapdata);
				}
			} catch (Exception e) {
				System.out.println(
						"=Exception Found for Client====================>" + codeValue + "\n" + stackTraceToString(e));
				e.printStackTrace();
			}

			/*
			 * 
			 */
			if (contextPath.equals("/ClockWork") || contextPath.equals("/RightAtHome")
					|| contextPath.equals("/MosquitoJoe"))
				try {
					StringBuilder muidQuery = new StringBuilder();
					muidQuery.append(
							"SELECT FO.FRANCHISE_OWNER_ID,CONCAT(FO.OWNER_FIRST_NAME,' ',FO.OWNER_LAST_NAME) OWNER_NAME,O.MUID_VALUE,U.USER_ID,GROUP_CONCAT(DISTINCT F.FRANCHISEE_NAME SEPARATOR ' | ') FRANCHISEE_NAME, COUNT(DISTINCT O.FRANCHISEE_NO) FRANCHISEE_COUNT,CONCAT(U.USER_FIRST_NAME,' ',U.USER_LAST_NAME) AS USER_NAME,FRU.EMAIL_ID AS FRAN_EMAIL_ID  FROM OWNERS O   INNER JOIN FIM_OWNERS FO ON FO.FRANCHISE_OWNER_ID = O.OWNER_ID  LEFT JOIN FRANCHISEE F ON F.FRANCHISEE_NO = O.FRANCHISEE_NO   LEFT JOIN FIM_ENTITY_LOCATION_MAPPING FELM ON FELM.FRANCHISEE_NO=F.FRANCHISEE_NO  LEFT JOIN FIM_ENTITY_DETAIL FED ON FED.FIM_ENTITY_ID=FELM.FIM_ENTITY_ID LEFT JOIN FIM_USERS FU ON FU.USER_TYPE_NO=FO.FRANCHISE_OWNER_ID LEFT JOIN USERS U ON U.USER_IDENTITY_NO=FU.FRANCHISEE_USER_NO LEFT JOIN FRANCHISEE_USERS FRU ON FRU.FRANCHISEE_USER_NO=U.USER_IDENTITY_NO  WHERE F.STATUS IN (1,3)  AND (F.IS_FRANCHISEE='Y' OR (F.IS_STORE='Y' AND IS_STORE_ARCHIVED!='Y')) AND U.STATUS=1 GROUP BY FO.FRANCHISE_OWNER_ID  HAVING FRANCHISEE_COUNT >1  ORDER BY OWNER_NAME");
					exchange = endpoint.createExchange();
					System.out.println(contextPath+muidQuery);
					exchange.getIn().setBody(muidQuery.toString());
					Exchange muidQueryOut = template.send(endpoint, exchange);
					List<Map<String, Object>> muidQueryOutResult = muidQueryOut.getOut().getBody(List.class);
					int muidUserCount = muidQueryOutResult.size();
					HashMap<String, String> muidUserCounthmap = null;
					HashMap<String, HashMap<String, String>> muidUserCounthmapdata = new HashMap<String, HashMap<String, String>>();
					for (int j = 0; j < muidUserCount; j++) {
						Map<String, Object> row = muidQueryOutResult.get(j);
						muidUserCounthmap = new HashMap<String, String>();
						if (row.get("FRANCHISEE_NAME") != null) {
							muidUserCounthmap.put("FRANCHISEE_NAME", row.get("FRANCHISEE_NAME") + "");
						} else {
							muidUserCounthmap.put("FRANCHISEE_NAME", "-1");
						}
						if (row.get("OWNER_NAME") != null) {
							muidUserCounthmap.put("OWNER_NAME", row.get("OWNER_NAME") + "");
						} else {
							muidUserCounthmap.put("OWNER_NAME", "-1");
						}
						if (row.get("USER_NAME") != null) {
							muidUserCounthmap.put("USER_NAME", row.get("USER_NAME") + "");
						} else {
							muidUserCounthmap.put("USER_NAME", "-1");
						}
						if (row.get("MUID_VALUE") != null) {
							muidUserCounthmap.put("MUID_VALUE", row.get("MUID_VALUE") + "");
						} else {
							muidUserCounthmap.put("MUID_VALUE", "-1");
						}
						if (row.get("FRANCHISEE_COUNT") != null) {
							muidUserCounthmap.put("FRANCHISEE_COUNT", row.get("FRANCHISEE_COUNT") + "");
						} else {
							muidUserCounthmap.put("FRANCHISEE_COUNT", "-1");
						}
						if (row.get("FRAN_EMAIL_ID") != null) {
							muidUserCounthmap.put("FRAN_EMAIL_ID", row.get("FRAN_EMAIL_ID") + "");
						} else {
							muidUserCounthmap.put("FRAN_EMAIL_ID", "-1");
						}
						if (row.get("FRANCHISE_OWNER_ID") != null) {
							muidUserCounthmap.put("FRANCHISE_OWNER_ID", row.get("FRANCHISE_OWNER_ID") + "");
						} else {
							muidUserCounthmap.put("FRANCHISE_OWNER_ID", "-1");
						}
						if (row.get("USER_ID") != null) {
							muidUserCounthmap.put("USER_ID", row.get("USER_ID") + "");
						} else {
							muidUserCounthmap.put("USER_ID", "-1");
						}
						muidUserCounthmapdata.put(j + "", muidUserCounthmap);
					}
					finaldata.put("muidLocationMap", muidUserCounthmapdata);
				} catch (Exception e) {
					System.out.println("=Exception Found for Client==========MUID section==========>" + codeValue + "\n"
							+ stackTraceToString(e));
					e.printStackTrace();
				}
			if ("/lightbridgeAcademy".equals(contextPath)) {
				try {
					Endpoint endpointInHouse = DBConnectionProvider.getInstance().getDBConnection("InHouse");
					String salesprivilege = "";
					HashMap<String, String> internetPrivilege = new HashMap<>();
					HashMap<String, String> openerPrivilege = new HashMap<>();
					int j = 0;
					String priviledgeQuery = "SELECT GROUP_CONCAT(PRIVILEGE_ID) AS PRIVILEGE_ID ,MODULE FROM FC_PRIVILEGES  WHERE MODULE IN ('Intranet') GROUP BY MODULE";
					Map<String, Object> row = null;
					List privilegeMap = QueryUtil.executeQuerySelect(endpointInHouse, priviledgeQuery);
					int privilegeSize = privilegeMap.size();
					String roleId = "";
					String openerID = "";
					for (int l = 0; l < privilegeSize; l++) {
						row = (Map<String, Object>) privilegeMap.get(l);
						salesprivilege = salesprivilege + row.get("PRIVILEGE_ID") + "";
						String allPrivilege[] = salesprivilege.split(",");
						for (String privilege : allPrivilege) {
							internetPrivilege.put(privilege, "");
						}
					}
					priviledgeQuery = "SELECT GROUP_CONCAT(PRIVILEGE_ID) AS PRIVILEGE_ID ,MODULE FROM FC_PRIVILEGES  WHERE MODULE IN ('Opener') GROUP BY MODULE";
					row = null;
					privilegeMap = QueryUtil.executeQuerySelect(endpointInHouse, priviledgeQuery);
					privilegeSize = privilegeMap.size();
					String openerPrivilge = "";
					for (int l = 0; l < privilegeSize; l++) {
						row = (Map<String, Object>) privilegeMap.get(l);
						salesprivilege = salesprivilege + row.get("PRIVILEGE_ID") + "";
						openerPrivilge = row.get("PRIVILEGE_ID") + "";
						for (String privilege : openerPrivilge.split(",")) {
							openerPrivilege.put(privilege, "");
						}
					}
					List data = null;
					Map<String, Object> row1 = null;
					String roleQuery = "SELECT GROUP_CONCAT(ROLE_ID) AS ROLE_ID FROM ROLE WHERE ROLE_ID IN (SELECT DISTINCT ROLE_ID  FROM ROLE_PRIVILEGES WHERE PRIVILEGE_ID IN ("
							+ salesprivilege + "))";
					List<Map<String, Object>> result = QueryUtil.executeQuerySelect(endpoint, roleQuery);
					for (j = 0; j < result.size(); j++) {
						row1 = result.get(j);
						roleId = roleId + row1.get("ROLE_ID") + "";
					}
					String openerQuery = "SELECT COUNT(DISTINCT F.FRANCHISEE_NO) AS COUNT,GROUP_CONCAT(DISTINCT F.FRANCHISEE_NO)  FRAN_NO  FROM FRANCHISEE F LEFT JOIN STORE_TYPE ST ON ST.ST_ID = F.ST_ID LEFT JOIN OWNERS O ON F.FRANCHISEE_NO= O.FRANCHISEE_NO LEFT JOIN MASTER_DATA MASTER ON MASTER.MASTER_DATA_ID = F.TRANSACTION_TYPE LEFT JOIN AREAS AR ON F.AREA_ID = AR.AREA_ID LEFT JOIN FIM_OWNERS FO ON O.OWNER_ID = FO.FRANCHISE_OWNER_ID   LEFT JOIN PROJECT_STATUS PS ON F.PROJECT_ID=PS.PROJECT_ID  LEFT JOIN FIM_ENTITY_LOCATION_MAPPING FELM ON F.FRANCHISEE_NO = FELM.FRANCHISEE_NO LEFT JOIN FIM_ENTITY_DETAIL FED ON FED.FIM_ENTITY_ID = FELM.FIM_ENTITY_ID WHERE  F.FRANCHISEE_NO = O.FRANCHISEE_NO AND O.OWNER_ID = FO.FRANCHISE_OWNER_ID AND IS_STORE ='Y'  AND IS_STORE_ARCHIVED='N'  ";
					result = QueryUtil.executeQuerySelect(endpoint, openerQuery);
					for (j = 0; j < result.size(); j++) {
						row1 = result.get(j);
						openerID = openerID + row1.get("FRAN_NO") + "";
					}
					String opnerHubUser = "SELECT F.FRANCHISEE_NO,FRANCHISEE_NAME,FM.FRANCHISEE_USER_NO,U.USER_ID,FU.FIRST_NAME,FU.LAST_NAME,FU.EMAIL_ID,FM.USER_TYPE,GROUP_CONCAT(DISTINCT(R.NAME)) AS ROLE ,GROUP_CONCAT(PRIVILEGE_ID) AS PRIVILEGE_ID FROM FRANCHISEE F  LEFT JOIN FIM_USERS FM ON F.FRANCHISEE_NO=FM.FRANCHISEE_NO LEFT JOIN  FRANCHISEE_USERS FU ON FM.FRANCHISEE_USER_NO=FU.FRANCHISEE_USER_NO  LEFT JOIN USERS U ON FU.FRANCHISEE_USER_NO=U.USER_IDENTITY_NO  LEFT JOIN  USER_ROLES UR ON UR.USER_NO=U.USER_NO LEFT JOIN ROLE R ON UR.ROLE_ID=R.ROLE_ID LEFT JOIN ROLE_PRIVILEGES RP ON RP.ROLE_ID=R.ROLE_ID  WHERE F.FRANCHISEE_NO IN ("
							+ openerID + ") AND R.ROLE_ID IN (" + roleId
							+ ") AND FM.FRANCHISEE_USER_NO IS NOT NULL AND U.STATUS=1 GROUP BY FRANCHISEE_USER_NO;";
					System.out.println(opnerHubUser);
					String privilegeId = "";
					result = QueryUtil.executeQuerySelect(endpoint, opnerHubUser);
					HashMap<String, HashMap<String, String>> lightbridgeMap = new HashMap<>();
					HashMap<String, String> lightbridgeMapDetail = new HashMap<>();
					String isOpenerAccess = "";
					String isHubAccess = "";
					for (j = 0; j < result.size(); j++) {
						row1 = result.get(j);
						lightbridgeMapDetail = new HashMap<>();
						lightbridgeMapDetail.put("FRANCHISEE_NO", row1.get("FRANCHISEE_NO") + "");
						lightbridgeMapDetail.put("FRANCHISEE_NAME", row1.get("FRANCHISEE_NAME") + "");
						lightbridgeMapDetail.put("FRANCHISEE_USER_NO", row1.get("FRANCHISEE_USER_NO") + "");
						lightbridgeMapDetail.put("USER_ID", row1.get("USER_ID") + "");
						lightbridgeMapDetail.put("FIRST_NAME", row1.get("FIRST_NAME") + "");
						lightbridgeMapDetail.put("LAST_NAME", row1.get("LAST_NAME") + "");
						lightbridgeMapDetail.put("EMAIL_ID", row1.get("EMAIL_ID") + "");
						lightbridgeMapDetail.put("USER_TYPE", row1.get("USER_TYPE") + "");
						lightbridgeMapDetail.put("ROLE", row1.get("ROLE") + "");
						privilegeId = row1.get("PRIVILEGE_ID") + "";
						String privilege[] = privilegeId.split(",");
						for (String p : privilege) {
							if (openerPrivilege.containsKey(p)) {
								isOpenerAccess = "Y";
								lightbridgeMapDetail.put("OPENER_ACCESS", isOpenerAccess);
								break;
							}
						}	
						for(String p : privilege)	{
							if (internetPrivilege.containsKey(p)) {
								isHubAccess = "Y";
								lightbridgeMapDetail.put("HUB_ACCESS", isHubAccess);
								break;
							}
						}
						lightbridgeMap.put(j + "", lightbridgeMapDetail);
					}
					finaldata.put("lightBridgeMap", lightbridgeMap);
				} catch (Exception e1) {
					System.out.println("=Exception Found forClient====================>" + codeValue + "\n"
							+ stackTraceToString(e1));
					e1.printStackTrace();
				}
			}
			/*
			 * MUID Check for Legacy Only 
			 */
			StringBuilder MUIDUserQuery = new StringBuilder("SELECT  GROUP_CONCAT(DISTINCT(USER_NO)) AS userNo  FROM   FRANCHISEE_USERS FU ,USERS U WHERE U.USER_IDENTITY_NO=FU.FRANCHISEE_USER_NO AND U.STATUS=1 AND FU.USER_TYPE='Owner' GROUP BY USER_TYPE_NO having COUNT(*)>1");
			List<Map<String, Object>> resultmuid = QueryUtil.executeQuerySelect(endpoint, MUIDUserQuery.toString());
			Map<String, Object> row1 = null;
			String muidUserlist = "";
			ArrayList<String> muidUser = new ArrayList<>();
			if(resultmuid!=null) {
			for (int j = 0; j < resultmuid.size(); j++) {
				row1 = resultmuid.get(j);
				muidUserlist=row1.get("userNo")+"".trim();
				muidUser.addAll(Arrays.asList(muidUserlist.split(",")));
			}
			}
			/*
			 * }
			 * 
			 * User data Start
			 * 
			 * 
			 */
			try {

				StringBuilder uniqueActiveUserQuery = null;

				if (isuserFirstNameAvailable) {

					if ("/frannet".equals(contextPath)) {
						uniqueActiveUserQuery = new StringBuilder(
								"SELECT COUNT(*) AS COUNT FROM (SELECT F.FRANCHISEE_NAME,F.AREA_ID,A.AREA_NAME,U.USER_ID,U.TIMEZONE,FU.USER_TYPE,(SELECT GROUP_CONCAT(NAME) FROM ROLE WHERE ROLE_ID IN (SELECT ROLE_ID FROM USER_ROLES WHERE USER_NO=U.USER_NO)) AS ROLE , (CASE WHEN U.CREATION_DATE = '0000-00-00 00:00:00' THEN '2010-01-01 01:00:00' ELSE U.CREATION_DATE END ) AS CREATION_DATE ,U.USER_NO,(CASE WHEN U.USER_LEVEL IN(1) THEN FU.EMAIL_ID ELSE CASE WHEN U.USER_LEVEL IN(6) THEN FRANU.EMAIL_ID ELSE F.EMAIL_ID END END) AS EMAIL_ID, U.USER_FIRST_NAME,U.USER_LAST_NAME,U.STATUS,U.IS_DELETED,U.USER_LEVEL FROM FRANCHISEE F LEFT JOIN AREAS A ON A.AREA_ID=F.AREA_ID,USERS U  LEFT JOIN FRANCHISEE_USERS FU ON FU.FRANCHISEE_USER_NO=U.USER_IDENTITY_NO  LEFT JOIN FRANCHISOR_USERS FRANU ON FRANU.USER_NO=U.USER_NO ");
					} else if ("/ufgCorp".equals(contextPath)) {
						uniqueActiveUserQuery = new StringBuilder(
								"SELECT COUNT(*) AS COUNT FROM (SELECT F.FRANCHISEE_NAME,F.AREA_ID,(SELECT GROUP_CONCAT(AREA_NAME) FROM AREAS WHERE AREA_ID IN (SELECT AREA_ID FROM AREA_USERS WHERE USER_NO=U.USER_NO)) AS AREA_NAME,U.USER_ID,U.TIMEZONE,FU.USER_TYPE,(SELECT GROUP_CONCAT(NAME) FROM ROLE WHERE ROLE_ID IN (SELECT ROLE_ID FROM USER_ROLES WHERE USER_NO=U.USER_NO)) AS ROLE,(CASE WHEN U.CREATION_DATE = '0000-00-00 00:00:00' THEN '2010-01-01 01:00:00' ELSE U.CREATION_DATE END ) AS CREATION_DATE ,U.USER_NO,(CASE WHEN U.USER_LEVEL IN(1) THEN FU.EMAIL_ID ELSE F.EMAIL_ID END) AS EMAIL_ID, U.USER_FIRST_NAME,U.USER_LAST_NAME,U.STATUS,U.IS_DELETED,U.USER_LEVEL FROM FRANCHISEE F LEFT JOIN AREAS A ON A.AREA_ID=F.AREA_ID,USERS U  LEFT JOIN FRANCHISEE_USERS FU ON FU.FRANCHISEE_USER_NO=U.USER_IDENTITY_NO  ");
					} // Changes in query For UFG For Multiple Regions
					else {
						uniqueActiveUserQuery = new StringBuilder(
								"SELECT COUNT(*) AS COUNT FROM (SELECT F.FRANCHISEE_NAME,F.AREA_ID,A.AREA_NAME,U.USER_ID,U.TIMEZONE,FU.USER_TYPE,(SELECT GROUP_CONCAT(NAME) FROM ROLE WHERE ROLE_ID IN (SELECT ROLE_ID FROM USER_ROLES WHERE USER_NO=U.USER_NO)) AS ROLE,(CASE WHEN U.CREATION_DATE = '0000-00-00 00:00:00' THEN '2010-01-01 01:00:00' ELSE U.CREATION_DATE END ) AS CREATION_DATE ,U.USER_NO,(CASE WHEN U.USER_LEVEL IN(1) THEN FU.EMAIL_ID ELSE F.EMAIL_ID END) AS EMAIL_ID, U.USER_FIRST_NAME,U.USER_LAST_NAME,U.STATUS,");
						if (isDeletedAvailable) {
							uniqueActiveUserQuery.append("U.IS_DELETED,");
						}
						uniqueActiveUserQuery.append(
								"U.USER_LEVEL FROM FRANCHISEE F LEFT JOIN AREAS A ON A.AREA_ID=F.AREA_ID,USERS U  LEFT JOIN FRANCHISEE_USERS FU ON FU.FRANCHISEE_USER_NO=U.USER_IDENTITY_NO");
					}

					/*
					 * uniqueActiveUserQuery.append(
					 * " LEFT JOIN ( SELECT MAX(CHANGE_DATE) AS CHANGE_DATE,USER_NO,REMARK FROM USER_STATUS_CHANGED WHERE (REMARK='Deactivated' OR REMARK='Deleted' OR REMARK='Deactivated From Franchisor' OR REMARK='deactivateed By WS' OR REMARK='deleteed By WS' OR REMARK='Franchise User has logged out forcefully and deactivated' ) "
					 * ); if (deactiveDeleted) { uniqueActiveUserQuery.
					 * append(" AND DEACTIVATED_DELETED IS NULL "); }
					 * 
					 * uniqueActiveUserQuery
					 * .append("  GROUP BY USER_NO) AS USC ON U.USER_NO=USC.USER_NO LEFT JOIN  USER_STATUS_CHANGED USC1 ON U.USER_NO=USC1.USER_NO AND (USC1.REMARK='Activated' OR USC1.REMARK='Activated From Franchisor' OR USC1.REMARK='activateed By WS') WHERE U.USER_ID!='adm' AND U.CREATION_DATE<='"
					 * + dateTo +
					 * " 23:59:59' AND ( (USC.CHANGE_DATE IS NULL OR USC.CHANGE_DATE>='"
					 * + dateFrom +
					 * " 00:00:00') OR ( USC1.CHANGE_DATE IS NOT NULL AND  USC1.CHANGE_DATE<='"
					 * + dateTo +
					 * " 23:59:59' AND USC.CHANGE_DATE<=USC1.CHANGE_DATE)  OR (USC.CHANGE_DATE IS NULL OR USC.CHANGE_DATE > '"
					 * + dateTo + "')) "); // Marcos_ENH_LOGIN_ANALYTICS//
					 * User_Account_Summary_Report_Issue
					 */
					uniqueActiveUserQuery
							.append(" WHERE U.USER_ID!='adm' AND U.CREATION_DATE<='" + dateTo + " 23:59:59' ");
					// if(!userNo.equals("1")){

					if (isBliable) {
						uniqueActiveUserQuery.append(
								" AND U.USER_NO NOT IN(1, 111111111,7777777,1000,2) AND U.IS_BILLABLE='Y' AND ((U.USER_LEVEL=1 ");
					} else {
						uniqueActiveUserQuery
								.append(" AND U.USER_NO NOT IN(1, 111111111,7777777,1000,2)  AND ((U.USER_LEVEL=1 ");
					}

					// userQuery.append(" AND (F.IS_FRANCHISEE='Y' OR
					// F.IS_STORE='Y') ");//Marcos_ENH_LOGIN_ANALYTICS
					// //BB-20141006-154
					// }
					// Marcos_ENH_LOGIN_ANALYTICS starts
					if (franchiseeNo != null && franchiseeNo.trim().length() != 0
							&& !franchiseeNo.trim().equalsIgnoreCase("null") && !"-1".equals(franchiseeNo)) {
						uniqueActiveUserQuery.append(" AND F.FRANCHISEE_NO IN (" + franchiseeNo + ") ");
					}

					uniqueActiveUserQuery.append(" ) OR (U.USER_LEVEL!=1 )) ");
					// ends
					uniqueActiveUserQuery.append(" AND F.FRANCHISEE_NO = U.FRANCHISEE_NO ");

					if ("/frannet".equals(contextPath)) {
						uniqueActiveUserQuery.append(
								" AND CASE WHEN U.USER_LEVEL IN(1) THEN FU.EMAIL_ID ELSE CASE WHEN U.USER_LEVEL IN(6) THEN FRANU.EMAIL_ID ELSE F.EMAIL_ID END END IS NOT NULL ");
						uniqueActiveUserQuery.append(
								" AND CASE WHEN U.USER_LEVEL IN(1) THEN FU.EMAIL_ID ELSE CASE WHEN U.USER_LEVEL IN(6) THEN FRANU.EMAIL_ID ELSE F.EMAIL_ID END END!=''  ");

						uniqueActiveUserQuery.append(
								" AND IF((CASE WHEN U.USER_LEVEL IN(1) THEN CONCAT(FU.FIRST_NAME,' ',FU.LAST_NAME) ELSE CASE WHEN U.USER_LEVEL IN(6) THEN CONCAT(FRANU.FIRST_NAME,' ',FRANU.LAST_NAME) ELSE CONCAT(U.USER_FIRST_NAME,' ',U.USER_LAST_NAME) END END LIKE '%test%'  OR CASE WHEN U.USER_LEVEL IN(1) THEN CONCAT(FU.FIRST_NAME,' ',FU.LAST_NAME) ELSE CASE WHEN U.USER_LEVEL IN(6) THEN CONCAT(FRANU.FIRST_NAME,' ',FRANU.LAST_NAME) ELSE CONCAT(U.USER_FIRST_NAME,' ',U.USER_LAST_NAME) END END LIKE '%user%' OR CASE WHEN U.USER_LEVEL IN(1) THEN CONCAT(FU.FIRST_NAME,' ',FU.LAST_NAME) ELSE CASE WHEN U.USER_LEVEL IN(6) THEN CONCAT(FRANU.FIRST_NAME,' ',FRANU.LAST_NAME) ELSE CONCAT(U.USER_FIRST_NAME,' ',U.USER_LAST_NAME) END END LIKE '%fran%') ");
						uniqueActiveUserQuery.append(",");
						uniqueActiveUserQuery.append(
								" ( CASE WHEN U.USER_LEVEL IN(1) THEN substring_index(IFNULL(FU.EMAIL_ID,'') ,'@',-1) ELSE CASE WHEN U.USER_LEVEL IN(6) THEN substring_index(IFNULL(FRANU.EMAIL_ID,'') ,'@',-1) ELSE substring_index(IFNULL(F.EMAIL_ID,'') ,'@',-1) END END  NOT LIKE '%franconnect%' ");
						uniqueActiveUserQuery.append(
								" AND CASE WHEN U.USER_LEVEL IN(1) THEN substring_index(IFNULL(FU.EMAIL_ID,'') ,'@',-1) ELSE CASE WHEN U.USER_LEVEL IN(6) THEN substring_index(IFNULL(FRANU.EMAIL_ID,'') ,'@',-1) ELSE substring_index(IFNULL(F.EMAIL_ID,'') ,'@',-1) END END NOT LIKE'%test%'");
						uniqueActiveUserQuery.append(
								" AND CASE WHEN U.USER_LEVEL IN(1) THEN substring_index(IFNULL(FU.EMAIL_ID,'') ,'@',-1) ELSE CASE WHEN U.USER_LEVEL IN(6) THEN substring_index(IFNULL(FRANU.EMAIL_ID,'') ,'@',-1) ELSE substring_index(IFNULL(F.EMAIL_ID,'') ,'@',-1) END END NOT LIKE '%franqa%' )");
						uniqueActiveUserQuery.append(", 1=1)");

						uniqueActiveUserQuery.append(
								" AND IF((CASE WHEN U.USER_LEVEL IN(1) THEN CONCAT(FU.FIRST_NAME,' ',FU.LAST_NAME) ELSE CASE WHEN U.USER_LEVEL IN(6) THEN CONCAT(FRANU.FIRST_NAME,' ',FRANU.LAST_NAME) ELSE CONCAT(U.USER_FIRST_NAME,' ',U.USER_LAST_NAME) END END LIKE '%test%'  OR CASE WHEN U.USER_LEVEL IN(1) THEN CONCAT(FU.FIRST_NAME,' ',FU.LAST_NAME) ELSE CASE WHEN U.USER_LEVEL IN(6) THEN CONCAT(FRANU.FIRST_NAME,' ',FRANU.LAST_NAME) ELSE CONCAT(U.USER_FIRST_NAME,' ',U.USER_LAST_NAME) END END LIKE '%user%' OR CASE WHEN U.USER_LEVEL IN(1) THEN CONCAT(FU.FIRST_NAME,' ',FU.LAST_NAME) ELSE CASE WHEN U.USER_LEVEL IN(6) THEN CONCAT(FRANU.FIRST_NAME,' ',FRANU.LAST_NAME) ELSE CONCAT(U.USER_FIRST_NAME,' ',U.USER_LAST_NAME) END END LIKE '%fran%') ");
						uniqueActiveUserQuery.append(",");
						uniqueActiveUserQuery.append(
								" ( CASE WHEN U.USER_LEVEL IN (1) THEN TRIM(IFNULL(FU.CITY,'')) ELSE CASE WHEN U.USER_LEVEL IN (6) THEN FRANU.CITY ELSE TRIM(IFNULL(F.CITY,'')) END END NOT LIKE 'Reston' ");
						uniqueActiveUserQuery.append(
								" AND CASE WHEN U.USER_LEVEL IN (1) THEN REPLACE(REPLACE(REPLACE(REPLACE(IFNULL(FU.PHONE1,''),'-',''),')',''),'(',''),' ','') ELSE CASE WHEN U.USER_LEVEL IN (6) THEN FRANU.PHONE ELSE REPLACE(REPLACE(REPLACE(REPLACE(IFNULL(F.PHONE1,''),'-',''),')',''),'(',''),' ','') END END NOT LIKE '7033909300'");
						uniqueActiveUserQuery.append(
								" AND CASE WHEN U.USER_LEVEL IN (1) THEN FU.REGION_NO ELSE CASE WHEN U.USER_LEVEL IN (6) THEN FRANU.STATE ELSE F.REGION_NO END END NOT LIKE '50' )");
						uniqueActiveUserQuery.append(", 2=2)");
						uniqueActiveUserQuery.append(
								" AND U.USER_ID IS NOT NULL AND 33=33 AND CASE WHEN  U.USER_LEVEL IN(1) THEN FU.FRANCHISEE_USER_NO IS NOT NULL ELSE FRANCHISEE_USER_NO IS NULL END");
					} else {
						uniqueActiveUserQuery.append(
								" AND CASE WHEN U.USER_LEVEL IN(1) THEN FU.EMAIL_ID ELSE F.EMAIL_ID END IS NOT NULL ");

						uniqueActiveUserQuery
								.append(" AND CASE WHEN U.USER_LEVEL IN(1) THEN FU.EMAIL_ID ELSE F.EMAIL_ID END!=''  ");

						uniqueActiveUserQuery.append(
								" AND IF((CASE WHEN U.USER_LEVEL IN(1) THEN CONCAT(FU.FIRST_NAME,' ',FU.LAST_NAME) ELSE CONCAT(U.USER_FIRST_NAME,' ',U.USER_LAST_NAME) END LIKE '%test%'  OR CASE WHEN U.USER_LEVEL IN(1) THEN CONCAT(FU.FIRST_NAME,' ',FU.LAST_NAME) ELSE CONCAT(U.USER_FIRST_NAME,' ',U.USER_LAST_NAME) END LIKE '%user%' OR CASE WHEN U.USER_LEVEL IN(1) THEN CONCAT(FU.FIRST_NAME,' ',FU.LAST_NAME) ELSE CONCAT(U.USER_FIRST_NAME,' ',U.USER_LAST_NAME) END LIKE '%fran%') ");
						uniqueActiveUserQuery.append(",");
						uniqueActiveUserQuery.append(
								" ( CASE WHEN U.USER_LEVEL IN(1) THEN substring_index(IFNULL(FU.EMAIL_ID,'') ,'@',-1) ELSE substring_index(IFNULL(F.EMAIL_ID,'') ,'@',-1) END  NOT LIKE '%franconnect%' ");
						uniqueActiveUserQuery.append(
								" AND CASE WHEN U.USER_LEVEL IN(1) THEN substring_index(IFNULL(FU.EMAIL_ID,'') ,'@',-1) ELSE substring_index(IFNULL(F.EMAIL_ID,'') ,'@',-1) END NOT LIKE'%test%'");
						uniqueActiveUserQuery.append(
								" AND CASE WHEN U.USER_LEVEL IN(1) THEN substring_index(IFNULL(FU.EMAIL_ID,'') ,'@',-1) ELSE substring_index(IFNULL(F.EMAIL_ID,'') ,'@',-1) END NOT LIKE '%franqa%' )");
						uniqueActiveUserQuery.append(", 1=1)");
						uniqueActiveUserQuery.append(
								" AND IF((CASE WHEN U.USER_LEVEL IN(1) THEN CONCAT(FU.FIRST_NAME,' ',FU.LAST_NAME) ELSE CONCAT(U.USER_FIRST_NAME,' ',U.USER_LAST_NAME) END LIKE '%test%'  OR CASE WHEN U.USER_LEVEL IN(1) THEN CONCAT(FU.FIRST_NAME,' ',FU.LAST_NAME) ELSE CONCAT(U.USER_FIRST_NAME,' ',U.USER_LAST_NAME) END LIKE '%user%' OR CASE WHEN U.USER_LEVEL IN(1) THEN CONCAT(FU.FIRST_NAME,' ',FU.LAST_NAME) ELSE CONCAT(U.USER_FIRST_NAME,' ',U.USER_LAST_NAME) END LIKE '%fran%') ");
						uniqueActiveUserQuery.append(",");
						uniqueActiveUserQuery.append(
								" ( CASE WHEN U.USER_LEVEL IN (1) THEN TRIM(IFNULL(FU.CITY,'')) ELSE TRIM(IFNULL(F.CITY,'')) END NOT LIKE 'Reston' ");
						uniqueActiveUserQuery.append(
								" AND CASE WHEN U.USER_LEVEL IN (1) THEN REPLACE(REPLACE(REPLACE(REPLACE(IFNULL(FU.PHONE1,''),'-',''),')',''),'(',''),' ','') ELSE REPLACE(REPLACE(REPLACE(REPLACE(IFNULL(F.PHONE1,''),'-',''),')',''),'(',''),' ','') END NOT LIKE '7033909300'");
						uniqueActiveUserQuery.append(
								" AND CASE WHEN U.USER_LEVEL IN (1) THEN FU.REGION_NO ELSE F.REGION_NO END NOT LIKE '50' )");
						uniqueActiveUserQuery.append(", 2=2)");
						uniqueActiveUserQuery.append(
								" AND U.USER_ID IS NOT NULL AND 33=33 AND CASE WHEN  U.USER_LEVEL IN(1) THEN FU.FRANCHISEE_USER_NO IS NOT NULL ELSE FRANCHISEE_USER_NO IS NULL END");
					}

					if ("/ace".equals(contextPath)) {
						uniqueActiveUserQuery.append(
								" UNION ALL SELECT FRANCHISEE_NAME,AREA_ID,AREA_NAME,USER_ID,TIMEZONE,USER_TYPE,ROLE,CREATION_DATE,USER_NO,EMAIL_ID, USER_FIRST_NAME,USER_LAST_NAME,STATUS,IS_DELETED,USER_LEVEL FROM (SELECT '' AS FRANCHISEE_NAME,'' AS AREA_ID,'' AS AREA_NAME,USER_ID,TIMEZONE,'Vender User' AS USER_TYPE,(SELECT GROUP_CONCAT(NAME) FROM ROLE WHERE ROLE_ID IN (SELECT ROLE_ID FROM USER_ROLES WHERE USER_NO=U.USER_NO)) AS ROLE,(CASE WHEN CREATION_DATE = '0000-00-00 00:00:00' THEN '2010-01-01 01:00:00' ELSE CREATION_DATE END ) AS CREATION_DATE,USER_NO,EMAIL_ID, USER_FIRST_NAME,USER_LAST_NAME,STATUS,U.IS_DELETED,'10' AS USER_LEVEL FROM VENDOR_USERS U WHERE IS_DELETED='N' AND STATUS=1");
						uniqueActiveUserQuery.append(" AND EMAIL_ID IS NOT NULL AND EMAIL_ID!='' ");

						uniqueActiveUserQuery.append(
								" AND IF((CONCAT(USER_FIRST_NAME,' ',USER_LAST_NAME)  LIKE '%test%'  OR CONCAT(USER_FIRST_NAME,' ',USER_LAST_NAME) LIKE '%user%' OR CONCAT(USER_FIRST_NAME,' ',USER_LAST_NAME) LIKE '%fran%') ");
						uniqueActiveUserQuery.append(",");
						uniqueActiveUserQuery
								.append(" ( substring_index(IFNULL(EMAIL_ID,'') ,'@',-1) NOT LIKE '%franconnect%' ");
						uniqueActiveUserQuery
								.append(" AND substring_index(IFNULL(EMAIL_ID,'') ,'@',-1) NOT LIKE'%test%'");
						uniqueActiveUserQuery
								.append(" AND substring_index(IFNULL(EMAIL_ID,'') ,'@',-1) NOT LIKE '%franqa%' )");
						uniqueActiveUserQuery.append(", 1=1)");

						uniqueActiveUserQuery.append(
								" AND IF((CONCAT(USER_FIRST_NAME,' ',USER_LAST_NAME) LIKE '%test%'  OR CONCAT(USER_FIRST_NAME,' ',USER_LAST_NAME) LIKE '%user%' OR CONCAT(USER_FIRST_NAME,' ',USER_LAST_NAME) LIKE '%fran%') ");
						uniqueActiveUserQuery.append(",");
						uniqueActiveUserQuery.append(" ( CITY  NOT LIKE 'Reston' ");
						uniqueActiveUserQuery.append(" AND PHONE1  NOT LIKE '7033909300'");
						uniqueActiveUserQuery.append(" AND REGION_NO NOT LIKE '50' )");
						uniqueActiveUserQuery.append(", 2=2)");
						uniqueActiveUserQuery.append(" AND USER_ID IS NOT NULL) AS TEMP WHERE 33=33 ");
					}

					uniqueActiveUserQuery.append("   GROUP BY EMAIL_ID ");

					uniqueActiveUserQuery.append(" UNION ALL ");

					if ("/frannet".equals(contextPath)) {
						uniqueActiveUserQuery.append(
								"SELECT F.FRANCHISEE_NAME,F.AREA_ID,A.AREA_NAME,U.USER_ID,U.TIMEZONE,FU.USER_TYPE,(SELECT GROUP_CONCAT(NAME) FROM ROLE WHERE ROLE_ID IN (SELECT ROLE_ID FROM USER_ROLES WHERE USER_NO=U.USER_NO)) AS ROLE,(CASE WHEN U.CREATION_DATE = '0000-00-00 00:00:00' THEN '2010-01-01 01:00:00' ELSE U.CREATION_DATE END ) AS CREATION_DATE,U.USER_NO,(CASE WHEN U.USER_LEVEL IN(1) THEN FU.EMAIL_ID ELSE CASE WHEN U.USER_LEVEL IN(6) THEN FRANU.EMAIL_ID ELSE F.EMAIL_ID END END) AS EMAIL_ID, U.USER_FIRST_NAME,U.USER_LAST_NAME,U.STATUS,U.IS_DELETED,U.USER_LEVEL FROM FRANCHISEE F LEFT JOIN AREAS A ON A.AREA_ID=F.AREA_ID,USERS U  LEFT JOIN FRANCHISEE_USERS FU ON FU.FRANCHISEE_USER_NO=U.USER_IDENTITY_NO  LEFT JOIN FRANCHISOR_USERS FRANU ON FRANU.USER_NO=U.USER_NO  ");
					} else if ("/ufgCorp".equals(contextPath)) {
						uniqueActiveUserQuery.append(
								"SELECT F.FRANCHISEE_NAME,F.AREA_ID,(SELECT GROUP_CONCAT(AREA_NAME) FROM AREAS WHERE AREA_ID IN (SELECT AREA_ID FROM AREA_USERS WHERE USER_NO=U.USER_NO)) AS AREA_NAME,U.USER_ID,U.TIMEZONE,FU.USER_TYPE,(SELECT GROUP_CONCAT(NAME) FROM ROLE WHERE ROLE_ID IN (SELECT ROLE_ID FROM USER_ROLES WHERE USER_NO=U.USER_NO)) AS ROLE,(CASE WHEN U.CREATION_DATE = '0000-00-00 00:00:00' THEN '2010-01-01 01:00:00' ELSE U.CREATION_DATE END ) AS CREATION_DATE,U.USER_NO,(CASE WHEN U.USER_LEVEL IN(1) THEN FU.EMAIL_ID ELSE F.EMAIL_ID END) AS EMAIL_ID, U.USER_FIRST_NAME,U.USER_LAST_NAME,U.STATUS,U.IS_DELETED,U.USER_LEVEL FROM FRANCHISEE F LEFT JOIN AREAS A ON A.AREA_ID=F.AREA_ID,USERS U  LEFT JOIN FRANCHISEE_USERS FU ON FU.FRANCHISEE_USER_NO=U.USER_IDENTITY_NO  ");
					} // Changes in query For UFG For Multiple Regions
					else {
						uniqueActiveUserQuery.append(
								"SELECT F.FRANCHISEE_NAME,F.AREA_ID,A.AREA_NAME,U.USER_ID,U.TIMEZONE,FU.USER_TYPE,(SELECT GROUP_CONCAT(NAME) FROM ROLE WHERE ROLE_ID IN (SELECT ROLE_ID FROM USER_ROLES WHERE USER_NO=U.USER_NO)) AS ROLE,(CASE WHEN U.CREATION_DATE = '0000-00-00 00:00:00' THEN '2010-01-01 01:00:00' ELSE U.CREATION_DATE END ) AS CREATION_DATE,U.USER_NO,(CASE WHEN U.USER_LEVEL IN(1) THEN FU.EMAIL_ID ELSE F.EMAIL_ID END) AS EMAIL_ID, U.USER_FIRST_NAME,U.USER_LAST_NAME,U.STATUS, ");
						if (isDeletedAvailable) {
							uniqueActiveUserQuery.append("U.IS_DELETED,");
						}
						uniqueActiveUserQuery.append(
								"U.USER_LEVEL FROM FRANCHISEE F LEFT JOIN AREAS A ON A.AREA_ID=F.AREA_ID,USERS U  LEFT JOIN FRANCHISEE_USERS FU ON FU.FRANCHISEE_USER_NO=U.USER_IDENTITY_NO ");
					}

					/*
						uniqueActiveUserQuery.append(" AND DEACTIVATED_DELETED IS NULL ");
					}

					uniqueActiveUserQuery
							.append("  GROUP BY USER_NO) AS USC ON U.USER_NO=USC.USER_NO LEFT JOIN  USER_STATUS_CHANGED USC1 ON U.USER_NO=USC1.USER_NO AND (USC1.REMARK='Activated' OR USC1.REMARK='Activated From Franchisor' OR USC1.REMARK='activateed By WS') WHERE U.USER_ID!='adm' AND U.CREATION_DATE<='"
									+ dateTo + " 23:59:59' AND ( (USC.CHANGE_DATE IS NULL OR USC.CHANGE_DATE>='"
									+ dateFrom
									+ " 00:00:00') OR ( USC1.CHANGE_DATE IS NOT NULL AND  USC1.CHANGE_DATE<='" + dateTo
									+ " 23:59:59' AND USC.CHANGE_DATE<=USC1.CHANGE_DATE)  OR (USC.CHANGE_DATE IS NULL OR USC.CHANGE_DATE > '"
									+ dateTo + "')) "); // Marcos_ENH_LOGIN_ANALYTICS//User_Account_Summary_Report_Issue
					 */
					// if(!userNo.equals("1")){
					uniqueActiveUserQuery
							.append(" WHERE U.USER_ID!='adm' AND U.CREATION_DATE<='" + dateTo + " 23:59:59' ");

					if (isBliable) {
						uniqueActiveUserQuery.append(
								" AND U.USER_NO NOT IN(1, 111111111,7777777,1000,2) AND U.IS_BILLABLE='Y' AND ((U.USER_LEVEL=1 ");
					} else {
						uniqueActiveUserQuery
								.append(" AND U.USER_NO NOT IN(1, 111111111,7777777,1000,2)  AND ((U.USER_LEVEL=1 ");
					}

					// uniqueActiveUserQuery.append(" AND (F.IS_FRANCHISEE='Y'
					// OR F.IS_STORE='Y') ");//Marcos_ENH_LOGIN_ANALYTICS
					// //BB-20141006-154
					// }
					// Marcos_ENH_LOGIN_ANALYTICS starts
					if (franchiseeNo != null && franchiseeNo.trim().length() != 0
							&& !franchiseeNo.trim().equalsIgnoreCase("null") && !"-1".equals(franchiseeNo)) {
						uniqueActiveUserQuery.append(" AND F.FRANCHISEE_NO IN (" + franchiseeNo + ") ");
					}

					uniqueActiveUserQuery.append(" ) OR (U.USER_LEVEL!=1 )) ");
					// ends
					uniqueActiveUserQuery.append(" AND F.FRANCHISEE_NO = U.FRANCHISEE_NO ");

					if ("/frannet".equals(contextPath)) {

						uniqueActiveUserQuery.append(
								" AND ( CASE WHEN U.USER_LEVEL IN(1) THEN FU.EMAIL_ID ELSE CASE WHEN U.USER_LEVEL IN(6) THEN FRANU.EMAIL_ID ELSE F.EMAIL_ID END END IS NULL OR CASE WHEN U.USER_LEVEL IN(1) THEN FU.EMAIL_ID ELSE CASE WHEN U.USER_LEVEL IN(6) THEN FRANU.EMAIL_ID ELSE F.EMAIL_ID END END='' ) ");

						uniqueActiveUserQuery.append(
								" AND IF((CASE WHEN U.USER_LEVEL IN(1) THEN CONCAT(FU.FIRST_NAME,' ',FU.LAST_NAME) ELSE CASE WHEN U.USER_LEVEL IN(6) THEN CONCAT(FRANU.FIRST_NAME,' ',FRANU.LAST_NAME) ELSE CONCAT(U.USER_FIRST_NAME,' ',U.USER_LAST_NAME) END END LIKE '%test%'  OR CASE WHEN U.USER_LEVEL IN(1) THEN CONCAT(FU.FIRST_NAME,' ',FU.LAST_NAME) ELSE CASE WHEN U.USER_LEVEL IN(6) THEN CONCAT(FRANU.FIRST_NAME,' ',FRANU.LAST_NAME) ELSE CONCAT(U.USER_FIRST_NAME,' ',U.USER_LAST_NAME) END END LIKE '%user%' OR CASE WHEN U.USER_LEVEL IN(1) THEN CONCAT(FU.FIRST_NAME,' ',FU.LAST_NAME) ELSE CASE WHEN U.USER_LEVEL IN(6) THEN CONCAT(FRANU.FIRST_NAME,' ',FRANU.LAST_NAME) ELSE CONCAT(U.USER_FIRST_NAME,' ',U.USER_LAST_NAME) END END LIKE '%fran%') ");
						uniqueActiveUserQuery.append(",");
						uniqueActiveUserQuery.append(
								" ( CASE WHEN U.USER_LEVEL IN (1) THEN TRIM(IFNULL(FU.CITY,'')) ELSE CASE WHEN U.USER_LEVEL IN (6) THEN FRANU.CITY ELSE TRIM(IFNULL(F.CITY,'')) END END NOT LIKE 'Reston' ");
						uniqueActiveUserQuery.append(
								" AND CASE WHEN U.USER_LEVEL IN (1) THEN REPLACE(REPLACE(REPLACE(REPLACE(IFNULL(FU.PHONE1,''),'-',''),')',''),'(',''),' ','') ELSE CASE WHEN U.USER_LEVEL IN (6) THEN FRANU.PHONE ELSE REPLACE(REPLACE(REPLACE(REPLACE(IFNULL(F.PHONE1,''),'-',''),')',''),'(',''),' ','') END END NOT LIKE '7033909300'");
						uniqueActiveUserQuery.append(
								" AND CASE WHEN U.USER_LEVEL IN (1) THEN FU.REGION_NO ELSE CASE WHEN U.USER_LEVEL IN (6) THEN FRANU.STATE ELSE F.REGION_NO END END NOT LIKE '50' )");
						uniqueActiveUserQuery.append(", 2=2)");
						uniqueActiveUserQuery.append(
								" AND U.USER_ID IS NOT NULL  AND 33=33 AND CASE WHEN  U.USER_LEVEL IN(1) THEN FU.FRANCHISEE_USER_NO IS NOT NULL ELSE FRANCHISEE_USER_NO IS NULL END ");
					} else {
						uniqueActiveUserQuery.append(
								" AND ( CASE WHEN U.USER_LEVEL IN(1) THEN FU.EMAIL_ID ELSE F.EMAIL_ID END IS NULL OR CASE WHEN U.USER_LEVEL IN(1) THEN FU.EMAIL_ID ELSE F.EMAIL_ID END='' ) ");

						uniqueActiveUserQuery.append(
								" AND IF((CASE WHEN U.USER_LEVEL IN(1) THEN CONCAT(FU.FIRST_NAME,' ',FU.LAST_NAME) ELSE CONCAT(U.USER_FIRST_NAME,' ',U.USER_LAST_NAME) END LIKE '%test%'  OR CASE WHEN U.USER_LEVEL IN(1) THEN CONCAT(FU.FIRST_NAME,' ',FU.LAST_NAME) ELSE CONCAT(U.USER_FIRST_NAME,' ',U.USER_LAST_NAME) END LIKE '%user%' OR CASE WHEN U.USER_LEVEL IN(1) THEN CONCAT(FU.FIRST_NAME,' ',FU.LAST_NAME) ELSE CONCAT(U.USER_FIRST_NAME,' ',U.USER_LAST_NAME) END LIKE '%fran%') ");
						uniqueActiveUserQuery.append(",");
						uniqueActiveUserQuery.append(
								" ( CASE WHEN U.USER_LEVEL IN (1) THEN TRIM(IFNULL(FU.CITY,'')) ELSE TRIM(IFNULL(F.CITY,'')) END NOT LIKE 'Reston' ");
						uniqueActiveUserQuery.append(
								" AND CASE WHEN U.USER_LEVEL IN (1) THEN REPLACE(REPLACE(REPLACE(REPLACE(IFNULL(FU.PHONE1,''),'-',''),')',''),'(',''),' ','') ELSE REPLACE(REPLACE(REPLACE(REPLACE(IFNULL(F.PHONE1,''),'-',''),')',''),'(',''),' ','') END NOT LIKE '7033909300'");
						uniqueActiveUserQuery.append(
								" AND CASE WHEN U.USER_LEVEL IN (1) THEN FU.REGION_NO ELSE F.REGION_NO END NOT LIKE '50' )");
						uniqueActiveUserQuery.append(", 2=2)");
						uniqueActiveUserQuery.append(
								" AND U.USER_ID IS NOT NULL  AND 33=33 AND CASE WHEN  U.USER_LEVEL IN(1) THEN FU.FRANCHISEE_USER_NO IS NOT NULL ELSE FRANCHISEE_USER_NO IS NULL END");
					}

					if ("/ace".equals(contextPath)) {
						uniqueActiveUserQuery.append(
								" UNION ALL SELECT FRANCHISEE_NAME,AREA_ID,AREA_NAME,USER_ID,TIMEZONE,USER_TYPE,ROLE,CREATION_DATE,USER_NO,EMAIL_ID, USER_FIRST_NAME,USER_LAST_NAME,STATUS,IS_DELETED,USER_LEVEL FROM (SELECT '' AS FRANCHISEE_NAME,'' AS AREA_ID,'' AS AREA_NAME,USER_ID,TIMEZONE,'Vender User' AS USER_TYPE,(SELECT GROUP_CONCAT(NAME) FROM ROLE WHERE ROLE_ID IN (SELECT ROLE_ID FROM USER_ROLES WHERE USER_NO=U.USER_NO)) AS ROLE,CREATION_DATE,USER_NO,EMAIL_ID, USER_FIRST_NAME,USER_LAST_NAME,STATUS,U.IS_DELETED,'10' AS USER_LEVEL FROM VENDOR_USERS U WHERE IS_DELETED='N' AND STATUS=1");
						uniqueActiveUserQuery.append(" AND (EMAIL_ID IS NULL OR EMAIL_ID='') ");

						uniqueActiveUserQuery.append(
								" AND IF((CONCAT(USER_FIRST_NAME,' ',USER_LAST_NAME) LIKE '%test%'  OR CONCAT(USER_FIRST_NAME,' ',USER_LAST_NAME) LIKE '%user%' OR CONCAT(USER_FIRST_NAME,' ',USER_LAST_NAME) LIKE '%fran%') ");
						uniqueActiveUserQuery.append(",");
						uniqueActiveUserQuery.append(" ( CITY  NOT LIKE 'Reston' ");
						uniqueActiveUserQuery.append(" AND PHONE1  NOT LIKE '7033909300'");
						uniqueActiveUserQuery.append(" AND REGION_NO NOT LIKE '50' )");
						uniqueActiveUserQuery.append(", 2=2)");

						uniqueActiveUserQuery.append(" AND USER_ID IS NOT NULL) AS TEMP1 WHERE 33=33 ");
					}

					uniqueActiveUserQuery.append(" ORDER BY USER_LEVEL ) AS T1");
				}

				else {

					if (isAreaIDAvailable) {
						uniqueActiveUserQuery = new StringBuilder(
								"SELECT COUNT(*) AS COUNT FROM (SELECT F.FRANCHISEE_NAME,F.AREA_ID,A.AREA_NAME,U.USER_ID,U.TIMEZONE,FU.USER_TYPE,(SELECT GROUP_CONCAT(NAME) FROM ROLE WHERE ROLE_ID IN (SELECT ROLE_ID FROM USER_ROLES WHERE USER_NO=U.USER_NO)) AS ROLE,U.CREATION_DATE,U.USER_NO,(CASE WHEN U.USER_LEVEL IN(1) THEN FU.EMAIL_ID ELSE F.EMAIL_ID END) AS EMAIL_ID, F.FIRST_NAME AS USER_FIRST_NAME,F.LAST_NAME AS USER_LAST_NAME,U.STATUS, ");
						if (isDeletedAvailable) {
							uniqueActiveUserQuery.append("U.IS_DELETED,");
						}
						uniqueActiveUserQuery.append(
								"U.USER_LEVEL FROM FRANCHISEE F LEFT JOIN AREAS A ON A.AREA_ID=F.AREA_ID,USERS U  LEFT JOIN FRANCHISEE_USERS FU ON FU.FRANCHISEE_USER_NO=U.USER_IDENTITY_NO ");
					} else {
						uniqueActiveUserQuery = new StringBuilder(
								"SELECT COUNT(*) AS COUNT FROM (SELECT F.FRANCHISEE_NAME,AFM.AREA_ID,A.AREA_NAME,U.USER_ID,U.TIMEZONE,FU.USER_TYPE,(SELECT GROUP_CONCAT(NAME) FROM ROLE WHERE ROLE_ID IN (SELECT ROLE_ID FROM USER_ROLES WHERE USER_NO=U.USER_NO)) AS ROLE,U.CREATION_DATE,U.USER_NO,(CASE WHEN U.USER_LEVEL IN(1) THEN FU.EMAIL_ID ELSE F.EMAIL_ID END) AS EMAIL_ID, F.FIRST_NAME AS USER_FIRST_NAME,F.LAST_NAME AS USER_LAST_NAME,U.STATUS,");
						if (isDeletedAvailable) {
							uniqueActiveUserQuery.append("U.IS_DELETED,");
						}
						uniqueActiveUserQuery.append(
								"U.USER_LEVEL FROM FRANCHISEE F LEFT JOIN  AREA_FRANCHISE_MAPPING AFM ON AFM.FRANCHISE_ID=F.FRANCHISEE_NO LEFT JOIN AREAS A ON A.AREA_ID=AFM.AREA_ID,USERS U  LEFT JOIN FRANCHISEE_USERS FU ON FU.FRANCHISEE_USER_NO=U.USER_IDENTITY_NO ");

					}

					/*
					 * uniqueActiveUserQuery.append(
					 * " LEFT JOIN ( SELECT MAX(CHANGE_DATE) AS CHANGE_DATE,USER_NO,REMARK FROM USER_STATUS_CHANGED WHERE (REMARK='Deactivated' OR REMARK='Deleted' OR REMARK='Deactivated From Franchisor' OR REMARK='deactivateed By WS' OR REMARK='deleteed By WS' OR REMARK='Franchise User has logged out forcefully and deactivated' ) "
					 * ); if (deactiveDeleted) { uniqueActiveUserQuery.
					 * append(" AND DEACTIVATED_DELETED IS NULL "); }

					 * uniqueActiveUserQuery
					 * .append("  GROUP BY USER_NO) AS USC ON U.USER_NO=USC.USER_NO LEFT JOIN  USER_STATUS_CHANGED USC1 ON U.USER_NO=USC1.USER_NO AND (USC1.REMARK='Activated' OR USC1.REMARK='Activated From Franchisor' OR USC1.REMARK='activateed By WS') WHERE U.USER_ID!='adm' AND U.CREATION_DATE<='"
									+ dateTo + " 23:59:59' AND ( (USC.CHANGE_DATE IS NULL OR USC.CHANGE_DATE>='"
									+ dateFrom
									+ " 00:00:00') OR ( USC1.CHANGE_DATE IS NOT NULL AND  USC1.CHANGE_DATE<='" + dateTo
									+ " 23:59:59' AND USC.CHANGE_DATE<=USC1.CHANGE_DATE)  OR (USC.CHANGE_DATE IS NULL OR USC.CHANGE_DATE > '"
									+ dateTo + "')) "); // Marcos_ENH_LOGIN_ANALYTICS//User_Account_Summary_Report_Issue
					 */
					// if(!userNo.equals("1")){
					uniqueActiveUserQuery
							.append(" WHERE U.USER_ID!='adm' AND U.CREATION_DATE<='" + dateTo + " 23:59:59' ");

					if (isBliable) {
						uniqueActiveUserQuery.append(
								" AND U.USER_NO NOT IN(1, 111111111,7777777,1000,2) AND U.IS_BILLABLE='Y' AND ((U.USER_LEVEL=1 ");
					} else {
						uniqueActiveUserQuery
								.append(" AND U.USER_NO NOT IN(1, 111111111,7777777,1000,2)  AND ((U.USER_LEVEL=1 ");
					}

					// userQuery.append(" AND (F.IS_FRANCHISEE='Y' OR
					// F.IS_STORE='Y') ");//Marcos_ENH_LOGIN_ANALYTICS
					// //BB-20141006-154
					// }
					// Marcos_ENH_LOGIN_ANALYTICS starts
					if (franchiseeNo != null && franchiseeNo.trim().length() != 0
							&& !franchiseeNo.trim().equalsIgnoreCase("null") && !"-1".equals(franchiseeNo)) {
						uniqueActiveUserQuery.append(" AND F.FRANCHISEE_NO IN (" + franchiseeNo + ") ");
					}

					uniqueActiveUserQuery.append(" ) OR (U.USER_LEVEL!=1 )) ");
					// ends
					uniqueActiveUserQuery.append(" AND F.FRANCHISEE_NO = U.FRANCHISEE_NO ");

					uniqueActiveUserQuery.append(
							" AND CASE WHEN U.USER_LEVEL IN(1) THEN FU.EMAIL_ID ELSE F.EMAIL_ID END IS NOT NULL ");

					uniqueActiveUserQuery
							.append(" AND CASE WHEN U.USER_LEVEL IN(1) THEN FU.EMAIL_ID ELSE F.EMAIL_ID END!=''  ");

					uniqueActiveUserQuery.append(
							" AND IF((CASE WHEN U.USER_LEVEL IN(1) THEN CONCAT(FU.FIRST_NAME,' ',FU.LAST_NAME) ELSE CONCAT(F.FIRST_NAME,' ',F.LAST_NAME) END LIKE '%test%'  OR CASE WHEN U.USER_LEVEL IN(1) THEN CONCAT(FU.FIRST_NAME,' ',FU.LAST_NAME) ELSE CONCAT(F.FIRST_NAME,' ',F.LAST_NAME) END LIKE '%user%' OR CASE WHEN U.USER_LEVEL IN(1) THEN CONCAT(FU.FIRST_NAME,' ',FU.LAST_NAME) ELSE CONCAT(F.FIRST_NAME,' ',F.LAST_NAME) END LIKE '%fran%') ");
					uniqueActiveUserQuery.append(",");
					uniqueActiveUserQuery.append(
							" ( CASE WHEN U.USER_LEVEL IN(1) THEN substring_index(IFNULL(FU.EMAIL_ID,'') ,'@',-1) ELSE substring_index(IFNULL(F.EMAIL_ID,'') ,'@',-1) END  NOT LIKE '%franconnect%' ");
					uniqueActiveUserQuery.append(
							" AND CASE WHEN U.USER_LEVEL IN(1) THEN substring_index(IFNULL(FU.EMAIL_ID,'') ,'@',-1) ELSE substring_index(IFNULL(F.EMAIL_ID,'') ,'@',-1) END NOT LIKE'%test%'");
					uniqueActiveUserQuery.append(
							" AND CASE WHEN U.USER_LEVEL IN(1) THEN substring_index(IFNULL(FU.EMAIL_ID,'') ,'@',-1) ELSE substring_index(IFNULL(F.EMAIL_ID,'') ,'@',-1) END NOT LIKE '%franqa%' )");
					uniqueActiveUserQuery.append(", 1=1)");

					uniqueActiveUserQuery.append(
							" AND IF((CASE WHEN U.USER_LEVEL IN(1) THEN CONCAT(FU.FIRST_NAME,' ',FU.LAST_NAME) ELSE CONCAT(F.FIRST_NAME,' ',F.LAST_NAME) END LIKE '%test%'  OR CASE WHEN U.USER_LEVEL IN(1) THEN CONCAT(FU.FIRST_NAME,' ',FU.LAST_NAME) ELSE CONCAT(F.FIRST_NAME,' ',F.LAST_NAME) END LIKE '%user%' OR CASE WHEN U.USER_LEVEL IN(1) THEN CONCAT(FU.FIRST_NAME,' ',FU.LAST_NAME) ELSE CONCAT(F.FIRST_NAME,' ',F.LAST_NAME) END LIKE '%fran%') ");
					uniqueActiveUserQuery.append(",");
					uniqueActiveUserQuery.append(
							" ( CASE WHEN U.USER_LEVEL IN (1) THEN TRIM(IFNULL(FU.CITY,'')) ELSE TRIM(IFNULL(F.CITY,'')) END NOT LIKE 'Reston' ");
					uniqueActiveUserQuery.append(
							" AND CASE WHEN U.USER_LEVEL IN (1) THEN REPLACE(REPLACE(REPLACE(REPLACE(IFNULL(FU.PHONE1,''),'-',''),')',''),'(',''),' ','') ELSE REPLACE(REPLACE(REPLACE(REPLACE(IFNULL(F.PHONE1,''),'-',''),')',''),'(',''),' ','') END NOT LIKE '7033909300'");
					uniqueActiveUserQuery.append(
							" AND CASE WHEN U.USER_LEVEL IN (1) THEN FU.REGION_NO ELSE F.REGION_NO END NOT LIKE '50' )");
					uniqueActiveUserQuery.append(", 2=2)");
					uniqueActiveUserQuery.append(
							" AND U.USER_ID IS NOT NULL AND 33=33 AND CASE WHEN  U.USER_LEVEL IN(1) THEN FU.FRANCHISEE_USER_NO IS NOT NULL ELSE FRANCHISEE_USER_NO IS NULL END");

					uniqueActiveUserQuery.append("   GROUP BY EMAIL_ID ");

					uniqueActiveUserQuery.append(" UNION ALL ");

					if (isAreaIDAvailable) {
						uniqueActiveUserQuery.append(
								"SELECT F.FRANCHISEE_NAME,F.AREA_ID,A.AREA_NAME,U.USER_ID,U.TIMEZONE,FU.USER_TYPE,(SELECT GROUP_CONCAT(NAME) FROM ROLE WHERE ROLE_ID IN (SELECT ROLE_ID FROM USER_ROLES WHERE USER_NO=U.USER_NO)) AS ROLE,(CASE WHEN U.CREATION_DATE = '0000-00-00 00:00:00' THEN '2010-01-01 01:00:00' ELSE U.CREATION_DATE END ) AS CREATION_DATE ,U.USER_NO,(CASE WHEN U.USER_LEVEL IN(1) THEN FU.EMAIL_ID ELSE F.EMAIL_ID END) AS EMAIL_ID, F.FIRST_NAME AS USER_FIRST_NAME,F.LAST_NAME AS USER_LAST_NAME,U.STATUS,");
						if (isDeletedAvailable) {
							uniqueActiveUserQuery.append("U.IS_DELETED,");
						}
						uniqueActiveUserQuery.append(
								"U.USER_LEVEL FROM FRANCHISEE F LEFT JOIN AREAS A ON A.AREA_ID=F.AREA_ID,USERS U  LEFT JOIN FRANCHISEE_USERS FU ON FU.FRANCHISEE_USER_NO=U.USER_IDENTITY_NO  ");
					} else {
						uniqueActiveUserQuery.append(
								"SELECT F.FRANCHISEE_NAME,AFM.AREA_ID,A.AREA_NAME,U.USER_ID,U.TIMEZONE,FU.USER_TYPE,(SELECT GROUP_CONCAT(NAME) FROM ROLE WHERE ROLE_ID IN (SELECT ROLE_ID FROM USER_ROLES WHERE USER_NO=U.USER_NO)) AS ROLE,(CASE WHEN U.CREATION_DATE = '0000-00-00 00:00:00' THEN '2010-01-01 01:00:00' ELSE U.CREATION_DATE END ) AS CREATION_DATE ,U.USER_NO,(CASE WHEN U.USER_LEVEL IN(1) THEN FU.EMAIL_ID ELSE F.EMAIL_ID END) AS EMAIL_ID, F.FIRST_NAME AS USER_FIRST_NAME,F.LAST_NAME AS USER_LAST_NAME,U.STATUS, ");
						if (isDeletedAvailable) {
							uniqueActiveUserQuery.append("U.IS_DELETED,");
						}
						uniqueActiveUserQuery.append(
								"U.USER_LEVEL FROM FRANCHISEE F LEFT JOIN AREA_FRANCHISE_MAPPING AFM ON AFM.FRANCHISE_ID=F.FRANCHISEE_NO LEFT JOIN AREAS A ON A.AREA_ID=AFM.AREA_ID,USERS U  LEFT JOIN FRANCHISEE_USERS FU ON FU.FRANCHISEE_USER_NO=U.USER_IDENTITY_NO ");
					}

					/*
						uniqueActiveUserQuery.append(" AND DEACTIVATED_DELETED IS NULL ");
					}
					 */
					uniqueActiveUserQuery
							.append(" WHERE U.USER_ID!='adm' AND U.CREATION_DATE<='" + dateTo + " 23:59:59' ");

					// if(!userNo.equals("1")){

					if (isBliable) {
						uniqueActiveUserQuery.append(
								" AND U.USER_NO NOT IN(1, 111111111,7777777,1000,2) AND U.IS_BILLABLE='Y' AND ((U.USER_LEVEL=1 ");
					} else {
						uniqueActiveUserQuery
								.append(" AND U.USER_NO NOT IN(1, 111111111,7777777,1000,2)  AND ((U.USER_LEVEL=1 ");
					}

					// uniqueActiveUserQuery.append(" AND (F.IS_FRANCHISEE='Y'
					// OR F.IS_STORE='Y') ");//Marcos_ENH_LOGIN_ANALYTICS
					// //BB-20141006-154
					// }
					// Marcos_ENH_LOGIN_ANALYTICS starts
					if (franchiseeNo != null && franchiseeNo.trim().length() != 0
							&& !franchiseeNo.trim().equalsIgnoreCase("null") && !"-1".equals(franchiseeNo)) {
						uniqueActiveUserQuery.append(" AND F.FRANCHISEE_NO IN (" + franchiseeNo + ") ");
					}

					uniqueActiveUserQuery.append(" ) OR (U.USER_LEVEL!=1 )) ");
					// ends
					uniqueActiveUserQuery.append(" AND F.FRANCHISEE_NO = U.FRANCHISEE_NO ");

					uniqueActiveUserQuery.append(
							" AND ( CASE WHEN U.USER_LEVEL IN(1) THEN FU.EMAIL_ID ELSE F.EMAIL_ID END IS NULL OR CASE WHEN U.USER_LEVEL IN(1) THEN FU.EMAIL_ID ELSE F.EMAIL_ID END='' ) ");

					uniqueActiveUserQuery.append(
							" AND IF((CASE WHEN U.USER_LEVEL IN(1) THEN CONCAT(FU.FIRST_NAME,' ',FU.LAST_NAME) ELSE CONCAT(F.FIRST_NAME,' ',F.LAST_NAME) END LIKE '%test%'  OR CASE WHEN U.USER_LEVEL IN(1) THEN CONCAT(FU.FIRST_NAME,' ',FU.LAST_NAME) ELSE CONCAT(F.FIRST_NAME,' ',F.LAST_NAME) END LIKE '%user%' OR CASE WHEN U.USER_LEVEL IN(1) THEN CONCAT(FU.FIRST_NAME,' ',FU.LAST_NAME) ELSE CONCAT(F.FIRST_NAME,' ',F.LAST_NAME) END LIKE '%fran%') ");
					uniqueActiveUserQuery.append(",");
					uniqueActiveUserQuery.append(
							" ( CASE WHEN U.USER_LEVEL IN (1) THEN TRIM(IFNULL(FU.CITY,'')) ELSE TRIM(IFNULL(F.CITY,'')) END NOT LIKE 'Reston' ");
					uniqueActiveUserQuery.append(
							" AND CASE WHEN U.USER_LEVEL IN (1) THEN REPLACE(REPLACE(REPLACE(REPLACE(IFNULL(FU.PHONE1,''),'-',''),')',''),'(',''),' ','') ELSE REPLACE(REPLACE(REPLACE(REPLACE(IFNULL(F.PHONE1,''),'-',''),')',''),'(',''),' ','') END NOT LIKE '7033909300'");
					uniqueActiveUserQuery.append(
							" AND CASE WHEN U.USER_LEVEL IN (1) THEN FU.REGION_NO ELSE F.REGION_NO END NOT LIKE '50' )");
					uniqueActiveUserQuery.append(", 2=2)");
					uniqueActiveUserQuery.append(
							" AND U.USER_ID IS NOT NULL  AND 33=33 AND CASE WHEN  U.USER_LEVEL IN(1) THEN FU.FRANCHISEE_USER_NO IS NOT NULL ELSE FRANCHISEE_USER_NO IS NULL END");
					uniqueActiveUserQuery.append(" ORDER BY USER_LEVEL) AS T1");
				}

			//	 System.out.println(codeValue+"=====uniqueActiveUserQuery======>"+uniqueActiveUserQuery.toString());

				String UtempQ = uniqueActiveUserQuery.toString();
				System.out.println("Query:::::::::::::unique::::::"+UtempQ);
				UtempQ = UtempQ.replace("GROUP BY EMAIL_ID", "");
				UtempQ = UtempQ.replace("SELECT COUNT(*) AS COUNT FROM (", "");
				UtempQ = UtempQ.replace(") AS T1", "");
				// System.out.println(clientName + "=====UtempQ=usersMap=====>"
				// + UtempQ.toString());
				String q = "";
				String areaName = "";
				exchange = endpoint.createExchange();
				exchange.getIn().setBody(UtempQ.toString());
				Exchange out = template.send(endpoint, exchange);
				List<Map<String, Object>> result = out.getOut().getBody(List.class);
				// System.out.println(codeValue+"\n======UsersQuery=========>"+UtempQ.toString());
				// Exchange noUserlocationQueryout = template.send(endpoint,
				// exchange);
				// List<Map<String, Object>> noUserlocationQueryresult =
				// noUserlocationQueryout.getOut().getBody(List.class);*/
				int noUserlocationQueryi = result.size();
				HashMap<String, String> usersMap = null;
				HashMap<String, HashMap<String, String>> usersMapdata = new HashMap<String, HashMap<String, String>>();
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String usertype = "";
				String status = "";
				String isDeleted = "";
				String datefrom = dateFrom + " 00:00:00";
				String dateto = dateTo + " 23:59:59";
				String userNo="";
				String deactivateDate = "";
				String activateDate = "";
				List deactivateusc = new ArrayList();
				int count11 = 0;
				List activateusc = new ArrayList();
				for (int j = 0; j < noUserlocationQueryi; j++) {
					usertype = "";
					deactivateDate="";
					activateDate="";
					Map<String, Object> row = result.get(j);
					usersMap = new HashMap<String, String>();
					deactivateusc = uscdeactiveMap.get(row.get("USER_NO") + "");
					activateusc = uscactiveMap.get(row.get("USER_NO") + "");
					if (deactivateusc != null) {
						deactivateDate = (String) deactivateusc.get(0);
					}
					if (activateusc != null) {
						activateDate = (String) activateusc.get(0);
					} 
					if ((!uscdeactiveMap.containsKey(row.get("USER_NO") + "")
							|| sdf.parse(deactivateDate).after(sdf.parse(datefrom)))
							|| (uscactiveMap.containsKey(row.get("USER_NO") + "")
									&& sdf.parse(activateDate).before(sdf.parse(dateto))
									&& sdf.parse(deactivateDate).before(sdf.parse(activateDate)))) {
					if (row.get("CREATION_DATE") != null) {
						usersMap.put("CREATION_DATE", row.get("CREATION_DATE") + "");
					} else {
						usersMap.put("CREATION_DATE", "");
					}
					if (row.get("TIMEZONE") != null) {
						String timeZone = "" + row.get("TIMEZONE");
						timeZone = searchDisplayGMTString(timeZone);
						usersMap.put("TIMEZONE", timeZone);
					} else {
						usersMap.put("TIMEZONE", "");
					}
					if (row.get("ROLE") != null) {
						usersMap.put("ROLE", row.get("ROLE") + "");
					} else {
						usersMap.put("ROLE", "");
					}
					if (row.get("IS_DELETED") != null) {
							isDeleted = row.get("IS_DELETED") + "";
						usersMap.put("IS_DELETED", row.get("IS_DELETED") + "");
					} else {
						usersMap.put("IS_DELETED", "");
					}
					if (row.get("USER_ID") != null) {
						usersMap.put("USER_ID", row.get("USER_ID") + "");
					} else {
						usersMap.put("USER_ID", "");
					}

					if (row.get("FRANCHISEE_NAME") != null) {
						usersMap.put("FRANCHISEE_NAME", row.get("FRANCHISEE_NAME") + "");
					} else {
						usersMap.put("FRANCHISEE_NAME", "");
					}

						if (row.get("AREA_ID") != null && !"".equals(row.get("AREA_ID"))
								&& !"-1".equals(row.get("AREA_ID")) && !" ".equals(row.get("AREA_ID"))) {

						usersMap.put("AREA_ID", row.get("AREA_ID") + "");

					} else {
						usersMap.put("AREA_ID", "-1");

					}

					if (row.get("AREA_NAME") != null) {

						usersMap.put("AREA_NAME", row.get("AREA_NAME") + "");

					} else {

						usersMap.put("AREA_NAME", "");
					}

					if (row.get("USER_NO") != null) {

						usersMap.put("USER_NO", row.get("USER_NO") + "");
					} else {
						usersMap.put("USER_NO", "-1");
					}

					if (row.get("USER_FIRST_NAME") != null) {

						usersMap.put("USER_FIRST_NAME", filterValue(row.get("USER_FIRST_NAME") + ""));
					} else {
						usersMap.put("USER_FIRST_NAME", "");
					}

					if (row.get("USER_LAST_NAME") != null) {

						usersMap.put("USER_LAST_NAME", filterValue(row.get("USER_LAST_NAME") + ""));
					} else {
						usersMap.put("USER_LAST_NAME", "");
					}

					if (row.get("EMAIL_ID") != null) {

						usersMap.put("EMAIL_ID", row.get("EMAIL_ID") + "");
					} else {
						usersMap.put("EMAIL_ID", "");
					}

					if (row.get("STATUS") != null) {

						usersMap.put("STATUS", row.get("STATUS") + "");
					} else {
						usersMap.put("STATUS", "");
					}

					if (row.get("USER_LEVEL") != null) {

						usersMap.put("USER_LEVEL", row.get("USER_LEVEL") + "");

						if ("0".equals(row.get("USER_LEVEL") + "")) {
							usertype = "Corporate User";
						} else if ("1".equals(row.get("USER_LEVEL") + "")) {
							usertype = "Franchise User";
							if (row.get("USER_TYPE") != null) {
									userNo=row.get("USER_NO")+"";
									if(muidUser.contains(userNo)){
										usersMap.put("SUB_USER_TYPE", ("Multi-Unit "+row.get("USER_TYPE") + ""));
									}else{
								usersMap.put("SUB_USER_TYPE", row.get("USER_TYPE") + "");
							}
								}
						} else if ("2".equals(row.get("USER_LEVEL") + "")) {
							usertype = "Regional User";

							if ("/ups".equals(contextPath)) {
								usertype = "Sales Area Users";
							}
						} else if ("6".equals(row.get("USER_LEVEL") + "")) {
							usertype = "DU";

							if (isNewUserLevel) {
								usertype = divUSerName;
							}
							if ("/frannet".equals(contextPath)) {
								usertype = "Franchisor Users";
							}

						}

						else if ("4".equals(row.get("USER_LEVEL") + "")) {
							usertype = "Supplier User";
						} else if ("9".equals(row.get("USER_LEVEL") + "")) {
							usertype = "Vendor User";
						} else if ("10".equals(row.get("USER_LEVEL") + "")) {
							usertype = "Vendor User";
						}
						usersMap.put("USER_TYPE", usertype);
					} else {
						usersMap.put("USER_LEVEL", "");
						usersMap.put("USER_TYPE", "");
					}

						status = row.get("STATUS") + "";
						usersMap.put("LAST_MODIFIED_DATE","");
						if (status.trim().equals("0") || isDeleted.trim().equals("Y")) {
							usersMap.put("LAST_MODIFIED_DATE", deactivateDate);
						}else
						    if (status.trim().equals("1")) {
							usersMap.put("LAST_MODIFIED_DATE", activateDate);
						}
						if (usersMap.get("LAST_MODIFIED_DATE").equals("") || usersMap.get("LAST_MODIFIED_DATE") == null) {
							usersMap.put("LAST_MODIFIED_DATE", row.get("CREATION_DATE") + "");
						}
					usersMapdata.put(j + "", usersMap);
					}
				}
				finaldata.put("usersMap", usersMapdata);

			} catch (Exception e) {
				System.out.println("=Exception Found for Client======uniqueActiveUserQuery==============>" + codeValue
						+ "\n" + stackTraceToString(e));
				e.printStackTrace();
			}
			/* User data end */
			String clientSystemName=getDbName(template, endpoint, exchange);
			calslocationhmap.put("clientSystemName",clientSystemName);
			System.out.println("clientName...." + clientName + "......................." + calslocationhmap);
			calslocationhmapdata.put("0", calslocationhmap);
			finaldata.put("calslocationMap", calslocationhmapdata);
			// System.out.println(clientName+"finaldata"+finaldata);
		} catch (Exception e) {
			System.out.println(
					"=Exception Found for Client====================>" + codeValue + "\n" + stackTraceToString(e));
			e.printStackTrace();
		}

		return finaldata;
	}

	public static String filterValue(String pValue) {

		if (pValue == null || pValue.length() == 0)
			return (pValue);

		// Return passed value if starts with "&#" and has ";" characters.(UTF-8
		// chars for chinese and european and other charsets)
		if (pValue.startsWith("&#") && pValue.contains(";")) {
			return pValue;
		}
		// end Return

		char[] content = pValue.toCharArray();
		int size = content.length;
		StringBuilder result = new StringBuilder(size + 50);
		int mark = 0;
		String replacement = null;
		boolean flag = false;// caribo-20140328-030
		for (int i = 0; i < size; i++) {
			// Juice-20131211-016 : starts
			if (flag || (i + 7 < size && "&".equals(content[i] + "") && "#".equals(content[i + 1] + "")
					&& "x".equals(content[i + 2] + "") && ";".equals(content[i + 7] + ""))) // caribo-20140328-030
			{
				// caribo-20140328-030 starts
				/*
				 * if(specialCharList.contains(content[i+3]+""+content[i+4]+""+
				 * content[i+5]+""+content[i+6]+"")){ i=i+7;
				 */
				flag = flag ? false : true;
				continue;
				// }
			} // Juice-20131211-016 : ends
				// caribo-20140328-030 ends
			switch (content[i]) {
			case '<':
				replacement = "&lt;";
				break;
			case '>':
				replacement = "&gt;";
				break;
			case '&':
				replacement = "&amp;";
				break;
			case '"':
				replacement = "&quot;";
				break;
			case '\n':
				replacement = "<br />";// AUDIT_ENHANCEMENT_CHANGES
				break;
			case '\r':
				replacement = "";
				break;
			case '\f':
				replacement = "";
				break;
			case '?':
				replacement = "&#63;";
				break;
			case '\'':
				replacement = "&#39;";
				break;
			case '#':
				replacement = "&#35;";
				break;
			case '\\':
				replacement = "&#92;";
				break;
			case '/':
				replacement = "&#47;";
				break;
			case '%':
				replacement = "&#37;";
				break;
			case '+':
				replacement = "&#43;";
				break;
			case '=':
				replacement = "&#61;";
				break;
			case '[':
				replacement = "&#91;";
				break;
			case ']':
				replacement = "&#93;";
				break;
			}

			if (replacement != null) {
				if (mark < i) {
					result.append(content, mark, i - mark);
				}
				result.append(replacement);
				replacement = null;
				mark = i + 1;
			}
		}
		if (mark < size) {
			result.append(content, mark, size - mark);
		}
		return (result.toString());

	}

	public static String stackTraceToString(Throwable e) {
		StringBuilder sb = new StringBuilder();
		for (StackTraceElement element : e.getStackTrace()) {
			sb.append(element.toString());
			sb.append("\n");
		}
		return sb.toString();
	}

}
