/*
 * //Exchange for Select Query
 *  FCSKYS-18302 | Users Fetching based on their Roles 
 */
package com.app.util;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.camel.Endpoint;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.jdbc.DefaultJdbcPrepareStatementStrategy;
import org.apache.camel.component.jdbc.JdbcEndpoint;
import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.beanutils.DynaClass;

public class QueryUtil 
{
	public static List executeQuerySelect(Endpoint dbConnection,String query) //Exchange for Select Query
	{
		//System.out.println(query);
		Exchange exchange = null;
		ProducerTemplate template = null;
		List data = null;
		Map<String, Object> row=null;
		StringBuffer version=null;
		try
		{
			exchange  = dbConnection.createExchange();
			template = exchange.getContext().createProducerTemplate();

			//dbConnection.co
			//Map
			
			exchange.getIn().setBody(query);
			exchange = template.send(dbConnection, exchange);
			//System.out.println("exchange..............."+exchange);
			if(!exchange.isFailed())
			{	
				data = (List) exchange.getOut().getBody();
			}
			else{
				System.out.println("Exception in "+exchange.EXCEPTION_CAUGHT);
				System.out.println("Exception in "+exchange.getExchangeId());
				System.out.println("Exception in "+exchange.getException().getCause());
			}
			/*if(data!=null)
			{
		        int i =data.size();
		        row=  (Map<String, Object>) data.get(0);
		        version=(StringBuffer) row.get("VERSION");
			}*/
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally 
		{
			exchange = null;
			template = null;
		}
	
		return data;
	}
	public static HashMap<String,Object> executeQuery(Endpoint dbConnection,String query)
	{	
		HashMap<String,Object> results = new HashMap<String, Object>();
		ArrayList<DynaBean> data = new ArrayList<DynaBean>();
		Exchange exchange = null;
		ProducerTemplate template = null;
		try
		{
			
			exchange  = dbConnection.createExchange();

			template = exchange.getContext().createProducerTemplate();

			exchange.getIn().setBody(query);
			exchange = template.send(dbConnection, exchange);
	        DecimalFormat df = new DecimalFormat("#");
	        df.setMaximumFractionDigits(10);
			if(!exchange.isFailed())
			{	
				Set<String> colNames = exchange.getOut().getHeader("CamelJdbcColumnNames",Set.class);
				DynaClass dClass = BeanUtil.createBean(colNames);
				List<Map<String, Object>> result = exchange.getOut().getBody(List.class);
				if(result!=null)
				{
			        int i =result.size();
			        for(int j =0;j<i;j++)
			        {	
			        	Map<String, Object> row = result.get(j);
			        	DynaBean bean = dClass.newInstance();
			        	Iterator<String> colItr = colNames.iterator();
			        	while(colItr.hasNext())
						{
							String colName = colItr.next();
							Object value = row.get(colName);
							String val = "";
							if(value instanceof Integer)
			    			{
			    				val	=  new String(""+(Integer)value);
			    			}
							else if (value instanceof Long) 
			    			{
			    				val = String.valueOf((Long)value); 
							}
							else if(value instanceof byte[])
							{
								val = new String((byte[])value);
							}
							else if(value instanceof BigDecimal)
				        	{
				        		val = df.format(value)+"";
				        	}
							else if(value instanceof Timestamp)
							{
								val =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(value);
							}
							else if(value instanceof Date)
							{
								val =  new SimpleDateFormat("yyyy-MM-dd").format(value);
							}
							else if(value instanceof Time)
							{
								val =  new SimpleDateFormat("HH:mm:ss").format(value);
							}
							else if(value instanceof Double)
							{
								val = df.format(value)+"";
							}
			    			else
			    			{
			    				val = (String)value;
			    			}
							bean.set(colName, val);
						}
			        	data.add(bean);
			        }
		        }
				results.put("IS_FAILED", false);
				results.put("COLUMN_NAMES", exchange.getOut().getHeader("CamelJdbcColumnNames",Set.class));
				System.out.println("####### Query Executed successfully for client : "+dbConnection + "query  "+query);
			}
			else
			{
				Exception ex = exchange.getException();
				results.put("IS_FAILED", true);
			 	results.put("ExceptionMessage", ex.getMessage());
			 	//System.out.println(">>>>>>> Query failed for client : "+dbConnection + " query  , message :  "+query + ex.getMessage() + " ex : "+ex);
			}
			results.put("DATA", data);
	    }
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally 
		{
			exchange = null;
			template = null;
		}
		return results;
	}
	public static LinkedHashMap<String,Object> executeQuery(Endpoint dbConnection,String query,String client)
	{	
		LinkedHashMap<String,Object> results = new LinkedHashMap<String, Object>();
		ArrayList<DynaBean> data = new ArrayList<DynaBean>();
		Exchange exchange = null;
		ProducerTemplate template = null;
        DecimalFormat df = new DecimalFormat("#");
        df.setMaximumFractionDigits(10);
		try
		{
			exchange  = dbConnection.createExchange();
			template = exchange.getContext().createProducerTemplate();
			exchange.getIn().setBody(query);
			exchange = template.send(dbConnection, exchange);
			if(!exchange.isFailed())
			{	
				Set<String> colNames = exchange.getOut().getHeader("CamelJdbcColumnNames",Set.class);
				DynaClass dClass = BeanUtil.createBean(colNames);
				List<Map<String, Object>> result = exchange.getOut().getBody(List.class);
				if(result!=null)
				{
			        int i =result.size();
			        for(int j =0;j<i;j++)
			        {	
			        	Map<String, Object> row = result.get(j);
			        	DynaBean bean = dClass.newInstance();
			        	Iterator<String> colItr = colNames.iterator();
			        	while(colItr.hasNext())
						{
							String colName = colItr.next();
							Object value = row.get(colName);
							String val = "";
							if(value instanceof Integer)
			    			{
			    				val	=  new String(""+(Integer)value);
			    			}
							else if (value instanceof Long) 
			    			{
			    				val = String.valueOf((Long)value); 
							}
							else if(value instanceof byte[])
							{
								val = new String((byte[])value);
							}
							else if(value instanceof BigDecimal)
				        	{
				        		val = String.valueOf((((BigDecimal)value)).longValue());
				        	}
							else if(value instanceof Timestamp)
							{
								val =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(value);
							}
							else if(value instanceof Date)
							{
								val =  new SimpleDateFormat("yyyy-MM-dd").format(value);
							}
							else if(value instanceof Double)
							{
								val = String.valueOf(df.format((Double)value));
							}
			    			else
			    			{
			    				val = (String)value;
			    			}
							//System.out.println("Value::::::::"+val);
							bean.set(colName, val);
						}
			        	data.add(bean);
			        }
		        }
				results.put("IS_FAILED", false);
				results.put("COLUMN_NAMES", exchange.getOut().getHeader("CamelJdbcColumnNames",Set.class));
			}
			else
			{
				Exception ex = exchange.getException();
				results.put("IS_FAILED", true);
			 	results.put("ExceptionMessage", ex.getMessage());
			}
			results.put("DATA", data);
	    }
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally 
		{
			exchange = null;
			template = null;
		}
		return results;
	}
	
	
	public static Exchange executeInsert(Endpoint dbConnection,String tableName,Collection<String> cols,DynaBean usageBean)
	{	
		JdbcEndpoint con = (JdbcEndpoint)dbConnection;
		
		con.setPrepareStatementStrategy(new DefaultJdbcPrepareStatementStrategy());
		con.setUseHeadersAsParameters(true);
		Exchange exchange = con.createExchange();
		
		ProducerTemplate template = exchange.getContext().createProducerTemplate();
	   	StringBuffer insertQuery = new StringBuffer("INSERT INTO ");
		StringBuffer colBuff =  null;
		
		StringBuffer vBuff = null;
		insertQuery.append(tableName);
		insertQuery.append("(");
		Message msg = exchange.getIn();
		
		Iterator<String> columns = cols.iterator();
		while(columns.hasNext())
		{
			String colName = columns.next();
			if(colBuff == null)
			{
				colBuff = new StringBuffer(colName);
				vBuff = new StringBuffer(":?"+colName);
			}
			else
			{
				colBuff.append(" , ");
				colBuff.append(colName);
				vBuff.append(" , :?");
				vBuff.append(colName);
			}
			msg.setHeader(colName, usageBean.get(colName));
		}
		insertQuery.append(colBuff);
		insertQuery.append( " ) VALUES (");
		
	    insertQuery.append(vBuff);
	    insertQuery.append(")");
	    
	    exchange = dbConnection.createExchange();
	    
	    //exchange.getIn().setBody(insertQuery);
	    
	    msg.setBody(insertQuery);
	    exchange.setIn(msg);
	    
	    Exchange output = template.send(dbConnection,exchange);
	    if(output.isFailed())
	    {
	    	output.getException().printStackTrace();
	    	System.out.println("insert query : "+insertQuery);
	    	System.out.println( output.getException());
	    }
	    
    	Message msg1 = exchange.getOut();
    	msg1.setBody("");
    	exchange.setOut(msg1);
		
    	return exchange;
	}
	
	public static Exchange executeDelete(Endpoint dbConnection,String query)
	{	
		Exchange exchange = null;
		ProducerTemplate template = null;
		try
		{	
			exchange  = dbConnection.createExchange();
			template = exchange.getContext().createProducerTemplate();
			exchange.getIn().setBody(query);
			exchange = template.send(dbConnection, exchange);
			if(!exchange.isFailed())
			{	
				List<Map<String, Object>> result = exchange.getOut().getBody(List.class);
				if(result!=null)
				{
					
				}
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return exchange;
	}
	
	public static Exchange executeUpdate(Endpoint dbConnection,String query)
	{	
		Exchange exchange = null;
		ProducerTemplate template = null;
		try
		{	
			exchange  = dbConnection.createExchange();
			template = exchange.getContext().createProducerTemplate();
			exchange.getIn().setBody(query);
			exchange = template.send(dbConnection, exchange);
			if(!exchange.isFailed())
			{	
				List<Map<String, Object>> result = exchange.getOut().getBody(List.class);
				if(result!=null)
				{
					
				}
				//System.out.println("####### Query Executed successfully for client : "+dbConnection + "query  "+query);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return exchange;
	}
	
	
	public static Exchange updateRecord(Endpoint dbConnection,String tableName,Collection<String> cols,DynaBean usageBean)
	{	
		JdbcEndpoint con = (JdbcEndpoint)dbConnection;
		
		con.setPrepareStatementStrategy(new DefaultJdbcPrepareStatementStrategy());
		con.setUseHeadersAsParameters(true);
		Exchange exchange = con.createExchange();
		
		ProducerTemplate template = exchange.getContext().createProducerTemplate();
	   	StringBuffer updateQuery = new StringBuffer("UPDATE ");
		StringBuffer colBuff =  null;
		
		updateQuery.append(tableName);
		updateQuery.append(" SET ");
		Message msg = exchange.getIn();
		
		Iterator<String> columns = cols.iterator();
		while(columns.hasNext())
		{
			String colName = columns.next();
			if(colBuff == null)
			{
				colBuff = new StringBuffer(colName);
				colBuff.append(" = "+":?"+colName);
			}
			else
			{
				colBuff.append(" , ");
				colBuff.append(colName);
				colBuff.append(" = "+":?"+colName);
			}
			msg.setHeader(colName, usageBean.get(colName));
		}
		updateQuery.append(colBuff);
		updateQuery.append( " WHERE CLIENT_NAME = :?W_CLIENT_NAME AND DATE_FROM = :?W_DATE_FROM AND DATE_TO = :?W_DATE_TO");
		
		msg.setHeader("W_CLIENT_NAME", usageBean.get("CLIENT_NAME"));
		msg.setHeader("W_DATE_FROM", usageBean.get("DATE_FROM"));
		msg.setHeader("W_DATE_TO", usageBean.get("DATE_TO"));
		exchange = dbConnection.createExchange();
	    
	    //exchange.getIn().setBody(insertQuery);
	    
	    msg.setBody(updateQuery);
	    exchange.setIn(msg);
	    System.out.println("update query : "+updateQuery);
	    Exchange output = template.send(dbConnection,exchange);
	    if(output.isFailed())
	    {
	    	output.getException().printStackTrace();
	    	System.out.println( output.getException());
	    }
	    
    	Message msg1 = exchange.getOut();
    	msg1.setBody("");
    	exchange.setOut(msg1);
		
    	return exchange;
	}
	
	
	
	
}
