/*
 * (FCSKYS-15522) : Bhavna Chugh : Inhouse Billing : FileName Change
 */

package com.app.util;

import java.io.IOException;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.app.base.Constants;

public class MailSender
{
	private static Session _mailSession = null;
	private static MailSender _mailSender = null;
	private static Properties props = null;
	private MailSender()
	{
		init();
	}
	public static MailSender getInstance()
	{
		if(_mailSender == null)
		{
			_mailSender = new MailSender();
		}
		return _mailSender;
	}
	public void init()
	{
		if(_mailSession == null)
		{
			 props = Utility.readProperties(Constants.mailerPropsFile);
			 _mailSession = Session.getDefaultInstance(props);
	         String username = props.getProperty("user");
	         String password = props.getProperty("password");
			 
			 if (isValid(username) && isValid(password) )
	         {
	             props.put("mail.smtp.auth", "true");
	             _mailSession = Session.getInstance(props, new javax.mail.Authenticator()
	             {
	                 @Override
	                 protected PasswordAuthentication getPasswordAuthentication()
	                 {
	                     return new PasswordAuthentication(username, password);
	                 }
	             });
	         } else
	         {
	             _mailSession = Session.getInstance(props, null);
	         }
		}
	}
	
	public static boolean isValid(String value)
	{
		if(value != null && !"".equals(value))
		{
			return true;
		}
		else
			return false;
	}
	public void sendMail(String subject,String mailText,String filePath)
	{	
		String from = props.getProperty("from");
		String to = props.getProperty("to");
		String cc = props.getProperty("cc");
		String bcc = props.getProperty("bcc");
		try 
		{
			Message message = new MimeMessage(_mailSession);
			Address _from = new InternetAddress(from);
			message.setFrom(_from);
			if(isValid(to))
			{
				message.setRecipients(RecipientType.TO, InternetAddress.parse(to,false));
			}
			
			if(isValid(cc))
			{
				message.setRecipients(RecipientType.CC, InternetAddress.parse(cc,false));
			}		
			
			if(isValid(bcc))
			{
				message.setRecipients(RecipientType.BCC, InternetAddress.parse(bcc,false));
			}	
			
			message.setSubject(subject);
		  	BodyPart messageBodyPart = new MimeBodyPart();
	        messageBodyPart.setContent(mailText, "text/html; charset=utf-8");
	        Multipart multipart = new MimeMultipart();
	        multipart.addBodyPart(messageBodyPart);
	        if(isValid(filePath))
	        {
		        messageBodyPart = new MimeBodyPart();
		        DataSource source = new FileDataSource(filePath);
		        messageBodyPart.setDataHandler(new DataHandler(source));
		        messageBodyPart.setFileName(filePath);
		        multipart.addBodyPart(messageBodyPart);
		    }
	        message.setContent(multipart);
			Transport.send(message);
			System.out.println(" message sent");
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		
	}
	//Change in File Name
	public void sendMailNew(String subject,String mailText,String filePath)
	{
		sendMailNew(subject, mailText,filePath,null);
	}
	public void sendMailNew(String subject,String mailText,String filePath,String fileName)
	{	
		String from = props.getProperty("fromNew");
		String to = props.getProperty("toNew");
		String cc = props.getProperty("ccNew");
		String bcc = props.getProperty("bccNew");
		try 
		{
			Message message = new MimeMessage(_mailSession);
			Address _from = new InternetAddress(from);
			message.setFrom(_from);
			if(isValid(to))
			{
				message.setRecipients(RecipientType.TO, InternetAddress.parse(to,false));
			}
			
			if(isValid(cc))
			{
				message.setRecipients(RecipientType.CC, InternetAddress.parse(cc,false));
			}		
			
			if(isValid(bcc))
			{
				message.setRecipients(RecipientType.BCC, InternetAddress.parse(bcc,false));
			}	
			
			message.setSubject(subject);
		  	BodyPart messageBodyPart = new MimeBodyPart();
	        messageBodyPart.setContent(mailText, "text/html; charset=utf-8");
	        Multipart multipart = new MimeMultipart();
	        multipart.addBodyPart(messageBodyPart);
	        if(isValid(filePath))
	        {
		        messageBodyPart = new MimeBodyPart();
		        DataSource source = new FileDataSource(filePath);
		        messageBodyPart.setDataHandler(new DataHandler(source));
		        //messageBodyPart.setFileName(filePath);
		        if(fileName!=null){
			        messageBodyPart.setFileName(fileName);
			        }
			        else{
			        	 messageBodyPart.setFileName(filePath);
			        }
		        multipart.addBodyPart(messageBodyPart);
		    }
	        message.setContent(multipart);
			Transport.send(message);
			System.out.println(" message sent");
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		
	}
	
	public void sendCustomerUsageMail(String subject,String mailText,String filePath,String fileName)
	{	
		String from = props.getProperty("fromAdvocacy");
		String to = props.getProperty("toAdvocacy");
		String cc = props.getProperty("ccAdvocacy");
		String bcc = props.getProperty("bccAdvocacy");
		try 
		{
			Message message = new MimeMessage(_mailSession);
			Address _from = new InternetAddress(from);
			message.setFrom(_from);
			if(isValid(to))
			{
				message.setRecipients(RecipientType.TO, InternetAddress.parse(to,false));
			}
			
			if(isValid(cc))
			{
				message.setRecipients(RecipientType.CC, InternetAddress.parse(cc,false));
			}		
			
			if(isValid(bcc))
			{
				message.setRecipients(RecipientType.BCC, InternetAddress.parse(bcc,false));
			}	
			
			message.setSubject(subject);
		  	BodyPart messageBodyPart = new MimeBodyPart();
	        messageBodyPart.setContent(mailText, "text/html; charset=utf-8");
	        Multipart multipart = new MimeMultipart();
	        multipart.addBodyPart(messageBodyPart);
	        if(isValid(filePath))
	        {
		        messageBodyPart = new MimeBodyPart();
		        DataSource source = new FileDataSource(filePath);
		        messageBodyPart.setDataHandler(new DataHandler(source));
		        //messageBodyPart.setFileName(filePath);
		        if(fileName!=null){
			        messageBodyPart.setFileName(fileName);
			        }
			        else{
			        	 messageBodyPart.setFileName(filePath);
			        }
		        multipart.addBodyPart(messageBodyPart);
		    }
	        message.setContent(multipart);
			Transport.send(message);
			System.out.println(" message sent");
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		
	}
	


}
