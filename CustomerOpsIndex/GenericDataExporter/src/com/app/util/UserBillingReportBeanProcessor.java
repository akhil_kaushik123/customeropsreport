/*
 * FCSKYS-16290 | Add new Column 'Logged Insight Users' | Fetch data not for 'franconnect or Test' locations
 * FCSKYS-15522  | Implemented Billing Changes
 *  FCSKYS-18245 | Automate MUID Reports
 *	FCSKYS-18302 | Users Fetching based on their Roles 
 *  	FCSKYS-18301 | Merging of users
 *  FCSKYS-18304 | bhavna chugh Special Report in the BrightStar for Cars Report
 *  (FCSKYS-19746) To include deletion date and deactivation date in user details
 */
package com.app.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.camel.CamelContext;
import org.apache.camel.Endpoint;
import org.apache.camel.Exchange;
import org.apache.camel.ProducerTemplate;
import org.w3c.dom.Element;

import com.app.base.BillingReportBean;
import com.app.base.Constants;
import com.app.helper.DBConnectionProvider;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class UserBillingReportBeanProcessor {

	private static String getChildElementContent(Element e, String childName) {
		NodeList children = e.getElementsByTagName(childName);
		if (children.getLength() > 0) {
			return children.item(0).getTextContent();
		}
		return "";
	}

	public static void euProcessBean(Exchange exchange) {
		BillingReportBean report = null;
		try {
			Object obj = exchange.getOut().getBody();
			if (obj instanceof BillingReportBean) {
				report = (BillingReportBean) obj;
				CamelContext context = exchange.getContext();

				Endpoint endpoint = DBConnectionProvider.getInstance().getDBConnection("InHouse");
				HashMap<String, HashMap<String, HashMap<String, String>>> locationMap = report.getEUMap();
				Exchange doExchange = endpoint.createExchange();
				ProducerTemplate template = context.createProducerTemplate();
				String locationCount = "0", leadCount = "0", crmContactCount = "0", crmLeadCount = "0",
						maxLocationCreationDate = null, maxFsLeadCreationDate = null, maxCrmLeadCreationDate = null,
						maxCrmContactCreationDate = null;
				if (locationMap != null && locationMap.size() != 0) {
					HashMap<String, HashMap<String, String>> activeLocationMap = locationMap
							.get(report.getClientName());
					String countries[] = { "3", "6", "12", "15", "16", "22", "23", "29", "34", "53", "55", "56", "65",
							"70", "71", "77", "78", "81", "93", "94", "99", "101", "105", "112", "117", "118", "119",
							"121", "127", "134", "135", "144", "154", "164", "165", "169", "170", "174", "178", "182",
							"183", "188", "196", "197", "207", "212", "214", "227" };
					int l = countries.length;
					for (String country : countries) {
						if (activeLocationMap != null && activeLocationMap.size() != 0) {
							StringBuilder activeLocationinsertQuery = new StringBuilder(
									"INSERT INTO EU_DATA (CLIENT_NAME,LEAD_COUNT,LOCATION_COUNT,CRM_LEAD_COUNT,CRM_CONTACT_COUNT,LOCATION_MAX_DATE,CRM_LEAD_MAX_DATE,CRM_CONTACT_MAX_DATE,FS_LEAD_MAX_DATE,COUNTRY_ID) VALUES ");
							String insertQueryActiveLocation = "";
							if (activeLocationMap.get("locationCount") != null)
								locationCount = activeLocationMap.get("locationCount").get(country);
							if (activeLocationMap.get("leadCount") != null)
								leadCount = activeLocationMap.get("leadCount").get(country);
							if (activeLocationMap.get("crmContactCount") != null)
								crmContactCount = activeLocationMap.get("crmContactCount").get(country);
							if (activeLocationMap.get("crmLeadCount") != null)
								crmLeadCount = activeLocationMap.get("crmLeadCount").get(country);
							if (activeLocationMap.get("maxLocationCreationDate") != null)
								maxLocationCreationDate = activeLocationMap.get("maxLocationCreationDate").get(country);
							if (activeLocationMap.get("maxFsLeadCreationDate") != null)
								maxFsLeadCreationDate = activeLocationMap.get("maxFsLeadCreationDate").get(country);
							if (activeLocationMap.get("maxCrmLeadCreationDate") != null)
								maxCrmLeadCreationDate = activeLocationMap.get("maxCrmLeadCreationDate").get(country);
							if (activeLocationMap.get("maxCrmContactCreationDate") != null)
								maxCrmContactCreationDate = activeLocationMap.get("maxCrmContactCreationDate")
										.get(country);
							if (maxLocationCreationDate == null || "".equals(maxLocationCreationDate)
									|| "null".equals(maxLocationCreationDate)) {
								maxLocationCreationDate = "0000-00-00";
							}
							if (maxCrmLeadCreationDate == null || "".equals(maxCrmLeadCreationDate)
									|| "null".equals(maxCrmLeadCreationDate)) {
								maxCrmLeadCreationDate = "0000-00-00";
							}
							if (maxCrmContactCreationDate == null || "".equals(maxCrmContactCreationDate)
									|| "null".equals(maxCrmContactCreationDate)) {
								maxCrmContactCreationDate = "0000-00-00";
							}
							if (maxFsLeadCreationDate == null || "".equals(maxFsLeadCreationDate)
									|| "null".equals(maxFsLeadCreationDate)) {
								maxFsLeadCreationDate = "0000-00-00";
							}
							activeLocationinsertQuery.append("(\"" + report.getClientName() + "\" , ");
							activeLocationinsertQuery.append("\"" + leadCount + "\" , ");
							activeLocationinsertQuery.append("\"" + locationCount + "\" , ");
							activeLocationinsertQuery.append("\"" + crmLeadCount + "\" , ");
							activeLocationinsertQuery.append("\"" + crmContactCount + "\" , ");
							activeLocationinsertQuery.append("\"" + maxLocationCreationDate + "\" , ");
							activeLocationinsertQuery.append("\"" + maxCrmLeadCreationDate + "\" , ");
							activeLocationinsertQuery.append("\"" + maxCrmContactCreationDate + "\" , ");
							activeLocationinsertQuery.append("\"" + maxFsLeadCreationDate + "\" , ");
							activeLocationinsertQuery.append("\"" + country + "\")  ");
							if (activeLocationinsertQuery != null && activeLocationinsertQuery.lastIndexOf(",") > 0) {
								insertQueryActiveLocation = activeLocationinsertQuery.substring(0,
										activeLocationinsertQuery.length() - 1);
							}
							if (!"".equals(insertQueryActiveLocation)) {
								doExchange.getIn().setBody(insertQueryActiveLocation);
								Exception excpetion2 = template.send(endpoint, doExchange).getException();
								if (excpetion2 != null) {
									System.out.println("client Name : 1111111111" + report.getClientName()
											+ " query for insert isFailed " + excpetion2.getMessage());
									excpetion2.printStackTrace();
								}
							}
						}
					}
				}

			}
		} catch (Exception ex) {
			System.out.println("Exception in " + report.getClientName());
			ex.printStackTrace();
		} finally {
			report = null;
		}
	}

	public static void processBean(Exchange exchange) {
		System.out.println(" inserting ");
		BillingReportBean report = null;
		try {
			Object obj = exchange.getOut().getBody();
			if (obj instanceof BillingReportBean) {
				report = (BillingReportBean) obj;
				CamelContext context = exchange.getContext();
				Endpoint endpoint = DBConnectionProvider.getInstance().getDBConnection("InHouse");
				Endpoint endpointfss = DBConnectionProvider.getInstance().getDBConnection("fss");
				// Endpoint endpoint = context.getEndpoint("direct:fss");
				Exchange doExchange = endpoint.createExchange();
				ProducerTemplate template = context.createProducerTemplate();
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
				Document doc = null;

				HashMap<String, HashMap<String, HashMap<String, String>>> locationMap = report.getLocationMap();
				Map<String, Object> brightStarCarReportMap = null;
				if (report.getCodeValue().equals("brightstar")) {
					brightStarCarReportMap = report.getBrightStarCarReportMap();
				}
				if (locationMap != null && locationMap.size() != 0) {
					HashMap<String, HashMap<String, String>> activeLocationMap = locationMap.get("activeLocationMap");
					HashMap<String, HashMap<String, String>> activeIndevelopmentLocationMap = locationMap.get("activeInDevelopmentLocationMap");
					
					HashMap<String, HashMap<String, String>> atleastOneUsersLocationMap = locationMap
							.get("atleastOneUsersLocationMap");
					HashMap<String, HashMap<String, String>> moreThanOrFiveUserslocationMap = locationMap
							.get("moreThanOrFiveUserslocationMap");
					HashMap<String, HashMap<String, String>> extraCalslocationMap = locationMap
							.get("extraCalslocationMap");
					HashMap<String, HashMap<String, String>> TotalCalslocationMap = locationMap
							.get("TotalCalslocationMap");
					HashMap<String, HashMap<String, String>> noUserlocationMap = locationMap.get("noUserlocationMap");
					HashMap<String, HashMap<String, String>> calslocationMap = locationMap.get("calslocationMap");
					HashMap<String, HashMap<String, String>> usersMap = locationMap.get("usersMap");
					HashMap<String, HashMap<String, String>> roleMap = locationMap.get("roleMap");
					HashMap<String, HashMap<String, String>> muidLocationMap = locationMap.get("muidLocationMap");
					HashMap<String, HashMap<String, String>> lightBridgeMap = locationMap.get("lightBridgeMap");
					String hostURL = report.getHostURL();

					String franchiseeNO = "", franchiseename = "", numberOfuser = "";
					if (calslocationMap != null && calslocationMap.size() != 0) {
						// StringBuilder activeLocationinsertQuery = new
						// StringBuilder("INSERT INTO USER_BILLING_THREAD_DATA
						// (BUILD_NAME,CODE_VALUE,DB_NAME,DATE_FROM,DATE_TO,AT_LEAST_ONE,MORE_THEN_FIVE,EXT_CAL,CAL,ACTIVE_LOCATIONS,NO_USER_LOCATIONS,FDD_COUNT)
						// VALUES ");
						StringBuilder activeLocationinsertQuery = new StringBuilder(
								"INSERT INTO USER_BILLING_THREAD_DATA (BUILD_NAME,CODE_VALUE,DB_NAME,DATE_FROM,DATE_TO,AT_LEAST_ONE,MORE_THEN_FIVE,EXT_CAL,CAL,ACTIVE_LOCATIONS,NO_USER_LOCATIONS,FDD_COUNT,INSIGHT_USER,TOTAL_SYSTEM_TIME,ADM_SYSTEM_TIME,CLIENT_SYSTEM_TIME,HOST_NAME,MODULE_NAME,MODULE_COUNT,IN_DEVELOPMENT_LOCATION,CLIENT_SYSTEM_NAME) VALUES ");
						String insertQueryActiveLocation = "";
						StringBuilder updateInsightQuery = null;
						StringBuilder updateInsightQuery1 = null;
						String atleastOne = "0", moreThanOrFive = "0", extraCals = "0", cals = "0", activeloc = "0",
								noUserLoc = "0", fddCount = "0", insightCount = "0", systemTime = "00:00:00",
								admsystemTime = "00:00:00", clientSystemTime = "00:00:00", modules = "",
								moduleCount = "",clientSystemName="",activeOpnerLoc="0";
						int moduleCountN = 0;
						for (String key : calslocationMap.keySet()) {

							HashMap<String, String> hmap = calslocationMap.get(key);
							atleastOne = hmap.get("atleastOne");
							moreThanOrFive = hmap.get("moreThanOrFive");
							extraCals = hmap.get("extraCals");
							cals = hmap.get("cals");
							activeloc = hmap.get("activeLoc");
							noUserLoc = hmap.get("noUserlocation");
							fddCount = hmap.get("fddCount");
							systemTime = hmap.get("systemTime");
							admsystemTime = hmap.get("admsystemTime");
							clientSystemTime = hmap.get("clientSystemTime");
							modules = hmap.get("modules");
							moduleCount = hmap.get("moduleCount");
							clientSystemName = hmap.get("clientSystemName");
							activeOpnerLoc=hmap.get("activeOpnerLoc");
							if (Constants.version.equals("SaaS")) {
								insightCount = hmap.get("insightCount");

								if (insightCount == null || "".equals(insightCount) || "null".equals(insightCount)) {
									insightCount = "0";
								}
							}
							if (atleastOne == null || "".equals(atleastOne) || "null".equals(atleastOne)) {
								atleastOne = "0";
							}

							if (moreThanOrFive == null || "".equals(moreThanOrFive) || "null".equals(moreThanOrFive)) {
								moreThanOrFive = "0";
							}

							if (extraCals == null || "".equals(extraCals) || "null".equals(extraCals)) {
								extraCals = "0";
							}
							if (cals == null || "".equals(cals) || "null".equals(cals)) {
								cals = "0";
							}

							if (activeloc == null || "".equals(activeloc) || "null".equals(activeloc)) {
								activeloc = "0";
							}

							if (noUserLoc == null || "".equals(noUserLoc) || "null".equals(noUserLoc)) {
								noUserLoc = "0";
							}
							if (activeOpnerLoc == null || "".equals(activeOpnerLoc) || "null".equals(activeOpnerLoc)) {
								activeOpnerLoc = "0";
							}
							if (fddCount == null || "".equals(fddCount) || "null".equals(fddCount)) {
								fddCount = "0";
							}
							if (systemTime == null || "".equals(systemTime) || "null".equals(systemTime)) {
								systemTime = "00:00:00";
							}
							if (admsystemTime == null || "".equals(admsystemTime) || "null".equals(admsystemTime)) {
								admsystemTime = "00:00:00";
							}
							if (clientSystemTime == null || "".equals(clientSystemTime)
									|| "null".equals(clientSystemTime)) {
								clientSystemTime = "00:00:00";
							}
							if (modules == null || "".equals(modules) || "null".equals(modules)) {
								try {
									List data = null;
									String path = "";
									File file;
									Map<String, Object> row1 = null;
									if (Constants.version.equals("Legacy")) {
										String keyvalQuery = "SELECT CONCAT(HOST_NAME,KEYVAL_PATH) AS KEYVAL_PATH FROM SPECIAL_KEYVAL_CLIENT WHERE BUILD_NAME='"
												+ report.getClientName() + "'";
										exchange = endpointfss.createExchange();
										exchange.getIn().setBody(keyvalQuery.toString());
										Exchange out = template.send(endpointfss, exchange);
										if (!exchange.isFailed()) {
											data = (List) exchange.getOut().getBody();
											System.out.println("**************88"+data+report.getClientName());
										}
										if (data != null && data.size()>0) {
											int i = data.size();
											row1 = (Map<String, Object>) data.get(0);
											path = row1.get("KEYVAL_PATH") + "";
											
										String[] command = { "curl", "-H", "Accept:application/json", "-u",
												"" + ":" + "", path };
										try {
											ProcessBuilder process = new ProcessBuilder(command);
											Process p;
											p = process.start();
											BufferedReader reader = new BufferedReader(
													new InputStreamReader(p.getInputStream()));
											StringBuilder builder = new StringBuilder();
											String line = null;
											while ((line = reader.readLine()) != null) {
												if (line.contains("modules")) {
													builder.append(line);
													break;
												}
											}
											String result = builder.toString();
											result = result.trim().replace("<modules>", "");
											result = result.trim().replace("</modules>", "");
											modules = result;
											p.waitFor();
											p.destroy();
										} catch (Exception e) {
											System.out.println("InterruptedException has been occured in this client"
													+ report.getClientName());
											e.printStackTrace();
										}
										String moduleSplit[] = modules.split(",");
										moduleCountN = moduleSplit.length;
										moduleCount = moduleCountN + "";
									}
									}
								} catch (Exception e) {
									e.printStackTrace();
								}
							}

							activeLocationinsertQuery.append("(\"" + report.getClientName() + "\" , ");
							activeLocationinsertQuery.append("\"" + report.getCodeValue() + "\" , ");
							activeLocationinsertQuery.append("\"" + report.getDbName() + "\" , ");
							activeLocationinsertQuery.append("\"" + report.getDateFrom() + "\" , ");
							activeLocationinsertQuery.append("\"" + report.getDateTo() + "\" , ");
							activeLocationinsertQuery.append("\"" + atleastOne + "\" , ");
							activeLocationinsertQuery.append("\"" + moreThanOrFive + "\" , ");
							activeLocationinsertQuery.append("\"" + extraCals + "\" , ");
							activeLocationinsertQuery.append("\"" + cals + "\" , ");
							activeLocationinsertQuery.append("\"" + activeloc + "\" , ");
							activeLocationinsertQuery.append("\"" + noUserLoc + "\" , ");
							activeLocationinsertQuery.append("\"" + fddCount + "\" , ");
							activeLocationinsertQuery.append("\"" + insightCount + "\",");
							activeLocationinsertQuery.append("\"" + systemTime + "\",");
							activeLocationinsertQuery.append("\"" + admsystemTime + "\",");
							activeLocationinsertQuery.append("\"" + clientSystemTime + "\",");
							activeLocationinsertQuery.append("\"" + report.getHostURL() + "\",");
							activeLocationinsertQuery.append("\"" + modules + "\",");
							activeLocationinsertQuery.append("\"" + moduleCount + "\",");
							activeLocationinsertQuery.append("\"" + activeOpnerLoc + "\",");
							activeLocationinsertQuery.append("\"" + clientSystemName + "\"),");
						}

						if (activeLocationinsertQuery != null && activeLocationinsertQuery.lastIndexOf(",") > 0) {
							insertQueryActiveLocation = activeLocationinsertQuery.substring(0,
									activeLocationinsertQuery.length() - 1);
						}

						if (!"".equals(insertQueryActiveLocation)) {
							// System.out.println("client Name :
							// "+report.getClientName()+" query for insert
							// "+insertQueryActiveLocation);
							doExchange.getIn().setBody(insertQueryActiveLocation);
							Exception excpetion2 = template.send(endpoint, doExchange).getException();
							if (excpetion2 != null) {
								System.out.println("client Name : 1111111111" + report.getClientName()
										+ " query for insert isFailed " + excpetion2.getMessage());
								excpetion2.printStackTrace();
							}
						}

					} // end of calslocationMap
					if (activeLocationMap != null && activeLocationMap.size() != 0) {
						StringBuilder activeLocationinsertQuery = new StringBuilder(
								"INSERT INTO ACTIVE_LOCATION_THREAD_DATA (BUILD_NAME,CODE_VALUE,DB_NAME,DATE_FROM,DATE_TO,FRANCHISEE_NO,FRANCHISEE_NAME,NO_USERS,OPENING_DATE) VALUES ");
						String insertQueryActiveLocation = "";
						franchiseeNO = "";
						franchiseename = "";
						numberOfuser = "";
						String openingDate = "";
						for (String key : activeLocationMap.keySet()) {

							HashMap<String, String> hmap = activeLocationMap.get(key);
							franchiseeNO = hmap.get("FRANCHISEE_NO");
							franchiseename = hmap.get("FRANCHISEE_NAME");
							numberOfuser = hmap.get("USER_COUNT");
							openingDate = hmap.get("OPENING_DATE");

							if (franchiseeNO == null || "".equals(franchiseeNO) || "null".equals(franchiseeNO)) {
								franchiseeNO = "-1";
							}

							if (franchiseename == null || "".equals(franchiseename) || "null".equals(franchiseename)) {
								franchiseename = "";
							}

							if (numberOfuser == null || "".equals(numberOfuser) || "null".equals(numberOfuser)) {
								numberOfuser = "0";
							}
							if (numberOfuser == null || "".equals(numberOfuser) || "null".equals(numberOfuser)) {
								numberOfuser = "0";
							}
							if (openingDate == null || "".equals(openingDate) || "null".equals(openingDate)) {
								openingDate = "";
							}

							activeLocationinsertQuery.append("(\"" + report.getClientName() + "\" , ");
							activeLocationinsertQuery.append("\"" + report.getCodeValue() + "\" , ");
							activeLocationinsertQuery.append("\"" + report.getDbName() + "\" , ");
							activeLocationinsertQuery.append("\"" + report.getDateFrom() + "\" , ");
							activeLocationinsertQuery.append("\"" + report.getDateTo() + "\" , ");
							activeLocationinsertQuery.append("\"" + franchiseeNO + "\" , ");
							activeLocationinsertQuery.append("\"" + franchiseename + "\" , ");
							activeLocationinsertQuery.append("\"" + numberOfuser + "\" ,");
							activeLocationinsertQuery.append("\"" + openingDate + "\" ),");
						}

						if (activeLocationinsertQuery != null && activeLocationinsertQuery.lastIndexOf(",") > 0) {
							insertQueryActiveLocation = activeLocationinsertQuery.substring(0,
									activeLocationinsertQuery.length() - 1);
						}

						if (!"".equals(insertQueryActiveLocation)) {
							// System.out.println("client Name :
							// "+report.getClientName()+" query for insert
							// "+insertQueryActiveLocation);
							doExchange.getIn().setBody(insertQueryActiveLocation);
							Exception excpetion = template.send(endpoint, doExchange).getException();
							if (excpetion != null) {
								System.out.println("client Name : " + report.getClientName()
										+ " query for insert isFailed " + excpetion.getMessage());
								excpetion.printStackTrace();
							}
						}

					} // end of activeIndevelopmentLocationMap

					if (activeIndevelopmentLocationMap != null && activeIndevelopmentLocationMap.size() != 0) {
						StringBuilder activeIndevelopmentLocationinsertQuery = new StringBuilder(
								"INSERT INTO IN_DEVELOPMENT_LOCATION_THREAD_DATA (BUILD_NAME,CODE_VALUE,DB_NAME,DATE_FROM,DATE_TO,FRANCHISEE_NO,FRANCHISEE_NAME,NO_USERS,OPENING_DATE) VALUES ");
						String insertQueryActiveLocation = "";
						franchiseeNO = "";
						franchiseename = "";
						numberOfuser = "";
						String openingDate = "";
						for (String key : activeIndevelopmentLocationMap.keySet()) {

							HashMap<String, String> hmap = activeIndevelopmentLocationMap.get(key);
							franchiseeNO = hmap.get("FRANCHISEE_NO");
							franchiseename = hmap.get("FRANCHISEE_NAME");
							numberOfuser = hmap.get("USER_COUNT");
							openingDate = hmap.get("OPENING_DATE");

							if (franchiseeNO == null || "".equals(franchiseeNO) || "null".equals(franchiseeNO)) {
								franchiseeNO = "-1";
							}

							if (franchiseename == null || "".equals(franchiseename) || "null".equals(franchiseename)) {
								franchiseename = "";
							}

							if (numberOfuser == null || "".equals(numberOfuser) || "null".equals(numberOfuser)) {
								numberOfuser = "0";
							}
							if (numberOfuser == null || "".equals(numberOfuser) || "null".equals(numberOfuser)) {
								numberOfuser = "0";
							}
							if (openingDate == null || "".equals(openingDate) || "null".equals(openingDate)) {
								openingDate = "";
							}

							activeIndevelopmentLocationinsertQuery.append("(\"" + report.getClientName() + "\" , ");
							activeIndevelopmentLocationinsertQuery.append("\"" + report.getCodeValue() + "\" , ");
							activeIndevelopmentLocationinsertQuery.append("\"" + report.getDbName() + "\" , ");
							activeIndevelopmentLocationinsertQuery.append("\"" + report.getDateFrom() + "\" , ");
							activeIndevelopmentLocationinsertQuery.append("\"" + report.getDateTo() + "\" , ");
							activeIndevelopmentLocationinsertQuery.append("\"" + franchiseeNO + "\" , ");
							activeIndevelopmentLocationinsertQuery.append("\"" + franchiseename + "\" , ");
							activeIndevelopmentLocationinsertQuery.append("\"" + numberOfuser + "\" ,");
							activeIndevelopmentLocationinsertQuery.append("\"" + openingDate + "\" ),");
						}

						if (activeIndevelopmentLocationinsertQuery != null && activeIndevelopmentLocationinsertQuery.lastIndexOf(",") > 0) {
							insertQueryActiveLocation = activeIndevelopmentLocationinsertQuery.substring(0,
									activeIndevelopmentLocationinsertQuery.length() - 1);
						}

						if (!"".equals(insertQueryActiveLocation)) {
							// System.out.println("client Name :
							// "+report.getClientName()+" query for insert
							// "+insertQueryActiveLocation);
							doExchange.getIn().setBody(insertQueryActiveLocation);
							Exception excpetion = template.send(endpoint, doExchange).getException();
							if (excpetion != null) {
								System.out.println("client Name : " + report.getClientName()
										+ " query for insert isFailed " + excpetion.getMessage());
								excpetion.printStackTrace();
							}
						}

					} // end of activeIndevelopmentLocationMap
					
					
					
					
					
					if (atleastOneUsersLocationMap != null && atleastOneUsersLocationMap.size() != 0) {
						StringBuilder activeLocationinsertQuery = new StringBuilder(
								"INSERT INTO UNIQUE_LOCATION_THREAD_DATA(BUILD_NAME,CODE_VALUE,DB_NAME,DATE_FROM,DATE_TO,FRANCHISEE_NO,FRANCHISEE_NAME,NO_USERS) VALUES ");
						String insertQueryActiveLocation = "";
						franchiseeNO = "";
						franchiseename = "";
						numberOfuser = "";
						for (String key : atleastOneUsersLocationMap.keySet()) {

							HashMap<String, String> hmap = atleastOneUsersLocationMap.get(key);
							franchiseeNO = hmap.get("FRANCHISEE_NO");
							franchiseename = hmap.get("FRANCHISEE_NAME");
							numberOfuser = hmap.get("USER_COUNT");
							if (franchiseeNO == null || "".equals(franchiseeNO) || "null".equals(franchiseeNO)) {
								franchiseeNO = "-1";
							}

							if (franchiseename == null || "".equals(franchiseename) || "null".equals(franchiseename)) {
								franchiseename = "";
							}

							if (numberOfuser == null || "".equals(numberOfuser) || "null".equals(numberOfuser)) {
								numberOfuser = "0";
							}
							activeLocationinsertQuery.append("(\"" + report.getClientName() + "\" , ");
							activeLocationinsertQuery.append("\"" + report.getCodeValue() + "\" , ");
							activeLocationinsertQuery.append("\"" + report.getDbName() + "\" , ");
							activeLocationinsertQuery.append("\"" + report.getDateFrom() + "\" , ");
							activeLocationinsertQuery.append("\"" + report.getDateTo() + "\" , ");
							activeLocationinsertQuery.append("\"" + franchiseeNO + "\" , ");
							activeLocationinsertQuery.append("\"" + franchiseename + "\" , ");
							activeLocationinsertQuery.append("\"" + numberOfuser + "\" ),");
						}

						if (activeLocationinsertQuery != null && activeLocationinsertQuery.lastIndexOf(",") > 0) {
							insertQueryActiveLocation = activeLocationinsertQuery.substring(0,
									activeLocationinsertQuery.length() - 1);
						}

						if (!"".equals(insertQueryActiveLocation)) {
							// System.out.println("client Name :
							// "+report.getClientName()+" query for insert
							// "+insertQueryActiveLocation);
							doExchange.getIn().setBody(insertQueryActiveLocation);
							Exception excpetion = template.send(endpoint, doExchange).getException();
							if (excpetion != null) {
								System.out.println("client Name :22222222222222 " + report.getClientName()
										+ " query for insert isFailed " + excpetion.getMessage());
								excpetion.printStackTrace();
							}
						}

					} // end of atleastOneUsersLocationMap

					if (moreThanOrFiveUserslocationMap != null && moreThanOrFiveUserslocationMap.size() != 0) {
						StringBuilder activeLocationinsertQuery = new StringBuilder(
								"INSERT INTO MORE_THEN_FIVE_LOCATION_THREAD_DATA(BUILD_NAME,CODE_VALUE,DB_NAME,DATE_FROM,DATE_TO,FRANCHISEE_NO,FRANCHISEE_NAME,NO_USERS) VALUES ");
						String insertQueryActiveLocation = "";
						franchiseeNO = "";
						franchiseename = "";
						numberOfuser = "";
						for (String key : moreThanOrFiveUserslocationMap.keySet()) {

							HashMap<String, String> hmap = moreThanOrFiveUserslocationMap.get(key);
							franchiseeNO = hmap.get("FRANCHISEE_NO");
							franchiseename = hmap.get("FRANCHISEE_NAME");
							numberOfuser = hmap.get("USER_COUNT");
							if (franchiseeNO == null || "".equals(franchiseeNO) || "null".equals(franchiseeNO)) {
								franchiseeNO = "-1";
							}

							if (franchiseename == null || "".equals(franchiseename) || "null".equals(franchiseename)) {
								franchiseename = "";
							}

							if (numberOfuser == null || "".equals(numberOfuser) || "null".equals(numberOfuser)) {
								numberOfuser = "0";
							}
							activeLocationinsertQuery.append("(\"" + report.getClientName() + "\" , ");
							activeLocationinsertQuery.append("\"" + report.getCodeValue() + "\" , ");
							activeLocationinsertQuery.append("\"" + report.getDbName() + "\" , ");
							activeLocationinsertQuery.append("\"" + report.getDateFrom() + "\" , ");
							activeLocationinsertQuery.append("\"" + report.getDateTo() + "\" , ");
							activeLocationinsertQuery.append("\"" + franchiseeNO + "\" , ");
							activeLocationinsertQuery.append("\"" + franchiseename + "\" , ");
							activeLocationinsertQuery.append("\"" + numberOfuser + "\" ),");
						}

						if (activeLocationinsertQuery != null && activeLocationinsertQuery.lastIndexOf(",") > 0) {
							insertQueryActiveLocation = activeLocationinsertQuery.substring(0,
									activeLocationinsertQuery.length() - 1);
						}

						if (!"".equals(insertQueryActiveLocation)) {
							// System.out.println("client Name :
							// "+report.getClientName()+" query for insert
							// "+insertQueryActiveLocation);
							doExchange.getIn().setBody(insertQueryActiveLocation);
							Exception excpetion = template.send(endpoint, doExchange).getException();
							if (excpetion != null) {
								System.out.println("client Name : 33333333333333" + report.getClientName()
										+ " query for insert isFailed " + excpetion.getMessage());
								excpetion.printStackTrace();
							}
						}

					} // end of moreThanOrFiveUserslocationMap

					if (extraCalslocationMap != null && extraCalslocationMap.size() != 0) {
						StringBuilder activeLocationinsertQuery = new StringBuilder(
								"INSERT INTO EXTRA_CAL_LOCATION_THREAD_DATA(BUILD_NAME,CODE_VALUE,DB_NAME,DATE_FROM,DATE_TO,FRANCHISEE_NO,FRANCHISEE_NAME,NO_USERS) VALUES ");
						String insertQueryActiveLocation = "";
						franchiseeNO = "";
						franchiseename = "";
						numberOfuser = "";
						for (String key : extraCalslocationMap.keySet()) {

							HashMap<String, String> hmap = extraCalslocationMap.get(key);
							franchiseeNO = hmap.get("FRANCHISEE_NO");
							franchiseename = hmap.get("FRANCHISEE_NAME");
							numberOfuser = hmap.get("USER_COUNT");
							if (franchiseeNO == null || "".equals(franchiseeNO) || "null".equals(franchiseeNO)) {
								franchiseeNO = "-1";
							}

							if (franchiseename == null || "".equals(franchiseename) || "null".equals(franchiseename)) {
								franchiseename = "";
							}

							if (numberOfuser == null || "".equals(numberOfuser) || "null".equals(numberOfuser)) {
								numberOfuser = "0";
							}
							activeLocationinsertQuery.append("(\"" + report.getClientName() + "\" , ");
							activeLocationinsertQuery.append("\"" + report.getCodeValue() + "\" , ");
							activeLocationinsertQuery.append("\"" + report.getDbName() + "\" , ");
							activeLocationinsertQuery.append("\"" + report.getDateFrom() + "\" , ");
							activeLocationinsertQuery.append("\"" + report.getDateTo() + "\" , ");
							activeLocationinsertQuery.append("\"" + franchiseeNO + "\" , ");
							activeLocationinsertQuery.append("\"" + franchiseename + "\" , ");
							activeLocationinsertQuery.append("\"" + numberOfuser + "\" ),");
						}

						if (activeLocationinsertQuery != null && activeLocationinsertQuery.lastIndexOf(",") > 0) {
							insertQueryActiveLocation = activeLocationinsertQuery.substring(0,
									activeLocationinsertQuery.length() - 1);
						}

						if (!"".equals(insertQueryActiveLocation)) {
							// System.out.println("client Name :
							// "+report.getClientName()+" query for insert
							// "+insertQueryActiveLocation);
							doExchange.getIn().setBody(insertQueryActiveLocation);
							Exception excpetion = template.send(endpoint, doExchange).getException();
							if (excpetion != null) {
								System.out.println("client Name : 4444444444" + report.getClientName()
										+ " query for insert isFailed " + excpetion.getMessage());
								excpetion.printStackTrace();
							}
						}

					} // end of extraCalslocationMap

					if (TotalCalslocationMap != null && TotalCalslocationMap.size() != 0) {
						StringBuilder activeLocationinsertQuery = new StringBuilder(
								"INSERT INTO TOTAL_CAL_LOCATION_THREAD_DATA(BUILD_NAME,CODE_VALUE,DB_NAME,DATE_FROM,DATE_TO,FRANCHISEE_NO,FRANCHISEE_NAME,NO_USERS) VALUES ");
						String insertQueryActiveLocation = "";
						franchiseeNO = "";
						franchiseename = "";
						numberOfuser = "";
						for (String key : TotalCalslocationMap.keySet()) {

							HashMap<String, String> hmap = TotalCalslocationMap.get(key);
							franchiseeNO = hmap.get("FRANCHISEE_NO");
							franchiseename = hmap.get("FRANCHISEE_NAME");
							numberOfuser = hmap.get("USER_COUNT");
							if (franchiseeNO == null || "".equals(franchiseeNO) || "null".equals(franchiseeNO)) {
								franchiseeNO = "-1";
							}

							if (franchiseename == null || "".equals(franchiseename) || "null".equals(franchiseename)) {
								franchiseename = "";
							}

							if (numberOfuser == null || "".equals(numberOfuser) || "null".equals(numberOfuser)) {
								numberOfuser = "0";
							}
							activeLocationinsertQuery.append("(\"" + report.getClientName() + "\" , ");
							activeLocationinsertQuery.append("\"" + report.getCodeValue() + "\" , ");
							activeLocationinsertQuery.append("\"" + report.getDbName() + "\" , ");
							activeLocationinsertQuery.append("\"" + report.getDateFrom() + "\" , ");
							activeLocationinsertQuery.append("\"" + report.getDateTo() + "\" , ");
							activeLocationinsertQuery.append("\"" + franchiseeNO + "\" , ");
							activeLocationinsertQuery.append("\"" + franchiseename + "\" , ");
							activeLocationinsertQuery.append("\"" + numberOfuser + "\" ),");
						}

						if (activeLocationinsertQuery != null && activeLocationinsertQuery.lastIndexOf(",") > 0) {
							insertQueryActiveLocation = activeLocationinsertQuery.substring(0,
									activeLocationinsertQuery.length() - 1);
						}

						if (!"".equals(insertQueryActiveLocation)) {
							// System.out.println("client Name :
							// "+report.getClientName()+" query for insert
							// "+insertQueryActiveLocation);
							doExchange.getIn().setBody(insertQueryActiveLocation);
							Exception excpetion = template.send(endpoint, doExchange).getException();
							if (excpetion != null) {
								System.out.println("client Name :5555555555555 " + report.getClientName()
										+ " query for insert isFailed " + excpetion.getMessage());
								excpetion.printStackTrace();
							}
						}

					} // end of TotalCalslocationMap

					if (noUserlocationMap != null && noUserlocationMap.size() != 0) {
						StringBuilder activeLocationinsertQuery = new StringBuilder(
								"INSERT INTO NOUSER_LOCATION_THREAD_DATA(BUILD_NAME,CODE_VALUE,DB_NAME,DATE_FROM,DATE_TO,FRANCHISEE_NO,FRANCHISEE_NAME,NO_USERS) VALUES ");
						String insertQueryActiveLocation = "";
						franchiseeNO = "";
						franchiseename = "";
						numberOfuser = "";
						for (String key : noUserlocationMap.keySet()) {

							HashMap<String, String> hmap = noUserlocationMap.get(key);
							franchiseeNO = hmap.get("FRANCHISEE_NO");
							franchiseename = hmap.get("FRANCHISEE_NAME");
							numberOfuser = hmap.get("USER_COUNT");
							if (franchiseeNO == null || "".equals(franchiseeNO) || "null".equals(franchiseeNO)) {
								franchiseeNO = "-1";
							}

							if (franchiseename == null || "".equals(franchiseename) || "null".equals(franchiseename)) {
								franchiseename = "";
							}

							if (numberOfuser == null || "".equals(numberOfuser) || "null".equals(numberOfuser)) {
								numberOfuser = "0";
							}
							activeLocationinsertQuery.append("(\"" + report.getClientName() + "\" , ");
							activeLocationinsertQuery.append("\"" + report.getCodeValue() + "\" , ");
							activeLocationinsertQuery.append("\"" + report.getDbName() + "\" , ");
							activeLocationinsertQuery.append("\"" + report.getDateFrom() + "\" , ");
							activeLocationinsertQuery.append("\"" + report.getDateTo() + "\" , ");
							activeLocationinsertQuery.append("\"" + franchiseeNO + "\" , ");
							activeLocationinsertQuery.append("\"" + franchiseename + "\" , ");
							activeLocationinsertQuery.append("\"" + numberOfuser + "\" ),");
						}

						if (activeLocationinsertQuery != null && activeLocationinsertQuery.lastIndexOf(",") > 0) {
							insertQueryActiveLocation = activeLocationinsertQuery.substring(0,
									activeLocationinsertQuery.length() - 1);
						}

						if (!"".equals(insertQueryActiveLocation)) {
							// System.out.println("client Name :
							// "+report.getClientName()+" query for insert
							// "+insertQueryActiveLocation);
							doExchange.getIn().setBody(insertQueryActiveLocation);
							Exception excpetion = template.send(endpoint, doExchange).getException();
							if (excpetion != null) {
								System.out.println("client Name :66666666666 " + report.getClientName()
										+ " query for insert isFailed " + excpetion.getMessage());
								excpetion.printStackTrace();
							}
						}

					} // end of noUserlocationMap

					if (usersMap != null && usersMap.size() != 0) {
						StringBuilder activeLocationinsertQuery = new StringBuilder(
								"INSERT INTO USERS_THREAD_DATA(BUILD_NAME,CODE_VALUE,DB_NAME,DATE_FROM,DATE_TO,USER_NO,FRANCHISEE_NAME,USER_FIRST_NAME,USER_LAST_NAME,EMAIL_ID,AREA_ID,AREA_NAME,USER_LEVEL,USER_TYPE,STATUS,USER_ID,CREATION_DATE,TIMEZONE,ROLE,IS_DELETED,SUB_USER_TYPE,LAST_MODIFIED_DATE) VALUES ");
						String insertQueryActiveLocation = "";
						String userNo = "";
						franchiseename = "";
						String userFirstName = "";
						String userLastName = "";
						String emailID = "";
						String areaID = "";
						String areaName = "";

						String userLevel = "";
						String userType = "";
						String status = "";
						String userID = "";
						String creationDate = "";
						String timeZone = "";
						String role = "";
						String subUserType = "";
						String isDeleted = "";
						String lastModifiedDate = "";
						for (String key : usersMap.keySet()) {

							HashMap<String, String> hmap = usersMap.get(key);
							userNo = hmap.get("USER_NO");
							franchiseename = hmap.get("FRANCHISEE_NAME");
							userFirstName = hmap.get("USER_FIRST_NAME");
							userLastName = hmap.get("USER_LAST_NAME");
							emailID = hmap.get("EMAIL_ID");
							areaID = hmap.get("AREA_ID");
							areaName = hmap.get("AREA_NAME");
							userLevel = hmap.get("USER_LEVEL");
							userType = hmap.get("USER_TYPE");
							status = hmap.get("STATUS");
							userID = hmap.get("USER_ID");
							creationDate = hmap.get("CREATION_DATE");
							timeZone = hmap.get("TIMEZONE");
							role = hmap.get("ROLE");
							subUserType = hmap.get("SUB_USER_TYPE");
							isDeleted = hmap.get("IS_DELETED");
							lastModifiedDate=hmap.get("LAST_MODIFIED_DATE");
							if (isDeleted == null || "".equals(isDeleted) || "null".equals(isDeleted)) {
								isDeleted = "N";
							}
							if (userNo == null || "".equals(userNo) || "null".equals(userNo)) {
								userNo = "-1";
							}
							if (franchiseename == null || "".equals(franchiseename) || "null".equals(franchiseename)) {
								franchiseename = "";
							}
							if (userFirstName == null || "".equals(userFirstName) || "null".equals(userFirstName)) {
								userFirstName = "";
							}
							if (userLastName == null || "".equals(userLastName) || "null".equals(userLastName)) {
								userLastName = "";
							}
							if (emailID == null || "".equals(emailID) || "null".equals(emailID)) {
								emailID = "";
							}
							if (areaID == null || "".equals(areaID) || "null".equals(areaID)) {
								areaID = "-1";
							}
							if (areaName == null || "".equals(areaName) || "null".equals(areaName)) {
								areaName = "";
							}
							if (userLevel == null || "".equals(userLevel) || "null".equals(userLevel)) {
								userLevel = "-1";
							}
							if (userType == null || "".equals(userType) || "null".equals(userType)) {
								userType = "";
							}
							if (status == null || "".equals(status) || "null".equals(status)) {
								status = "-1";
							}
							if (userID == null || "".equals(userID) || "null".equals(userID)) {
								userID = "";
							}
							if (creationDate == null || "".equals(creationDate) || "null".equals(creationDate)) {
								creationDate = "";
							}
							activeLocationinsertQuery.append("(\"" + report.getClientName() + "\" , ");
							activeLocationinsertQuery.append("\"" + report.getCodeValue() + "\" , ");
							activeLocationinsertQuery.append("\"" + report.getDbName() + "\" , ");
							activeLocationinsertQuery.append("\"" + report.getDateFrom() + "\" , ");
							activeLocationinsertQuery.append("\"" + report.getDateTo() + "\" , ");
							activeLocationinsertQuery.append("\"" + userNo + "\" , ");
							activeLocationinsertQuery.append("\"" + franchiseename + "\" , ");
							activeLocationinsertQuery.append("\"" + userFirstName + "\" , ");
							activeLocationinsertQuery.append("\"" + userLastName + "\" , ");
							activeLocationinsertQuery.append("\"" + emailID + "\" , ");
							activeLocationinsertQuery.append("\"" + areaID + "\" , ");
							activeLocationinsertQuery.append("\"" + areaName + "\" , ");
							activeLocationinsertQuery.append("\"" + userLevel + "\" , ");
							activeLocationinsertQuery.append("\"" + userType + "\" , ");
							activeLocationinsertQuery.append("\"" + status + "\" ,");
							activeLocationinsertQuery.append("\"" + userID + "\" ,");
							activeLocationinsertQuery.append("\"" + creationDate + "\" ,");
							activeLocationinsertQuery.append("\"" + timeZone + "\" ,");
							activeLocationinsertQuery.append("\"" + role + "\" ,");
							activeLocationinsertQuery.append("\"" + isDeleted + "\" ,");
							activeLocationinsertQuery.append("\"" + subUserType + "\" ,");
							activeLocationinsertQuery.append("\"" + lastModifiedDate + "\" ),");
						}

						if (activeLocationinsertQuery != null && activeLocationinsertQuery.lastIndexOf(",") > 0) {
							insertQueryActiveLocation = activeLocationinsertQuery.substring(0,
									activeLocationinsertQuery.length() - 1);
						}

						if (!"".equals(insertQueryActiveLocation)) {
							// System.out.println("client Name :
							// "+report.getClientName()+" query for insert
							// "+insertQueryActiveLocation);
							doExchange.getIn().setBody(insertQueryActiveLocation);
							Exception excpetion = template.send(endpoint, doExchange).getException();
							if (excpetion != null) {
								System.out.println("client Name :7777777777777 " + report.getClientName()
										+ " query for insert isFailed " + excpetion.getMessage());
								excpetion.printStackTrace();
							}
						}

					} // end of usersMap
					if (roleMap != null && roleMap.size() > 0) {
						StringBuilder activeSalesRole = new StringBuilder(
								"INSERT INTO SALES_ROLE (ROLE_ID,ROLE_NAME) VALUES");
						String insertQueryActiveSaleRole = "";
						for (String key : roleMap.keySet()) {
							HashMap<String, String> hmap = roleMap.get(key);
							Iterator it = hmap.entrySet().iterator();
							while (it.hasNext()) {
								Map.Entry pair = (Map.Entry) it.next();
								activeSalesRole.append("(\"" + pair.getKey() + "\" , ");
								activeSalesRole.append("\"" + pair.getValue() + "\" ),");
								it.remove(); // avoids a
												// ConcurrentModificationException
							}
						}
						if (activeSalesRole != null && activeSalesRole.lastIndexOf(",") > 0) {
							insertQueryActiveSaleRole = activeSalesRole.substring(0, activeSalesRole.length() - 1);
						}
						if (!"".equals(insertQueryActiveSaleRole)) {
							doExchange.getIn().setBody(insertQueryActiveSaleRole);
							Exception excpetion = template.send(endpoint, doExchange).getException();
							if (excpetion != null) {
								System.out.println("client Name :7777777777777 " + report.getClientName()
										+ " query for insert isFailed " + excpetion.getMessage());
								excpetion.printStackTrace();
							}
						}
					}
					if (muidLocationMap != null && muidLocationMap.size() > 0) {
						StringBuilder muidUser = new StringBuilder(
								"INSERT INTO MUID_USERS_THREAD_DATA (BUILD_NAME,CODE_VALUE,DB_NAME,DATE_FROM,DATE_TO,FRANCHISEE_NAME,OWNER_NAME,USER_NAME,EMAIL_ID,MUID_VALUE,USER_ID,FRANCHISE_OWNER_ID,FRANCHISEE_COUNT) VALUES");
						String insertMuidQuery = new String();
						for (String key : muidLocationMap.keySet()) {
							HashMap<String, String> hmap = muidLocationMap.get(key);
							String franchiseeName = hmap.get("FRANCHISEE_NAME");
							String ownerName = hmap.get("OWNER_NAME");
							String userName = hmap.get("OWNER_NAME");
							String muidValue = hmap.get("MUID_VALUE");
							String franchiseeCount = hmap.get("FRANCHISEE_COUNT");
							String emailId = hmap.get("FRAN_EMAIL_ID");
							String franchiseeOwnerID = hmap.get("FRANCHISE_OWNER_ID");
							String userId = hmap.get("USER_ID");
							muidUser.append("(\"" + report.getClientName() + "\" , ");
							muidUser.append("\"" + report.getCodeValue() + "\" , ");
							muidUser.append("\"" + report.getDbName() + "\" , ");
							muidUser.append("\"" + report.getDateFrom() + "\" , ");
							muidUser.append("\"" + report.getDateTo() + "\" , ");
							muidUser.append("\"" + franchiseeName + "\" , ");
							muidUser.append("\"" + ownerName + "\" , ");
							muidUser.append("\"" + userName + "\" , ");
							muidUser.append("\"" + emailId + "\" , ");
							muidUser.append("\"" + muidValue + "\" , ");
							muidUser.append("\"" + userId + "\" , ");
							muidUser.append("\"" + franchiseeOwnerID + "\" , ");
							muidUser.append("\"" + franchiseeCount + "\" ),");
						}
						if (muidUser != null && muidUser.lastIndexOf(",") > 0) {
							insertMuidQuery = muidUser.substring(0, muidUser.length() - 1);
						}
						if (!"".equals(insertMuidQuery)) {
							doExchange.getIn().setBody(insertMuidQuery);
							Exception excpetion = template.send(endpoint, doExchange).getException();
							if (excpetion != null) {
								System.out.println("client Name :7777777777777 " + report.getClientName()
										+ " MUID query for insert isFailed " + excpetion.getMessage());
								excpetion.printStackTrace();
							}
						}
					}
					/*
					 * Bright Star Billig Report
					 * 
					 * 
					 */
					if (brightStarCarReportMap != null && brightStarCarReportMap.size() > 0) {
						java.util.HashSet orphanSet = null;
						String insertorphanLocationQuery = null;
						orphanSet = (java.util.HashSet) brightStarCarReportMap.get("orphaonLocationNameSet");
						java.util.Map orphanLocationWiseUser = (java.util.Map) brightStarCarReportMap
								.get("orphanLocationWiseUser");
						StringBuilder locationQuery = new StringBuilder(
								"INSERT INTO BRIGHTSTAR_BILLING_USER_DATA (BUILD_NAME,LOCATION_TYPE,FRANCHISEE_NAME,USER_COUNT) VALUES");
						String locationNameTemp = "";
						int devidedCount = 0;
						int totalOrphan = 0;
						int orphanSetG1 = 0;
						if (orphanSet != null && !orphanSet.isEmpty()) {
							java.util.Iterator it = orphanSet.iterator();
							while (it.hasNext()) {
								locationNameTemp = (String) it.next();
								devidedCount = orphanLocationWiseUser.get(locationNameTemp) == null ? 0
										: ((Integer) orphanLocationWiseUser.get(locationNameTemp)).intValue();
								devidedCount = devidedCount == 0 ? 0
										: (devidedCount % 7 == 0 ? (devidedCount / 7) : (devidedCount / 7 + 1));
								if (devidedCount > 0) {
									orphanSetG1++;
								}
								totalOrphan += devidedCount;
								locationQuery.append("(\"" + report.getClientName() + "\" , ");
								locationQuery.append("\"" + "Orphan Location" + "\" , ");
								locationQuery.append("\"" + locationNameTemp + "\" , ");
								locationQuery.append("\"" + devidedCount + "\" ),");
							}
						}
						if (locationQuery != null && locationQuery.lastIndexOf(",") > 0) {
							insertorphanLocationQuery = locationQuery.substring(0, locationQuery.length() - 1);
						}
						if (!"".equals(insertorphanLocationQuery)) {
							doExchange.getIn().setBody(insertorphanLocationQuery);
							Exception excpetion = template.send(endpoint, doExchange).getException();
							if (excpetion != null) {
								System.out.println("client Name :7777777777777 " + report.getClientName()
										+ " Orphan Query for insert isFailed " + excpetion.getMessage());
								excpetion.printStackTrace();
							}
						}
						java.util.HashSet parentSet = null;
						parentSet = (java.util.HashSet) brightStarCarReportMap.get("parentLocationName");
						java.util.Map parentLocationWiseUser = (java.util.Map) brightStarCarReportMap
								.get("parentLocationWiseUser");
						locationQuery = new StringBuilder(
								"INSERT INTO BRIGHTSTAR_BILLING_USER_DATA (BUILD_NAME,LOCATION_TYPE,FRANCHISEE_NAME,USER_COUNT) VALUES");
						String locationName = "";
						int devided = 0;
						int totalParent = 0;
						int parentSetG1 = 0;

						if (parentSet != null && !parentSet.isEmpty()) {

							java.util.Iterator it = parentSet.iterator();
							while (it.hasNext()) {
								locationName = (String) it.next();

								devided = parentLocationWiseUser.get(locationName) == null ? 0
										: ((Integer) parentLocationWiseUser.get(locationName)).intValue();
								devided = devided == 0 ? 0 : (devided % 7 == 0 ? (devided / 7) : (devided / 7 + 1));
								if (devided > 0) {
									parentSetG1++;
								}
								totalParent += devided;
								locationQuery.append("(\"" + report.getClientName() + "\" , ");
								locationQuery.append("\"" + "Parent Location" + "\" , ");
								locationQuery.append("\"" + locationName + "\" , ");
								locationQuery.append("\"" + devided + "\" ),");
							}

						}

						if (locationQuery != null && locationQuery.lastIndexOf(",") > 0) {

							insertorphanLocationQuery = locationQuery.substring(0, locationQuery.length() - 1);
						}

						if (!"".equals(insertorphanLocationQuery)) {
							// System.out.println(insertMuidQuery);
							doExchange.getIn().setBody(insertorphanLocationQuery);
							Exception excpetion = template.send(endpoint, doExchange).getException();
							if (excpetion != null) {
								System.out.println("client Name :7777777777777 " + report.getClientName()
										+ " Parent  query for insert isFailed " + excpetion.getMessage());
								excpetion.printStackTrace();
							}
						}

						java.util.Set childSet = null;
						java.util.Map childParentFranchiseeNames = (java.util.HashMap) brightStarCarReportMap
								.get("childParentFranchiseeNames");

						java.util.Map childLocationWiseUser = (java.util.Map) brightStarCarReportMap
								.get("childLocationWiseUser");

						int totalChild = 0;
						int childSetG1 = 0;
						int counts = 0;
						String parentName = "";
						String locationName1 = "";
						locationQuery = new StringBuilder(
								"INSERT INTO BRIGHTSTAR_BILLING_USER_DATA (BUILD_NAME,LOCATION_TYPE,FRANCHISEE_NAME,USER_COUNT) VALUES");
						java.util.Iterator itr = childParentFranchiseeNames.keySet().iterator();
						//System.out.println("childParentFranchiseeNames SIZE" + childParentFranchiseeNames.size());
						//System.out.println("childLocationWiseUser SIZE" + childLocationWiseUser.size());
						//System.out.println("childParentFranchiseeNames***********" + childParentFranchiseeNames);
						//System.out.println("childLocationWiseUser***********" + childLocationWiseUser);
						while (itr.hasNext()) {

							parentName = (String) itr.next();
							if (parentName.trim() != "") {
								childSet = (java.util.Set) childParentFranchiseeNames.get(parentName);
								if (childSet != null && !childSet.isEmpty()) {

									java.util.Iterator it = childSet.iterator();
									while (it.hasNext()) {
										locationName1 = (String) it.next();
										counts = childLocationWiseUser.get(locationName1) == null ? 0
												: ((Integer) childLocationWiseUser.get(locationName1)).intValue();
										counts = counts == 0 ? 0 : (counts % 7 == 0 ? (counts / 7) : (counts / 7 + 1));
										if (counts > 0) {
											childSetG1++;
										}
										totalChild += counts;
										locationQuery.append("(\"" + report.getClientName() + "\" , ");
										locationQuery.append("\"" + "Child Location" + "\" , ");
										locationQuery.append("\"" + locationName1 + "\" , ");
										locationQuery.append("\"" + counts + "\" ),");

									}
								}
								if (locationQuery != null && locationQuery.lastIndexOf(",") > 0) {

									insertorphanLocationQuery = locationQuery.substring(0, locationQuery.length() - 1);
								}
							}
						}
						// System.out.println("insertorphanLocationQuery"+insertorphanLocationQuery);
						if (!"".equals(insertorphanLocationQuery)) {
							// System.out.println(insertMuidQuery);
							doExchange.getIn().setBody(insertorphanLocationQuery);
							Exception excpetion = template.send(endpoint, doExchange).getException();
							if (excpetion != null) {
								System.out.println("client Name :7777777777777 " + report.getClientName()
										+ " Child  query for insert isFailed " + excpetion.getMessage());
								excpetion.printStackTrace();
							}
						}

						locationQuery = new StringBuilder(
								"INSERT INTO BRIGHTSTAR_BILLING_DATA (BUILD_NAME,CODE_VALUE,ORPHAN_LOCATION,PARENT_LOCATION,CHILD_LOCATION,ORPHAN_LOCATION_USER_BATCH,PARENT_LOCATION_USER_BATCH,CHILD_LOCATION_USER_BATCH,ORPHAN_LOCATION_USER_COUNT,CHILD_LOCATION_USER_COUNT,PARENT_LOCATION_USER_COUNT) VALUES");
						locationQuery.append("(\"" + report.getClientName() + "\" , ");
						locationQuery.append("\"" + report.getCodeValue() + "\" , ");
						locationQuery.append("\"" + (String) brightStarCarReportMap.get("orphanLocations") + "\" , ");
						locationQuery.append("\"" + (String) brightStarCarReportMap.get("parentLocation") + "\" , ");
						locationQuery.append("\"" + (String) brightStarCarReportMap.get("childLocation") + "\" , ");
						locationQuery.append("\"" + (totalOrphan - orphanSetG1) + "\" , ");
						locationQuery.append("\"" + (totalParent - parentSetG1) + "\" , ");
						locationQuery.append("\"" + (totalChild - childSetG1) + "\" , ");
						locationQuery.append("\"" + totalOrphan + "\" , ");
						locationQuery.append("\"" + totalChild + "\" , ");
						locationQuery.append("\"" + totalParent + "\" ),");
						// System.out.println("locationQuery..........."+locationQuery);

						if (locationQuery != null && locationQuery.lastIndexOf(",") > 0) {

							insertorphanLocationQuery = locationQuery.substring(0, locationQuery.length() - 1);
						}

						// System.out.println("insertorphanLocationQuery"+insertorphanLocationQuery);
						if (!"".equals(insertorphanLocationQuery)) {
							// System.out.println(insertMuidQuery);
							doExchange.getIn().setBody(insertorphanLocationQuery);
							Exception excpetion = template.send(endpoint, doExchange).getException();
							if (excpetion != null) {
								System.out.println("client Name :7777777777777 " + report.getClientName()
										+ " Summary  query for insert isFailed " + excpetion.getMessage());
								excpetion.printStackTrace();
							}
						}

					} // end for the BrightStar Billing
					//Start of the lightBridge Billing
					
					if(lightBridgeMap !=null && lightBridgeMap.size()>0) {
						//System.out.println(lightBridgeMap);
						StringBuilder lightBridgeUser = new StringBuilder(
								"INSERT INTO LIGHTBRIDGE_BILLING_DATA (BUILD_NAME,CODE_VALUE,STORE_NAME,USER_TYPE,USER_ID,FIRST_NAME,LAST_NAME,EMAIL_ID,ROLE,IS_OPENER_ACCESS,IS_HUB_ACCESS) VALUES");
						String insertLightBridgrQuery = new String();
						for (String key : lightBridgeMap.keySet()) {
							HashMap<String, String> hmap = lightBridgeMap.get(key);
							String franchiseeName = hmap.get("FRANCHISEE_NAME");
							String userId = hmap.get("USER_ID");
							String userFirstName = hmap.get("FIRST_NAME");
							String userLastName = hmap.get("LAST_NAME");
							String emailId = hmap.get("EMAIL_ID");
							String userType = hmap.get("USER_TYPE");
							String role = hmap.get("ROLE");
							String isOpenerAccess = hmap.get("OPENER_ACCESS");
							String isHubAccess = hmap.get("HUB_ACCESS");
							if(isOpenerAccess==null){
								isOpenerAccess="";
							}
							if(isHubAccess==null){
								isHubAccess="";
							}
							

							lightBridgeUser.append("(\"" +report.getClientName() + "\" , ");
							lightBridgeUser.append("\"" + report.getCodeValue() + "\" , ");
							lightBridgeUser.append("\"" + franchiseeName + "\" , ");
							lightBridgeUser.append("\"" + userType + "\" , ");
							lightBridgeUser.append("\"" + userId + "\" , ");
							lightBridgeUser.append("\"" + userFirstName + "\" , ");
							lightBridgeUser.append("\"" + userLastName + "\" , ");
							lightBridgeUser.append("\"" + emailId + "\" , ");
							lightBridgeUser.append("\"" + role + "\" ,");
							lightBridgeUser.append("\"" + isOpenerAccess + "\" ,");
							lightBridgeUser.append("\"" + isHubAccess + "\" ),");

						}

						if (lightBridgeUser != null && lightBridgeUser.lastIndexOf(",") > 0) {
							insertLightBridgrQuery = lightBridgeUser.substring(0, lightBridgeUser.length() - 1);
						}
						//System.out.println(insertLightBridgrQuery);
						if (!"".equals(insertLightBridgrQuery)) {
							// System.out.println(insertMuidQuery);
							doExchange.getIn().setBody(insertLightBridgrQuery);
							Exception excpetion = template.send(endpoint, doExchange).getException();
							if (excpetion != null) {
								System.out.println("client Name :7777777777777 " + report.getClientName()
										+ " Light Bridge for insert isFailed " + excpetion.getMessage());
								excpetion.printStackTrace();
							}
						}
					}
					
					
				} // end of locationMap

			} else {
				System.out.println(" not inserted**************************88" + report.getClientName());
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			report = null;
		}
	}
}
