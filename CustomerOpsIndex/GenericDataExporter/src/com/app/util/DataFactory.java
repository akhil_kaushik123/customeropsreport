/*
 * EU Countries Data Fetch | FCSKYS-17758
 */
package com.app.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.camel.Endpoint;
import org.apache.camel.Exchange;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.support.SynchronizationAdapter;
import org.apache.commons.beanutils.DynaBean;

import com.app.base.ClientInfo;
import com.app.base.Constants;
import com.app.base.ModuleInfo;
import com.app.helper.DBConnectionProvider;

public class DataFactory
{
	
	private static final Future<Exchange> Exchange = new Future<Exchange>() {
		@Override
		public boolean isDone() {
			return false;
		}
		@Override
		public boolean isCancelled() {
			return false;
		}
		@Override
		public Exchange get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
			return null;
		}
		@Override
		public Exchange get() throws InterruptedException, ExecutionException {
			return null;
		}
		@Override
		public boolean cancel(boolean mayInterruptIfRunning) {
			return false;
		}
	};
	public void prepareData(HashMap<String,ClientInfo> clients, HashMap<String, String> clientModulesMap,HashMap<String,ModuleInfo> moduleQueriesMap)
	{	
		List<String> clientsNotMatched  = new ArrayList<String>();
		Iterator<String> clientIt = clients.keySet().iterator();
		while(clientIt.hasNext())
		{	
			String clientName = clientIt.next();
			ClientInfo dbProps = clients.get(clientName);
			String codeValue = dbProps.getCodeValue();
			String modules = null;
			
			if(clientModulesMap.containsKey(codeValue) || "'CD','HD'".equals(Constants.exportType))
			{
				modules = clientModulesMap.get(codeValue);
				Endpoint connection = DBConnectionProvider.getInstance().getDBConnection(clientName);
				Exchange exchange = connection.createExchange();
				ProducerTemplate template = exchange.getContext().createProducerTemplate();
				//template.
				//connection.co
				Future<Exchange> future=template.asyncCallback(connection, new DataBeanProducer(clientName,codeValue,modules,moduleQueriesMap) , new SynchronizationAdapter() 
	        	{
					@Override
	                public void onDone(Exchange exchange) {
						//System.out.println("does flow got here");
	                	DataBeanProcessor.processBean(exchange);
	                }
	            });
				
				try {
					System.out.println("future object::::::"+future.get());
				} catch (InterruptedException | ExecutionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				//System.out.println("does flow got here again asking as not sure");
			}
			else
			{	
				clientsNotMatched.add(clientName);
				Constants.clCVNotMatched.append(clientName + " ,");
			}
		}
		System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! CodeValue not matched for follwoing clients :  "+clientsNotMatched +"   ######\n");
	}
	
	public void prepareData(HashMap<String,ClientInfo> clients)
	{
		SystemUsageUtility.dateFrom = Constants.dateFrom;
		SystemUsageUtility.dateTo = Constants.dateTo;
		/*if("SU".equals(type.getType()))
		{
			ExecutorHelper.removeOldData(dateFrom);
		}*/
		
		Iterator<String> clientIt = clients.keySet().iterator();
		while(clientIt.hasNext())
		{	
			//System.out.println(" inside while");
			String clientName = clientIt.next();
			ClientInfo dbProps = clients.get(clientName);
			
			Endpoint connection = DBConnectionProvider.getInstance().getDBConnection(clientName);
			Exchange exchange = connection.createExchange();
			ProducerTemplate template = exchange.getContext().createProducerTemplate();
			template.asyncCallback(connection, new SystemUsageDataProducer(clientName,dbProps) , new SynchronizationAdapter() 
        	{
				@Override
                public void onDone(Exchange exchange) {
                	ReportBeanProcessor.processBean(exchange);
                }
            });
			
		}
		
		String specialClients[] = {"CartridgeWorld","Allegra","Signs By Tomorrow","SignsNow","hogiyogi71072","HouseMaster","Spring-Green","MosquitoJoe93","Newks","NTY90","ChildrenOrchard90","DevicePitStop90","ClothesMentor90","NewUses90","Caringtransitions","HomeHelpers92","OutdoorLightingPerspectives","MosquitoSquad","Archadeck","RenewCrew","laboiteapizza","EstheticCenter","mythicBurger"};
		
		System.out.println(" special :"+specialClients.length);
    	for(int k= 0;k<specialClients.length;k++)
    	{
    		
    		String cName = specialClients[k];
    		if(clients.containsKey(cName))
    		{
    			System.out.println(" special ::::::::::::::::::::::::::::::: 	");
	    		Endpoint endpoint = DBConnectionProvider.getInstance().getDBConnection(cName);
	        	Exchange exchange = endpoint.createExchange();
	        	ProducerTemplate template = exchange.getContext().createProducerTemplate();
	        	HashMap<String,Object> results = SystemUsageUtility.getUserNames(template, endpoint, exchange, cName);
	        	Endpoint connection = DBConnectionProvider.getInstance().getDBConnection("InHouse");
	        	boolean isFailed = (boolean)results.get("IS_FAILED");
	    		if(!isFailed)
	    		{
	    			Set<String> columns = (Set)results.get("COLUMN_NAMES");
	    			ArrayList<DynaBean> dataBeans = (ArrayList<DynaBean>) results.get("DATA");
	    	    	if(dataBeans != null && columns !=null)
	    	    	{	
	    	    		Iterator<DynaBean> dataItr = dataBeans.iterator();
	    		    	while(dataItr.hasNext())
	    		    	{	
	    			    	DynaBean bean = dataItr.next();
	    			    	QueryUtil.executeInsert(connection,"CLIENT_SPECIAL_BILLING_DATA",columns,bean);
	    		    	}
	    	    	}
	    		}
    		}
        }
    	
    	if(clients.containsKey("TitleBoxingClub"))
    	{
	    	Endpoint endpoint = DBConnectionProvider.getInstance().getDBConnection("TitleBoxingClub");
	    	Exchange exchange = endpoint.createExchange();
	        ProducerTemplate template = exchange.getContext().createProducerTemplate();
	    	HashMap<String,Object> results = SystemUsageUtility.getTBCLifeCycleRepoData(endpoint);
	    	Endpoint connection = DBConnectionProvider.getInstance().getDBConnection("InHouse");
	    	boolean isFailed = (boolean)results.get("IS_FAILED");
			if(!isFailed)
			{
				Set<String> columns = (Set)results.get("COLUMN_NAMES");
				ArrayList<DynaBean> dataBeans = (ArrayList<DynaBean>) results.get("DATA");
		    	if(dataBeans != null && columns !=null)
		    	{	
		    		Iterator<DynaBean> dataItr = dataBeans.iterator();
			    	while(dataItr.hasNext())
			    	{	
				    	DynaBean bean = dataItr.next();
				    	QueryUtil.executeInsert(connection,"TBC_LIFECYCLE_REPORT_DATA",columns,bean);
			    	}
		    	}
			}
    	}
	}
	
	/*User Billing New Changes Start
	 * 
	 */
	
	public void prepareDataUserBilling(HashMap<String,ClientInfo> clients) throws InterruptedException, ExecutionException, TimeoutException
	{
		UserBillingSystemUsageUtility.dateFrom = Constants.dateFrom;
		UserBillingSystemUsageUtility.dateTo = Constants.dateTo;
		
		Iterator<String> clientIt = clients.keySet().iterator();
		Future<Exchange> future = Exchange;
		while(clientIt.hasNext())
		{	
			//System.out.println(" inside while");
			String clientName = clientIt.next();
			ClientInfo dbProps = clients.get(clientName);
			
			Endpoint connection = DBConnectionProvider.getInstance().getDBConnection(clientName);
			System.out.println("connection..........."+connection);
			Exchange exchange = connection.createExchange();
			ProducerTemplate template = exchange.getContext().createProducerTemplate();
			synchronized (clientName) {
				 future  = template.asyncCallback(connection, new SystemUsageUserBillingDataProducer(clientName,dbProps) , new SynchronizationAdapter() 
        	{
				@Override
                public void onDone(Exchange exchange) {
                	UserBillingReportBeanProcessor.processBean(exchange);
                }
            });
			System.out.println(future.toString());
			System.out.println(future.get());	
			
			}
		}
		System.out.println("Current Threead Future has been done "+future.isDone());
	}
	
	public void prepareDataEUUser(HashMap<String,ClientInfo> clients)
	{
		UserBillingSystemUsageUtility.dateFrom = Constants.dateFrom;
		UserBillingSystemUsageUtility.dateTo = Constants.dateTo;
		Iterator<String> clientIt = clients.keySet().iterator();
		System.out.println(clientIt);
		while(clientIt.hasNext())
		{	
			String clientName = clientIt.next();
			ClientInfo dbProps = clients.get(clientName);
			Endpoint connection = DBConnectionProvider.getInstance().getDBConnection(clientName);
			Exchange exchange = connection.createExchange();
			ProducerTemplate template = exchange.getContext().createProducerTemplate();
			template.asyncCallback(connection, new EUDataProducer(clientName,dbProps) , new SynchronizationAdapter() 
        	{
				@Override
                public void onDone(Exchange exchange) {
                	UserBillingReportBeanProcessor.euProcessBean(exchange);
                }
            });
	   }
	}
	/*User Billing New Changes end
	 * 
	 */
	
	  public void preparePGData(HashMap<String, ClientInfo> clients, HashMap<String, String> clientModulesMap, HashMap<String, ModuleInfo> moduleQueriesMap) {
		    Iterator<String> clientIt = clients.keySet().iterator();
		    System.out.println("clients::::::::::"+clients);
		    while (clientIt.hasNext()) {
		      String clientName = clientIt.next();
		      ClientInfo dbProps = clients.get(clientName);
		      String codeValue = dbProps.getCodeValue();
		      String dbName = dbProps.getDbName();
		      String tomcatServer=dbProps.getTomcatServer();
		      String modules = null;
		      modules = clientModulesMap.get(codeValue);
		      Endpoint connection = DBConnectionProvider.getInstance().getDBConnection("PostGres");
		      Exchange exchange = connection.createExchange();
		      ProducerTemplate template = exchange.getContext().createProducerTemplate();
		      Future<Exchange> future=template.asyncCallback(connection, new PGDataBeanProducer(clientName, codeValue, moduleQueriesMap, dbName, tomcatServer), new SynchronizationAdapter() {
		            public void onDone(Exchange exchange) {
		              PGDataBeanProcessor.processBean(exchange);
		            }
		          });
				try {
					System.out.println("future object::::::"+future.get());
				} catch (InterruptedException | ExecutionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		      
		      
		    } 
		  }
	
}