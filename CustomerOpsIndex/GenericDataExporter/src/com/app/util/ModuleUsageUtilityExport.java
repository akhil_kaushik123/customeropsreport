
package com.app.util;

import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import jxl.write.Number;
import jxl.write.WritableSheet;
import org.apache.camel.ProducerTemplate;
import org.apache.commons.beanutils.DynaBean;
import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Endpoint;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import com.app.base.ClientInfo;
import com.app.base.Constants;
import com.app.dataProcessor.DataProcessor;

import jxl.write.WritableCell;
import jxl.format.CellFormat;
import jxl.write.Label;
import jxl.format.Alignment;
import jxl.biff.DisplayFormat;
import jxl.write.NumberFormat;
import jxl.write.BorderLineStyle;
import jxl.write.Border;
import jxl.write.VerticalAlignment;
import jxl.write.WritableCellFormat;
import jxl.write.Colour;
import jxl.write.WritableFont;
import jxl.write.WritableHyperlink;
import jxl.Workbook;
import jxl.WorkbookSettings;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.lang.reflect.InvocationTargetException;
import java.text.DecimalFormat;

import com.app.helper.DBConnectionProvider;
import com.enterprisedt.net.ftp.FileTransferClient;

public class ModuleUsageUtilityExport
{
    public static String dateFrom;
    public static String dateTo;
    
    public static void genrateMUExportFile(String exportFilepath) {
        Endpoint connection = DBConnectionProvider.getInstance().getDBConnection("InHouse");
        Exchange exchange = connection.createExchange();
        DecimalFormat df = new DecimalFormat("#");
        df.setMaximumFractionDigits(10);
        String query = "";
        CamelContext context = exchange.getContext();
        Endpoint endpoint = context.getEndpoint("direct:InHouse");
        ProducerTemplate template = context.createProducerTemplate();
        exchange = endpoint.createExchange();
        System.out.println("======================================" + exchange.toString());
        Exchange Queryout = null;
        List<Map<String, Object>> Queryresult = null;
        int i = 0, sheetCount=1;
        String fileName = exportFilepath;
        int rowMonthCal = 0;
        int columnMonthCal = 0;
        int rowClientCal = 0;
        int colClientCal=0;
        WritableSheet sheetMonthCal = null;
        WritableSheet clientSheet = null;
        WritableFont headerFont = null;
        WritableCellFormat headerFormat = null;
        WritableCellFormat subHeaderFormat = null;
        WritableCellFormat columnHeaderFormat = null;
        WritableFont textFont = null;
        WritableCellFormat textFormat = null;
        WritableCellFormat cellFormat = null;
        WritableCellFormat numberFormat = null;
        WritableCellFormat stringFormat = null;
        NumberFormat decimalNo = null;
        Label label = null;
        Number numberCell = null;
        String codeValue = "";
        WritableWorkbook workbook = null;
        try {
            File file = new File(fileName);
            if (file.exists()) {
                file.delete();
            }
            WorkbookSettings wbSettings = new WorkbookSettings();
            wbSettings.setRationalization(false);
            workbook = Workbook.createWorkbook(file, wbSettings);
            sheetMonthCal = workbook.createSheet("Operations Index", 0);
        }
        catch (Exception e) {
            System.out.println("=Exception Found in reading file====================>" + fileName + "\n" + stackTraceToString(e));
            e.printStackTrace();
        }
        rowMonthCal = 0;
        try {
            headerFont = new WritableFont(WritableFont.TAHOMA, 10, WritableFont.BOLD);
            headerFont.setColour(Colour.BLACK);
            headerFormat = new WritableCellFormat(headerFont);
            headerFormat.setBackground(Colour.GREY_40_PERCENT);
            headerFormat.setWrap(true);
            headerFormat.setVerticalAlignment(VerticalAlignment.TOP);
            headerFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
            decimalNo = new NumberFormat("###,###.00");
            numberFormat = new WritableCellFormat(decimalNo);
            stringFormat = new WritableCellFormat();
            stringFormat.setAlignment(Alignment.RIGHT);
            textFont = new WritableFont(WritableFont.TAHOMA, 10);
            textFormat = new WritableCellFormat(textFont);
            textFormat.setWrap(true);
            WritableCellFormat cellFormatEmail = new WritableCellFormat();
            cellFormatEmail.setWrap(true);
            cellFormat = new WritableCellFormat(headerFont);
            cellFormat.setBackground(Colour.GREY_40_PERCENT);
            cellFormat.setAlignment(jxl.write.Alignment.CENTRE);
            cellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
            cellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
            cellFormat.setWrap(true);
            
            subHeaderFormat = new WritableCellFormat(headerFont);
            subHeaderFormat.setBackground(Colour.GREY_25_PERCENT);
            subHeaderFormat.setWrap(true);
            subHeaderFormat.setVerticalAlignment(VerticalAlignment.TOP);
            subHeaderFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
            
            columnHeaderFormat = new WritableCellFormat(headerFont);
            columnHeaderFormat.setBackground(Colour.LIGHT_TURQUOISE);
            columnHeaderFormat.setWrap(true);
            columnHeaderFormat.setVerticalAlignment(VerticalAlignment.TOP);
            columnHeaderFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
            
            int heightInPoints = 780;
            int heightInPointsSubHeader=500;
            int n = 155;
            if(Constants.version!=null && !"Legacy".equals(Constants.version))
            	n=163;
            if("WUEXPORT".equals(Constants.exportType)) {
            	n=159;
            	if(Constants.version!=null && !"Legacy".equals(Constants.version))
            		n=167;
            }
            for (int j = 0; j < n; ++j) {
                sheetMonthCal.setColumnView(j, 29);
            }
            sheetMonthCal.setRowView(rowMonthCal, heightInPoints);
            if (rowMonthCal == 0) {
                sheetMonthCal.mergeCells(0, rowMonthCal, n, rowMonthCal);
                label = new Label(0, 0, "Usage By Module", headerFormat);
                sheetMonthCal.addCell(label);
                ++rowMonthCal;
            }
            if (rowMonthCal == 1) {
                columnMonthCal = 0;
                label = new Label(columnMonthCal, 1, "", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, 1, "", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                
                if(Constants.version!=null && !"Legacy".equals(Constants.version))
                {
                	sheetMonthCal.mergeCells(2, rowMonthCal, 23, rowMonthCal); // Modules
                    sheetMonthCal.mergeCells(24, rowMonthCal, 39, rowMonthCal);// The Hub
                    sheetMonthCal.mergeCells(40, rowMonthCal, 52, rowMonthCal); // Sales
                    sheetMonthCal.mergeCells(53, rowMonthCal, 56, rowMonthCal); // Opener 
                    sheetMonthCal.mergeCells(57, rowMonthCal, 72, rowMonthCal); // Info Mgr 
                    sheetMonthCal.mergeCells(73, rowMonthCal, 79, rowMonthCal); // Support 
                    sheetMonthCal.mergeCells(80, rowMonthCal, 88, rowMonthCal); // Training  
                    sheetMonthCal.mergeCells(89, rowMonthCal, 107, rowMonthCal); // CRM 
                    sheetMonthCal.mergeCells(108, rowMonthCal, 112, rowMonthCal); // Ads 
                    sheetMonthCal.mergeCells(113, rowMonthCal, 121, rowMonthCal); // Field Ops 
                    sheetMonthCal.mergeCells(122, rowMonthCal, 124, rowMonthCal); // Sites 
                    /***********/
                    sheetMonthCal.mergeCells(125, rowMonthCal, 130, rowMonthCal); // Shop 
                    sheetMonthCal.mergeCells(131, rowMonthCal, 132, rowMonthCal); // Listing 
                    sheetMonthCal.mergeCells(133, rowMonthCal, 137, rowMonthCal); // Reputation
                    sheetMonthCal.mergeCells(138, rowMonthCal, 147, rowMonthCal); // Finance
                    //sheetMonthCal.mergeCells(132, rowMonthCal, 121, rowMonthCal);
                    if("WUEXPORT".equals(Constants.exportType)) {
                        sheetMonthCal.mergeCells(148, rowMonthCal, 167, rowMonthCal); // FranConnect Admin
                    }else {
                        sheetMonthCal.mergeCells(148, rowMonthCal, 163, rowMonthCal); // FranConnect Admin               	
                    }
                }
                else
                {
                sheetMonthCal.mergeCells(2, rowMonthCal, 15, rowMonthCal); // Modules
                sheetMonthCal.mergeCells(16, rowMonthCal, 31, rowMonthCal);// The Hub
                sheetMonthCal.mergeCells(32, rowMonthCal, 44, rowMonthCal); // Sales
                sheetMonthCal.mergeCells(45, rowMonthCal, 48, rowMonthCal); // Opener 
                sheetMonthCal.mergeCells(49, rowMonthCal, 64, rowMonthCal); // Info Mgr 
                sheetMonthCal.mergeCells(65, rowMonthCal, 71, rowMonthCal); // Support 
                sheetMonthCal.mergeCells(72, rowMonthCal, 80, rowMonthCal); // Training  
                sheetMonthCal.mergeCells(81, rowMonthCal, 99, rowMonthCal); // CRM 
                sheetMonthCal.mergeCells(100, rowMonthCal, 104, rowMonthCal); // Ads 
                sheetMonthCal.mergeCells(105, rowMonthCal, 113, rowMonthCal); // Field Ops 
                sheetMonthCal.mergeCells(114, rowMonthCal, 116, rowMonthCal); // Sites 
                /***********/
                sheetMonthCal.mergeCells(117, rowMonthCal, 122, rowMonthCal); // Shop 
                sheetMonthCal.mergeCells(123, rowMonthCal, 124, rowMonthCal); // Listing 
                sheetMonthCal.mergeCells(125, rowMonthCal, 129, rowMonthCal); // Reputation
                //sheetMonthCal.mergeCells(119, rowMonthCal, 122, rowMonthCal);
                sheetMonthCal.mergeCells(130, rowMonthCal, 139, rowMonthCal); // Finance
                //sheetMonthCal.mergeCells(132, rowMonthCal, 121, rowMonthCal);
                if("WUEXPORT".equals(Constants.exportType)) {
                    sheetMonthCal.mergeCells(140, rowMonthCal, 159, rowMonthCal); // FranConnect Admin
                }else {
                    sheetMonthCal.mergeCells(140, rowMonthCal, 155, rowMonthCal); // FranConnect Admin               	
                }
                }
                
                if(Constants.version!=null && !"Legacy".equals(Constants.version))
                {
                    label = new Label(2, 1, "Modules", headerFormat);
                    sheetMonthCal.addCell(label);
                    label = new Label(24, 1, "The Hub", headerFormat);
                    sheetMonthCal.addCell(label);
                    label = new Label(40, 1, "Sales", headerFormat);
                    sheetMonthCal.addCell(label);
                    label = new Label(53, 1, "Opener", headerFormat);
                    sheetMonthCal.addCell(label);
                    label = new Label(57, 1, "Information Manager", headerFormat);
                    sheetMonthCal.addCell(label);
                    label = new Label(73, 1, "Support", headerFormat);
                    sheetMonthCal.addCell(label);
                    label = new Label(80, 1, "Training", headerFormat);
                    sheetMonthCal.addCell(label);
                    label = new Label(89, 1, "CRM", headerFormat);
                    sheetMonthCal.addCell(label);
                    label = new Label(108, 1, "Ads", headerFormat);
                    sheetMonthCal.addCell(label);
                    label = new Label(113, 1, "Field Ops", headerFormat);
                    sheetMonthCal.addCell(label);
                    label = new Label(122, 1, "Sites", headerFormat);
                    sheetMonthCal.addCell(label);
                    label = new Label(125, 1, "Shop", headerFormat);
                    sheetMonthCal.addCell(label);
                    label = new Label(131, 1, "Listings", headerFormat);
                    sheetMonthCal.addCell(label);
                    label = new Label(133, 1, "Reputation", headerFormat);
                    sheetMonthCal.addCell(label);
//                    label = new Label(119, 1, "CRM Campaigns", headerFormat);
//                    sheetMonthCal.addCell(label);
                    label = new Label(138, 1, "Finance", headerFormat);
                    sheetMonthCal.addCell(label);
                    label = new Label(148, 1, "FranConnect Admin", headerFormat);
                    sheetMonthCal.addCell(label);
                }else
                {
                    label = new Label(2, 1, "Modules", headerFormat);
                    sheetMonthCal.addCell(label);
                    label = new Label(16, 1, "The Hub", headerFormat);
                    sheetMonthCal.addCell(label);
                    label = new Label(32, 1, "Sales", headerFormat);
                    sheetMonthCal.addCell(label);
                    label = new Label(45, 1, "Opener", headerFormat);
                    sheetMonthCal.addCell(label);
                    label = new Label(49, 1, "Information Manager", headerFormat);
                    sheetMonthCal.addCell(label);
                    label = new Label(65, 1, "Support", headerFormat);
                    sheetMonthCal.addCell(label);
                    label = new Label(72, 1, "Training", headerFormat);
                    sheetMonthCal.addCell(label);
                    label = new Label(81, 1, "CRM", headerFormat);
                    sheetMonthCal.addCell(label);
                    label = new Label(100, 1, "Ads", headerFormat);
                    sheetMonthCal.addCell(label);
                    label = new Label(105, 1, "Field Ops", headerFormat);
                    sheetMonthCal.addCell(label);
                    label = new Label(114, 1, "Sites", headerFormat);
                    sheetMonthCal.addCell(label);
                    label = new Label(117, 1, "Shop", headerFormat);
                    sheetMonthCal.addCell(label);
                    label = new Label(123, 1, "Listings", headerFormat);
                    sheetMonthCal.addCell(label);
                    label = new Label(125, 1, "Reputation", headerFormat);
                    sheetMonthCal.addCell(label);
//                    label = new Label(119, 1, "CRM Campaigns", headerFormat);
//                    sheetMonthCal.addCell(label);
                    label = new Label(130, 1, "Finance", headerFormat);
                    sheetMonthCal.addCell(label);
                    label = new Label(140, 1, "FranConnect Admin", headerFormat);
                    sheetMonthCal.addCell(label);
                }
            }
            if (++rowMonthCal == 2) {
            	columnMonthCal = 0;
            	sheetMonthCal.setRowView(rowMonthCal, heightInPoints);
                label = new Label(columnMonthCal, rowMonthCal, "Customer Name", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Build Name", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                //insert if
                if(Constants.version!=null && !"Legacy".equals(Constants.version))
                {
                label = new Label(columnMonthCal, rowMonthCal, "Command Center", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                }// end if
                label = new Label(columnMonthCal, rowMonthCal, "Hub", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Sales", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Opener", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Info Mgr", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Support", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Training", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "CRM", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "AdBuilder", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Field Ops", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Local Websites", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Shop", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Local Listing", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Reputation Management", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Finance", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                
                // insert if
                if(Constants.version!=null && !"Legacy".equals(Constants.version)) {
                label = new Label(columnMonthCal, rowMonthCal, "Marketing", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Landing Pages", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Planner", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Online Ads", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Surveys", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Social", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Smartconnect", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                }//end if
                
                label = new Label(columnMonthCal, rowMonthCal, "Library Downloads", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Messages & Alerts", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "News Stories Posted", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "FranBuzz Posts", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Calendar Items Posted", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Polls Created", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Active Library Documents", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Active Library Documents Monthly Average", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Top Story", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Top Story Monthly Average", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Number Of Polls", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Number Of Polls Monthly Average", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Links", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "SSO Tabs", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "FranBuzz Comments", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "FranBuzz Comments Monthly Average", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "New Leads Added", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Emails Sent", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Calls Logged", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Tasks Created", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "FDDs Sent", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Workflows Active", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Leads Closed", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Average Time to Close Leads (days)", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Campaigns Created", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Tabs Added", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Sections Added", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Fields Added", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Campaign Emails Sent", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Locations Added", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Tasks Completed", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Locations Opened", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Tasks Overdue", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Emails Sent", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Messages sent", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Calls Logged", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Tasks Created", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "FDD Sent", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Renewals Added", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Campaigns Created", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Email Templates Created", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Workflows Active", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Custom Reports Added", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Tabs Added", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Sections Added", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Fields Added", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Campaign Emails Sent", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Location Emails Sent", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Location Emails Sent Monthly Average", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Tickets Created", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Tickets Closed", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Average Time to Close Tickets(Days)", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Median Time to Close Tickets(Days)", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Maximum Time to Close Tickets(Days)", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Minimum Time to Close Tickets(Days)", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "FAQ Exists", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "New Course Added", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "New Course Added Monthly Average", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Plans Created", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Plans Completed", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Certificates Awarded", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Courses In Progress", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Courses Completed", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Quizzes Taken", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Passed Percentage (%)", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Contacts Added", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "New Leads Added", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Customer Added", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Opportunities Added", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Transactions Added", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Accounts Added", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Emails Sent", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Calls Logged", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Workflows Active", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Tasks Created", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Leads Closed", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Custom Report Created", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Tabs Added", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Sections Added", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Fields Added", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Campaigns Created", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Campaign Emails Sent", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Campaign Emails Failed", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Templates Added", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;                
                label = new Label(columnMonthCal, rowMonthCal, "No. of Ads Saved", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "No. of Carts Saved", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Ads Uploaded By Corporate", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "No. of Ads Emailed", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "No. of Ads Downloaded", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Visits Created", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Visits Completed", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Tasks Created", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "New Forms Added", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Average Compliance Score", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Minimum Compliance Score", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Maximum Compliance Score", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Median Compliance Score", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Non Compliant Franchisees", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Sites Added", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Changes Published", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Average Updates By Franchisees", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "New Orders", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Order Total", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "No. of Purchase Orders", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "No. of Products Sold", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "% of Successful Orders", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Top Selling Products", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Incomplete Listings", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "% of Locations Activated for Listings", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "% of Locations Activated for Reputation ", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Average Reputation Score", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Maximum Reputation Score", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Minimum Reputation Score", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Median Reputation Score", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "No. of Submitted Sales", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Total Royalty generated", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Total Royalty collected", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "No. of locations generating Royalties", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Total Revenue", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "No. of locations generating Revenue", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Average Revenue by locations", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Average Royalty By Locations", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Total System Profitability", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Average Profitability by Location", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Active Locations", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "InDevelopment Locations", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Corporate Locations", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Terminated Locations", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Unique CAL's (Locations having atleast one User)", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Active Corporate Users", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Active Brand Users", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Active Regional Users", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Active Franchisee Users", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Logged In Corporate Users", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Logged In Brand Users", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Brand Names", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Logged In Regional Users", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Logged In Franchisee Users", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                if("WUEXPORT".equals(Constants.exportType)) {
                    label = new Label(columnMonthCal, rowMonthCal, "Total System Time(In Seconds)", headerFormat);
                    sheetMonthCal.addCell(label);
                    ++columnMonthCal;
                    label = new Label(columnMonthCal, rowMonthCal, "# of Logged in Corp/Brand/Regional Users vs Active Corp/Brand/Regional Users", headerFormat);
                    sheetMonthCal.addCell(label);
                    ++columnMonthCal;
                    label = new Label(columnMonthCal, rowMonthCal, "# of Logged in Franchisee Users vs. Active Franchisee Users", headerFormat);
                    sheetMonthCal.addCell(label);
                    ++columnMonthCal;
                    label = new Label(columnMonthCal, rowMonthCal, "Total amount of time logged in for Corp/Brand/Regional Users(In Seconds)", headerFormat);
                    sheetMonthCal.addCell(label);
                    ++columnMonthCal;
                    label = new Label(columnMonthCal, rowMonthCal, "Total amount of time logged in for Franchisee Users(In Seconds)", headerFormat);
                    sheetMonthCal.addCell(label);
                    ++columnMonthCal;
                }else {
                    label = new Label(columnMonthCal, rowMonthCal, "Total System Time", headerFormat);
                    sheetMonthCal.addCell(label);
                    ++columnMonthCal;
                }
                label = new Label(columnMonthCal, rowMonthCal, "Adoption Rate", headerFormat);
                sheetMonthCal.addCell(label);
            }
            ++rowMonthCal;
            StringBuffer moduleUsageQuery = new StringBuffer(QueryBean.getCustomerOpsSql(Constants.dateTo, Constants.dateFrom));
            System.out.println("==============================" + (Object)moduleUsageQuery);
            exchange.getIn().setBody(moduleUsageQuery.toString());
            Queryout = template.send(endpoint, exchange);
            Queryresult = Queryout.getOut().getBody(List.class);
            i = Queryresult.size();
            WritableHyperlink hl;
            final int clientSheetRowOffset=5, clientSheetDataWriteRowOffset=8; // row offset for client sheet
            final int trainingDataOffset=0,libDataOffset = 5, libLinkPollDataOffset=9,libLinkDataOffset=9,libPollDataOffset=10, finDataOffset = 12, finFinanceDataOffset=12,
            		finNonFinanceDataOffset=15, finPLDataOffset=18, finLocDataOffset=21, crmDataOffset=27, crmLocBasedDataOffset=27,
            		fieldOpsDataOffset=36, fieldOpsLocDataOffset=36,
            		fimDataOffset=41, fimLocDataOffset = 41, fimCampaignDataOffset=46; // column offset for client sheet according to modules
            ByteArrayInputStream bais;
            ObjectInputStream ins;
            ArrayList<HashMap<String,Object>> beanHM;
            for (int k = 0; k < i; ++k) {
                Map<String, Object> row1 = Queryresult.get(k);
                /* client sheet write start */
                rowClientCal=0;
                colClientCal=0;
                
				clientSheet = workbook.createSheet((String)row1.get("CLIENT_NAME"), sheetCount);
				for (int j = 0; j <= 100; ++j) {
					clientSheet.setColumnView(j, 25);
				}
				sheetCount++; //increment sheet for next client
				
				label=new Label(colClientCal, rowClientCal, "Customer Name", headerFormat);
				clientSheet.addCell(label);
				colClientCal++;
				label=new Label(colClientCal, rowClientCal, (String)row1.get("CLIENT_NAME"), textFormat);
				clientSheet.addCell(label);
				
				colClientCal=0;
				rowClientCal++;
				label=new Label(colClientCal, rowClientCal, "Build Name", headerFormat);
				clientSheet.addCell(label);
				colClientCal++;
				label=new Label(colClientCal, rowClientCal, (String)row1.get("CODE_VALUE"), textFormat);
				clientSheet.addCell(label);	
				
				colClientCal++;
				label=new Label(colClientCal, rowClientCal, "<- Main Sheet", textFormat);
				clientSheet.addCell(label);	
				hl = new WritableHyperlink(colClientCal, rowClientCal,"<- Main Sheet", sheetMonthCal, 0, 0);
				clientSheet.addHyperlink(hl);
				colClientCal=0;
				rowClientCal++;
				label=new Label(colClientCal, rowClientCal, "Modules", headerFormat);
				clientSheet.addCell(label);
				colClientCal++;

				List<String> moduleList=new ArrayList<String>();
				
				if(Constants.version!=null && !Constants.version.equals("Legacy"))
				{
					if(row1.get("CC_EXISTS").equals("Yes"))
						moduleList.add("Command Center");					
					
				}

				if(row1.get("INTRANET_EXISTS").equals("Yes"))
				moduleList.add("Hub");
				if(row1.get("FS_EXISTS").equals("Yes"))
				moduleList.add("Sales");
				if(row1.get("SM_EXISTS").equals("Yes"))
				moduleList.add("Opener");
				if(row1.get("FIM_EXISTS").equals("Yes"))
				moduleList.add("Info Mgr");
				if(Constants.version!=null && !Constants.version.equals("Legacy"))
				{


						if(row1.get("SMARTCONNECT_EXISTS").equals("Yes"))
						moduleList.add("Smartconnect");

					
					
				}
				if(row1.get("SUPPORT_EXISTS").equals("Yes"))
				moduleList.add("Support");
				if(row1.get("TRAINING_EXISTS").equals("Yes"))
				moduleList.add("Training");
				if(row1.get("CM_EXISTS").equals("Yes"))
				moduleList.add("CRM");
				if(row1.get("ADMAKER_EXISTS").equals("Yes"))
				moduleList.add("AdBuilder");
				if(row1.get("FIELDOPS_EXISTS").equals("Yes"))
				moduleList.add("Field Ops");
				
				if(row1.get("WB_EXISTS").equals("Yes"))
				moduleList.add("Local Websites");
				if(row1.get("SUPPLIES_EXISTS").equals("Yes"))
				moduleList.add("Shop");
				if(row1.get("LL_EXISTS").equals("Yes"))
				moduleList.add("Local Listing");
				if(row1.get("RM_EXISTS").equals("Yes"))
				moduleList.add("Reputation Management");
				if(row1.get("FINANCIAL_EXISTS").equals("Yes"))
				moduleList.add("Finance");
				
				if(Constants.version!=null && !Constants.version.equals("Legacy"))
				{

						if(row1.get("MARKETING_EXISTS").equals("Yes"))
						moduleList.add("Marketing");
						if(row1.get("LP_EXISTS").equals("Yes"))
						moduleList.add("Landing Pages");
						if(row1.get("PLANNER_EXISTS").equals("Yes"))
						moduleList.add("Planner");
						if(row1.get("OA_EXISTS").equals("Yes"))
						moduleList.add("Online Ads");
						if(row1.get("SURVEYS_EXISTS").equals("Yes"))
						moduleList.add("Surveys");
						
						if(row1.get("FB_EXISTS").equals("Yes"))
							moduleList.add("Social");
					
					
				}
				String includedModules=moduleList.stream().map(String::valueOf).collect(Collectors.joining(", "));
				label=new Label(colClientCal, rowClientCal, includedModules, textFormat);
				clientSheet.addCell(label);
				

				/* Training Start*/
				
				rowClientCal = clientSheetRowOffset;
				colClientCal=trainingDataOffset;
				clientSheet.setRowView(rowClientCal, heightInPoints);
				clientSheet.mergeCells( colClientCal,rowClientCal, colClientCal+4, rowClientCal); // Hub Header
				label=new Label(colClientCal, rowClientCal, "Training", headerFormat);
				clientSheet.addCell(label);
				
				rowClientCal++; 
				clientSheet.setRowView(rowClientCal, heightInPointsSubHeader);
				clientSheet.mergeCells( colClientCal,rowClientCal, colClientCal+3, rowClientCal);
				label=new Label(colClientCal, rowClientCal, "Training Courses", subHeaderFormat);
				clientSheet.addCell(label);
				 label=new Label(trainingDataOffset+3+1, rowClientCal, "", subHeaderFormat);
				 clientSheet.addCell(label);
				 clientSheet.setColumnView(trainingDataOffset+3+1, 10);
				rowClientCal++;

				// training data starts
				String[] trainingColumnHeaders={"Course Category","Course Title","Lesson Title","Participants Count"};
				String[] trainingDataHeaders={"CATEGORY_NAME","COURSE_NAME","SECTION_NAME","CNT"};
				 generateSingleDataset(clientSheet, columnHeaderFormat, textFormat, numberFormat, clientSheetRowOffset,
						clientSheetDataWriteRowOffset, trainingDataOffset, row1.get("TRAINING_COURSE_DATA"),trainingColumnHeaders,trainingDataHeaders);
				// training data ends
				
				/* Training Ends*/
				
				/*Hub Starts*/
				rowClientCal = clientSheetRowOffset;
				colClientCal=libDataOffset;
				clientSheet.setRowView(rowClientCal, heightInPoints);
				clientSheet.mergeCells( colClientCal,rowClientCal, colClientCal+6, rowClientCal); // Hub Header
				label=new Label(colClientCal, rowClientCal, "Hub", headerFormat);
				clientSheet.addCell(label);
				
				rowClientCal++; 
				clientSheet.setRowView(rowClientCal, heightInPointsSubHeader);
				clientSheet.mergeCells( colClientCal,rowClientCal, colClientCal+3, rowClientCal);
				label=new Label(colClientCal, rowClientCal, "Library Documents", subHeaderFormat);
				clientSheet.addCell(label);
				
				
				colClientCal=libLinkPollDataOffset;
				clientSheet.mergeCells( colClientCal,rowClientCal, colClientCal+1, rowClientCal);
				label=new Label(colClientCal, rowClientCal, "Links & Polls", subHeaderFormat);
				clientSheet.addCell(label);
				 label=new Label(libLinkPollDataOffset+1+1, rowClientCal, "", subHeaderFormat);
				 clientSheet.addCell(label);
				clientSheet.setColumnView(libLinkPollDataOffset+2, 10);
				//label=new Label(colClientCal+2, rowClientCal, "", subHeaderFormat);
				//clientSheet.addCell(label);
				
				
				rowClientCal++;
				colClientCal=libDataOffset;
				label=new Label(colClientCal, rowClientCal, "Folder Name", columnHeaderFormat);
				clientSheet.addCell(label);
				colClientCal++;
				
				label=new Label(colClientCal, rowClientCal, "Subfolder Name", columnHeaderFormat);
				clientSheet.addCell(label);
				colClientCal++;
				
				label=new Label(colClientCal, rowClientCal, "Document Title", columnHeaderFormat);
				clientSheet.addCell(label);
				colClientCal++;
				
				label=new Label(colClientCal, rowClientCal, "Number of Downloads", columnHeaderFormat);
				clientSheet.addCell(label);
				colClientCal++;
				clientSheet.setRowView(rowClientCal, heightInPoints);
				
				rowClientCal++;

				try {
				if(row1.get("DOCUMENTS_DOWNLOAD_DETAIL") !=null)
				{
				
				bais = new ByteArrayInputStream((byte[]) row1.get("DOCUMENTS_DOWNLOAD_DETAIL"));
				
				ins = new ObjectInputStream(bais);
				beanHM = (ArrayList<HashMap<String, Object>>) ins.readObject();
				if(beanHM !=null)
				{
					colClientCal = libDataOffset;
					for(HashMap<String,Object> data : beanHM)
					{
						colClientCal = libDataOffset;
						label=new Label(colClientCal, rowClientCal, data.get("FOLDER_NAME")+"", textFormat);
						clientSheet.addCell(label);
						colClientCal++;
						label=new Label(colClientCal, rowClientCal, data.get("SUB_FOLDER_NAME")+"", textFormat);
						clientSheet.addCell(label);
						colClientCal++;
						label=new Label(colClientCal, rowClientCal, data.get("DOCUMENT_TITLE")+"", textFormat);
						clientSheet.addCell(label);
						colClientCal++;
						
						label=new Label(colClientCal, rowClientCal, data.get("TOTAL_DOCUMENTS_DOWNLOADED")+"", textFormat);
						clientSheet.addCell(label);
						colClientCal++;
						rowClientCal++;
						
					}
					
				}
				}
				}
				catch(Exception e)
				{
					System.out.println("exception::::"+e);
					System.out.println("for Client::::"+(String)row1.get("CLIENT_NAME"));
				}
				// hub link data starts	
				String[] hubLinkDataColumns={"Link Title"};
				String[] hubLinkDataHeaders={"TITLE"};
				 generateSingleDataset(clientSheet, columnHeaderFormat, textFormat, numberFormat, clientSheetRowOffset,
						clientSheetDataWriteRowOffset, libLinkDataOffset, row1.get("HUB_LINKS_DATA"),hubLinkDataColumns,hubLinkDataHeaders);
				//hub link data ends
				 
					// hub poll data starts
					String[] hubPollDataColumns={"Poll Title"};
					String[] hubPollDataHeaders={"EPOLL_TITLE"};
					 generateSingleDataset(clientSheet, columnHeaderFormat, textFormat, numberFormat, clientSheetRowOffset,
							clientSheetDataWriteRowOffset, libPollDataOffset, row1.get("HUB_EPOLL_DATA"),hubPollDataColumns,hubPollDataHeaders);
					 //label=new Label(colClientCal, rowClientCal, "", columnHeaderFormat);
					 //clientSheet.addCell(label);
					//hub link data ends
				/*Hub Ends*/
				
				/*Financial Starts*/
				colClientCal=finDataOffset;
				rowClientCal=clientSheetRowOffset;
				clientSheet.mergeCells(finDataOffset, rowClientCal, finDataOffset+14, rowClientCal);
				label=new Label(finDataOffset, rowClientCal, "Finance", headerFormat);
				clientSheet.addCell(label);
				rowClientCal++;
				
				clientSheet.mergeCells(finFinanceDataOffset, rowClientCal, finFinanceDataOffset+2, rowClientCal);
				label=new Label(finFinanceDataOffset, rowClientCal, "Financial Categories", subHeaderFormat);
				clientSheet.addCell(label);
				
				clientSheet.mergeCells(finNonFinanceDataOffset, rowClientCal, finNonFinanceDataOffset+2, rowClientCal);
				label=new Label(finNonFinanceDataOffset, rowClientCal, "Non Financials Categories", subHeaderFormat);
				clientSheet.addCell(label);	
				
				clientSheet.mergeCells(finPLDataOffset, rowClientCal, finPLDataOffset+2, rowClientCal);
				label=new Label(finPLDataOffset, rowClientCal, "Financial Profit & Loss Categories", subHeaderFormat);
				clientSheet.addCell(label);	
				
				clientSheet.mergeCells(finLocDataOffset, rowClientCal, finLocDataOffset+4, rowClientCal);
				label=new Label(finLocDataOffset, rowClientCal, "Sales based on franchisee location", subHeaderFormat);
				clientSheet.addCell(label);
				 label=new Label(finLocDataOffset+4+1, rowClientCal, "", subHeaderFormat);
				 clientSheet.addCell(label);
				 clientSheet.setColumnView(finLocDataOffset+4+1, 10);
				rowClientCal++;
				
				
				// fin category dataset starts
				System.out.println("client ::"+row1.get("CLIENT_NAME"));
				String[] finFinanceColumnHeaders={"Financial Category Name","Financial SubCategory Name","Amount($)"};
				String[] finFinaceDataHeaders={"CATEGORY","SUBCATEGORY","TOTAL_FINCATEGORY_AMOUNT"};
				 generateSingleDataset(clientSheet, columnHeaderFormat, textFormat, numberFormat, clientSheetRowOffset,
						clientSheetDataWriteRowOffset, finFinanceDataOffset, row1.get("FIN_FINCATEGORY_DATA"),finFinanceColumnHeaders,finFinaceDataHeaders,"com.app.dataProcessor.FinDataProcessor");
				

				 
				// fin category dataset ends
				 
				 //non fin category dataset starts
				String[] finNonFinanceColumnHeaders={"Non-Financial Category Name","Non-Financial SubCategory Name","Amount"};
				String[] finNonFinaceDataHeaders={"CATEGORY","SUBCATEGORY","TOTAL_NONFINCATEGORY_AMOUNT"};
				 generateSingleDataset(clientSheet, columnHeaderFormat, textFormat, numberFormat, clientSheetRowOffset,
						clientSheetDataWriteRowOffset, finNonFinanceDataOffset, row1.get("FIN_NONFINCATEGORY_DATA"),finNonFinanceColumnHeaders,finNonFinaceDataHeaders,"com.app.dataProcessor.FinDataProcessor");
				 //non fin category dataset ends
				 
				 // pl category dataset starts
				
				 String[] finPLColumnHeaders={"Financial Profit Category Name","Financial Profit Subcategory Name","Amount($)"};
					String[] finPLDataHeaders={"CATEGORY","SUBCATEGORY","TOTAL_PLCATEGORY_AMOUNT"};
					 generateSingleDataset(clientSheet, columnHeaderFormat, textFormat, numberFormat, clientSheetRowOffset,
							clientSheetDataWriteRowOffset, finPLDataOffset, row1.get("FIN_PLCATEGORY_DATA"),finPLColumnHeaders,finPLDataHeaders);
				 // pl category dataset ends
					 
					 // loc based data starts
					 String[] finLocColumnHeaders={"Franchise Type","Franchise ID","State","FBC","Average Sales($) (Amount)"};
						String[] finLocDataHeaders={"FRANCHISEE_TYPE","FRANCHISEE_NAME","STATE","FBC","TOTAL_SALES"};
						 generateSingleDataset(clientSheet, columnHeaderFormat, textFormat, numberFormat, clientSheetRowOffset,
								clientSheetDataWriteRowOffset, finLocDataOffset, row1.get("FIN_LOCATIONBASED_DATA"),finLocColumnHeaders,finLocDataHeaders);	 
					// loc based data ends
				
				/*Financial Ends*/
				
						 
						 /* CRM Starts */
							colClientCal=crmDataOffset;
							rowClientCal=clientSheetRowOffset;
							clientSheet.mergeCells(crmDataOffset, rowClientCal, crmDataOffset+8, rowClientCal);
							label=new Label(crmDataOffset, rowClientCal, "CRM", headerFormat);
							clientSheet.addCell(label);
							rowClientCal++;
							
							clientSheet.mergeCells(crmLocBasedDataOffset, rowClientCal, crmLocBasedDataOffset+7, rowClientCal);
							label=new Label(crmLocBasedDataOffset, rowClientCal, "Leads & contacts data on the bases of Franchisee Location", subHeaderFormat);
							clientSheet.addCell(label);
							 label=new Label(crmLocBasedDataOffset+7+1, rowClientCal, "", subHeaderFormat);
							 clientSheet.addCell(label);
							 clientSheet.setColumnView(crmLocBasedDataOffset+7+1, 10);

							rowClientCal++;
							
							
						 
						 // loc based data starts
							 String[] crmLocColumnHeaders={"Franchise ID","Number of Leads","Contact Count","Number of Customers","Number of Emails Sent(Lead)",
									 "Number of Campaigns Sent (lead)","Number of Emails Sent (contact)","Number of Campaigns Sent (Contact)"};
								String[] crmLocDataHeaders={"FRANCHISEE_NAME","LEAD_COUNT","CONTACT_COUNT","CUSTOMER_COUNT","LEAD_EMAIL_COUNT",
										"LEAD_CAMPAIGN_EMAIL_COUNT","CONTACT_EMAIL_COUNT","CONTACT_CAMPAIGN_EMAIL_COUNT"};
								 generateSingleDataset(clientSheet, columnHeaderFormat, textFormat, numberFormat, clientSheetRowOffset,
										clientSheetDataWriteRowOffset, crmLocBasedDataOffset, row1.get("CM_FRANCHISEE_DATA"),crmLocColumnHeaders,crmLocDataHeaders);
						 //loc based data ends
						 
						 /* CRM Ends */
								 
								 /* Field Ops Starts */
									colClientCal=fieldOpsDataOffset;
									rowClientCal=clientSheetRowOffset;
									clientSheet.mergeCells(fieldOpsDataOffset, rowClientCal, fieldOpsDataOffset+4, rowClientCal);
									label=new Label(fieldOpsDataOffset, rowClientCal, "Field Ops", headerFormat);
									clientSheet.addCell(label);
									rowClientCal++;
									
									clientSheet.mergeCells(fieldOpsLocDataOffset, rowClientCal, fieldOpsLocDataOffset+3, rowClientCal);
									label=new Label(fieldOpsLocDataOffset, rowClientCal, "Visits", subHeaderFormat);
									clientSheet.addCell(label);
									 label=new Label(fieldOpsLocDataOffset+3+1, rowClientCal, "", subHeaderFormat);
									 clientSheet.addCell(label);
									 clientSheet.setColumnView(fieldOpsLocDataOffset+3+1, 10);
									rowClientCal++;
								 
								 
								 //loc based data saas starts
									 String[] fieldOpsLocColumnHeaders={"Franchise ID","Number of Visits","Visit Frequency","Compliance Score"};
										String[] fieldOpsLocDataHeaders={"franchisee_name","visi_count","visit_avg","compliance_score"};
										 generateSingleDataset(clientSheet, columnHeaderFormat, textFormat, numberFormat, clientSheetRowOffset,
												clientSheetDataWriteRowOffset, fieldOpsLocDataOffset, row1.get("audit_visits_franchisee_data"),fieldOpsLocColumnHeaders,fieldOpsLocDataHeaders);
								 
								//loc based data saas ends
										 
										//loc based data legacy starts
										 
										 String[] fieldOpsLegacyLocDataHeaders={"FRANCHISEE_NAME","NO_OF_VISITS","VISITS_FREQ","COMPLIANCE_SCORE"};
										 generateSingleDataset(clientSheet, columnHeaderFormat, textFormat, numberFormat, clientSheetRowOffset,
												clientSheetDataWriteRowOffset, fieldOpsLocDataOffset, row1.get("AUDIT_DATA"),fieldOpsLocColumnHeaders,fieldOpsLegacyLocDataHeaders);
										//loc based data legacy ends
								 
								 /* Field Ops Ends */
								 
						/* Fim Starts */
									colClientCal=fimDataOffset;
									rowClientCal=clientSheetRowOffset;
									clientSheet.mergeCells(fimDataOffset, rowClientCal, fimDataOffset+7, rowClientCal);
									label=new Label(fimDataOffset, rowClientCal, "Info Mgr", headerFormat);
									clientSheet.addCell(label);
									rowClientCal++;
									
									clientSheet.mergeCells(fimLocDataOffset, rowClientCal, fimLocDataOffset+4, rowClientCal);
									label=new Label(fimLocDataOffset, rowClientCal, "Franchisee location’s Info", subHeaderFormat);
									clientSheet.addCell(label);
									
									clientSheet.mergeCells(fimCampaignDataOffset, rowClientCal, fimCampaignDataOffset+1, rowClientCal);
									label=new Label(fimCampaignDataOffset, rowClientCal, "Mail Info", subHeaderFormat);
									clientSheet.addCell(label);
									 label=new Label(fimCampaignDataOffset+1+1, rowClientCal, "", subHeaderFormat);
									 clientSheet.addCell(label);
									 clientSheet.setColumnView(fimCampaignDataOffset+1+1, 10);

									rowClientCal++;
								 
								 // fim loc activity based data starts
								 
									 String[] fimLocColumnHeaders={"Franchise ID","Number of Emails/Messages Sent","Number of Remarks","Is Active","Default and Termination Reasons"};
										String[] fimLocDataHeaders={"FRANCHISEE_NAME","MAIL_COUNT","REMARKS_COUNT","STATUS","DEACTIVATE_REASON_NAME"};
										 generateSingleDataset(clientSheet, columnHeaderFormat, textFormat, numberFormat, clientSheetRowOffset,
												clientSheetDataWriteRowOffset, fimLocDataOffset, row1.get("FIM_LOC_ACTIVITY_DATA"),fimLocColumnHeaders,fimLocDataHeaders);
								 // fim loc activity based data ends
										 
										 // fim loc campaign based data starts
										 
										 String[] fimCampaignColumnHeaders={"Campaign Name","Template Name"};
											String[] fimCampaignDataHeaders={"CAMPAIGN_NAME","TEMPLATE_NAME"};
											 generateSingleDataset(clientSheet, columnHeaderFormat, textFormat, numberFormat, clientSheetRowOffset,
													clientSheetDataWriteRowOffset, fimCampaignDataOffset, row1.get("FIM_TEMPLATE_DATA"),fimCampaignColumnHeaders,fimCampaignDataHeaders);
									 // fim loc campaign based data ends
								 
								 
						/* Fim Ends */
				/* client sheet write end */
				
                columnMonthCal = 0;
                sheetMonthCal.setColumnView(columnMonthCal, 30);
                label = new Label(columnMonthCal, rowMonthCal, row1.get("CLIENT_NAME") + "", textFormat);
                sheetMonthCal.addCell(label);
				hl = new WritableHyperlink(columnMonthCal, rowMonthCal,(String)row1.get("CLIENT_NAME") , clientSheet, 0, 0);
				sheetMonthCal.addHyperlink(hl);
                ++columnMonthCal;
                sheetMonthCal.setColumnView(columnMonthCal, 30);
                label = new Label(columnMonthCal, rowMonthCal, row1.get("CODE_VALUE") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                //insert if
                if(Constants.version!=null && !"Legacy".equals(Constants.version))
                {
                label = new Label(columnMonthCal, rowMonthCal, row1.get("CC_EXISTS") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                }//end if
                label = new Label(columnMonthCal, rowMonthCal, row1.get("INTRANET_EXISTS") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("FS_EXISTS") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("SM_EXISTS") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("FIM_EXISTS") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("SUPPORT_EXISTS") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("TRAINING_EXISTS") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("CM_EXISTS") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("ADMAKER_EXISTS") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("FIELDOPS_EXISTS") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("WB_EXISTS") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("SUPPLIES_EXISTS") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("LL_EXISTS") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("RM_EXISTS") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                
                label = new Label(columnMonthCal, rowMonthCal, row1.get("FINANCIAL_EXISTS") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                
                if(Constants.version!=null && !"Legacy".equals(Constants.version))
                {
                	
                	label = new Label(columnMonthCal, rowMonthCal, row1.get("MARKETING_EXISTS") + "", textFormat);
                    sheetMonthCal.addCell(label);
                    ++columnMonthCal;
                    label = new Label(columnMonthCal, rowMonthCal, row1.get("LP_EXISTS") + "", textFormat);
                    sheetMonthCal.addCell(label);
                    ++columnMonthCal;
                    label = new Label(columnMonthCal, rowMonthCal, row1.get("PLANNER_EXISTS") + "", textFormat);
                    sheetMonthCal.addCell(label);
                    ++columnMonthCal;
                    label = new Label(columnMonthCal, rowMonthCal, row1.get("OA_EXISTS") + "", textFormat);
                    sheetMonthCal.addCell(label);
                    ++columnMonthCal;
                    label = new Label(columnMonthCal, rowMonthCal, row1.get("SURVEYS_EXISTS") + "", textFormat);
                    sheetMonthCal.addCell(label);
                    ++columnMonthCal;
                    label = new Label(columnMonthCal, rowMonthCal, row1.get("FB_EXISTS") + "", textFormat);
                    sheetMonthCal.addCell(label);
                    ++columnMonthCal;
                    label = new Label(columnMonthCal, rowMonthCal, row1.get("SMARTCONNECT_EXISTS") + "", textFormat);
                    sheetMonthCal.addCell(label);
                    ++columnMonthCal;
                }


                label = new Label(columnMonthCal, rowMonthCal, row1.get("TOTAL_DOCUMENTS_DOWNLOADED") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("TOTAL_MESSAGES_ALERTS_SENT_HUB") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("STORIES_POSTED_HUB") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("FRANBUZZ_POSTS_HUB") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("CALENDAR_ITEMS_POSTED_HUB") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("EPOLL_STARTED_HUB") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("ACTIVE_INTRANET_DOC") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("ACTIVE_INTRANET_DOC_MONTH_AVERAGE") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("STORY_COUNT") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("STORY_COUNT_MONTH_AVERAGE") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("EPOLL_ADDED") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("EPOLL_ADDED_MONTH_AVERAGE") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("LINKS_COUNT") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                
                label = new Label(columnMonthCal, rowMonthCal, row1.get("SSO_TABS") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("FRANBUZZ_COMMENTS") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("FRANBUZZ_COMMENTS_MONTH_AVERAGE") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("LEADS_ADDED_FS") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("EMAILS_SENT_FS") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("CALLS_LOGGED_FS") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("TASKS_CREATED_FS") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("FDD_SENT_FS") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("FS_WORKFLOW") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("LEADS_CLOSED_FS") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("AVG_DAYS_TO_CLOSE_LEADS_FS") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("CAMPAIGNS_CREATED_FS") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("TABS_CREATED_FS") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("SECTIONS_CREATED_FS") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("FIELDS_ADDED_FS") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("FS_CAMPAIGN_EMAILS_SENT") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("LOCATIONS_ADDED") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("TASKS_COMPLETED_FO") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("LOCATIONS_OPENED") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("TASKS_OVERDUE_FO") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("EMAILS_SENT_FIM") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("MESSAGE_SENT_FIM") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("CALLS_LOGGED_FIM") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("TASKS_CREATED_FIM") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("FDD_SENT_FIM") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("RENEWALS_ADDED_FIM") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("CAMPAIGNS_CREATED_FIM") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("EMAIL_TEMPLATES_FIM") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("FIM_WORKFLOW") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("CUSTOM_REPORTS_FIM") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("TABS_ADDED_FIM") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("SECTIONS_ADDED_FIM") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("FIELDS_ADDED_FIM") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("FIM_CAMPAIGN_EMAILS_SENT") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("LOCATION_SENT_EMAIL") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("LOCATION_SENT_EMAIL_MONTH_AVERAGE") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("TICKETS_CREATED") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("TICKETS_CLOSED_SUPPORT") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("AVG_TICKETS_CLOSED_SUPPORT") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("MEDIAN_TICKETS_CLOSED_SUPPORT") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("MAX_TICKETS_CLOSED_SUPPORT") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("MIN_TICKETS_CLOSED_SUPPORT") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("FAQ_EXISTS_SUPPORT") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("NEW_COURSES_ADDED_TRAINING") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("NEW_COURSES_ADDED_TRAINING_MONTH_AVERAGE") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("PLANS_CREATED") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("PLANS_COMPLETED") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("CERTIFICATES_AWARDED") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("COURSES_IN_PROGRESS") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("COURSES_COMPLETED_TRAINING") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("TOTAL_QUIZZES_TRAINING") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("PASSED_PERCENTAGE_TRAINING") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("CRM_CONTACTS_ADDED_CRM") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("CRM_LEAD_ADDED") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("CRM_CUSTOMER_ADDED") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("OPPORTUNIITY_ADDED_CRM") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("TRANSACTIONS_ADDED_CRM") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("ACCOUNTS_ADDED_CRM") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("EMAIL_SENT_CRM") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("CALLS_LOGGED_CRM") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("CRM_WORKFLOWS") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("TASKS_CREATED_CRM") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("LEADS_CLOSED_CRM") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("CRM_REPORTS") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("TABS_CREATED_CRM") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("SECTIONS_CREATED_CRM") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("FIELDS_CREATED_CRM") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("CAMPAIGNS_CREATED") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("CAMPAIGN_EMAILS_SENT") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("CAMPAIGN_EMAILS_FAILED") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("TEMPLATES_ADDED") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;                
                label = new Label(columnMonthCal, rowMonthCal, row1.get("ARTWORK_SAVED_ADS") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("SAVED_ARTWORK_CARTS_ADS") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("ADS_BY_CORPORATE") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("ADS_EMAILED_ADS") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("ADS_DOWNLOADED_ADS") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("VISTIS_CREATED_FIELD_OPS") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("VISITS_COMPLETED") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("TASKS_CREATED_FIELD_OPS") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("NEW_FORMS_ADDED_FIELD_OPS") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("AVG_COMPLIANCE_SCORE_FIELD_OPS") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("MIN_COMPLIANCE_SCORE_FIELD_OPS") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("MAX_COMPLIANCE_SCORE_FIELD_OPS") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("MEDIAN_COMPLIANCE_SCORE_FIELD_OPS") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("NON_COMPLIANCE_ZEES_FIELD_OPS") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("SITES_ADDED") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("CHANGES_PUBLISHED") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("AVG_UPDATES_ZEES") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("NEW_ORDERS_SHOP") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("ORDER_TOTAL_SHOP") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("NO_PURCHASE_ORDERS") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("NO_PRODUCTS_SOLD_SHOP") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("SUCCESSFUL_ORDER_PERCENTAGE_SHOP") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("TOP_SELLING_PRODUCT_SHOP") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("NO_INCOMPLETE_LISTINGS") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("LOCATIONS_LISTINGS_ACTIVATED_PERCENTAGE") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("LOCATIONS_REPUTATION_ACTIVATED_PERCENTAGE") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("AVG_REPUTATION_SCORE_REPUTATION") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("MAX_REPUTATION_SCORE_REPUTATION") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("MIN_REPUTATION_SCORE_REPUTATION") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("MEDIAN_REPUTATION_SCORE_REPUTATION") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("TOTAL_SALES_SUBMITTED") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("ROYALTIES_TOTAL") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("ROYALTIES_TOTAL_PAID") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("ROYALTY_LOCATIONS") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("TOTAL_REVENUE") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("SALES_LOCATION") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("AVERAGE_REVENUE") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("AVERAGE_ROYALTY") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("SYSTEM_PROFITABILITY") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("SYSTEM_PROFITABILITY_LOCATION_AVERAGE") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("ACTIVE_LOCATIONS") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("INDEV_LOCATIONS") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("CORPORATE_LOCATIONS") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("TERMINATED_LOCATIONS") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("UNIQUE_CALS") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("ACTIVE_CU") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("ACTIVE_DU") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("ACTIVE_RU") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("ACTIVE_FU") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("LOGGED_IN_CU") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("LOGGED_IN_DU") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("BRAND_NAMES") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("LOGGED_IN_RU") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("LOGGED_IN_FU") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;                
                if("WUEXPORT".equals(Constants.exportType)) {
                    label = new Label(columnMonthCal, rowMonthCal, row1.get("TOTAL_TIME") + "", textFormat);
                    sheetMonthCal.addCell(label);
                    ++columnMonthCal;
                    label = new Label(columnMonthCal, rowMonthCal, row1.get("RATIO1") + "", textFormat);
                    sheetMonthCal.addCell(label);
                    ++columnMonthCal;
                    label = new Label(columnMonthCal, rowMonthCal, row1.get("RATIO2") + "", textFormat);
                    sheetMonthCal.addCell(label);
                    ++columnMonthCal;
                    label = new Label(columnMonthCal, rowMonthCal, df.format(row1.get("TOTAL_TIME1")) + "", textFormat);
                    sheetMonthCal.addCell(label);
                    ++columnMonthCal;
                    label = new Label(columnMonthCal, rowMonthCal, row1.get("TOTAL_TIME2") + "", textFormat);
                    sheetMonthCal.addCell(label);
                    ++columnMonthCal;
                }else {
                    label = new Label(columnMonthCal, rowMonthCal, row1.get("TOTAL_TIME") + "", textFormat);
                    sheetMonthCal.addCell(label);
                    ++columnMonthCal;
                }
                label = new Label(columnMonthCal, rowMonthCal, row1.get("ADOPTION_RATE") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                ++rowMonthCal;
            }
            try {
            workbook.write();
            workbook.close();
            
            String dbFileName = "";
			if (fileName != null) {
				String[] temp = fileName.split("GenericDataExporter/");

				if (temp != null && temp.length > 1) {
					dbFileName = temp[1];
				} else {
					dbFileName = "";
				}

			}

			if (!"".equals(dbFileName)) {
				String flag = "0";
				String hostName = "";
				String userName = "";
				String password = "";
				String ftpFoldername = "./CustomerOpsIndexReport/";

				query = "SELECT FTP_SERVER_ID, HOST_NAME, USER_NAME, PASSWORD, FLAG, HOST_URL, IS_DEFAULT,DIRECTORY FROM FILE_UPLOAD_OPS_FTP_SERVER";
				exchange.getIn().setBody(query.toString());

				Queryout = template.send(endpoint, exchange);
				Queryresult = Queryout.getOut().getBody(List.class);
				i = Queryresult.size();

				for (int j = 0; j < i; j++) {
					Map<String, Object> row1 = Queryresult.get(j);
					if (row1.get("HOST_NAME") != null) {
						hostName = row1.get("HOST_NAME") + "";
					} else {
						hostName = "";
					}
					if (row1.get("USER_NAME") != null) {
						userName = row1.get("USER_NAME") + "";
					} else {
						userName = "";
					}

					if (row1.get("PASSWORD") != null) {
						password = row1.get("PASSWORD") + "";
					} else {
						password = "";
					}

					if (row1.get("FLAG") != null) {
						flag = row1.get("FLAG") + "";
					} else {
						flag = "0";
					}
					if (row1.get("DIRECTORY") != null) {
						ftpFoldername = row1.get("DIRECTORY") + "";
					} else {
						ftpFoldername = "./CustomerOpsIndexReport/";
					}
				}
				boolean isFileUpload=false;
				isFileUpload = uploadFilesToFtpServer(fileName, dbFileName, flag, hostName, userName, password,ftpFoldername);
				System.out.println("File Uploaded ::::"+isFileUpload);
						
            }}
			catch(Exception e)
            {
            	e.printStackTrace();
            }
        }
         
        catch (Exception e) {
            System.out.println("=Exception Found in writing workbook ====================>" + stackTraceToString(e));
            e.printStackTrace();
        }
    }

    
    public static void genrateAWSS3DocRemovalSheet(String exportFilepath,LinkedHashMap<String,ClientInfo> clients) {
    	


        String patterns[] = Constants.patterns;
        DecimalFormat df = new DecimalFormat("#");
        df.setMaximumFractionDigits(10);
        String query = "";
        List<String> excludeTableList = new ArrayList<String>();
        excludeTableList.add("CM_CONTACT_DETAILS_POS_SAMPLE");
        excludeTableList.add("CM_POS_FILE_FIELD_DETAILS");
        excludeTableList.add("FS_CANDIDATE_POST_MAIL");
        excludeTableList.add("FIM_FTP_UPLOAD_ERROR_LOG");
        excludeTableList.add("FIM_FTP_UPLOAD_HISTORY");
        excludeTableList.add("FIN_FTP_MAIL_INFO");
        excludeTableList.add("TERRITORY_FTP_UPLOAD_ERROR_LOG");
        excludeTableList.add("TERRITORY_FTP_UPLOAD_HISTORY");
        excludeTableList.add("CM_POS_FILE_FIELD_DETAILS");
        excludeTableList.add("FS_CANDIDATE_POST_MAIL");
        excludeTableList.add("FIN_POS_SEND_MAIL");
        excludeTableList.add("FIN_POS_THREAD_TIMINGS");
        excludeTableList.add("FTP_DOWNLOAD_STATUS");
        excludeTableList.add("COMM_FTP_UPLOAD_ERROR_LOG");
        excludeTableList.add("FIM_FTP_MYSTERY_UPLOAD_ERROR_LOG");
        excludeTableList.add("FIM_FTP_MYSTERY_UPLOAD_HISTORY");
        excludeTableList.add("FIN_FTP_DOCUMENT_THREAD_CONFIG");
        
        int i = 0, sheetCount=1;
        String fileName = exportFilepath;
        int rowMonthCal = 0;
        int columnMonthCal = 0;
        int rowClientCal = 0;
        int colClientCal=0;
        WritableSheet sheetMonthCal = null;
        WritableSheet clientSheet = null;
        WritableFont headerFont = null;
        WritableCellFormat headerFormat = null;
        WritableCellFormat subHeaderFormat = null;
        WritableCellFormat columnHeaderFormat = null;
        WritableFont textFont = null;
        WritableCellFormat textFormat = null;
        WritableCellFormat cellFormat = null;
        WritableCellFormat numberFormat = null;
        WritableCellFormat stringFormat = null;
        NumberFormat decimalNo = null;
        Label label = null;
        Number numberCell = null;
        String codeValue = "";
        WritableWorkbook workbook = null;
        try {
            File file = new File(fileName);
            if (file.exists()) {
                file.delete();
            }
            WorkbookSettings wbSettings = new WorkbookSettings();
            wbSettings.setRationalization(false);
            workbook = Workbook.createWorkbook(file, wbSettings);
            sheetMonthCal = workbook.createSheet("Generic Table Data", 0);
        }
        catch (Exception e) {
            System.out.println("=Exception Found in reading file====================>" + fileName + "\n" + stackTraceToString(e));
            e.printStackTrace();
        }
        rowMonthCal = 0;
        try {
            headerFont = new WritableFont(WritableFont.TAHOMA, 10, WritableFont.BOLD);
            headerFont.setColour(Colour.BLACK);
            headerFormat = new WritableCellFormat(headerFont);
            headerFormat.setBackground(Colour.GREY_40_PERCENT);
            headerFormat.setWrap(true);
            headerFormat.setVerticalAlignment(VerticalAlignment.TOP);
            headerFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
            decimalNo = new NumberFormat("###,###.00");
            numberFormat = new WritableCellFormat(decimalNo);
            stringFormat = new WritableCellFormat();
            stringFormat.setAlignment(Alignment.RIGHT);
            textFont = new WritableFont(WritableFont.TAHOMA, 10);
            textFormat = new WritableCellFormat(textFont);
           // textFormat.setAlignment(jxl.write.Alignment.CENTRE);
            textFormat.setWrap(true);
            WritableCellFormat cellFormatEmail = new WritableCellFormat();
            cellFormatEmail.setWrap(true);
            cellFormat = new WritableCellFormat(headerFont);
            cellFormat.setBackground(Colour.GREY_40_PERCENT);
            cellFormat.setAlignment(jxl.write.Alignment.CENTRE);
            cellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
            cellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
            cellFormat.setWrap(true);
            
            subHeaderFormat = new WritableCellFormat(headerFont);
            subHeaderFormat.setBackground(Colour.GREY_25_PERCENT);
            subHeaderFormat.setWrap(true);
            subHeaderFormat.setVerticalAlignment(VerticalAlignment.TOP);
            subHeaderFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
            
            columnHeaderFormat = new WritableCellFormat(headerFont);
            columnHeaderFormat.setBackground(Colour.LIGHT_TURQUOISE);
            columnHeaderFormat.setWrap(true);
            columnHeaderFormat.setVerticalAlignment(VerticalAlignment.TOP);
            columnHeaderFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
            
            int heightInPoints = 780;
            int heightInPointsSubHeader=500;
            int n = 3;

            for (int j = 0; j < 100; ++j) {
                sheetMonthCal.setColumnView(j, 29);
            }
            sheetMonthCal.setRowView(rowMonthCal, heightInPoints);
            if (rowMonthCal == 0) {
                sheetMonthCal.mergeCells(0, rowMonthCal, n, rowMonthCal);
                label = new Label(0, 0, "Generic Table Data Report", headerFormat);
                sheetMonthCal.addCell(label);
                ++rowMonthCal;
            }
            if (rowMonthCal == 1) {
                columnMonthCal = 0;

                

                
                {
                sheetMonthCal.mergeCells(columnMonthCal, rowMonthCal, columnMonthCal+3, rowMonthCal); // Modules


                }
                

                {
                    label = new Label(0, 1, "Customer wise Generic Table Records", headerFormat);
                    sheetMonthCal.addCell(label);

                }
            }
            if (++rowMonthCal == 2) {
            	columnMonthCal = 0;
            	sheetMonthCal.setRowView(rowMonthCal, heightInPoints);
                label = new Label(columnMonthCal, rowMonthCal, "Customer", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Table Name", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;

                //insert if

               
            }
            ++rowMonthCal;
           // StringBuffer luminReportData = new StringBuffer(QueryBean.getLuminReportSql(Constants.dateTo, Constants.dateFrom));
            //System.out.println("==============================" + (Object)luminReportData);


            Iterator<String> clientIt = clients.keySet().iterator();
            String clientName;
            columnMonthCal = 0;
            while(clientIt.hasNext())
    		{	columnMonthCal = 0;
            	
            	clientName = clientIt.next();
    			ClientInfo dbProps = clients.get(clientName);
                sheetMonthCal.setColumnView(columnMonthCal, 30);
                //label = new Label(columnMonthCal, rowMonthCal, clientName + "", textFormat);
                //sheetMonthCal.addCell(label);
                ++columnMonthCal;
               // ++rowMonthCal;
                
    			Endpoint dbConnection = DBConnectionProvider.getInstance().getDBConnection(clientName);
    			HashMap<String,Object> results = null;
    			Set<String> colNames;
    			ArrayList<DynaBean> data;
    			for(String pattern: patterns)
    			{
    			results = QueryUtil.executeQuery(dbConnection, "SHOW TABLES LIKE '%"+pattern+"%'");
    			boolean isFailed = (boolean)results.get("IS_FAILED");
    			if(!isFailed)
    			{
	    	    	colNames = (Set<String>)results.get("COLUMN_NAMES");
	    			Iterator<String> colItr = colNames.iterator();
	    			data=(ArrayList<DynaBean>)results.get("DATA");
	    			int size = colNames.size();
	    			HashMap<String,Object> tableData = null;
	    			
	    	    	for(DynaBean tableBean: data)
	    	    	{
	    	    		colItr = colNames.iterator();
	    	    		while(colItr.hasNext())
	    	    		{
	    	    		String tableName= (String)tableBean.get(colItr.next());
	                    columnMonthCal=1;
	    	    		label = new Label(columnMonthCal, rowMonthCal, tableName + "", textFormat);
	                    
	                    sheetMonthCal.addCell(label);
	                    ++columnMonthCal;
	                    
	                    
	                    System.out.println(tableName);
	    	    		tableData = QueryUtil.executeQuery(dbConnection, "SELECT * FROM "+tableName+" LIMIT 10");/*+(!Constants.dataLimit.equals("NA")?" LIMIT "+Constants.dataLimit+" ":"")*/
	    	    		System.out.println(tableName);
	    	    		if(!(boolean)tableData.get("IS_FAILED") && !excludeTableList.contains(tableName))
	    	    		{
	    	    			System.out.println(tableName);
	    	    			rowMonthCal= generateGenericDataset(sheetMonthCal, columnHeaderFormat, textFormat, numberFormat, rowMonthCal,
	   						rowMonthCal, columnMonthCal, tableData.get("DATA"),(Set<String>)tableData.get("COLUMN_NAMES"),(Set<String>)tableData.get("COLUMN_NAMES"),clientName,tableName);
	    	    			 		
	    	    		}
	    	    		}
	    	    	}
    			}
    			}
    		
    		}
            
            try {
            workbook.write();
            workbook.close();
            
            String dbFileName = "";
			if (fileName != null) {
				String[] temp = fileName.split("GenericDataExporter/");

				if (temp != null && temp.length > 1) {
					dbFileName = temp[1];
				} else {
					dbFileName = "";
				}

			}

			}
			catch(Exception e)
            {
            	e.printStackTrace();
            }
        }
         
        catch (Exception e) {
            System.out.println("=Exception Found in writing workbook ====================>" + stackTraceToString(e));
            e.printStackTrace();
        }
    
    
    }
    
    
    public static void genrateGenericTableDataRecordFile(String exportFilepath,LinkedHashMap<String,ClientInfo> clients) {
    	


        String patterns[] = Constants.patterns;
        DecimalFormat df = new DecimalFormat("#");
        df.setMaximumFractionDigits(10);
        String query = "";
        List<String> excludeTableList = new ArrayList<String>();
        excludeTableList.add("CM_CONTACT_DETAILS_POS_SAMPLE");
        excludeTableList.add("CM_POS_FILE_FIELD_DETAILS");
        excludeTableList.add("FS_CANDIDATE_POST_MAIL");
        excludeTableList.add("FIM_FTP_UPLOAD_ERROR_LOG");
        excludeTableList.add("FIM_FTP_UPLOAD_HISTORY");
        excludeTableList.add("FIN_FTP_MAIL_INFO");
        excludeTableList.add("TERRITORY_FTP_UPLOAD_ERROR_LOG");
        excludeTableList.add("TERRITORY_FTP_UPLOAD_HISTORY");
        excludeTableList.add("CM_POS_FILE_FIELD_DETAILS");
        excludeTableList.add("FS_CANDIDATE_POST_MAIL");
        excludeTableList.add("FIN_POS_SEND_MAIL");
        excludeTableList.add("FIN_POS_THREAD_TIMINGS");
        excludeTableList.add("FTP_DOWNLOAD_STATUS");
        excludeTableList.add("COMM_FTP_UPLOAD_ERROR_LOG");
        excludeTableList.add("FIM_FTP_MYSTERY_UPLOAD_ERROR_LOG");
        excludeTableList.add("FIM_FTP_MYSTERY_UPLOAD_HISTORY");
        excludeTableList.add("FIN_FTP_DOCUMENT_THREAD_CONFIG");
        
        int i = 0, sheetCount=1;
        String fileName = exportFilepath;
        int rowMonthCal = 0;
        int columnMonthCal = 0;
        int rowClientCal = 0;
        int colClientCal=0;
        WritableSheet sheetMonthCal = null;
        WritableSheet clientSheet = null;
        WritableFont headerFont = null;
        WritableCellFormat headerFormat = null;
        WritableCellFormat subHeaderFormat = null;
        WritableCellFormat columnHeaderFormat = null;
        WritableFont textFont = null;
        WritableCellFormat textFormat = null;
        WritableCellFormat cellFormat = null;
        WritableCellFormat numberFormat = null;
        WritableCellFormat stringFormat = null;
        NumberFormat decimalNo = null;
        Label label = null;
        Number numberCell = null;
        String codeValue = "";
        WritableWorkbook workbook = null;
        try {
            File file = new File(fileName);
            if (file.exists()) {
                file.delete();
            }
            WorkbookSettings wbSettings = new WorkbookSettings();
            wbSettings.setRationalization(false);
            workbook = Workbook.createWorkbook(file, wbSettings);
            sheetMonthCal = workbook.createSheet("Generic Table Data", 0);
        }
        catch (Exception e) {
            System.out.println("=Exception Found in reading file====================>" + fileName + "\n" + stackTraceToString(e));
            e.printStackTrace();
        }
        rowMonthCal = 0;
        try {
            headerFont = new WritableFont(WritableFont.TAHOMA, 10, WritableFont.BOLD);
            headerFont.setColour(Colour.BLACK);
            headerFormat = new WritableCellFormat(headerFont);
            headerFormat.setBackground(Colour.GREY_40_PERCENT);
            headerFormat.setWrap(true);
            headerFormat.setVerticalAlignment(VerticalAlignment.TOP);
            headerFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
            decimalNo = new NumberFormat("###,###.00");
            numberFormat = new WritableCellFormat(decimalNo);
            stringFormat = new WritableCellFormat();
            stringFormat.setAlignment(Alignment.RIGHT);
            textFont = new WritableFont(WritableFont.TAHOMA, 10);
            textFormat = new WritableCellFormat(textFont);
           // textFormat.setAlignment(jxl.write.Alignment.CENTRE);
            textFormat.setWrap(true);
            WritableCellFormat cellFormatEmail = new WritableCellFormat();
            cellFormatEmail.setWrap(true);
            cellFormat = new WritableCellFormat(headerFont);
            cellFormat.setBackground(Colour.GREY_40_PERCENT);
            cellFormat.setAlignment(jxl.write.Alignment.CENTRE);
            cellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
            cellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
            cellFormat.setWrap(true);
            
            subHeaderFormat = new WritableCellFormat(headerFont);
            subHeaderFormat.setBackground(Colour.GREY_25_PERCENT);
            subHeaderFormat.setWrap(true);
            subHeaderFormat.setVerticalAlignment(VerticalAlignment.TOP);
            subHeaderFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
            
            columnHeaderFormat = new WritableCellFormat(headerFont);
            columnHeaderFormat.setBackground(Colour.LIGHT_TURQUOISE);
            columnHeaderFormat.setWrap(true);
            columnHeaderFormat.setVerticalAlignment(VerticalAlignment.TOP);
            columnHeaderFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
            
            int heightInPoints = 780;
            int heightInPointsSubHeader=500;
            int n = 3;

            for (int j = 0; j < 100; ++j) {
                sheetMonthCal.setColumnView(j, 29);
            }
            sheetMonthCal.setRowView(rowMonthCal, heightInPoints);
            if (rowMonthCal == 0) {
                sheetMonthCal.mergeCells(0, rowMonthCal, n, rowMonthCal);
                label = new Label(0, 0, "Generic Table Data Report", headerFormat);
                sheetMonthCal.addCell(label);
                ++rowMonthCal;
            }
            if (rowMonthCal == 1) {
                columnMonthCal = 0;

                

                
                {
                sheetMonthCal.mergeCells(columnMonthCal, rowMonthCal, columnMonthCal+3, rowMonthCal); // Modules


                }
                

                {
                    label = new Label(0, 1, "Customer wise Generic Table Records", headerFormat);
                    sheetMonthCal.addCell(label);

                }
            }
            if (++rowMonthCal == 2) {
            	columnMonthCal = 0;
            	sheetMonthCal.setRowView(rowMonthCal, heightInPoints);
                label = new Label(columnMonthCal, rowMonthCal, "Customer", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Table Name", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;

                //insert if

               
            }
            ++rowMonthCal;
           // StringBuffer luminReportData = new StringBuffer(QueryBean.getLuminReportSql(Constants.dateTo, Constants.dateFrom));
            //System.out.println("==============================" + (Object)luminReportData);


            Iterator<String> clientIt = clients.keySet().iterator();
            String clientName;
            columnMonthCal = 0;
            while(clientIt.hasNext())
    		{	columnMonthCal = 0;
            	
            	clientName = clientIt.next();
    			ClientInfo dbProps = clients.get(clientName);
                sheetMonthCal.setColumnView(columnMonthCal, 30);
                //label = new Label(columnMonthCal, rowMonthCal, clientName + "", textFormat);
                //sheetMonthCal.addCell(label);
                ++columnMonthCal;
               // ++rowMonthCal;
                
    			Endpoint dbConnection = DBConnectionProvider.getInstance().getDBConnection(clientName);
    			HashMap<String,Object> results = null;
    			Set<String> colNames;
    			ArrayList<DynaBean> data;
    			for(String pattern: patterns)
    			{
    			results = QueryUtil.executeQuery(dbConnection, "SHOW TABLES LIKE '%"+pattern+"%'");
    			boolean isFailed = (boolean)results.get("IS_FAILED");
    			if(!isFailed)
    			{
	    	    	colNames = (Set<String>)results.get("COLUMN_NAMES");
	    			Iterator<String> colItr = colNames.iterator();
	    			data=(ArrayList<DynaBean>)results.get("DATA");
	    			int size = colNames.size();
	    			HashMap<String,Object> tableData = null;
	    			
	    	    	for(DynaBean tableBean: data)
	    	    	{
	    	    		colItr = colNames.iterator();
	    	    		while(colItr.hasNext())
	    	    		{
	    	    		String tableName= (String)tableBean.get(colItr.next());
	                    columnMonthCal=1;
	    	    		label = new Label(columnMonthCal, rowMonthCal, tableName + "", textFormat);
	                    
	                    sheetMonthCal.addCell(label);
	                    ++columnMonthCal;
	                    
	                    
	                    System.out.println(tableName);
	    	    		tableData = QueryUtil.executeQuery(dbConnection, "SELECT * FROM "+tableName+" LIMIT 10");/*+(!Constants.dataLimit.equals("NA")?" LIMIT "+Constants.dataLimit+" ":"")*/
	    	    		System.out.println(tableName);
	    	    		if(!(boolean)tableData.get("IS_FAILED") && !excludeTableList.contains(tableName))
	    	    		{
	    	    			System.out.println(tableName);
	    	    			rowMonthCal= generateGenericDataset(sheetMonthCal, columnHeaderFormat, textFormat, numberFormat, rowMonthCal,
	   						rowMonthCal, columnMonthCal, tableData.get("DATA"),(Set<String>)tableData.get("COLUMN_NAMES"),(Set<String>)tableData.get("COLUMN_NAMES"),clientName,tableName);
	    	    			 		
	    	    		}
	    	    		}
	    	    	}
    			}
    			}
    		
    		}
            
            try {
            workbook.write();
            workbook.close();
            
            String dbFileName = "";
			if (fileName != null) {
				String[] temp = fileName.split("GenericDataExporter/");

				if (temp != null && temp.length > 1) {
					dbFileName = temp[1];
				} else {
					dbFileName = "";
				}

			}

			}
			catch(Exception e)
            {
            	e.printStackTrace();
            }
        }
         
        catch (Exception e) {
            System.out.println("=Exception Found in writing workbook ====================>" + stackTraceToString(e));
            e.printStackTrace();
        }
    
    
    }
    
	private static int generateGenericDataset(WritableSheet clientSheet, WritableCellFormat headerFormat,
			WritableCellFormat textFormat, WritableCellFormat numberFormat, final int clientSheetRowOffset,
			final int clientSheetDataWriteRowOffset, final int dataHeaderOffset, Object dataColumn, Set<String> dataColumnHeaders, Set<String> dataHeaders, String clientName, String tableName)
			throws WriteException, RowsExceededException, IOException, ClassNotFoundException {
		int rowClientCal;
		int colClientCal;
		Label label;
		ByteArrayInputStream bais;
		ObjectInputStream ins;
		ArrayList<DynaBean> beanHM=null;
		String dataUnit="";
		colClientCal=dataHeaderOffset;
		 rowClientCal=clientSheetDataWriteRowOffset; //data headers row
		 
		 if(dataColumn!=null)
		 {
				beanHM = null; 
				beanHM = (ArrayList<DynaBean>) dataColumn;
				if(beanHM!=null && (beanHM.size()>5 || beanHM.size()==0))
						
					return rowClientCal;
			 
		 }
		// rowClientCal++;
			label=new Label(0, rowClientCal, clientName, textFormat);
			clientSheet.addCell(label);
			label=new Label(1, rowClientCal, tableName, textFormat);
			clientSheet.addCell(label);
		 for(String dch:dataColumnHeaders)
		 {
			 System.out.println("columHeaders::::"+dch);
			label=new Label(colClientCal, rowClientCal, dch, headerFormat);
			clientSheet.addCell(label);
			colClientCal++;
		 }



			//System.out.println(beanHM);
			 
			 colClientCal=dataHeaderOffset;
			 rowClientCal+=1;
			 if(beanHM!=null)
			 {
				 colClientCal=dataHeaderOffset;
				 for(DynaBean data : beanHM)
				 {
					 colClientCal=dataHeaderOffset;
						label=new Label(0, rowClientCal, clientName, textFormat);
						clientSheet.addCell(label);
						label=new Label(1, rowClientCal, tableName, textFormat);
						clientSheet.addCell(label);
					 for(String dh:dataHeaders)
					 {
						 dataUnit=(String)data.get(dh);
						 if(dataUnit==null)
							 dataUnit="";
						 if( dataUnit.length()>255)
							 dataUnit=dataUnit.substring(0,255);
						 //System.out.println("data::get(dh)::::"+data.get(dh));
						label=new Label(colClientCal, rowClientCal, dataUnit, textFormat);
						clientSheet.addCell(label);
						colClientCal++;
					 }

						
						rowClientCal++;
				 
				 }
			 }
		
	return rowClientCal;
	}
    
    public static void genrateLuminExportFile(String exportFilepath) {
        Endpoint connection = DBConnectionProvider.getInstance().getDBConnection("InHouse");
        Exchange exchange = connection.createExchange();
        DecimalFormat df = new DecimalFormat("#");
        df.setMaximumFractionDigits(10);
        String query = "";
        CamelContext context = exchange.getContext();
        Endpoint endpoint = context.getEndpoint("direct:InHouse");
        ProducerTemplate template = context.createProducerTemplate();
        exchange = endpoint.createExchange();
        System.out.println("======================================" + exchange.toString());
        Exchange Queryout = null;
        List<Map<String, Object>> Queryresult = null;
        int i = 0, sheetCount=1;
        String fileName = exportFilepath;
        int rowMonthCal = 0;
        int columnMonthCal = 0;
        int rowClientCal = 0;
        int colClientCal=0;
        WritableSheet sheetMonthCal = null;
        WritableSheet clientSheet = null;
        WritableFont headerFont = null;
        WritableCellFormat headerFormat = null;
        WritableCellFormat subHeaderFormat = null;
        WritableCellFormat columnHeaderFormat = null;
        WritableFont textFont = null;
        WritableCellFormat textFormat = null;
        WritableCellFormat cellFormat = null;
        WritableCellFormat numberFormat = null;
        WritableCellFormat stringFormat = null;
        NumberFormat decimalNo = null;
        Label label = null;
        Number numberCell = null;
        String codeValue = "";
        WritableWorkbook workbook = null;
        try {
            File file = new File(fileName);
            if (file.exists()) {
                file.delete();
            }
            WorkbookSettings wbSettings = new WorkbookSettings();
            wbSettings.setRationalization(false);
            workbook = Workbook.createWorkbook(file, wbSettings);
            sheetMonthCal = workbook.createSheet("Operations Index", 0);
        }
        catch (Exception e) {
            System.out.println("=Exception Found in reading file====================>" + fileName + "\n" + stackTraceToString(e));
            e.printStackTrace();
        }
        rowMonthCal = 0;
        try {
            headerFont = new WritableFont(WritableFont.TAHOMA, 10, WritableFont.BOLD);
            headerFont.setColour(Colour.BLACK);
            headerFormat = new WritableCellFormat(headerFont);
            headerFormat.setBackground(Colour.GREY_40_PERCENT);
            headerFormat.setWrap(true);
            headerFormat.setVerticalAlignment(VerticalAlignment.TOP);
            headerFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
            decimalNo = new NumberFormat("###,###.00");
            numberFormat = new WritableCellFormat(decimalNo);
            stringFormat = new WritableCellFormat();
            stringFormat.setAlignment(Alignment.RIGHT);
            textFont = new WritableFont(WritableFont.TAHOMA, 10);
            textFormat = new WritableCellFormat(textFont);
            textFormat.setWrap(true);
            WritableCellFormat cellFormatEmail = new WritableCellFormat();
            cellFormatEmail.setWrap(true);
            cellFormat = new WritableCellFormat(headerFont);
            cellFormat.setBackground(Colour.GREY_40_PERCENT);
            cellFormat.setAlignment(jxl.write.Alignment.CENTRE);
            cellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
            cellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
            cellFormat.setWrap(true);
            
            subHeaderFormat = new WritableCellFormat(headerFont);
            subHeaderFormat.setBackground(Colour.GREY_25_PERCENT);
            subHeaderFormat.setWrap(true);
            subHeaderFormat.setVerticalAlignment(VerticalAlignment.TOP);
            subHeaderFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
            
            columnHeaderFormat = new WritableCellFormat(headerFont);
            columnHeaderFormat.setBackground(Colour.LIGHT_TURQUOISE);
            columnHeaderFormat.setWrap(true);
            columnHeaderFormat.setVerticalAlignment(VerticalAlignment.TOP);
            columnHeaderFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
            
            int heightInPoints = 780;
            int heightInPointsSubHeader=500;
            int n = 3;

            for (int j = 0; j < n; ++j) {
                sheetMonthCal.setColumnView(j, 29);
            }
            sheetMonthCal.setRowView(rowMonthCal, heightInPoints);
            if (rowMonthCal == 0) {
                sheetMonthCal.mergeCells(0, rowMonthCal, n, rowMonthCal);
                label = new Label(0, 0, "Lumin Leads Report", headerFormat);
                sheetMonthCal.addCell(label);
                ++rowMonthCal;
            }
            if (rowMonthCal == 1) {
                columnMonthCal = 0;

                

                
                {
                sheetMonthCal.mergeCells(columnMonthCal, rowMonthCal, columnMonthCal+3, rowMonthCal); // Modules


                }
                

                {
                    label = new Label(0, 1, "Customer wise Leads data", headerFormat);
                    sheetMonthCal.addCell(label);

                }
            }
            if (++rowMonthCal == 2) {
            	columnMonthCal = 0;
            	sheetMonthCal.setRowView(rowMonthCal, heightInPoints);
                label = new Label(columnMonthCal, rowMonthCal, "Customer", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Billing Month", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Lead Count", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, "Appointment Count", headerFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                //insert if

               
            }
            ++rowMonthCal;
            StringBuffer luminReportData = new StringBuffer(QueryBean.getLuminReportSql(Constants.dateTo, Constants.dateFrom));
            System.out.println("==============================" + (Object)luminReportData);
            exchange.getIn().setBody(luminReportData.toString());
            Queryout = template.send(endpoint, exchange);
            Queryresult = Queryout.getOut().getBody(List.class);
            i = Queryresult.size();
            WritableHyperlink hl;
            final int clientSheetRowOffset=5, clientSheetDataWriteRowOffset=8; // row offset for client sheet
            final int fsDataOffset=0,libDataOffset = 5, libLinkPollDataOffset=9,libLinkDataOffset=9,libPollDataOffset=10, finDataOffset = 12, finFinanceDataOffset=12,
            		finNonFinanceDataOffset=15, finPLDataOffset=18, finLocDataOffset=21, crmDataOffset=27, crmLocBasedDataOffset=27,
            		fieldOpsDataOffset=36, fieldOpsLocDataOffset=36,
            		fimDataOffset=41, fimLocDataOffset = 41, fimCampaignDataOffset=46; // column offset for client sheet according to modules
            ByteArrayInputStream bais;
            ObjectInputStream ins;
            ArrayList<HashMap<String,Object>> beanHM;
            for (int k = 0; k < i; ++k) {
                Map<String, Object> row1 = Queryresult.get(k);
                /* client sheet write start */
                rowClientCal=0;
                colClientCal=0;
                
				clientSheet = workbook.createSheet((String)row1.get("CLIENT_NAME"), sheetCount);
				for (int j = 0; j <= 100; ++j) {
					clientSheet.setColumnView(j, 25);
				}
				sheetCount++; //increment sheet for next client
				
				label=new Label(colClientCal, rowClientCal, "Customer Name", headerFormat);
				clientSheet.addCell(label);
				colClientCal++;
				label=new Label(colClientCal, rowClientCal, (String)row1.get("CLIENT_NAME"), textFormat);
				clientSheet.addCell(label);
				
				colClientCal++;
				label=new Label(colClientCal, rowClientCal, "<- Main Sheet", textFormat);
				clientSheet.addCell(label);	
				hl = new WritableHyperlink(colClientCal, rowClientCal,"<- Main Sheet", sheetMonthCal, 0, 0);
				clientSheet.addHyperlink(hl);
				colClientCal=0;



				

				/* FS Starts*/
				
				rowClientCal = clientSheetRowOffset;
				colClientCal=fsDataOffset;
				clientSheet.setRowView(rowClientCal, heightInPoints);
				clientSheet.mergeCells( colClientCal,rowClientCal, colClientCal+10, rowClientCal); // Hub Header
				label=new Label(colClientCal, rowClientCal, "FS", headerFormat);
				clientSheet.addCell(label);
				
				rowClientCal++; 
				clientSheet.setRowView(rowClientCal, heightInPointsSubHeader);
				clientSheet.mergeCells( colClientCal,rowClientCal, colClientCal+10, rowClientCal);
				label=new Label(colClientCal, rowClientCal, "Leads Data", subHeaderFormat);
				clientSheet.addCell(label);

				rowClientCal++;

				// training data starts
				String[] fsColumnHeaders={"Date","First Name","Last Name","Logged By","Subject", "Comments","Call Status", "Communication Type","Time","Type","Cancel status (hours)"};
				String[] fsDataHeaders={"CALL_DATE","FIRST_NAME","LAST_NAME","LOGGED_BY","SUBJECT","COMMENTS","CALL_STATUS_NAME","CALL_TYPE_NAME","TIME","TYPE","CANCEL_STATUS"};
				 generateSingleDatasetForLumin(clientSheet, columnHeaderFormat, textFormat, numberFormat, clientSheetRowOffset,
						clientSheetDataWriteRowOffset, fsDataOffset, row1.get("LUMIN_LEAD_DATA"),fsColumnHeaders,fsDataHeaders);
				// training data ends
				
				/* Fs Ends*/
				
				
				/* client sheet write end */
				
                columnMonthCal = 0;
                sheetMonthCal.setColumnView(columnMonthCal, 30);
                label = new Label(columnMonthCal, rowMonthCal, row1.get("CLIENT_NAME") + "", textFormat);
                sheetMonthCal.addCell(label);
				hl = new WritableHyperlink(columnMonthCal, rowMonthCal,(String)row1.get("CLIENT_NAME") , clientSheet, 0, 0);
				sheetMonthCal.addHyperlink(hl);
                ++columnMonthCal;

                
                label = new Label(columnMonthCal, rowMonthCal, row1.get("FS_LUMIN_DATE") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("FS_LUMINLEAD_COUNT") + "", textFormat);
                sheetMonthCal.addCell(label);
                ++columnMonthCal;
                label = new Label(columnMonthCal, rowMonthCal, row1.get("FS_LUMINCALL_COUNT") + "", textFormat);
                sheetMonthCal.addCell(label);
                rowMonthCal++;
            }
            try {
            workbook.write();
            workbook.close();
            
            String dbFileName = "";
			if (fileName != null) {
				String[] temp = fileName.split("GenericDataExporter/");

				if (temp != null && temp.length > 1) {
					dbFileName = temp[1];
				} else {
					dbFileName = "";
				}

			}

			if (!"".equals(dbFileName)) {
				String flag = "0";
				String hostName = "";
				String userName = "";
				String password = "";
				String ftpFoldername = "./CustomerOpsIndexReport/";

				query = "SELECT FTP_SERVER_ID, HOST_NAME, USER_NAME, PASSWORD, FLAG, HOST_URL, IS_DEFAULT,DIRECTORY FROM FILE_UPLOAD_OPS_FTP_SERVER";
				exchange.getIn().setBody(query.toString());

				Queryout = template.send(endpoint, exchange);
				Queryresult = Queryout.getOut().getBody(List.class);
				i = Queryresult.size();

				for (int j = 0; j < i; j++) {
					Map<String, Object> row1 = Queryresult.get(j);
					if (row1.get("HOST_NAME") != null) {
						hostName = row1.get("HOST_NAME") + "";
					} else {
						hostName = "";
					}
					if (row1.get("USER_NAME") != null) {
						userName = row1.get("USER_NAME") + "";
					} else {
						userName = "";
					}

					if (row1.get("PASSWORD") != null) {
						password = row1.get("PASSWORD") + "";
					} else {
						password = "";
					}

					if (row1.get("FLAG") != null) {
						flag = row1.get("FLAG") + "";
					} else {
						flag = "0";
					}
					if (row1.get("DIRECTORY") != null) {
						ftpFoldername = row1.get("DIRECTORY") + "";
					} else {
						ftpFoldername = "./CustomerOpsIndexReport/";
					}
				}
				boolean isFileUpload=false;
				isFileUpload = uploadFilesToFtpServer(fileName, dbFileName, flag, hostName, userName, password,ftpFoldername);
				System.out.println("File Uploaded ::::"+isFileUpload);
						
            }}
			catch(Exception e)
            {
            	e.printStackTrace();
            }
        }
         
        catch (Exception e) {
            System.out.println("=Exception Found in writing workbook ====================>" + stackTraceToString(e));
            e.printStackTrace();
        }
    }    
    
    
    
    
        
        public static boolean uploadFilesToFtpServer(String basePath, String UserFileName, String flag, String hostName,
    			String userName, String password, String ftpFoldername) {
    		boolean isCopied = false;

    		FileTransferClient ftp = null;
    		if (!"1".equals(flag)) {
    			return false;
    		}

    		String UserInformationFile = basePath;

    		System.out.println("=======UserInformationFile=========" + UserInformationFile);
    		System.out.println("=======UserFileName=========" + UserFileName);
    		try {
    			ftp = new FileTransferClient();

    			// set FTP server settings
    			ftp.setTimeout(100000);
    			ftp.setRemoteHost(hostName);
    			ftp.setUserName(userName);
    			ftp.setPassword(password);

    			// connect to the server
    			ftp.connect();
    			

    			// Check valid files put on FTP server for the
    			try {
    				ftp.uploadFile(UserInformationFile, ftpFoldername + UserFileName);
    				isCopied = true;
    			} catch (Exception e) {
    				System.out.println("=uploadFilesToFtpServer====================>" + stackTraceToString(e));
    				e.printStackTrace();
    			}

    		} catch (Exception e) {
    			System.out.println("=uploadFilesToFtpServer====================>" + stackTraceToString(e));
    			e.printStackTrace();
    			isCopied = false;

    			try {
    				ftp.disconnect();
    			} catch (Exception e2) {
    				System.out.println("=uploadFilesToFtpServer====================>" + stackTraceToString(e2));
    				e2.printStackTrace();
    			}
    		}

    		try {
    			// boolean isDeletedDir = new File(tempDirName).delete();
    		} catch (Exception e) {
    			System.out.println("=uploadFilesToFtpServer====================>" + stackTraceToString(e));
    			e.printStackTrace();
    		}

    		return isCopied;
    	}


	private static void generateSingleDataset(WritableSheet clientSheet, WritableCellFormat headerFormat,
			WritableCellFormat textFormat, WritableCellFormat numberFormat, final int clientSheetRowOffset,
			final int clientSheetDataWriteRowOffset, final int dataHeaderOffset, Object dataColumn, String[] dataColumnHeaders, String[] dataHeaders)
			throws WriteException, RowsExceededException, IOException, ClassNotFoundException {
		int rowClientCal;
		int colClientCal;
		Label label;
		ByteArrayInputStream bais;
		ObjectInputStream ins;
		ArrayList<HashMap<String, Object>> beanHM;
		colClientCal=dataHeaderOffset;
		 rowClientCal=clientSheetRowOffset+2; //data headers row
		 for(String dch:dataColumnHeaders)
		 {
			label=new Label(colClientCal, rowClientCal, dch, headerFormat);
			clientSheet.addCell(label);
			colClientCal++;
		 }
		 label=new Label(colClientCal, rowClientCal, "", headerFormat);
		 clientSheet.addCell(label);

			if(dataColumn !=null)
			{
			
			bais = new ByteArrayInputStream((byte[]) dataColumn);
			
			ins = new ObjectInputStream(bais);
			beanHM = null; 
			beanHM = (ArrayList<HashMap<String, Object>>) ins.readObject();
			//System.out.println(beanHM);
			 
			 colClientCal=dataHeaderOffset;
			 rowClientCal=clientSheetDataWriteRowOffset;
			 if(beanHM!=null)
			 {
				 colClientCal=dataHeaderOffset;
				 for(HashMap<String,Object> data : beanHM)
				 {
					 colClientCal=dataHeaderOffset;
					 for(String dh:dataHeaders)
					 {
						 //System.out.println("data::get(dh)::::"+data.get(dh));
						label=new Label(colClientCal, rowClientCal, data.get(dh)==null?"0.0":data.get(dh)+"", textFormat);
						clientSheet.addCell(label);
						colClientCal++;
					 }

						
						rowClientCal++;
				 
				 }
			 }
		}
	}
	
	
	private static void generateSingleDatasetForLumin(WritableSheet clientSheet, WritableCellFormat headerFormat,
			WritableCellFormat textFormat, WritableCellFormat numberFormat, final int clientSheetRowOffset,
			final int clientSheetDataWriteRowOffset, final int dataHeaderOffset, Object dataColumn, String[] dataColumnHeaders, String[] dataHeaders)
			throws WriteException, RowsExceededException, IOException, ClassNotFoundException {
		int rowClientCal;
		int colClientCal;
		Label label;
		ByteArrayInputStream bais;
		ObjectInputStream ins;
		ArrayList<HashMap<String, Object>> beanHM;
		colClientCal=dataHeaderOffset;
		 rowClientCal=clientSheetRowOffset+2; //data headers row
		 for(String dch:dataColumnHeaders)
		 {
			label=new Label(colClientCal, rowClientCal, dch, headerFormat);
			clientSheet.addCell(label);
			colClientCal++;
		 }


			if(dataColumn !=null)
			{
			
			bais = new ByteArrayInputStream((byte[]) dataColumn);
			
			ins = new ObjectInputStream(bais);
			beanHM = null; 
			beanHM = (ArrayList<HashMap<String, Object>>) ins.readObject();
			//System.out.println(beanHM);
			 
			 colClientCal=dataHeaderOffset;
			 rowClientCal=clientSheetDataWriteRowOffset;
			 if(beanHM!=null)
			 {
				 colClientCal=dataHeaderOffset;
				 for(HashMap<String,Object> data : beanHM)
				 {
					 colClientCal=dataHeaderOffset;
					 for(String dh:dataHeaders)
					 {
						 //System.out.println("data::get(dh)::::"+data.get(dh));
						label=new Label(colClientCal, rowClientCal, data.get(dh)==null?"0.0":data.get(dh)+"", textFormat);
						clientSheet.addCell(label);
						colClientCal++;
					 }

						
						rowClientCal++;
				 
				 }
			 }
		}
	}
	
	
	
	
	private static void generateSingleDataset(WritableSheet clientSheet, WritableCellFormat headerFormat,
			WritableCellFormat textFormat, WritableCellFormat numberFormat, final int clientSheetRowOffset,
			final int clientSheetDataWriteRowOffset, final int dataHeaderOffset, Object dataColumn, String[] dataColumnHeaders, String[] dataHeaders, String processingClassName)
			throws WriteException, RowsExceededException, IOException, ClassNotFoundException {
		int rowClientCal;
		int colClientCal;
		Label label;
		ByteArrayInputStream bais;
		ObjectInputStream ins;
		ArrayList<HashMap<String, Object>> beanHM;
		colClientCal=dataHeaderOffset;
		 rowClientCal=clientSheetRowOffset+2; //data headers row
		 for(String dch:dataColumnHeaders)
		 {
			label=new Label(colClientCal, rowClientCal, dch, headerFormat);
			clientSheet.addCell(label);
			colClientCal++;
		 }
		 label=new Label(colClientCal, rowClientCal, "", headerFormat);
		 clientSheet.addCell(label);

			if(dataColumn !=null)
			{
			
			bais = new ByteArrayInputStream((byte[]) dataColumn);
			
			ins = new ObjectInputStream(bais);
			beanHM = null; 
			beanHM = (ArrayList<HashMap<String, Object>>) ins.readObject();
			//System.out.println(beanHM);
			 
			 colClientCal=dataHeaderOffset;
			 rowClientCal=clientSheetDataWriteRowOffset;
			 
 			Class<DataProcessor> processingClass=(Class<DataProcessor>) Class.forName(processingClassName);
 			DataProcessor processor;
			try {
				processor = (DataProcessor)processingClass.getConstructor().newInstance();
				beanHM=processor.postProcessing(beanHM);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			 
			 if(beanHM!=null)
			 {
				 colClientCal=dataHeaderOffset;
				 for(HashMap<String,Object> data : beanHM)
				 {
					 colClientCal=dataHeaderOffset;
					 for(String dh:dataHeaders)
					 {
						 //System.out.println("data::get(dh)::::"+data.get(dh));
						label=new Label(colClientCal, rowClientCal, data.get(dh)==null?"0.0":data.get(dh)+"", textFormat);
						clientSheet.addCell(label);
						colClientCal++;
					 }

						
						rowClientCal++;
				 
				 }
			 }
		}
	}
    
    public static String stackTraceToString(Throwable e) {
        StringBuilder sb = new StringBuilder();
        for (StackTraceElement element : e.getStackTrace()) {
            sb.append(element.toString());
            sb.append("\n");
        }
        return sb.toString();
    }
}
