package com.app.util;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.camel.Endpoint;
import org.apache.camel.Exchange;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

public class SystemUsageUtility 
{
	public static String dateFrom;
	public static String dateTo;
	
	
	 public static ArrayList<String> getActiveBW(ProducerTemplate template,Endpoint endpoint,Exchange exchange,String billableLevel)
	 {	
	    	ArrayList<String[]> data = new ArrayList<String[]>();
	    	ArrayList<String> data1				= new ArrayList<String>();
	    	String franchiseeNo = "";
	    	try
	    	{
	    		StringBuffer query = new StringBuffer("SELECT COUNT(FRANCHISEE_NO) AS COUNT, GROUP_CONCAT(FRANCHISEE_NO) AS FRANCHISEE_NO FROM FRANCHISEE WHERE");
	    		query.append("( (IS_FRANCHISEE='Y' AND STATUS IN (0,4) AND DEACTIVATION_DATE >= '").append(dateFrom).append(" 00:00:00' AND DEACTIVATION_DATE <= '").append(dateTo).append(" 23:59:59')");
	    		query.append(" OR (IS_FRANCHISEE='Y' AND STATUS IN (1,3) AND CREATION_DATE <= '").append(dateTo).append(" 23:59:59') OR (IS_FRANCHISEE='Y' AND STATUS IN (0,4) AND DEACTIVATION_DATE > '").append(dateTo).append(" 23:59:59' AND CREATION_DATE <= '"+dateTo+" 23:59:59') )");
	    		if("L".equals(billableLevel) || "B".equals(billableLevel)  )
	    		{
	    			query.append(" AND IS_BILLABLE ='1'");
	    		}
	    		exchange  = endpoint.createExchange();
	    		exchange.getIn().setBody(query);
	    		System.out.println(query);
	    		Exchange out = template.send(endpoint, exchange);
	            List<Map<String, Object>> result = out.getOut().getBody(List.class);
		        int i =result.size();
	        	for(int j =0;j<i;j++)
		        {
	        		Map<String, Object> row = result.get(j);
	        		if(row.get("FRANCHISEE_NO")!=null)
	        		{	
	        			if(row.get("FRANCHISEE_NO") instanceof byte[])
	        			{
	        				final String pass = new String((byte[])row.get("FRANCHISEE_NO"));
		        			franchiseeNo =  pass;
	        			}
	        			else
	        			{
	        				franchiseeNo = (String)row.get("FRANCHISEE_NO");
	        			}
	        				
	        			
	        		}
	        		else
	        		{
	        			franchiseeNo = "";
	        		}
		    	}
	        	StringBuffer sQuery	= new StringBuffer("SELECT COUNT(DISTINCT U.USER_NO) AS COUNT,U.USER_LEVEL FROM FRANCHISEE F,USERS U"
	    			+ " LEFT JOIN FRANCHISEE_USERS FU ON U.USER_IDENTITY_NO = FU.FRANCHISEE_USER_NO "
	    			+ "  LEFT JOIN ( SELECT MAX(CHANGE_DATE) AS CHANGE_DATE,USER_NO,REMARK FROM USER_STATUS_CHANGED WHERE (REMARK='Deactivated' OR REMARK='Deleted' OR REMARK='Deactivated From Franchisor' OR REMARK='deactivateed By WS' OR REMARK='deleteed By WS' OR REMARK='Franchise User has logged out forcefully and deactivated' ) GROUP BY USER_NO) AS USC ON U.USER_NO=USC.USER_NO LEFT JOIN  USER_STATUS_CHANGED USC1 ON U.USER_NO=USC1.USER_NO AND (USC1.REMARK='Activated' OR USC1.REMARK='Activated From Franchisor' OR USC1.REMARK='activateed By WS') WHERE U.USER_ID!='adm' AND U.CREATION_DATE<='"+dateTo+" 23:59:59' AND ( (USC.CHANGE_DATE IS NULL OR USC.CHANGE_DATE>='"+dateFrom+" 00:00:00') OR ( USC1.CHANGE_DATE IS NOT NULL AND  USC1.CHANGE_DATE<='"+dateTo+" 23:59:59' AND USC.CHANGE_DATE<=USC1.CHANGE_DATE)  OR (USC.CHANGE_DATE IS NULL OR USC.CHANGE_DATE > '"+dateTo+"')) "); //Marcos_ENH_LOGIN_ANALYTICS
	        	sQuery.append(" AND U.USER_NO!=1 AND ((U.USER_LEVEL=1 AND F.IS_FRANCHISEE='Y' ");
	        	if(franchiseeNo != null && franchiseeNo.trim().length() != 0 && !franchiseeNo.trim().equalsIgnoreCase("null")) 
	        	{
	    			sQuery.append(" AND F.FRANCHISEE_NO IN ("+franchiseeNo+") ");
	        	}
	        	sQuery.append(" ) OR U.USER_LEVEL!=1) ");	
	        	sQuery.append("AND F.FRANCHISEE_NO = U.FRANCHISEE_NO AND ((CASE WHEN U.USER_LEVEL IN(1) THEN FU.EMAIL_ID ELSE F.EMAIL_ID END) NOT LIKE('%franconnect%')) ");
	        	if("U".equals(billableLevel) || "B".equals(billableLevel)  )
	    		{
	    			sQuery.append(" AND U.IS_BILLABLE ='1'");
	    		}
	    		sQuery.append("   GROUP BY USER_LEVEL");
	        	exchange  = endpoint.createExchange();
	        	
	        	System.out.println(" userQuery : "+sQuery +" endpoint : "+endpoint);
	        	
	        	exchange.getIn().setBody(sQuery);
	    		//System.out.println("endpoint : "+endpoint+" query for count : "+sQuery );
		    	out = template.send(endpoint, exchange);
		        result = out.getOut().getBody(List.class);
		        if(result!=null)
		        {
			        i =result.size();
		        	for(int j =0;j<i;j++)
			        {
		        		Map<String, Object> row = result.get(j);
		    			String[] details	= new String[2];
		    			if(row.get("USER_LEVEL") instanceof Integer)
		    			{
		    				details[0]			=  new String(""+(Integer)row.get("USER_LEVEL"));
		    			}
		    			else
		    			{
		    				details[0]			= (String)row.get("USER_LEVEL");
		    			}
		    			{
		    				if (row.get("COUNT") instanceof Long) 
		    				details[1] = String.valueOf((Long)row.get("COUNT")); 
						}
		    			data.add(details);
		    		}
	        	}
		        String dCorporate		= "0";
        		String dFranchise		= "0";
        		String dRegional		= "0";
        		String dDivisonal 		= "0";
	        	if(data!=null)
	        	{
	        		int size=data.size();
	        		
	        		int dIntRegional		= 0;
	        		
	        		for(int k=0;k<size;k++)
	        		{
	        			String[] dataList		= (String[])data.get(k);
	        			if( dataList[0].equals("0"))
	        			{
	        				dCorporate		= dataList[1];
	        			}
	        			if( dataList[0].equals("1"))
	        			{
	        				dFranchise		= dataList[1];
	        			}
	        			if( dataList[0].equals("2") || dataList[0].equals("3"))
	        			{
	        				dRegional		= dataList[1];
	        				dIntRegional	= dIntRegional + Integer.parseInt(dRegional);
	        			}
	        			if( dataList[0].equals("6") )
	        			{
	        				dDivisonal		= dataList[1];
	        			}
	        		}
	        		dRegional=""+dIntRegional;
	        	}
	        	data1.add(dCorporate);
        		data1.add(dFranchise);
        		data1.add(dRegional);
        		data1.add(dDivisonal);
        		data=null;
	    	}
	    	catch(Exception e)
	    	{
	    		System.out.println(" Endpoint in exception : "+endpoint);
	    		e.printStackTrace();
	    	}
	    	return data1;
	   }
	 	
	 
	 public static ArrayList<String> getTestActiveBW(ProducerTemplate template,Endpoint endpoint,Exchange exchange,String billableLevel)
	 {	
	    	ArrayList<String[]> data = new ArrayList<String[]>();
	    	ArrayList<String> data1				= new ArrayList<String>();
	    	String franchiseeNo = "";
	    	try
	    	{
	    		StringBuffer query = new StringBuffer("SELECT COUNT(FRANCHISEE_NO) AS COUNT, GROUP_CONCAT(FRANCHISEE_NO) AS FRANCHISEE_NO FROM FRANCHISEE WHERE");
	    		query.append("( (IS_FRANCHISEE='Y' AND STATUS IN (0,4) AND DEACTIVATION_DATE >= '").append(dateFrom).append(" 00:00:00' AND DEACTIVATION_DATE <= '").append(dateTo).append(" 23:59:59')");
	    		query.append(" OR (IS_FRANCHISEE='Y' AND STATUS IN (1,3) AND CREATION_DATE <= '").append(dateTo).append(" 23:59:59') OR (IS_FRANCHISEE='Y' AND STATUS IN (0,4) AND DEACTIVATION_DATE > '").append(dateTo).append(" 23:59:59' AND CREATION_DATE <= '"+dateTo+" 23:59:59') )");
	    		if("L".equals(billableLevel) || "B".equals(billableLevel)  )
	    		{
	    			query.append(" AND IS_BILLABLE ='1'");
	    		}
	    		exchange  = endpoint.createExchange();
	    		exchange.getIn().setBody(query);
	    		Exchange out = template.send(endpoint, exchange);
		        List<Map<String, Object>> result = out.getOut().getBody(List.class);
		        if(result!=null)
		        {
			        int i =result.size();
			        for(int j =0;j<i;j++)
			        {
		        		Map<String, Object> row = result.get(j);
		        		if(row.get("FRANCHISEE_NO")!=null)
		        		{	
		        			if(row.get("FRANCHISEE_NO") instanceof byte[])
		        			{
		        				final String pass = new String((byte[])row.get("FRANCHISEE_NO"));
			        			franchiseeNo =  pass;
		        			}
		        			else
		        			{
		        				franchiseeNo = (String)row.get("FRANCHISEE_NO");
		        			}
		        		}
		        		else
		        		{
		        			franchiseeNo =  "";
		        		}
			    	}
		        }
		        StringBuffer sQuery	= new StringBuffer("SELECT COUNT(DISTINCT U.USER_NO) AS COUNT,U.USER_LEVEL FROM FRANCHISEE F,USERS U "
	    			+ " LEFT JOIN FRANCHISEE_USERS FU ON U.USER_IDENTITY_NO = FU.FRANCHISEE_USER_NO "
	    			+ "LEFT JOIN ( SELECT MAX(CHANGE_DATE) AS CHANGE_DATE,USER_NO,REMARK FROM USER_STATUS_CHANGED WHERE (REMARK='Deactivated' OR REMARK='Deleted' OR REMARK='Deactivated From Franchisor' OR REMARK='deactivateed By WS' OR REMARK='deleteed By WS' OR REMARK='Franchise User has logged out forcefully and deactivated' ) GROUP BY USER_NO) AS USC ON U.USER_NO=USC.USER_NO LEFT JOIN  USER_STATUS_CHANGED USC1 ON U.USER_NO=USC1.USER_NO AND (USC1.REMARK='Activated' OR USC1.REMARK='Activated From Franchisor' OR USC1.REMARK='activateed By WS') WHERE U.USER_ID!='adm' AND U.CREATION_DATE<='"+dateTo+" 23:59:59' AND ( (USC.CHANGE_DATE IS NULL OR USC.CHANGE_DATE>='"+dateFrom+" 00:00:00') OR ( USC1.CHANGE_DATE IS NOT NULL AND  USC1.CHANGE_DATE<='"+dateTo+" 23:59:59' AND USC.CHANGE_DATE<=USC1.CHANGE_DATE)  OR (USC.CHANGE_DATE IS NULL OR USC.CHANGE_DATE > '"+dateTo+"')) "); //Marcos_ENH_LOGIN_ANALYTICS
		        sQuery.append(" AND U.USER_NO!=1 AND ((U.USER_LEVEL=1 AND F.IS_FRANCHISEE='Y' ");
		        if(franchiseeNo != null && franchiseeNo.trim().length() != 0 && !franchiseeNo.trim().equalsIgnoreCase("null")) 
		        {
	    			sQuery.append(" AND F.FRANCHISEE_NO IN ("+franchiseeNo+") ");
		        }
		        sQuery.append(" ) OR U.USER_LEVEL!=1) ");	
		        sQuery.append("AND F.FRANCHISEE_NO = U.FRANCHISEE_NO"
	    			+" AND ((CASE WHEN U.USER_LEVEL IN(1) THEN FU.EMAIL_ID ELSE F.EMAIL_ID END) LIKE('%franconnect%'))");	
		        
		        if("U".equals(billableLevel) || "B".equals(billableLevel)  )
		    	{
		    		sQuery.append(" AND U.IS_BILLABLE ='1'");
		    	}
		    	sQuery.append("   GROUP BY USER_LEVEL");
		        	
		        exchange  = endpoint.createExchange();
	    		exchange.getIn().setBody(sQuery);
		    	out = template.send(endpoint, exchange);
		        result = out.getOut().getBody(List.class);
		        if(result!=null)
		        {
			        int i =result.size();
		        	for(int j =0;j<i;j++)
			        {
		        		Map<String, Object> row = result.get(j);
		    			String[] details	= new String[2];
		    			if(row.get("USER_LEVEL") instanceof Integer)
		    			{
		    				details[0]			= new String(""+(Integer)row.get("USER_LEVEL"));
		    			}
		    			else
		    			{
		    				details[0]			= (String)row.get("USER_LEVEL");
		    			}
		    			if (row.get("COUNT") instanceof Long) 
		    			{
		    				details[1] = String.valueOf((Long)row.get("COUNT")); 
						};
		    			data.add(details);
		    		}
	        	}
		        String dCorporate		= "0";
        		String dFranchise		= "0";
        		String dRegional		= "0";
        		String dDivisonal 		= "0";
        		
	        	if(data!=null)
	        	{
	        		int size=data.size();
	        		int dIntRegional		= 0;
	
	        		for(int k=0;k<size;k++)
	        		{
	        			String[] dataList		= (String[])data.get(k);
	        			if( dataList[0].equals("0"))
	        			{
	        				dCorporate		= dataList[1];
	        			}	
	        			if( dataList[0].equals("1"))
	        			{
	        				dFranchise		= dataList[1];
	        			}
	        			if( dataList[0].equals("2") || dataList[0].equals("3"))
	        			{
	        				dRegional		= dataList[1];
	        				dIntRegional	= dIntRegional + Integer.parseInt(dRegional);
	        			}
	        			if( dataList[0].equals("6") )
	        			{
	        				dDivisonal		= dataList[1];
	        			}
	        		}
	        		dRegional=""+dIntRegional;
	        		data1.add(dCorporate);
	        		data1.add(dFranchise);
	        		data1.add(dRegional);
	        		data1.add(dDivisonal);
	        		data=null;
	        	}
	    	}
	    	catch(Exception e)
	    	{
	    		System.out.println(" Endpoint in exception : "+endpoint);
	    		e.printStackTrace();
	    	}
	    	return data1;
	 }
	 
	 
	 
	 public static ArrayList<String> getTotalSystemTime(ProducerTemplate template,Endpoint endpoint,Exchange exchange)
	 {	 
		 String timeDis1		= "00:00:00";
		 ArrayList<Long> sysTime = new ArrayList<Long>(); 
		 ArrayList<String> usageList = null;
	     long systemTime = 0;
         try
         {
        	 String queryTotal = "SELECT IFNULL(SUM(UNIX_TIMESTAMP(LOGOUT_DATE)),0)-IFNULL(SUM(UNIX_TIMESTAMP(LOGIN_DATE)),0) TIME FROM LOGIN_DETAILS WHERE (LOGIN_DATE>='"+dateFrom+" 00:00:00 ' AND LOGIN_DATE<='"+dateTo+" 23:59:59')";                   
        	 exchange  = endpoint.createExchange();
        	 exchange.getIn().setBody(queryTotal);
             Exchange out = template.send(endpoint, exchange);
            // System.out.println(queryTotal);
		     List<Map<String, Object>> result = out.getOut().getBody(List.class);
		     int i =result.size();
	         for(int j =0;j<i;j++)
		     {
	        	 Map<String, Object> row = result.get(j);
	        	if(row.get("TIME") instanceof BigDecimal)
	        	{
	        		sysTime.add((((BigDecimal)row.get("TIME")) ).longValue());
	        	}
	        	else 
	        	{
	        		sysTime.add((((Double)row.get("TIME")) ).longValue());
	        	}
		     }
	         
	         String queryAdmin = "SELECT IFNULL(SUM(UNIX_TIMESTAMP(LOGOUT_DATE)),0)-IFNULL(SUM(UNIX_TIMESTAMP(LOGIN_DATE)),0) TIME FROM LOGIN_DETAILS WHERE (LOGIN_DATE>='"+dateFrom+" 00:00:00 ' AND LOGIN_DATE<='"+dateTo+" 23:59:59') and USER_ID IN (1,1000)";
	         exchange  = endpoint.createExchange();
	         exchange.getIn().setBody(queryAdmin);
	         out = template.send(endpoint,exchange);
	         result = out.getOut().getBody(List.class);
		     i =result.size();
	         for(int j =0;j<i;j++)
		     {
	        	 Map<String, Object> row = result.get(j);
	        	 if(row.get("TIME") instanceof BigDecimal)
		        	{
		        		sysTime.add((((BigDecimal)row.get("TIME")) ).longValue());
		        	}
		        	else 
		        	{
		        		sysTime.add((((Double)row.get("TIME")) ).longValue());
		        	}
		     }
	         
	         String queryNetUsage = "SELECT IFNULL(SUM(UNIX_TIMESTAMP(LOGOUT_DATE)),0)-IFNULL(SUM(UNIX_TIMESTAMP(LOGIN_DATE)),0) TIME FROM LOGIN_DETAILS WHERE (LOGIN_DATE>='"+dateFrom+" 00:00:00 ' AND LOGIN_DATE<='"+dateTo+" 23:59:59') and USER_ID NOT IN (1,1000)";
	         exchange  = endpoint.createExchange();
	         exchange.getIn().setBody(queryNetUsage);
	         out = template.send(endpoint,exchange);
	         //System.out.println("net "+queryNetUsage);
	         result = out.getOut().getBody(List.class);
		     i =result.size();
	         for(int j =0;j<i;j++)
		     {
	        	 Map<String, Object> row = result.get(j);
	        	 
	        	 if(row.get("TIME") instanceof BigDecimal)
		        	{
		        		sysTime.add((((BigDecimal)row.get("TIME")) ).longValue());
		        	}
		        	else 
		        	{
		        		sysTime.add((((Double)row.get("TIME")) ).longValue());
		        	}
		     }
	         usageList = new ArrayList<String>();
	         Iterator<Long> sysTimeItr = sysTime.iterator();
	         while(sysTimeItr.hasNext())
	         {	
	        	timeDis1		= "00:00:00";
	        	systemTime = sysTimeItr.next();
	         	if(systemTime !=0)
				{
		            long k			= systemTime;
        			String  hour	= ""+(k/3600);
					k				= k%3600;
					String min		= ""+(k/60);
					String sec		= ""+(k%60);
					if(hour.length() == 1) hour = "0" + hour;
					if(min.length() == 1) min = "0" + min;
					if(sec.length() == 1) sec = "0" + sec;
					timeDis1 = hour+":"+min+":"+sec;
					
				}
	         	usageList.add(timeDis1);
	         }
         }
         catch(Exception e)
         {
        	 System.out.println(" Endpoint in exception : "+endpoint);
        	 e.printStackTrace();
         }
         return usageList;
	 }
	 
	 public static HashMap<String,String> getData(ProducerTemplate template,Endpoint endpoint,Exchange exchange)
	 {
		 HashMap<String,String> data = new HashMap<String,String>();
		 String activeCals="";
		 String uniqueCals="";
	  	 String extraCals = "";
	  	 String totalCals = "";
	  	 String fddCount = "";
	  	 StringBuffer mainQuery = null;
	  	 mainQuery = new StringBuffer(" SELECT COUNT(FRANCHISEE_NO) AS COUNT, GROUP_CONCAT(FRANCHISEE_NO) AS FRANCHISEE_NO FROM FRANCHISEE WHERE (IS_FRANCHISEE='Y' AND STATUS IN (0,4) AND DEACTIVATION_DATE >= '"+dateFrom+" 00:00:00' AND DEACTIVATION_DATE <= '"+dateTo+" 23:59:59') OR (IS_FRANCHISEE='Y' AND STATUS IN (1,3) AND CREATION_DATE <= '"+dateTo+" 23:59:59') OR (IS_FRANCHISEE='Y' AND STATUS IN (0,4) AND DEACTIVATION_DATE > '"+dateTo+" 23:59:59' AND CREATION_DATE <= '"+dateTo+" 23:59:59') ");
	  	 
	  	 StringBuffer mainQuery1 = null;
	  	 mainQuery1 = new StringBuffer(" SELECT COUNT(DISTINCT F.FRANCHISEE_NO) AS COUNT FROM FRANCHISEE F,USERS U LEFT JOIN ( SELECT MAX(CHANGE_DATE) AS CHANGE_DATE,USER_NO,REMARK FROM USER_STATUS_CHANGED WHERE (REMARK='Deactivated' OR REMARK='Deleted' OR REMARK='Deactivated From Franchisor' OR REMARK='deactivateed By WS' OR REMARK='deleteed By WS' ) GROUP BY USER_NO) AS USC ON U.USER_NO=USC.USER_NO LEFT JOIN  USER_STATUS_CHANGED USC1 ON U.USER_NO=USC1.USER_NO AND (USC1.REMARK='Activated' OR USC1.REMARK='Activated From Franchisor' OR USC1.REMARK='activateed By WS') JOIN USER_ROLES UR ON U.USER_NO = UR.USER_NO  WHERE U.USER_ID!='adm' AND U.CREATION_DATE<='"+dateTo+" 23:59:59' AND ( (USC.CHANGE_DATE IS NULL OR USC.CHANGE_DATE>='"+dateFrom+" 00:00:00') OR ( USC1.CHANGE_DATE IS NOT NULL AND  USC1.CHANGE_DATE<='"+dateTo+" 23:59:59' AND USC.CHANGE_DATE<=USC1.CHANGE_DATE) OR (USC.CHANGE_DATE IS NULL OR USC.CHANGE_DATE > '"+dateTo+" 23:59:59')) AND U.USER_NO!=1 AND U.USER_LEVEL = '1'  AND F.IS_FRANCHISEE='Y' AND F.FRANCHISEE_NO IN (SELECT FRANCHISEE_NO FROM FRANCHISEE WHERE (IS_FRANCHISEE='Y' AND STATUS IN (0,4) AND DEACTIVATION_DATE >= '"+dateFrom+" 00:00:00' AND DEACTIVATION_DATE <= '"+dateTo+" 23:59:59') OR (IS_FRANCHISEE='Y' AND STATUS IN (1,3) AND CREATION_DATE <= '"+dateTo+" 23:59:59') OR (IS_FRANCHISEE='Y' AND STATUS IN (0,4) AND DEACTIVATION_DATE > '"+dateTo+" 23:59:59' AND CREATION_DATE <= '"+dateTo+" 23:59:59')) AND F.FRANCHISEE_NO = U.FRANCHISEE_NO ");
		
	  	 StringBuffer mainQuery3 = null;
	  	 mainQuery3 = new StringBuffer(" SELECT IFNULL(SUM(IF(COUNT%5!=0 ,FLOOR(COUNT/5) ,FLOOR(COUNT/5 -1) )),0) AS COUNT FROM (SELECT COUNT(DISTINCT U.USER_NO) AS COUNT,F.FRANCHISEE_NO FROM FRANCHISEE F,USERS U LEFT JOIN ( SELECT MAX(CHANGE_DATE) AS CHANGE_DATE,USER_NO,REMARK FROM USER_STATUS_CHANGED WHERE (REMARK='Deactivated' OR REMARK='Deleted' OR REMARK='Deactivated From Franchisor' OR REMARK='deactivateed By WS' OR REMARK='deleteed By WS' ) AND CHANGE_DATE <= '"+dateTo+" 23:59:59' GROUP BY USER_NO) AS USC ON U.USER_NO=USC.USER_NO LEFT JOIN USER_STATUS_CHANGED USC1 ON U.USER_NO=USC1.USER_NO AND (USC1.REMARK='Activated' OR USC1.REMARK='Activated From Franchisor' OR USC1.REMARK='activateed By WS') WHERE F.FRANCHISEE_NO = U.FRANCHISEE_NO AND U.USER_ID!='adm' AND U.CREATION_DATE<='"+dateTo+" 23:59:59' AND U.USER_NO!=1 AND U.USER_LEVEL = '1' AND F.IS_FRANCHISEE='Y' AND ( (USC.CHANGE_DATE IS NULL OR USC.CHANGE_DATE>='"+dateFrom+" 00:00:00') OR ( USC1.CHANGE_DATE IS NOT NULL AND  USC1.CHANGE_DATE<='"+dateTo+" 23:59:59' AND USC.CHANGE_DATE<=USC1.CHANGE_DATE) OR (USC.CHANGE_DATE IS NULL OR USC.CHANGE_DATE > '"+dateTo+" 23:59:59')) AND EXISTS (SELECT F1.FRANCHISEE_NO FROM FRANCHISEE F1 WHERE ((F1.IS_FRANCHISEE='Y' AND F1.STATUS IN (0,4) AND F1.DEACTIVATION_DATE >= '"+dateFrom+" 00:00:00' AND F1.DEACTIVATION_DATE <= '"+dateTo+" 23:59:59') OR (F1.IS_FRANCHISEE='Y' AND F1.STATUS IN (1,3) AND F1.CREATION_DATE <= '"+dateTo+" 23:59:59') OR (F1.IS_FRANCHISEE='Y' AND F1.STATUS IN (0,4) AND F1.DEACTIVATION_DATE >= '"+dateTo+" 23:59:59' AND F1.CREATION_DATE <= '"+dateTo+" 23:59:59')) AND F1.FRANCHISEE_NO=F.FRANCHISEE_NO) AND F.FRANCHISEE_NO = U.FRANCHISEE_NO GROUP BY FRANCHISEE_NO HAVING COUNT > 5) A");
	  	
	  	 StringBuffer mainQuery4 = null;
	  	 mainQuery4 = new StringBuffer(" SELECT IFNULL(SUM(IF(COUNT%5!=0 ,FLOOR(COUNT/5 + 1) ,FLOOR(COUNT/5) )),0) AS COUNT FROM (SELECT COUNT(DISTINCT U.USER_NO) AS COUNT,F.FRANCHISEE_NO FROM FRANCHISEE F,USERS U LEFT JOIN ( SELECT MAX(CHANGE_DATE) AS CHANGE_DATE,USER_NO,REMARK FROM USER_STATUS_CHANGED WHERE (REMARK='Deactivated' OR REMARK='Deleted' OR REMARK='Deactivated From Franchisor' OR REMARK='deactivateed By WS' OR REMARK='deleteed By WS' ) GROUP BY USER_NO) AS USC ON U.USER_NO=USC.USER_NO LEFT JOIN  USER_STATUS_CHANGED USC1 ON U.USER_NO=USC1.USER_NO AND (USC1.REMARK='Activated' OR USC1.REMARK='Activated From Franchisor' OR USC1.REMARK='activateed By WS') JOIN USER_ROLES UR ON U.USER_NO = UR.USER_NO  WHERE U.USER_ID!='adm' AND U.CREATION_DATE<='"+dateTo+" 23:59:59' AND ( (USC.CHANGE_DATE IS NULL OR USC.CHANGE_DATE>='"+dateFrom+" 00:00:00') OR ( USC1.CHANGE_DATE IS NOT NULL AND  USC1.CHANGE_DATE<='"+dateTo+" 23:59:59' AND USC.CHANGE_DATE<=USC1.CHANGE_DATE) OR (USC.CHANGE_DATE IS NULL OR USC.CHANGE_DATE > '"+dateTo+" 23:59:59')) AND U.USER_NO!=1 AND U.USER_LEVEL = '1'  AND F.IS_FRANCHISEE='Y' AND F.FRANCHISEE_NO IN (SELECT FRANCHISEE_NO FROM FRANCHISEE WHERE (IS_FRANCHISEE='Y' AND STATUS IN (0,4) AND DEACTIVATION_DATE >= '"+dateFrom+" 00:00:00' AND DEACTIVATION_DATE <= '"+dateTo+" 23:59:59') OR (IS_FRANCHISEE='Y' AND STATUS IN (1,3) AND CREATION_DATE <= '"+dateTo+" 23:59:59') OR (IS_FRANCHISEE='Y' AND STATUS IN (0,4) AND DEACTIVATION_DATE >= '"+dateTo+" 23:59:59' AND CREATION_DATE <= '"+dateTo+" 23:59:59')) AND F.FRANCHISEE_NO = U.FRANCHISEE_NO GROUP BY FRANCHISEE_NO) A ");
		
	  	 StringBuffer mainQuery5 = null;
	  	 mainQuery5 = new StringBuffer("SELECT COUNT(*) AS UFOC_COUNT FROM UFOC A,UFOC_SEND_SCHEDULE B,FS_LEAD_DETAILS C WHERE A.UFOC_ID=B.UFOC_ID AND B.LEAD_ID=C.LEAD_ID AND SENT_DATE BETWEEN '"+dateFrom+" 00:00:00' AND '"+dateTo+" 23:59:59'");
	  	 try
	  	 {
		  	 exchange  = endpoint.createExchange();
			 exchange.getIn().setBody(mainQuery);
	         Exchange out = template.send(endpoint, exchange);
		     List<Map<String, Object>> result = out.getOut().getBody(List.class);
		     if(result!=null)
		     {
			     int i =result.size();
		         for(int j =0;j<i;j++)
			     {
		        	 Map<String, Object> row = result.get(j);
					activeCals = String.valueOf((Long)row.get("COUNT"));
					data.put("ACTIVE_CALS",activeCals);
				 }
	         }
	         exchange  = endpoint.createExchange();
	         exchange.getIn().setBody(mainQuery1);
	         out = template.send(endpoint, exchange);
		     result = out.getOut().getBody(List.class);
		     if(result!=null)
		     {
			     int i =result.size();
		         for(int j =0;j<i;j++)
			     {
		        	 Map<String, Object> row = result.get(j);
					 uniqueCals = String.valueOf((Long)row.get("COUNT"));
					 data.put("UNIQUE_CALS", uniqueCals);
				 }
		     }
		     
		     exchange  = endpoint.createExchange();
		     exchange.getIn().setBody(mainQuery3);
		     out = template.send(endpoint, exchange);
		     result = out.getOut().getBody(List.class);
		     if(result!=null)
		     {
		    	 int i =result.size();
		    	 for(int j =0;j<i;j++)
		    	 {
		    		 Map<String, Object> row = result.get(j);
		    		 if(row.get("COUNT") instanceof BigDecimal)
		    		 {
		    			 extraCals = String.valueOf((BigDecimal)row.get("COUNT"));
		    		 }
		    		 else
		    		 {
		    			 extraCals = String.valueOf((Double)row.get("COUNT"));
		    		 }
		    		 data.put("EXTRA_CALS", extraCals);
		    	 }
	        }
		     
	        exchange  = endpoint.createExchange();
	        exchange.getIn().setBody(mainQuery4);
	        out = template.send(endpoint, exchange);
		    result = out.getOut().getBody(List.class);
		    if(result!=null)
		    {
		    	int i =result.size();
		        for(int j =0;j<i;j++)
			    {
		        	Map<String, Object> row = result.get(j);
		        	if(row.get("COUNT") instanceof BigDecimal)
		        	{
		        		totalCals = String.valueOf((BigDecimal)row.get("COUNT"));
		        	}
		        	else
		        	{
		        		totalCals = String.valueOf((Double)row.get("COUNT"));
		        	}
		        	data.put("TOTAL_CALS", totalCals);
				}	
		    }
	        exchange  = endpoint.createExchange();
	        exchange.getIn().setBody(mainQuery5);
	        out = template.send(endpoint, exchange);
	        Exception ex = out.getException();
	        boolean isFailed = out.isFailed();
	        System.out.println("fdd failed :"+isFailed);
	        if(ex == null ) 
	        {
			    result = out.getOut().getBody(List.class);
			    if(result!=null)
			    {
				    int i =result.size();
				    for(int j =0;j<i;j++)
				    {
				    	Map<String, Object> row = result.get(j);
				    	fddCount = String.valueOf((Long)row.get("UFOC_COUNT"));
				    	data.put("FDD_COUNT",fddCount);
					}
			    }
	        }
	        else
	        {
	        	System.out.println("inside fdd else");
	        	System.out.println(ex.getStackTrace());
	        	data.put("FDD_COUNT","0");
	        }
        }
	  	catch(Exception ex)
	  	{
	  		ex.printStackTrace();
	  	}
        return data;
        
	  }
	 
	 
	 public static ArrayList<String> getLoggedInUsers(ProducerTemplate template,Endpoint endpoint,Exchange exchange)
	 {
		ArrayList<String[]> data = new ArrayList<String[]>();
	    ArrayList<String> data1				= new ArrayList<String>();
	    try
	    {
			StringBuffer loggedUsersQuery = new StringBuffer();
			loggedUsersQuery.append("SELECT COUNT(DISTINCT(LD.USER_ID)) AS USER_COUNT, U.USER_LEVEL FROM LOGIN_DETAILS LD LEFT JOIN USERS U ON LD.USER_ID=U.USER_NO  WHERE (LOGIN_DATE>='"+dateFrom+" 00:00:00 ' AND LOGIN_DATE<='"+dateTo+" 23:59:59') AND LD.USER_ID NOT IN (1,1000) GROUP BY U.USER_LEVEL");
			System.out.println(" Logged Users Query : "+loggedUsersQuery + " endpoint : "+endpoint);
			
			exchange  = endpoint.createExchange();
			exchange.getIn().setBody(loggedUsersQuery);
		    Exchange out = template.send(endpoint, exchange);
		    List<Map<String, Object>> result = out.getOut().getBody(List.class);
		    if(result!=null)
		    {
		    	int i =result.size();
			    for(int j =0;j<i;j++)
			    {
		        	Map<String, Object> row = result.get(j);
		        	String[] details	= new String[2];
		        	if(row.get("USER_LEVEL") instanceof Integer)
		        	{
		        		details[0]			= new String(""+(Integer)row.get("USER_LEVEL"));
		        	}
		        	else 
		        	{
		        		details[0]			= (String)row.get("USER_LEVEL");
		        	}
					if (row.get("USER_COUNT") instanceof Long) 
					{
						details[1] = String.valueOf((Long)row.get("USER_COUNT")); 
					}
					else if(row.get("USER_COUNT") instanceof Integer)
					{
						details[1] = new String(""+(Integer)row.get("USER_COUNT"));	
					}
					else
					{
						details[1] = (String)row.get("USER_COUNT");
					}
					data.add(details);
				}
			}
			String dCorporate		= "0";
			String dFranchise		= "0";
			String dRegional		= "0";
			String dDivisonal 		= "0";
			if(data!=null)
	    	{
	    		int size=data.size();
	    		int dIntRegional		= 0;
	
	    		for(int k=0;k<size;k++)
	    		{
	    			String[] dataList		= (String[])data.get(k);
	    			if( "0".equals(dataList[0]))
	    			{
	    				dCorporate		= dataList[1];
	    			}	
	    			if( "1".equals(dataList[0]))
	    			{
	    				dFranchise		= dataList[1];
	    			}
	    			if( "2".equals(dataList[0]) || "3".equals(dataList[0]))
	    			{
	    				dRegional		= dataList[1];
	    				dIntRegional	= dIntRegional + Integer.parseInt(dRegional);
	    			}
	    			if( "6".equals(dataList[0]))
	    			{
	    				dDivisonal		= dataList[1];
	    			}
	    		}
	    		dRegional=""+dIntRegional;
	    		data1.add(dCorporate);
	    		data1.add(dFranchise);
	    		data1.add(dRegional);
	    		data1.add(dDivisonal);
	    		data=null;
	    	}
		}
	    catch(Exception ex)
	    {
    		ex.printStackTrace();
    	}
    	return data1;
	 }
	 
	 
	 public static ArrayList<String> getLoggedUserHrs(ProducerTemplate template,Endpoint endpoint,Exchange exchange)
	 {
		ArrayList<String> data = new ArrayList<String>();
		try
		{
			StringBuffer loggedUsersQuery = new StringBuffer();
			loggedUsersQuery.append("SELECT IFNULL(SUM(UNIX_TIMESTAMP(LOGOUT_DATE)),0)-IFNULL(SUM(UNIX_TIMESTAMP(LOGIN_DATE)),0) TIME ,U.USER_LEVEL FROM LOGIN_DETAILS LD LEFT JOIN USERS U ON LD.USER_ID=U.USER_NO  WHERE (LOGIN_DATE>='"+dateFrom+" 00:00:00 ' AND LOGIN_DATE<='"+dateTo+" 23:59:59') AND LD.USER_ID NOT IN (1,1000) GROUP BY U.USER_LEVEL");
			System.out.println(" logged Hrs Query : "+loggedUsersQuery + "endpoint : "+endpoint);
			exchange  = endpoint.createExchange();
			exchange.getIn().setBody(loggedUsersQuery);
		    Exchange out = template.send(endpoint, exchange);
		    
		    List<Map<String, Object>> result = out.getOut().getBody(List.class);
		    String dCorporate		= "00:00:00";
    		String dFranchise		= "00:00:00";
    		String dRegional		= "00:00:00";
    		String dDivisonal 		= "00:00:00";
    		if(result!=null)
    		{
    			int i =result.size();
    			for(int j =0;j<i;j++)
	    		{	
		        	Map<String, Object> row = result.get(j);
		        	//String[] details	= new String[2];
		        	String userLevel = "";
		        	String timeDis1 ="";
					long systemTime;
					if(row.get("USER_LEVEL") instanceof Integer )
					{
						userLevel			= new String(""+(Integer)row.get("USER_LEVEL"));
					}
					else
					{
						userLevel			= (String)row.get("USER_LEVEL");
		        	}
					if (row.get("TIME") instanceof BigDecimal) 
					{
						systemTime = ((BigDecimal)row.get("TIME")).longValue(); 
					}
					else 
		        	{
		        		systemTime = ((Double)row.get("TIME")).longValue();
		        	}
					if(systemTime !=0)
					{
			            long k			= systemTime;
	        			String  hour	= ""+(k/3600);
						k				= k%3600;
						String min		= ""+(k/60);
						String sec		= ""+(k%60);
						if(hour.length() == 1) hour = "0" + hour;
						if(min.length() == 1) min = "0" + min;
						if(sec.length() == 1) sec = "0" + sec;
						timeDis1 = hour+":"+min+":"+sec;
					}
					if( "0".equals(userLevel))
	    			{
	    				dCorporate		= timeDis1;
	    			}	
	    			if( "1".equals(userLevel))
	    			{
	    				dFranchise		= timeDis1;
	    			}
	    			if( "2".equals(userLevel))
	    			{
	    				dRegional		= timeDis1;
	    			}
	    			if( "6".equals(userLevel) )
	    			{
	    				dDivisonal		= timeDis1;
	    			}
		    	}
			}
		    data.add(dCorporate);
			data.add(dFranchise);
			data.add(dRegional);
			data.add(dDivisonal);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	    return data;
	 }
	 
	 
	 public static String getDbName(ProducerTemplate template,Endpoint endpoint,Exchange exchange)
	 {
		 String dbName = "";
		 String query = "SELECT DATA_VALUE FROM MASTER_DATA WHERE DATA_TYPE = '111311'";
		 String query2 = "SELECT DATA_VALUE FROM MASTER_DATA_FOR_SCHEDULER WHERE DATA_TYPE=10009";
		 try
		 {
			 exchange  = endpoint.createExchange();
			 exchange.getIn().setBody(query);
			 Exchange out = template.send(endpoint, exchange);
			 List<Map<String, Object>> result = out.getOut().getBody(List.class);
			 if(result !=null)
			 {
				 int i =result.size();
				 for(int j =0;j<i;j++)
				 {	
					 Map<String, Object> row = result.get(j);
					 if(row.get("DATA_VALUE") != null && !"".equals((String)row.get("DATA_VALUE")) )
					 {
						 dbName = (String)row.get("DATA_VALUE");
					 }
				 }
			 }
			 if(dbName==null || "".equals(dbName) || "null".equals(dbName))
			 {
				 exchange  = endpoint.createExchange();
				 exchange.getIn().setBody(query2);
				 Exchange out2 = template.send(endpoint, exchange);
				 List<Map<String, Object>> result2 = out2.getOut().getBody(List.class);
				 if(result2!=null)
				 {
					 int i2 =result2.size();
					 for(int j2 =0;j2<i2;j2++)
					 {	
						 Map<String, Object> row2 = result2.get(j2);
						 dbName = (String)row2.get("DATA_VALUE");
					 }
				 }
			 }
		 }
		 catch(Exception ex)
		 {
			 ex.printStackTrace();
		 }
		 if(dbName.contains(" "))
		 {
			 dbName = dbName.replaceAll(" ", "_");
		 }
		 if(dbName.contains("'"))
		 {
			 dbName = dbName.replaceAll("'", "");
		 }
		 return dbName;
	 }
	 
	 public static HashMap<String,Object> getUserNames(ProducerTemplate template , Endpoint endpoint,Exchange exchange,String clientName)
	 {		
		 System.out.println("clientName : "+clientName + " endpoint : "+endpoint);
		 StringBuffer query = new StringBuffer("");
		 String franchiseeNo =""; 
		 query.append("SELECT COUNT(FRANCHISEE_NO) AS COUNT, GROUP_CONCAT(FRANCHISEE_NO) AS FRANCHISEE_NO FROM FRANCHISEE WHERE (IS_FRANCHISEE='Y' AND STATUS IN (0,4) AND DEACTIVATION_DATE >= '"+dateFrom+" 00:00:00' AND DEACTIVATION_DATE <= '"+dateTo+" 23:59:59') OR (IS_FRANCHISEE='Y' AND STATUS IN (1,3) AND CREATION_DATE <= '"+dateTo+" 23:59:59') OR (IS_FRANCHISEE='Y' AND STATUS IN (0,4) AND DEACTIVATION_DATE > '"+dateTo+" 23:59:59' AND CREATION_DATE <= '"+dateTo+" 23:59:59')");
		 System.out.println(" query for franchisee no. : "+query);
		 exchange  = endpoint.createExchange();
		 exchange.getIn().setBody(query);
		 Exchange out = template.send(endpoint, exchange);
 		 List<Map<String, Object>> result = out.getOut().getBody(List.class);
 		 if(result!=null)
 		 {
		     int i =result.size();
	     	 for(int j =0;j<i;j++)
		     {
	     		 Map<String, Object> row = result.get(j);
	     		 if(row.get("FRANCHISEE_NO")!=null)
	     		 {	
	     			if(row.get("FRANCHISEE_NO") instanceof byte[])
	     			{
	     				final String pass = new String((byte[])row.get("FRANCHISEE_NO"));
		        			franchiseeNo =  pass;
	     			}
	     			else
	     			{
	     				franchiseeNo = (String)row.get("FRANCHISEE_NO");
	     			}
	     		 }	
		     }
     	 }
		StringBuffer usersQuery = new StringBuffer("");
		usersQuery.append("SELECT ");
		usersQuery.append("'"+clientName+"' AS CLIENT_NAME ,");
		usersQuery.append("'"+dateFrom+"' AS DATE_FROM ,");
		usersQuery.append("'"+dateTo+"' AS DATE_TO , ");
		usersQuery.append("F.FRANCHISEE_NAME , CONCAT((CASE WHEN U.USER_LEVEL IN(1) THEN FU.FIRST_NAME ELSE U.USER_FIRST_NAME END),' ',(CASE WHEN U.USER_LEVEL IN(1) THEN FU.LAST_NAME ELSE U.USER_LAST_NAME END)) AS USER_FULL_NAME , U.USER_LEVEL FROM FRANCHISEE F,USERS U LEFT JOIN FRANCHISEE_USERS FU ON U.USER_IDENTITY_NO = FU.FRANCHISEE_USER_NO  LEFT JOIN ( SELECT MAX(CHANGE_DATE) AS CHANGE_DATE,USER_NO,REMARK FROM USER_STATUS_CHANGED WHERE (REMARK='Deactivated' OR REMARK='Deleted' OR REMARK='Deactivated From Franchisor' OR REMARK='deactivateed By WS' OR REMARK='deleteed By WS' OR REMARK='Franchise User has logged out forcefully and deactivated' ) GROUP BY USER_NO) AS USC ON U.USER_NO=USC.USER_NO LEFT JOIN  USER_STATUS_CHANGED USC1 ON U.USER_NO=USC1.USER_NO AND (USC1.REMARK='Activated' OR USC1.REMARK='Activated From Franchisor' OR USC1.REMARK='activateed By WS') WHERE U.USER_ID!='adm' AND U.CREATION_DATE<='"+dateTo+" 23:59:59' AND ( (USC.CHANGE_DATE IS NULL OR USC.CHANGE_DATE>='"+dateFrom+" 00:00:00') OR ( USC1.CHANGE_DATE IS NOT NULL AND  USC1.CHANGE_DATE<='"+dateTo+" 23:59:59' AND USC.CHANGE_DATE<=USC1.CHANGE_DATE)  OR (USC.CHANGE_DATE IS NULL OR USC.CHANGE_DATE > '"+dateTo+"')) AND U.USER_NO!=1   AND ((U.USER_LEVEL=1 AND F.IS_FRANCHISEE='Y' ");
		if(franchiseeNo != null && franchiseeNo.trim().length() != 0 && !franchiseeNo.trim().equalsIgnoreCase("null")) 
    	{
			usersQuery.append(" AND F.FRANCHISEE_NO IN ("+franchiseeNo+") ");
    	}
    	usersQuery.append(" ) OR U.USER_LEVEL!=1) ");	
		usersQuery.append(" AND F.FRANCHISEE_NO = U.FRANCHISEE_NO AND ((CASE WHEN U.USER_LEVEL IN(1) THEN FU.EMAIL_ID ELSE F.EMAIL_ID END) NOT LIKE('%abc%'))");
		System.out.println("special billing +Clientg "+clientName+" : "+usersQuery);
		return QueryUtil.executeQuery(endpoint,usersQuery.toString());
	 }
	 
	 
	 
	 public static HashMap<String,Object> getTBCLifeCycleRepoData(Endpoint endpoint)
	 {
		 StringBuffer reportQuery = new StringBuffer();
		 reportQuery.append("SELECT  '"+dateFrom+"' AS DATE_FROM, '"+dateTo+"' AS DATE_TO ,");
		 reportQuery.append("FRANCHISEE1.FRANCHISEE_NAME AS FRANCHISE_ID , FRANCHISEE1.CENTER_NAME AS CENTER_NAME , IFNULL(FRANCHISEE1.REPORT_PERIOD_START_DATE,\"\") AS ROYALTY_REPORT_PERIOD_START_DATE , FRANCHISEE1.OPENING_DATE OPENING_DATE , STORE_TYPE2.ST_NAME LIFE_CYCLE , FRANCHISEE1.ADDRESS AS STREET_ADDRESS, FRANCHISEE1.CITY CITY , FRANCHISEE1.STATE AS STATE , FRANCHISEE1.ZIPCODE AS ZIP_CODE , FRANCHISEE1.CONTACT_FIRST_NAME CONTACT_FIRST_NAME , FRANCHISEE1.CONTACT_LAST_NAME CONTACT_LAST_NAME , IFNULL(FIM_REAL_ESTATE3._TARGET_INSTALL_DATE_966371569,\"\") AS TARGET_INSTALL_DATE  FROM FRANCHISEE FRANCHISEE1 LEFT JOIN STORE_TYPE  STORE_TYPE2 ON STORE_TYPE2.ST_ID=FRANCHISEE1.ST_ID LEFT JOIN FIM_REAL_ESTATE FIM_REAL_ESTATE3 ON FIM_REAL_ESTATE3.ENTITY_ID = FRANCHISEE1.FRANCHISEE_NO  WHERE 1=1  AND FRANCHISEE1.IS_ADMIN='N' AND FRANCHISEE1.FRANCHISEE_NO <> 100000 AND (FRANCHISEE1.IS_STORE_ARCHIVED <> 'Y' OR (FRANCHISEE1.IS_STORE_ARCHIVED = 'Y' AND FRANCHISEE1.IS_FRANCHISEE='Y') OR FRANCHISEE1.IS_STORE_ARCHIVED IS NULL)  AND FRANCHISEE1.STORE_STATUS IN ('1774572484', '498', '497') AND ( (INSTR(',1,' ,  CONCAT(',',REPLACE(FRANCHISEE1.STATUS,', ',','),',')) AND FRANCHISEE1.IS_FRANCHISEE='Y' ) OR  (INSTR(',3,' ,  CONCAT(',',REPLACE(FRANCHISEE1.STATUS,', ',','),',')) AND FRANCHISEE1.IS_FRANCHISEE='Y' ) )  AND FRANCHISEE1.STATUS !='2' ");
		 System.out.println(reportQuery);
		 return QueryUtil.executeQuery(endpoint,reportQuery.toString()); 
	 }
	
	 
	 public static String getTotalLogins(ProducerTemplate template,Endpoint endpoint,Exchange exchange)
	 {
		String total = "0";
		String totalLogins = "SELECT COUNT((LD.USER_ID)) AS USER_COUNT FROM LOGIN_DETAILS LD LEFT JOIN USERS U ON LD.USER_ID=U.USER_NO  WHERE (LOGIN_DATE>='"+dateFrom+" 00:00:00 ' AND LOGIN_DATE<='"+dateTo+" 23:59:59') AND LD.USER_ID NOT IN (1,1000)";
		System.out.println(" query for total logins : "+totalLogins + "endpoint : "+endpoint);
		
		exchange  = endpoint.createExchange();
		exchange.getIn().setBody(totalLogins);
		Exchange out = template.send(endpoint, exchange);
		List<Map<String, Object>> result = out.getOut().getBody(List.class);
		if(result!=null)
		{
		   	int i =result.size();
		    for(int j =0;j<i;j++)
		    {
		       	Map<String, Object> row = result.get(j);
		       	if(row.get("USER_COUNT") instanceof Integer)
		       	{
		       		total			= new String(""+(Integer)row.get("USER_COUNT"));
		       	}
		       	else if(row.get("USER_COUNT") instanceof Long) 
				{
					total = String.valueOf((Long)row.get("USER_COUNT"));	
				}
				else
				{
					total = (String)row.get("USER_COUNT");
				}
			}
		}
		return total;
	}

	public static boolean isValidDate(String inDate) 
	{
	    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	    dateFormat.setLenient(false);
	    try {
	      dateFormat.parse(inDate.trim());
	      System.out.println(dateFormat.parse(inDate.trim()));
	    } catch (ParseException pe) {
	      return false;
	    }
	    return true;
	  }
	
	public static DataSource setupDataSource(String connectURI) 
	{
		String user = "fsslogin"; //fsslogin
	   	String password ="fss@l0g1n"; //fss@l0g1n
	    return setupDataSource(connectURI, user, password);
	}
	
	public static DataSource setupDataSource(String connectURI,String user,String password) 
	{
		MysqlDataSource ds = new MysqlDataSource();
	   	try
	   	{
	   		ds.setUrl("jdbc:mysql://"+connectURI);
	   		ds.setUser(user);
	   		ds.setPassword(password);
	   	}
	    catch (Exception e) 
	   	{
	    	e.printStackTrace();
	   	}
	    return ds;
	}
	
	public static DataSource setupInHouseDataSource() 
	{
		MysqlDataSource ds = new MysqlDataSource();
	   	try
	   	{
	   		ds.setUrl("jdbc:mysql://192.168.1.127/InHouse90");
	   		ds.setUser("clientlogin");//appnetix
	   		ds.setPassword("cl13ntl0g1n");//fss@l0g1n
	   	}
	    catch (Exception e) 
	   	{
	    	e.printStackTrace();
	   	}
	    return ds;
	}
	
	public static RouteBuilder configure(final String clientName) throws Exception 
	{
		return new RouteBuilder() {
			
			@Override
			public void configure() throws Exception {
				from("direct:"+clientName).to("jdbc:"+clientName);
				
			}
		};
	}
}
