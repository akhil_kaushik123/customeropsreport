/*
 FCSKYS-15522  : Bhavna Chugh : Changes Regarding Report Structure Change
 FCSKYS-15522 : Bhavna Chugh : Inhouse Billing : Resolve Ordering Issue in xls Report 
 FCSKYS-15522 : Bhavna Chugh : Inhouse Billing : Email Column Formatting
 FCSKYS-15522 : Bhavna Chugh ::Indexing Issue
  FCSKYS-15522 : Bhavna Chugh : Changes in Query 'Codevalue' Replace with Client Name
  FCSKYS-16290 | Add new Column 'Logged Insight Users' | Fetch data not for 'franconnect or Test' locations
 FCSKYS-15522  | Implemented Billing Changes
 FCSKYS-18245 | Automate MUID Reports
 FCSKYS-18302 | Users Fetching based on their Roles 
 FCSKYS-18301 | Merging of users
 (FCSKYS-18304) Special Report in the BrightStar for Cars Report
 FCSKYS-18765) Automated report of LightBridge Academy
 **/
package com.app.util;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.sql.DataSource;

import org.apache.camel.CamelContext;
import org.apache.camel.Endpoint;
import org.apache.camel.Exchange;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;

import com.app.base.ClientInfo;
import com.app.base.Constants;
import com.app.helper.DBConnectionProvider;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.write.*;
import java.util.*;
import java.io.IOException;
import java.io.File;
import java.io.FileInputStream;
import java.io.*;
import com.enterprisedt.net.ftp.FileTransferClient;

public class SystemUsageUtilityExport {
	public static String dateFrom;
	public static String dateTo;

	public static void genrateSUSpecialExportFile(HashMap<String, ClientInfo> clients1, String exportFilepath) {
		Endpoint connection = DBConnectionProvider.getInstance().getDBConnection("InHouse");
		Endpoint connectionfss = DBConnectionProvider.getInstance().getDBConnection("fss");
		Exchange exchange = connection.createExchange();
		String query = "";
		String brandQuery = "";
		CamelContext context = exchange.getContext();
		Endpoint endpoint = context.getEndpoint("direct:InHouse");
		ProducerTemplate template = context.createProducerTemplate();
		exchange = endpoint.createExchange();
		HashMap<String, String> brandCorp = new HashMap();
		HashMap<String, HashMap<String, Integer>> brandCorpDetailMap = new HashMap();
		Exchange Queryout = null;
		List<Map<String, Object>> Queryresult = null;
		HashMap<String, Integer> uniqueUserBrand = new HashMap<>();
		int i = 0;

		Map<String, ClientInfo> clients = new TreeMap<>(clients1);
		Iterator<String> clientIt = clients.keySet().iterator();
		String clientName = "";
		String fileName = exportFilepath;
		int row = 0;
		int column = 0;
		int rowMonthCal = 0;
		int columnMonthCal = 0;
		int previousRow = row;
		int nextColumn = column;
		int preColumn = 0;
		WritableSheet sheet = null;
		WritableSheet sheetMonthCal = null;
		WritableFont headerFont = null;
		WritableCellFormat headerFormat = null;
		WritableFont textFont = null;
		WritableCellFormat textFormat = null;
		WritableCellFormat duplicatecellFormat = null;
		WritableCellFormat cellFormat = null;
		WritableCellFormat numberFormat = null;
		WritableCellFormat stringFormat = null;
		SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat format2 = new SimpleDateFormat("dd-MM-yyyy");
		String DBdate = null;
		NumberFormat decimalNo = null;
		Label label = null;
		jxl.write.Number numberCell = null;
		String codeValue = "";
		WritableWorkbook workbook = null;
		ClientInfo dbProps = null;
		try {
			File file = new File(fileName);
			if (file.exists()) {
				file.delete();
			}
			WorkbookSettings wbSettings = new WorkbookSettings();
			wbSettings.setRationalization(false);
			workbook = Workbook.createWorkbook(file, wbSettings);
		} catch (Exception e) {
			System.out.println(
					"=Exception Found for Client====================>" + codeValue + "\n" + stackTraceToString(e));
			e.printStackTrace();
		}
		String extCal = "0", cal = "0", activeLocation = "0", withoutUserLoc = "0", fdd = "0";
		String userFirstName = "", userID = "", emailID = "", usertype = "", areaName = "", franchiseID = "",
				creationDate = "", systemTime = "", admsystemTime = "", clientSystemTime = "", roleName = "",
				muidValue = "",storeName="",firstName="",lastName="";
		String countACU = "0", countBARU = "0", countADU = "0", userLevel1 = "", countSpreengreenVenderUser9 = "0",
				countDeconetSupplierUser4 = "0";
		int countDupClieUser = 0, atleastOne = 0, moreThanFive = 0;
		String countAceVendorUser10 = "0", countATU = "0", countAFU = "0";
		String numberOfUsers = "0", openingDate = "";
		String orphanLocation = "0", childLocation = "0", ParentLocation = "", orphanUserBatch = "",
				childUserBatch = "", parentUserBatch = "", orphanUserCount = "", parentUserCount = "",
				chileUserCount = "", countO = "";
		int count = 0;
		int cals = 0;
		int extraCals = 0;
		String insight1 = "0", insight2 = "0";
		List<String> brandClient = new ArrayList<String>();
		int sheetCount = 1;
		rowMonthCal = 0;
		List data = null;
		while (clientIt.hasNext()) {
			clientName = clientIt.next();
			exchange = connectionfss.createExchange();
			String mapClientQuery = "";
			String brand = "";
			Map<String, Object> row2 = null;
			Map<String, Object> row3 = null;
			mapClientQuery = "SELECT BUILD_NAME,BUILD_ID,REPORT_TYPE FROM SPECIAL_CLIENT WHERE BUILD_NAME='" + clientName + "'";
			System.out.println(mapClientQuery);
			exchange.getIn().setBody(mapClientQuery.toString());
			Exchange out = template.send(connectionfss, exchange);
			if (!exchange.isFailed()) {
				data = (List) exchange.getOut().getBody(List.class);
				System.out.println("**************88" + data);
			}
			if (data != null && data.size() > 0) {
				row2 = (Map<String, Object>) data.get(0);
				String buildID = row2.get("BUILD_ID") + "";
				String reportType=row2.get("REPORT_TYPE") + "";
				dbProps = clients.get(clientName);
				String brandClientQuery = "SELECT MAP_CLIENT FROM SPECIAL_CLIENT_MAPPING WHERE BUILD_ID='" + buildID
						+ "'";
				System.out.println("brandClientQuery..........." + brandClientQuery);
				exchange.getIn().setBody(brandClientQuery.toString());
				out = template.send(connectionfss, exchange);
				if (!exchange.isFailed()) {
					data = (List) exchange.getOut().getBody(List.class);
				}
				brandClient = new ArrayList<String>();
				if (data != null && data.size() > 0) {
					i = data.size();
					System.out.println(i);
					for (int j = 0; j < i; j++) {
						row3 = (Map<String, Object>) data.get(j);
						System.out.println(row3.get("MAP_CLIENT") + "");
						brandClient.add(row3.get("MAP_CLIENT") + "");
					}
				}
				System.out.println("brandClient........." + brandClient);
				codeValue = dbProps.getCodeValue();
				System.out.println("codeValue...**************" + codeValue);
				//if (!codeValue.equals("newToYountyClothingExchange") && !codeValue.equals("houseMasters") && !codeValue.equals("franchiseDynamics") && !codeValue.equals("Franchise Dyanamices") && codeValue.equals("Franchise Dyanamices")) {
				if(!"U".equals(reportType)){	
					try {
						headerFont = new WritableFont(WritableFont.TAHOMA, 10, WritableFont.BOLD);
						headerFont.setColour(Colour.BLACK);
						headerFormat = new WritableCellFormat(headerFont);
						headerFormat.setBackground(Colour.GREY_40_PERCENT);
						headerFormat.setWrap(true);
						headerFormat.setVerticalAlignment(VerticalAlignment.TOP);
						headerFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
						decimalNo = new NumberFormat("#");
						numberFormat = new WritableCellFormat(decimalNo);
						stringFormat = new WritableCellFormat();
						stringFormat.setAlignment(jxl.format.Alignment.RIGHT);
						textFont = new WritableFont(WritableFont.TAHOMA, 10);
						textFormat = new WritableCellFormat(textFont);
						duplicatecellFormat = new WritableCellFormat(textFont);
						textFormat.setWrap(true);
						duplicatecellFormat.setWrap(true);
						duplicatecellFormat.setBackground(jxl.format.Colour.VERY_LIGHT_YELLOW);
						WritableCellFormat cellFormatEmail = new WritableCellFormat();
						cellFormatEmail.setWrap(true);
						cellFormat = new WritableCellFormat(headerFont);
						cellFormat.setBackground(Colour.GREY_40_PERCENT);
						cellFormat.setAlignment(Alignment.CENTRE);
						cellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
						cellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
						cellFormat.setWrap(true);
						sheet = workbook.createSheet(clientName, sheetCount);
						sheetCount++;
						// Light Bridge Acdemy Report

						if (codeValue.equals("lightbridgeAcademy")) {
							row = 0;
							previousRow = row;
							column = 0;
							for (i = 0; i < 5; i++) {
								sheet.setRowView(i, 500);
							}
							sheet.setColumnView(column, 30);
							label = new Label(column, row, "Customer Name", headerFormat);
							sheet.addCell(label);
							column++;
							label = new Label(column, row, clientName, textFormat);
							sheet.addCell(label);
							sheet.setColumnView(column, 30);
							row++;
							column = 0;
							sheet.setColumnView(column, 30);
							label = new Label(column, row, "Code Name", headerFormat);
							sheet.addCell(label);
							column++;
							label = new Label(column, row, codeValue, textFormat);
							sheet.addCell(label);
							sheet.setColumnView(column, 30);
							row++;
							column=0;
							sheet.setColumnView(column, 30);
							label = new Label(column, row, "Store Name", headerFormat);
							sheet.addCell(label);
							column++;
							label = new Label(column, row, "User Type", headerFormat);
							sheet.addCell(label);
							sheet.setColumnView(column, 30);
							column++;
							label = new Label(column, row, "User Login ID", headerFormat);
							sheet.addCell(label);
							sheet.setColumnView(column, 30);
							column++;
							label = new Label(column, row, "First Name", headerFormat);
							sheet.addCell(label);
							sheet.setColumnView(column, 30);
							column++;
							label = new Label(column, row, "Last Name", headerFormat);
							sheet.addCell(label);
							sheet.setColumnView(column, 30);
							column++;
							label = new Label(column, row, "Email Id", headerFormat);
							sheet.addCell(label);
							sheet.setColumnView(column, 30);
							column++;
							label = new Label(column, row, "Opener Access", headerFormat);
							sheet.addCell(label);
							sheet.setColumnView(column, 30);
							column++;
							label = new Label(column, row, "Hub Access", headerFormat);
							sheet.addCell(label);
							sheet.setColumnView(column, 30);
							column++;
							label = new Label(column, row, "Role", headerFormat);
							sheet.addCell(label);
							sheet.setColumnView(column, 30);
							jxl.SheetSettings setSetting = sheet.getSettings();
							setSetting.setVerticalFreeze(3);
							query = "SELECT STORE_NAME,USER_TYPE,USER_ID,FIRST_NAME,LAST_NAME,EMAIL_ID,ROLE,IS_OPENER_ACCESS,IS_HUB_ACCESS FROM LIGHTBRIDGE_BILLING_DATA ORDER BY STORE_NAME";
							exchange.getIn().setBody(query.toString());
							Queryout = template.send(endpoint, exchange);
							Queryresult = Queryout.getOut().getBody(List.class);
							i = Queryresult.size();
							String moduleAccess="";
							for (int j = 0; j < i; j++) {
								row++;
								column=0;
								Map<String, Object> row1 = Queryresult.get(j);
								if (row1.get("STORE_NAME") != null) {
									userFirstName = reverseFilterValue(row1.get("STORE_NAME") + "");
								} else {
									userFirstName = "";
								}
								label = new Label(column, row, userFirstName, textFormat);
								sheet.addCell(label);
								column++;
								if (row1.get("USER_TYPE") != null) {
									usertype = reverseFilterValue(row1.get("USER_TYPE") + "");
								} else {
									usertype = "";
								}
								label = new Label(column, row, usertype, textFormat);
								sheet.addCell(label);
								column++;
								if (row1.get("USER_ID") != null) {
									userID = reverseFilterValue(row1.get("USER_ID") + "");
								} else {
									userID = "";
								}
								label = new Label(column, row, userID, textFormat);
								sheet.addCell(label);
								column++;
								if (row1.get("FIRST_NAME") != null) {
									firstName = reverseFilterValue(row1.get("FIRST_NAME") + "");
								} else {
									firstName = "";
								}
								label = new Label(column, row, firstName, textFormat);
								sheet.addCell(label);
								column++;
								if (row1.get("LAST_NAME") != null) {
									lastName = reverseFilterValue(row1.get("LAST_NAME") + "");
								} else {
									lastName = "";
								}
								label = new Label(column, row, lastName, textFormat);
								sheet.addCell(label);
								column++;
								if (row1.get("EMAIL_ID") != null) {
									emailID = reverseFilterValue(row1.get("EMAIL_ID") + "");
									if(emailID.contains("@")) {
										emailID = "****"+emailID.substring(emailID.indexOf("@"), emailID.length());
								} else {
										emailID = "****";
									}
								} else {
									emailID = "";
								}
								label = new Label(column, row, emailID, textFormat);
								sheet.addCell(label);
								column++;
								if (row1.get("IS_OPENER_ACCESS") != null) {
									moduleAccess = reverseFilterValue(row1.get("IS_OPENER_ACCESS") + "");
								} else {
									moduleAccess = "";
								}
								label = new Label(column, row, moduleAccess, textFormat);
								sheet.addCell(label);
								column++;
								if (row1.get("IS_HUB_ACCESS") != null) {
									moduleAccess = reverseFilterValue(row1.get("IS_HUB_ACCESS") + "");
								} else {
									moduleAccess = "";
								}
								label = new Label(column, row, moduleAccess, textFormat);
								sheet.addCell(label);
								column++;
								if (row1.get("ROLE") != null) {
									roleName = reverseFilterValue(row1.get("ROLE") + "");
								} else {
									roleName = "";
								}
								label = new Label(column, row, roleName, textFormat);
								sheet.addCell(label);
								column++;
							}
						}
						else if (codeValue.equals("brightstar")) {
							query = "SELECT ORPHAN_LOCATION,PARENT_LOCATION,CHILD_LOCATION,ORPHAN_LOCATION_USER_BATCH,PARENT_LOCATION_USER_BATCH,CHILD_LOCATION_USER_BATCH,ORPHAN_LOCATION_USER_COUNT,CHILD_LOCATION_USER_COUNT,PARENT_LOCATION_USER_COUNT FROM BRIGHTSTAR_BILLING_DATA";
							exchange.getIn().setBody(query.toString());
							Queryout = template.send(endpoint, exchange);
							Queryresult = Queryout.getOut().getBody(List.class);
							i = Queryresult.size();
							for (int j = 0; j < i; j++) {
								Map<String, Object> row1 = Queryresult.get(j);
								orphanLocation = row1.get("ORPHAN_LOCATION") + "";
								ParentLocation = row1.get("PARENT_LOCATION") + "";
								childLocation = row1.get("CHILD_LOCATION") + "";
								orphanUserBatch = row1.get("ORPHAN_LOCATION_USER_BATCH") + "";
								parentUserBatch = row1.get("PARENT_LOCATION_USER_BATCH") + "";
								childUserBatch = row1.get("CHILD_LOCATION_USER_BATCH") + "";
								chileUserCount = row1.get("CHILD_LOCATION_USER_COUNT") + "";
								parentUserCount = row1.get("PARENT_LOCATION_USER_COUNT") + "";
								orphanUserCount = row1.get("ORPHAN_LOCATION_USER_COUNT") + "";
							}
							row = 0;
							previousRow = row;
							column = 0;
							for (i = 0; i < 17; i++) {
								sheet.setRowView(i, 500);
							}
							sheet.setColumnView(column, 30);
							label = new Label(column, row, "Customer Name", headerFormat);
							sheet.addCell(label);
							column++;
							label = new Label(column, row, clientName, textFormat);
							sheet.addCell(label);
							sheet.setColumnView(column, 30);
							row++;
							column = 0;
							sheet.setColumnView(column, 30);
							label = new Label(column, row, "Code Name", headerFormat);
							sheet.addCell(label);
							column++;
							label = new Label(column, row, codeValue, textFormat);
							sheet.addCell(label);
							sheet.setColumnView(column, 30);
							column = 0;
							row++;
							row++;
							sheet.setColumnView(column, 30);
							label = new Label(column, row, "Total Orphan Location User Count / 7 ", headerFormat);
							sheet.addCell(label);
							column++;
							sheet.setColumnView(column, 30);
							label = new Label(column, row, orphanUserCount, textFormat);
							sheet.addCell(label);
							column++;
							column++;
							sheet.setColumnView(column, 30);
							label = new Label(column, row, "Total Parent Location User Count / 7", headerFormat);
							sheet.addCell(label);
							column++;
							sheet.setColumnView(column, 30);
							label = new Label(column, row, parentUserCount, textFormat);
							sheet.addCell(label);
							column++;
							column++;
							sheet.setColumnView(column, 30);
							label = new Label(column, row, "Total Child Location User Count / 7", headerFormat);
							sheet.addCell(label);
							column++;
							sheet.setColumnView(column, 30);
							label = new Label(column, row, chileUserCount, textFormat);
							sheet.addCell(label);
							row++;
							column = 0;
							sheet.setColumnView(column, 30);
							label = new Label(column, row, "Total of 7 Orphan Users Batch > 1", headerFormat);
							sheet.addCell(label);
							column++;
							sheet.setColumnView(column, 30);
							label = new Label(column, row, orphanUserBatch, textFormat);
							sheet.addCell(label);
							column++;
							column++;
							sheet.setColumnView(column, 30);
							label = new Label(column, row, "Total of 7 Parent Users Batch > 1", headerFormat);
							sheet.addCell(label);
							column++;
							sheet.setColumnView(column, 30);
							label = new Label(column, row, parentUserBatch, textFormat);
							sheet.addCell(label);
							column++;
							column++;
							sheet.setColumnView(column, 30);
							label = new Label(column, row, "Total of 7 Child Users Batch > 1", headerFormat);
							sheet.addCell(label);
							column++;
							sheet.setColumnView(column, 30);
							label = new Label(column, row, childUserBatch, textFormat);
							sheet.addCell(label);
							row++;
							row++;
							column = 0;
							sheet.setColumnView(column, 30);
							label = new Label(column, row, "Summary", headerFormat);
							sheet.addCell(label);
							row++;
							row++;
							column = 0;
							sheet.setColumnView(column, 30);
							label = new Label(column, row, "Total Orphan Locations (Single Unit Location count)",
									headerFormat);
							sheet.addCell(label);
							column++;
							sheet.setColumnView(column, 30);
							label = new Label(column, row, orphanLocation, textFormat);
							sheet.addCell(label);
							row++;
							column = 0;
							sheet.setColumnView(column, 30);
							label = new Label(column, row, "Total Parent Location (1st Multi Unit Location count)",
									headerFormat);
							sheet.addCell(label);
							column++;
							sheet.setColumnView(column, 30);
							label = new Label(column, row, ParentLocation, textFormat);
							sheet.addCell(label);
							row++;
							column = 0;
							sheet.setColumnView(column, 30);
							label = new Label(column, row,
									"Total Child Locations (Additional Location for Multi Unit Owners count)",
									headerFormat);
							sheet.addCell(label);
							column++;
							sheet.setColumnView(column, 30);
							label = new Label(column, row, childLocation, textFormat);
							sheet.addCell(label);
							row++;
							column = 0;
							sheet.setColumnView(column, 30);
							label = new Label(column, row,
									"Total of 7 Users Batch > 1 (This consist of all users all together i.e. Orphans, Parent & Child)",
									headerFormat);
							sheet.addCell(label);
							column++;
							sheet.setColumnView(column, 30);
							label = new Label(column, row, (Integer.parseInt(childUserBatch)
									+ Integer.parseInt(parentUserBatch) + Integer.parseInt(orphanUserBatch)) + "",
									textFormat);
							sheet.addCell(label);
							row++;
							row++;
							column = 0;
							previousRow = row;// 18
							preColumn = column;
							nextColumn++;// 13
							sheet.mergeCells(column, row, nextColumn, row);
							label = new Label(column, row, "List of All Orphan Locations", cellFormat);
							sheet.addCell(label);
							column = nextColumn;
							column++;
							column++;
							nextColumn = column + 1;
							sheet.mergeCells(column, row, nextColumn, row);
							label = new Label(column, row, "List of All Parent Locations", cellFormat);
							sheet.addCell(label);
							column = nextColumn;
							column++;
							column++;
							nextColumn = column + 1;
							sheet.mergeCells(column, row, nextColumn, row);
							label = new Label(column, row, "List of All Child Locations", cellFormat);
							sheet.addCell(label);
							row++;
							column = 0;
							preColumn = column;
							previousRow = row;
							label = new Label(column, row, "Orphan Locations (Single Unit Locations)", cellFormat);
							sheet.addCell(label);
							column++;
							label = new Label(column, row, "User Count / 7", headerFormat);
							sheet.addCell(label);
							query = "SELECT FRANCHISEE_NAME,USER_COUNT FROM BRIGHTSTAR_BILLING_USER_DATA WHERE LOCATION_TYPE='Orphan Location'";
							exchange.getIn().setBody(query.toString());
							Queryout = template.send(endpoint, exchange);
							Queryresult = Queryout.getOut().getBody(List.class);
							i = Queryresult.size();
							row++;
							for (int j = 0; j < i; j++) {
								Map<String, Object> row1 = Queryresult.get(j);
								franchiseID = row1.get("FRANCHISEE_NAME") + "";
								countO = row1.get("USER_COUNT") + "";
								column = preColumn;
								label = new Label(column, row, franchiseID, textFormat);
								sheet.addCell(label);
								column++;
								label = new Label(column, row, countO, textFormat);
								sheet.addCell(label);
								row++;
							}
							row = previousRow;
							column++;
							column++;
							preColumn = column;
							previousRow = row;
							label = new Label(column, row, "Multi Unit 1st Locations (Parent Locations)", cellFormat);
							sheet.addCell(label);
							column++;
							label = new Label(column, row, "User Count / 7", cellFormat);
							sheet.addCell(label);
							query = "SELECT FRANCHISEE_NAME,USER_COUNT FROM BRIGHTSTAR_BILLING_USER_DATA WHERE LOCATION_TYPE='Parent Location' AND  FRANCHISEE_NAME !=''";
							exchange.getIn().setBody(query.toString());
							Queryout = template.send(endpoint, exchange);
							Queryresult = Queryout.getOut().getBody(List.class);
							i = Queryresult.size();
							row++;
							for (int j = 0; j < i; j++) {
								Map<String, Object> row1 = Queryresult.get(j);
								franchiseID = row1.get("FRANCHISEE_NAME") + "";
								countO = row1.get("USER_COUNT") + "";
								column = preColumn;
								label = new Label(column, row, franchiseID, textFormat);
								sheet.addCell(label);
								column++;
								label = new Label(column, row, countO, textFormat);
								sheet.addCell(label);
								row++;
							}
							row = previousRow;
							column++;
							column++;
							preColumn = column;
							label = new Label(column, row, "Child Locations", cellFormat);
							sheet.addCell(label);
							column++;
							label = new Label(column, row, "User Count / 7", cellFormat);
							sheet.addCell(label);
							previousRow = row;
							query = "SELECT DISTINCT(FRANCHISEE_NAME),USER_COUNT FROM BRIGHTSTAR_BILLING_USER_DATA WHERE LOCATION_TYPE='Child Location'";
							exchange.getIn().setBody(query.toString());
							Queryout = template.send(endpoint, exchange);
							Queryresult = Queryout.getOut().getBody(List.class);
							i = Queryresult.size();
							row++;
							for (int j = 0; j < i; j++) {
								Map<String, Object> row1 = Queryresult.get(j);
								franchiseID = row1.get("FRANCHISEE_NAME") + "";
								countO = row1.get("USER_COUNT") + "";
								column = preColumn;
								label = new Label(column, row, franchiseID, textFormat);
								sheet.addCell(label);
								column++;
								label = new Label(column, row, countO, textFormat);
								sheet.addCell(label);
								row++;
							}
						} // MUID Reports
						else if (codeValue.equals("clockworkdirectEnergy") || codeValue.equals("mosquitoJoe")
								|| codeValue.equals("rightAtHomeInc")) {
							row = 0;
							column = 0;
							sheet.setRowView(row, 500);
							jxl.SheetSettings setSetting = sheet.getSettings();
							setSetting.setVerticalFreeze(1);
							sheet.setColumnView(column, 30);
							label = new Label(column, row, "MUID Owner Name", headerFormat);
							sheet.addCell(label);
							column++;
							label = new Label(column, row, "MUID Name", headerFormat);
							sheet.addCell(label);
							sheet.setColumnView(column, 30);
							column++;
							label = new Label(column, row, "Franchsiee No", headerFormat);
							sheet.addCell(label);
							sheet.setColumnView(column, 30);
							column++;
							label = new Label(column, row, "Franchsiee Count", headerFormat);
							sheet.addCell(label);
							sheet.setColumnView(column, 30);
							column++;
							label = new Label(column, row, "Login ID", headerFormat);
							sheet.addCell(label);
							sheet.setColumnView(column, 30);
							column++;
							label = new Label(column, row, "User Name", headerFormat);
							sheet.addCell(label);
							sheet.setColumnView(column, 30);
							column++;
							label = new Label(column, row, "Email ID", headerFormat);
							sheet.addCell(label);
							sheet.setColumnView(column, 30);
							query = "SELECT OWNER_NAME,MUID_VALUE,FRANCHISEE_NAME,FRANCHISEE_COUNT,USER_ID,USER_NAME,EMAIL_ID FROM MUID_USERS_THREAD_DATA  WHERE CODE_VALUE='"
									+ codeValue + "' ORDER BY OWNER_NAME ";
							exchange.getIn().setBody(query.toString());
							Queryout = template.send(endpoint, exchange);
							Queryresult = Queryout.getOut().getBody(List.class);
							i = Queryresult.size();
							for (int j = 0; j < i; j++) {
								Map<String, Object> row1 = Queryresult.get(j);
								if (row1.get("OWNER_NAME") != null) {
									userFirstName = reverseFilterValue(row1.get("OWNER_NAME") + "");
								} else {
									userFirstName = "";
								}
								row++;
								column = 0;
								label = new Label(column, row, userFirstName, textFormat);
								sheet.addCell(label);
								column++;
								if (row1.get("MUID_VALUE") != null) {
									muidValue = reverseFilterValue(row1.get("MUID_VALUE") + "");
								} else {
									muidValue = "";
								}
								label = new Label(column, row, muidValue, textFormat);
								sheet.addCell(label);
								column++;
								if (row1.get("FRANCHISEE_NAME") != null) {
									franchiseID = reverseFilterValue(row1.get("FRANCHISEE_NAME") + "");
								} else {
									franchiseID = "";
								}
								label = new Label(column, row, franchiseID, textFormat);
								sheet.addCell(label);
								column++;
								if (row1.get("FRANCHISEE_COUNT") != null) {
									franchiseID = reverseFilterValue(row1.get("FRANCHISEE_COUNT") + "");
								} else {
									franchiseID = "";
								}
								label = new Label(column, row, franchiseID, textFormat);
								sheet.addCell(label);
								column++;
								if (row1.get("USER_ID") != null) {
									userID = reverseFilterValue(row1.get("USER_ID") + "");
								} else {
									userID = "";
								}
								label = new Label(column, row, userID, textFormat);
								sheet.addCell(label);
								column++;
								if (row1.get("USER_NAME") != null) {
									userFirstName = reverseFilterValue(row1.get("USER_NAME") + "");
								} else {
									userFirstName = "";
								}
								label = new Label(column, row, userFirstName, textFormat);
								sheet.addCell(label);
								column++;
								if (row1.get("EMAIL_ID") != null) {
									emailID = reverseFilterValue(row1.get("EMAIL_ID") + "");
									if(emailID.contains("@")) {
										emailID = "****"+emailID.substring(emailID.indexOf("@"), emailID.length());
								} else {
										emailID = "****";
									}
								} else {
									emailID = "";
								}
								label = new Label(column, row, emailID, textFormat);
								sheet.addCell(label);
								column++;
							}
						}
						else{
							query = "SELECT COUNT(*) AS COUNT ,USER_LEVEL FROM (SELECT USER_NO,USER_LEVEL FROM USERS_THREAD_DATA WHERE USER_LEVEL IN (0,2,6,4,9,10) AND EMAIL_ID IS NOT NULL AND EMAIL_ID!='' AND EMAIL_ID NOT LIKE '%franconnect.com%' AND EMAIL_ID NOT LIKE '%franconnect.net%' AND USER_FIRST_NAME NOT LIKE 'Test' AND STATUS=1  AND BUILD_NAME='"
									+ clientName
									+ "' GROUP BY EMAIL_ID,USER_LEVEL UNION ALL  SELECT USER_NO,USER_LEVEL FROM USERS_THREAD_DATA WHERE USER_LEVEL IN (0,2,6,4,9,10) AND (EMAIL_ID IS NULL OR EMAIL_ID='') AND EMAIL_ID NOT LIKE '%franconnect.com%' AND EMAIL_ID NOT LIKE '%franconnect.net%' AND USER_FIRST_NAME NOT LIKE 'Test' AND BUILD_NAME='"
									+ clientName + "' AND STATUS=1) AS TEMP GROUP BY USER_LEVEL";
							exchange.getIn().setBody(query.toString());
							Queryout = template.send(endpoint, exchange);
							Queryresult = Queryout.getOut().getBody(List.class);
							i = Queryresult.size();
							userLevel1 = "";
							String countACU1 = "0";
							countACU = "0";
							for (int j = 0; j < i; j++) {
								Map<String, Object> row1 = Queryresult.get(j);
								if (row1.get("USER_LEVEL") != null) {
									userLevel1 = row1.get("USER_LEVEL") + "";
								} else {
									userLevel1 = "";
								}
								if (row1.get("COUNT") != null) {
									countACU1 = row1.get("COUNT") + "";
								} else {
									countACU1 = "0";
								}
								if ("0".equals(userLevel1)) {
									countACU = countACU1;
								}
							}
							for (int j = 0; j < brandClient.size(); j++) {
								brandQuery = "SELECT COUNT(*) AS COUNT ,USER_LEVEL FROM (SELECT USER_NO,USER_LEVEL FROM USERS_THREAD_DATA WHERE USER_LEVEL IN (0,2,6,4,9,10) AND EMAIL_ID IS NOT NULL AND EMAIL_ID!='' AND EMAIL_ID NOT LIKE '%franconnect.com%' AND EMAIL_ID NOT LIKE '%franconnect.net%' AND STATUS=1 AND BUILD_NAME='"
										+ brandClient.get(j)
										+ "' GROUP BY EMAIL_ID,USER_LEVEL UNION ALL  SELECT USER_NO,USER_LEVEL FROM USERS_THREAD_DATA WHERE USER_LEVEL IN (0,2,6,4,9,10) AND (EMAIL_ID IS NULL OR EMAIL_ID='') AND EMAIL_ID NOT LIKE '%franconnect.com%' AND EMAIL_ID NOT LIKE '%franconnect.net%' AND STATUS=1 AND BUILD_NAME='"
										+ brandClient.get(j) + "') AS TEMP GROUP BY USER_LEVEL";
								exchange.getIn().setBody(brandQuery.toString());
								Queryout = template.send(endpoint, exchange);
								Queryresult = Queryout.getOut().getBody(List.class);
								i = Queryresult.size();
								userLevel1 = "";
								String count1 = "0";
								countBARU = "0";
								for (int k = 0; k < i; k++) {
									Map<String, Object> row1 = Queryresult.get(k);
									if (row1.get("USER_LEVEL") != null) {
										userLevel1 = row1.get("USER_LEVEL") + "";
									} else {
										userLevel1 = "";
									}
									if (row1.get("COUNT") != null) {
										count1 = row1.get("COUNT") + "";
									} else {
										count1 = "0";
									}
									if ("0".equals(userLevel1)) {
										countBARU = count1;
									}
								}
								brandCorp.put(brandClient.get(j), countBARU);
							}
							query = "SELECT COUNT(*) AS COUNT  FROM (SELECT USER_NO,USER_LEVEL FROM USERS_THREAD_DATA WHERE USER_LEVEL IN (1) AND EMAIL_ID IS NOT NULL AND EMAIL_ID!='' AND EMAIL_ID NOT LIKE '%franconnect.com%' AND EMAIL_ID NOT LIKE '%franconnect.net%'  AND USER_ID NOT LIKE 'test%' AND FRANCHISEE_NAME NOT LIKE 'Test%' AND FRANCHISEE_NAME NOT LIKE '55555CD' AND STATUS=1  AND BUILD_NAME='"
									+ clientName
									+ "' GROUP BY EMAIL_ID,FRANCHISEE_NAME UNION ALL  SELECT USER_NO,USER_LEVEL FROM USERS_THREAD_DATA WHERE USER_LEVEL IN (1) AND (EMAIL_ID IS NULL OR EMAIL_ID='') AND EMAIL_ID NOT LIKE '%franconnect.com%' AND EMAIL_ID NOT LIKE '%franconnect.net%' AND USER_ID NOT LIKE 'test%' AND FRANCHISEE_NAME NOT LIKE 'Test%' AND FRANCHISEE_NAME NOT LIKE '55555CD' AND STATUS=1  AND BUILD_NAME='"
									+ clientName + "') AS TEMP";
							exchange.getIn().setBody(query.toString());
							Queryout = template.send(endpoint, exchange);
							Queryresult = Queryout.getOut().getBody(List.class);
							i = Queryresult.size();
							countAFU = "0";
							for (int j = 0; j < i; j++) {
								Map<String, Object> row1 = Queryresult.get(j);
								if (row1.get("COUNT") != null) {
									countAFU = row1.get("COUNT") + "";
								} else {
									countAFU = "0";
								}
							}
							query = "SELECT FRANCHISEE_NAME,COUNT(*) AS NO_USERS FROM (SELECT CONCAT(USER_FIRST_NAME,' ',USER_LAST_NAME) AS USER_NAME,USER_ID,CREATION_DATE,EMAIL_ID,FRANCHISEE_NAME FROM USERS_THREAD_DATA WHERE USER_LEVEL IN (1) AND EMAIL_ID IS NOT NULL AND EMAIL_ID!=''  AND EMAIL_ID NOT LIKE '%franconnect.com%' AND EMAIL_ID NOT LIKE '%franconnect.net%' AND USER_ID NOT LIKE 'test%' AND FRANCHISEE_NAME NOT LIKE '55555CD' AND  FRANCHISEE_NAME NOT LIKE 'Test%' AND STATUS=1 AND BUILD_NAME='"
									+ clientName
									+ "'  GROUP BY EMAIL_ID,FRANCHISEE_NAME UNION ALL  SELECT CONCAT(USER_FIRST_NAME,' ',USER_LAST_NAME) AS USER_NAME,USER_ID,CREATION_DATE,EMAIL_ID,FRANCHISEE_NAME FROM USERS_THREAD_DATA WHERE USER_LEVEL IN (1) AND (EMAIL_ID IS NULL OR EMAIL_ID='')  AND EMAIL_ID NOT LIKE '%franconnect.com%' AND EMAIL_ID NOT LIKE '%franconnect.net%' AND USER_ID NOT LIKE 'test%' AND FRANCHISEE_NAME NOT LIKE 'Test%' AND FRANCHISEE_NAME NOT LIKE '55555CD' AND STATUS=1  AND BUILD_NAME='"
									+ clientName
									+ "' ORDER BY FRANCHISEE_NAME) AS TEMP GROUP BY FRANCHISEE_NAME ORDER BY FRANCHISEE_NAME ";
							exchange.getIn().setBody(query.toString());
							Queryout = template.send(endpoint, exchange);
							Queryresult = Queryout.getOut().getBody(List.class);
							i = Queryresult.size();
							atleastOne = 0;
							moreThanFive = 0;
							extraCals = 0;
							for (int j = 0; j < i; j++) {
								Map<String, Object> row1 = Queryresult.get(j);
								count = Integer.parseInt(row1.get("NO_USERS") + "");
								if (count >= 1) {
									if (row1.get("FRANCHISEE_NAME") != null) {
										atleastOne++;
										if (Integer.parseInt(row1.get("NO_USERS") + "") > 5) {
											moreThanFive++;
											if (Integer.parseInt(row1.get("NO_USERS") + "") % 5 != 0) {
												extraCals += count / 5;
											} else if (count % 5 == 0) {
												extraCals += count / 5 - 1;
											}
											if (count % 5 == 0)
												cals += count / 5;
											else
												cals += count / 5 + 1;
										}
									}
								}
							}
							query = "SELECT CONCAT(USER_FIRST_NAME,' ',USER_LAST_NAME) AS USER_NAME,USER_ID,CREATION_DATE,EMAIL_ID FROM USERS_THREAD_DATA WHERE USER_LEVEL IN (0) AND EMAIL_ID IS NOT NULL AND EMAIL_ID!='' AND EMAIL_ID NOT LIKE '%franconnect.com%' AND EMAIL_ID NOT LIKE '%franconnect.net%' AND USER_FIRST_NAME NOT LIKE 'Test' AND STATUS=1 AND BUILD_NAME='"
									+ clientName + "'";
							if (codeValue.equals("g6Hospitality(Motel6)")) {
								query = query + " AND ROLE IN (SELECT ROLE_NAME FROM SALES_ROLE)";
							}
							query = query
									+ " GROUP BY EMAIL_ID UNION ALL  SELECT CONCAT(USER_FIRST_NAME,' ',USER_LAST_NAME) AS USER_NAME ,USER_ID,CREATION_DATE,EMAIL_ID FROM USERS_THREAD_DATA WHERE USER_LEVEL IN (0) AND (EMAIL_ID IS NULL OR EMAIL_ID='') AND EMAIL_ID NOT LIKE '%franconnect.com%' AND EMAIL_ID NOT LIKE '%franconnect.net%' AND USER_FIRST_NAME NOT LIKE 'Test' AND STATUS=1 AND BUILD_NAME='"
									+ clientName + "'";
							if (codeValue.equals("g6Hospitality(Motel6)")) {
								query = query + " AND ROLE IN (SELECT ROLE_NAME FROM SALES_ROLE)";
							}
							query = query + " ORDER BY USER_NAME ";
							exchange.getIn().setBody(query.toString());
							Queryout = template.send(endpoint, exchange);
							Queryresult = Queryout.getOut().getBody(List.class);
							HashMap<String, Integer> uniqueUser = new HashMap<>();
							i = Queryresult.size();
							userFirstName = "";
							userID = "";
							emailID = "";
							creationDate = "";
							int ccUserCount = 0;
							for (int j = 0; j < i; j++) {
								Map<String, Object> row1 = Queryresult.get(j);
								int userCount = 1;
								if (row1.get("USER_NAME") != null) {
									userFirstName = reverseFilterValue(row1.get("USER_NAME") + "");
									if (uniqueUser.containsKey(userFirstName)) {
										userCount = uniqueUser.get(userFirstName);
										userCount++;
										uniqueUser.put(userFirstName, userCount);
									} else {
										uniqueUser.put(userFirstName, userCount);
										if (userFirstName.endsWith("CC") && codeValue.equals("oxiFresh")) {
											ccUserCount++;
										}
									}
								} else {
									userFirstName = "";
								}
							}
							try {
								for (int j = 0; j < brandClient.size(); j++) {
									query = "SELECT CONCAT(USER_FIRST_NAME,' ',USER_LAST_NAME) AS USER_NAME,USER_ID,CREATION_DATE,EMAIL_ID FROM USERS_THREAD_DATA WHERE USER_LEVEL IN (0) AND EMAIL_ID IS NOT NULL AND EMAIL_ID!='' AND EMAIL_ID NOT LIKE '%franconnect.com%' AND EMAIL_ID NOT LIKE '%franconnect.net%' AND STATUS=1 AND BUILD_NAME='"
											+ brandClient.get(j)
											+ "'  GROUP BY EMAIL_ID UNION ALL  SELECT CONCAT(USER_FIRST_NAME,' ',USER_LAST_NAME) AS USER_NAME ,USER_ID,CREATION_DATE,EMAIL_ID FROM USERS_THREAD_DATA WHERE USER_LEVEL IN (0) AND (EMAIL_ID IS NULL OR EMAIL_ID='') AND EMAIL_ID NOT LIKE '%franconnect.com%' AND EMAIL_ID NOT LIKE '%franconnect.net%' AND STATUS=1 AND BUILD_NAME='"
											+ brandClient.get(j) + "' ORDER BY USER_NAME ";
									exchange.getIn().setBody(query.toString());
									Queryout = template.send(endpoint, exchange);
									Queryresult = Queryout.getOut().getBody(List.class);
									i = Queryresult.size();
									userFirstName = "";
									userID = "";
									emailID = "";
									creationDate = "";
									for (int k = 0; k < i; k++) {
										Map<String, Object> row1 = Queryresult.get(k);
										int userCount = 1;
										if (row1.get("USER_NAME") != null) {
											userFirstName = reverseFilterValue(row1.get("USER_NAME") + "");
											if (uniqueUser.containsKey(userFirstName)) {
												countDupClieUser++;
											}
											if (uniqueUserBrand.containsKey(userFirstName)) {
												userCount = uniqueUserBrand.get(userFirstName);
												userCount++;
												uniqueUserBrand.put(userFirstName, userCount);
											} else {
												uniqueUserBrand.put(userFirstName, userCount);
											}
										} else {
											userFirstName = "";
										}
									}
									brandCorpDetailMap.put(brandClient.get(j), uniqueUserBrand);
								}
							} catch (Exception e) {
								e.printStackTrace();
							}
							row = 0;
							column = 0;
							for (int j = 0; j < 10; j++) {
								sheet.setRowView(j, 500);
							}
							sheet.setColumnView(column, 30);
							label = new Label(column, row, "Customer Name", headerFormat);
							sheet.addCell(label);
							column++;
							label = new Label(column, row, clientName, textFormat);
							sheet.addCell(label);
							sheet.setColumnView(column, 30);
							for (int j = 0; j < brandClient.size(); j++) {
								column = column + 2;
								sheet.setColumnView(column, 30);
								label = new Label(column, row, "Brand Name", headerFormat);
								sheet.addCell(label);
								column++;
								label = new Label(column, row, brandClient.get(j), textFormat);
								sheet.addCell(label);
								sheet.setColumnView(column, 30);
							}
							row++;
							row++;
							column = 0;
							label = new Label(column, row, "Total Active Corporate Users", headerFormat);
							sheet.addCell(label);
							column++;
							if (isValidNew(countACU)) {
								numberCell = new jxl.write.Number(column, row, Integer.parseInt(countACU),
										numberFormat);
								sheet.addCell(numberCell);
							} else {
								numberCell = new jxl.write.Number(column, row, 0, numberFormat);
								sheet.addCell(numberCell);
							}
							for (int j = 0; j < brandClient.size(); j++) {
								column = column + 2;
								label = new Label(column, row, "Total Active Corporate Users", headerFormat);
								sheet.addCell(label);
								column++;
								if (isValidNew(countBARU)) {
									System.out.println(brandCorp.get(brandClient.get(j)));
									numberCell = new jxl.write.Number(column, row,
											Integer.parseInt(brandCorp.get(brandClient.get(j))), numberFormat);
									sheet.addCell(numberCell);
								} else {
									numberCell = new jxl.write.Number(column, row, 0, numberFormat);
									sheet.addCell(numberCell);
								}
							}
							if (!codeValue.equals("g6Hospitality(Motel6)")) {
								row++;
								column = 0;
								label = new Label(column, row, "Duplicate Corporate Users", headerFormat);
								sheet.addCell(label);
								column++;
								if (isValidNew(countBARU)) {
									numberCell = new jxl.write.Number(column, row,
											Integer.parseInt(countACU) - uniqueUser.size(), numberFormat);
									sheet.addCell(numberCell);
								} else {
									numberCell = new jxl.write.Number(column, row, 0, numberFormat);
									sheet.addCell(numberCell);
								}
							}
							if (codeValue.equals("oxiFresh")) {
								row++;
								column = 0;
								label = new Label(column, row, "CC Users", headerFormat);
								sheet.addCell(label);
								column++;
								numberCell = new jxl.write.Number(column, row, ccUserCount, numberFormat);
								sheet.addCell(numberCell);
							}
							for (int j = 0; j < brandClient.size(); j++) {
								column = column + 2;
								label = new Label(column, row, "Duplicate Corporate Users", headerFormat);
								sheet.addCell(label);
								column++;
								if (isValidNew(countBARU)) {
									numberCell = new jxl.write.Number(column, row,
											Integer.parseInt(countBARU) - uniqueUserBrand.size(), numberFormat);
									sheet.addCell(numberCell);
								} else {
									numberCell = new jxl.write.Number(column, row, 0, numberFormat);
									sheet.addCell(numberCell);
								}
								row++;
								column = 0;
								column = column + 3;
								label = new Label(column, row, "Duplicate Corporate Users in " + clientName,
										headerFormat);
								sheet.addCell(label);
								column++;
								numberCell = new jxl.write.Number(column, row, countDupClieUser, numberFormat);
								sheet.addCell(numberCell);
							}
							row++;
							column = 0;
							label = new Label(column, row, "Total Billing Users", headerFormat);
							sheet.addCell(label);
							column++;
							if (codeValue.equals("oxiFresh")) {
								numberCell = new jxl.write.Number(column, row, uniqueUser.size() - ccUserCount,
										numberFormat);
							} else {
								numberCell = new jxl.write.Number(column, row, uniqueUser.size(), numberFormat);
							}
							sheet.addCell(numberCell);
							for (int j = 0; j < brandClient.size(); j++) {
								column = column + 2;
								label = new Label(column, row, "Total Billing Users", headerFormat);
								sheet.addCell(label);
								column++;
								numberCell = new jxl.write.Number(column, row,
										uniqueUserBrand.size() - countDupClieUser, numberFormat);
								sheet.addCell(numberCell);
							}
							row++;
							row++;// 18
							previousRow = row;// 18
							column = 0;
							nextColumn = column + 1;// 2
							sheet.mergeCells(column, row, nextColumn, row);
							label = new Label(column, row, "Active Corporate Users (" + countACU + ")", cellFormat);
							sheet.addCell(label);
							previousRow = row;
							if (codeValue.equals("g6Hospitality(Motel6)")) {
								column = nextColumn;
								column++;
								column++;
								label = new Label(column, row, "Sales Roles", cellFormat);
								sheet.setColumnView(column, 30);
								sheet.addCell(label);
								row++;
								row++;
								try {
									query = "SELECT ROLE_NAME,ROLE_ID FROM SALES_ROLE";
									exchange.getIn().setBody(query.toString());
									Queryout = template.send(endpoint, exchange);
									Queryresult = Queryout.getOut().getBody(List.class);
									i = Queryresult.size();
									for (int j = 0; j < i; j++) {
										Map<String, Object> row1 = Queryresult.get(j);
										roleName = row1.get("ROLE_NAME") + "";
										label = new Label(column, row, roleName, textFormat);
										sheet.addCell(label);
										row++;
									}
								} catch (Exception e) {
									e.printStackTrace();
								}
								row = previousRow;
								column++;
								column++;
								nextColumn = column + 1;
								sheet.mergeCells(column, row, nextColumn, row);
								label = new Label(column, row, "Users having sales module privilege", cellFormat);
								sheet.setColumnView(column, 30);
								sheet.addCell(label);
								preColumn = column;
								row++;
								label = new Label(column, row, "User Name", headerFormat);
								sheet.addCell(label);
								column++;
								label = new Label(column, row, "Email ID", headerFormat);
								sheet.setColumnView(column, 35);
								sheet.addCell(label);
								row++;
								try {
									query = "SELECT CONCAT(USER_FIRST_NAME,' ',USER_LAST_NAME) AS USER_NAME,USER_ID,CREATION_DATE,EMAIL_ID FROM USERS_THREAD_DATA WHERE USER_LEVEL IN (0) AND EMAIL_ID IS NOT NULL AND EMAIL_ID!='' AND EMAIL_ID NOT LIKE '%franconnect.com%' AND  EMAIL_ID NOT LIKE '%franconnect.net%' AND USER_FIRST_NAME NOT LIKE 'Test' AND STATUS=1 AND BUILD_NAME='"
											+ clientName
											+ "' AND ROLE IN (SELECT ROLE_NAME FROM SALES_ROLE)  GROUP BY EMAIL_ID UNION ALL  SELECT CONCAT(USER_FIRST_NAME,' ',USER_LAST_NAME) AS USER_NAME ,USER_ID,CREATION_DATE,EMAIL_ID FROM USERS_THREAD_DATA WHERE USER_LEVEL IN (0) AND (EMAIL_ID IS NULL OR EMAIL_ID='') AND EMAIL_ID NOT LIKE '%franconnect.com%' AND EMAIL_ID NOT LIKE '%franconnect.net%' AND USER_FIRST_NAME NOT LIKE 'Test' AND STATUS=1 AND BUILD_NAME='"
											+ clientName
											+ "' AND ROLE IN (SELECT ROLE_NAME FROM SALES_ROLE) ORDER BY USER_NAME ";
									//System.out.println("query........." + query);
									exchange.getIn().setBody(query.toString());
									Queryout = template.send(endpoint, exchange);
									Queryresult = Queryout.getOut().getBody(List.class);
									i = Queryresult.size();
									for (int j = 0; j < i; j++) {
										Map<String, Object> row1 = Queryresult.get(j);
										if (row1.get("USER_NAME") != null) {
											userFirstName = reverseFilterValue(row1.get("USER_NAME") + "");
										} else {
											userFirstName = "";
										}
										column = preColumn;
										label = new Label(column, row, userFirstName, textFormat);
										sheet.addCell(label);
										if (row1.get("EMAIL_ID") != null) {
											emailID = row1.get("EMAIL_ID") + "";
											if(emailID.contains("@")) {
												emailID = "****"+emailID.substring(emailID.indexOf("@"), emailID.length());
											}else {
												emailID = "****";
											}
										} else {
											emailID = "";
										}
										column++;
										label = new Label(column, row, emailID, textFormat);
										sheet.setColumnView(column, 35);
										sheet.addCell(label);
										row++;
									}
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
							row = previousRow;
							row++;// 19
							column = 0;
							label = new Label(column, row, "User Name", headerFormat);
							sheet.addCell(label);
							column++;
							sheet.setColumnView(column, 35);
							label = new Label(column, row, "Email ID", headerFormat);
							sheet.addCell(label);
							query = "SELECT CONCAT(USER_FIRST_NAME,' ',USER_LAST_NAME) AS USER_NAME,USER_ID,CREATION_DATE,EMAIL_ID FROM USERS_THREAD_DATA WHERE USER_LEVEL IN (0) AND EMAIL_ID IS NOT NULL AND EMAIL_ID!='' AND EMAIL_ID NOT LIKE '%franconnect.com%' AND  EMAIL_ID NOT LIKE '%franconnect.net%' AND USER_FIRST_NAME NOT LIKE 'Test' AND STATUS=1 AND BUILD_NAME='"
									+ clientName
									+ "'  GROUP BY EMAIL_ID UNION ALL  SELECT CONCAT(USER_FIRST_NAME,' ',USER_LAST_NAME) AS USER_NAME ,USER_ID,CREATION_DATE,EMAIL_ID FROM USERS_THREAD_DATA WHERE USER_LEVEL IN (0) AND (EMAIL_ID IS NULL OR EMAIL_ID='') AND EMAIL_ID NOT LIKE '%franconnect.com%' AND EMAIL_ID NOT LIKE '%franconnect.net%' AND USER_FIRST_NAME NOT LIKE 'Test' AND STATUS=1 AND BUILD_NAME='"
									+ clientName + "' ORDER BY USER_NAME ";
							exchange.getIn().setBody(query.toString());
							Queryout = template.send(endpoint, exchange);
							Queryresult = Queryout.getOut().getBody(List.class);
							i = Queryresult.size();
							userFirstName = "";
							userID = "";
							emailID = "";
							creationDate = "";
							for (int j = 0; j < i; j++) {
								Map<String, Object> row1 = Queryresult.get(j);
								int userCount = 1;
								if (row1.get("USER_NAME") != null) {
									userFirstName = reverseFilterValue(row1.get("USER_NAME") + "");
									if (!"g6Hospitality(Motel6)".equals(codeValue)) {
										userCount = uniqueUser.get(userFirstName);
									}
								} else {
									userFirstName = "";
								}
								if (row1.get("EMAIL_ID") != null) {
									emailID = row1.get("EMAIL_ID") + "";
									if(emailID.contains("@")) {
										emailID = "****"+emailID.substring(emailID.indexOf("@"), emailID.length());
									}else {
										emailID = "****";
									}
								} else {
									emailID = "";
								}
								row++;
								column = 0;
								if (userFirstName.endsWith("CC") && codeValue.equals("oxiFresh")) {
									label = new Label(column, row, userFirstName, duplicatecellFormat);
								} else {
									if (userCount > 1 || uniqueUserBrand.containsKey(userFirstName)) {
										label = new Label(column, row, userFirstName, duplicatecellFormat);
									} else {
										label = new Label(column, row, userFirstName, textFormat);
									}
								}
								sheet.addCell(label);
								column++;
								label = new Label(column, row, emailID, cellFormatEmail);
								sheet.setColumnView(column, 35);
								sheet.addCell(label);
							}
							for (int k = 0; k < brandClient.size(); k++) {
								row = previousRow;// 18
								nextColumn++;// 3
								column = nextColumn + 1;// 4
								preColumn = column;// 4
								nextColumn = column + 1;// 7
								sheet.mergeCells(column, row, nextColumn, row);
								label = new Label(column, row, "Active Brand Corporate Users (" + countBARU + ")",
										cellFormat);
								sheet.addCell(label);
								row++;// 19
								label = new Label(column, row, "User Name", headerFormat);
								sheet.addCell(label);
								column++;
								sheet.setColumnView(column, 35);
								label = new Label(column, row, "Email ID", headerFormat);
								sheet.addCell(label);
								query = "SELECT CONCAT(USER_FIRST_NAME,' ',USER_LAST_NAME) AS USER_NAME,USER_ID,CREATION_DATE,EMAIL_ID FROM USERS_THREAD_DATA WHERE USER_LEVEL IN (0) AND EMAIL_ID IS NOT NULL AND EMAIL_ID!='' AND EMAIL_ID NOT LIKE '%franconnect.com%' AND EMAIL_ID NOT LIKE '%franconnect.net%' AND STATUS=1 AND BUILD_NAME='"
										+ brandClient.get(k)
										+ "'  GROUP BY EMAIL_ID UNION ALL  SELECT CONCAT(USER_FIRST_NAME,' ',USER_LAST_NAME) AS USER_NAME ,USER_ID,CREATION_DATE,EMAIL_ID FROM USERS_THREAD_DATA WHERE USER_LEVEL IN (0) AND (EMAIL_ID IS NULL OR EMAIL_ID='') AND EMAIL_ID NOT LIKE '%franconnect.com%' AND EMAIL_ID NOT LIKE '%franconnect.net%' AND STATUS=1 AND BUILD_NAME='"
										+ brandClient.get(k) + "' ORDER BY USER_NAME ";
								exchange.getIn().setBody(query.toString());
								Queryout = template.send(endpoint, exchange);
								Queryresult = Queryout.getOut().getBody(List.class);
								i = Queryresult.size();
								userFirstName = "";
								userID = "";
								emailID = "";
								creationDate = "";
								for (int j = 0; j < i; j++) {
									Map<String, Object> row1 = Queryresult.get(j);
									int userCount = 1;
									if (row1.get("USER_NAME") != null) {
										userFirstName = reverseFilterValue(row1.get("USER_NAME") + "");
										userCount = uniqueUserBrand.get(userFirstName);
									} else {
										userFirstName = "";
									}
									if (row1.get("EMAIL_ID") != null) {
										emailID = row1.get("EMAIL_ID") + "";
										if(emailID.contains("@")) {
											emailID = "****"+emailID.substring(emailID.indexOf("@"), emailID.length());
									} else {
											emailID = "****";
										}
									} else {
										emailID = "";
									}
									row++;
									column = preColumn;
									if (userCount > 1 || uniqueUser.containsKey(userFirstName)) {
										label = new Label(column, row, userFirstName, duplicatecellFormat);
									} else {
										label = new Label(column, row, userFirstName, textFormat);
									}
									sheet.addCell(label);
									column++;
									label = new Label(column, row, emailID, cellFormatEmail);
									sheet.setColumnView(column, 35);
									sheet.addCell(label);
								}
							}
							if (codeValue.equals("homeHelpers") || codeValue.equals("oxiFresh")) {
								row = previousRow;// 18
								nextColumn++;// 13
								column = nextColumn + 1;// 14
								preColumn = column;// 14
								nextColumn = column + 4;// 17
								sheet.mergeCells(column, row, nextColumn, row);
								label = new Label(column, row, "Active Franchise Users (" + countAFU + ")", cellFormat);
								sheet.addCell(label);
								row++;// 19
								label = new Label(column, row, "User Name", headerFormat);
								sheet.addCell(label);
								sheet.setColumnView(column, 18);
								column++;// 15
								label = new Label(column, row, "User ID", headerFormat);
								sheet.setColumnView(column, 18);
								sheet.addCell(label);
								column++;// 15
								label = new Label(column, row, "Creation Date", headerFormat);
								sheet.setColumnView(column, 18);
								sheet.addCell(label);
								column++;// 16
								label = new Label(column, row, "Email ID", headerFormat);
								sheet.setColumnView(column, 35);
								sheet.addCell(label);
								column++;// 17
								label = new Label(column, row, "Franchise ID", headerFormat);
								sheet.setColumnView(column, 18);
								sheet.addCell(label);
								query = "SELECT CONCAT(USER_FIRST_NAME,' ',USER_LAST_NAME) AS USER_NAME,USER_ID,CREATION_DATE,EMAIL_ID,FRANCHISEE_NAME FROM USERS_THREAD_DATA WHERE USER_LEVEL IN (1) AND EMAIL_ID IS NOT NULL AND EMAIL_ID!=''  AND EMAIL_ID NOT LIKE '%franconnect.com%' AND EMAIL_ID NOT LIKE '%franconnect.net%' AND USER_ID NOT LIKE 'test%' AND FRANCHISEE_NAME NOT LIKE '55555CD' AND  FRANCHISEE_NAME NOT LIKE 'Test%' AND STATUS=1 AND BUILD_NAME='"
										+ clientName
										+ "'  GROUP BY EMAIL_ID,FRANCHISEE_NAME UNION ALL  SELECT CONCAT(USER_FIRST_NAME,' ',USER_LAST_NAME) AS USER_NAME,USER_ID,CREATION_DATE,EMAIL_ID,FRANCHISEE_NAME FROM USERS_THREAD_DATA WHERE USER_LEVEL IN (1) AND (EMAIL_ID IS NULL OR EMAIL_ID='')  AND EMAIL_ID NOT LIKE '%franconnect.com%' AND EMAIL_ID NOT LIKE '%franconnect.net%' AND USER_ID NOT LIKE 'test%' AND FRANCHISEE_NAME NOT LIKE 'Test%' AND FRANCHISEE_NAME NOT LIKE '55555CD' AND STATUS=1  AND BUILD_NAME='"
										+ clientName + "' ORDER BY FRANCHISEE_NAME";
								exchange.getIn().setBody(query.toString());
								Queryout = template.send(endpoint, exchange);
								Queryresult = Queryout.getOut().getBody(List.class);
								i = Queryresult.size();
								userFirstName = "";
								userID = "";
								emailID = "";
								areaName = "";
								usertype = "";
								franchiseID = "";
								creationDate = "";
								String preFranchisee = "";
								String Newfranchisee = "";
								String duplicateuser = "";
								int duplicateuserCount = 0;
								Map<String, Set<String>> map = new HashMap<String, Set<String>>();
								Set<String> valSetOne = null;
								for (int j = 0; j < i; j++) {
									Map<String, Object> row1 = Queryresult.get(j);
									if (row1.get("USER_NAME") != null) {
										userFirstName = reverseFilterValue(row1.get("USER_NAME") + "");
									} else {
										userFirstName = "";
									}
									if (row1.get("CREATION_DATE") != null) {
										creationDate = reverseFilterValue(row1.get("CREATION_DATE") + "");
										creationDate = format2.format((format1.parse(creationDate)));
									} else {
										creationDate = "";
									}
									if (row1.get("USER_ID") != null) {
										userID = reverseFilterValue(row1.get("USER_ID") + "");
									} else {
										userID = "";
									}
									if (row1.get("EMAIL_ID") != null) {
										emailID = row1.get("EMAIL_ID") + "";
										if(emailID.contains("@")) {
											emailID = "****"+emailID.substring(emailID.indexOf("@"), emailID.length());
									} else {
											emailID = "****";
										}
									} else {
										emailID = "";
									}
									if (row1.get("FRANCHISEE_NAME") != null) {
										franchiseID = row1.get("FRANCHISEE_NAME") + "";
										if (map.containsKey(franchiseID)) {
											if (valSetOne.contains(userFirstName)) {
												duplicateuser = userFirstName;
											}
										} else {
											valSetOne = new HashSet<String>();
											valSetOne.add(userFirstName);
											duplicateuser = "";
											map.put(franchiseID, valSetOne);
										}
									} else {
										franchiseID = "";
									}
									row++;// 20
									column = preColumn;// 14
									if (duplicateuser != "") {
										label = new Label(column, row, duplicateuser, duplicatecellFormat);
										duplicateuserCount++;
										sheet.addCell(label);
									} else {
										label = new Label(column, row, userFirstName, textFormat);
										sheet.addCell(label);
									}
									column++;// 15
									label = new Label(column, row, userID, textFormat);
									sheet.addCell(label);
									column++;// 15
									label = new Label(column, row, creationDate, textFormat);
									sheet.addCell(label);
									column++;// 16
									label = new Label(column, row, emailID, textFormat);
									sheet.setColumnView(column, 25);
									sheet.addCell(label);
									column++;// 17
									label = new Label(column, row, franchiseID, textFormat);
									sheet.addCell(label);
								}
								row = 3;
								column = 6;
								label = new Label(column, row, "Total Active Franchisee Users", headerFormat);
								sheet.setColumnView(column, 30);
								sheet.addCell(label);
								column++;
								numberCell = new jxl.write.Number(column, row, Integer.parseInt(countAFU),
										numberFormat);
								sheet.setColumnView(column, 30);
								sheet.addCell(numberCell);
								row++;
								column = 6;
								label = new Label(column, row, "Duplicate Franchisee Users", headerFormat);
								sheet.setColumnView(column, 30);
								sheet.addCell(label);
								column++;
								numberCell = new jxl.write.Number(column, row, duplicateuserCount, numberFormat);
								sheet.setColumnView(column, 30);
								sheet.addCell(numberCell);
								row++;
								column = 6;
								label = new Label(column, row, "Billing Franchisee Users", headerFormat);
								sheet.setColumnView(column, 30);
								sheet.addCell(label);
								column++;
								numberCell = new jxl.write.Number(column, row,
										Integer.parseInt(countAFU) - duplicateuserCount, numberFormat);
								sheet.addCell(numberCell);
								row = previousRow;// 18
								nextColumn++;// 23
								column = nextColumn + 1;// 24
								preColumn = column;// 24
								nextColumn = column + 1;// 25
								sheet.mergeCells(column, row, nextColumn, row);
								int totalcal = atleastOne + extraCals;
								label = new Label(column, row,
										"Total CAL's (Unique CAL's + Extra CAL's) (" + totalcal + ")", cellFormat);
								sheet.addCell(label);
								row++;// 19
								label = new Label(column, row, "Franchise ID", headerFormat);
								sheet.addCell(label);
								sheet.setColumnView(column, 18);

								column++;// 25
								label = new Label(column, row, "No. of Users Associated", headerFormat);
								sheet.addCell(label);
								sheet.setColumnView(column, 18);

								query = "SELECT FRANCHISEE_NAME,COUNT(*) AS NO_USERS FROM (SELECT CONCAT(USER_FIRST_NAME,' ',USER_LAST_NAME) AS USER_NAME,USER_ID,CREATION_DATE,EMAIL_ID,FRANCHISEE_NAME FROM USERS_THREAD_DATA WHERE USER_LEVEL IN (1) AND EMAIL_ID IS NOT NULL AND EMAIL_ID!=''  AND EMAIL_ID NOT LIKE '%franconnect.com%' AND EMAIL_ID NOT LIKE '%franconnect.net%' AND USER_ID NOT LIKE 'test%' AND FRANCHISEE_NAME NOT LIKE '55555CD' AND  FRANCHISEE_NAME NOT LIKE 'Test%' AND STATUS=1 AND BUILD_NAME='"
										+ clientName
										+ "'  GROUP BY EMAIL_ID,FRANCHISEE_NAME UNION ALL  SELECT CONCAT(USER_FIRST_NAME,' ',USER_LAST_NAME) AS USER_NAME,USER_ID,CREATION_DATE,EMAIL_ID,FRANCHISEE_NAME FROM USERS_THREAD_DATA WHERE USER_LEVEL IN (1) AND (EMAIL_ID IS NULL OR EMAIL_ID='')  AND EMAIL_ID NOT LIKE '%franconnect.com%' AND EMAIL_ID NOT LIKE '%franconnect.net%' AND USER_ID NOT LIKE 'test%' AND FRANCHISEE_NAME NOT LIKE 'Test%' AND FRANCHISEE_NAME NOT LIKE '55555CD' AND STATUS=1  AND BUILD_NAME='"
										+ clientName
										+ "' ORDER BY FRANCHISEE_NAME) AS TEMP GROUP BY FRANCHISEE_NAME ORDER BY FRANCHISEE_NAME ";
								exchange.getIn().setBody(query.toString());
								Queryout = template.send(endpoint, exchange);
								Queryresult = Queryout.getOut().getBody(List.class);
								i = Queryresult.size();
								franchiseID = "";
								numberOfUsers = "0";
								for (int j = 0; j < i; j++) {
									Map<String, Object> row1 = Queryresult.get(j);
									if (row1.get("FRANCHISEE_NAME") != null) {
										franchiseID = row1.get("FRANCHISEE_NAME") + "";
									} else {
										franchiseID = "";
									}
									if (row1.get("NO_USERS") != null) {
										numberOfUsers = row1.get("NO_USERS") + "";
									} else {
										numberOfUsers = "0";
									}

									row++;// 20
									column = preColumn;// 24

									label = new Label(column, row, franchiseID, textFormat);
									sheet.addCell(label);

								column++;// 25
									// label = new
									// Label(column,row,numberOfUsers,textFormat);
									// sheet.addCell(label);
									if (isValidNew(numberOfUsers)) {
										numberCell = new jxl.write.Number(column, row, Integer.parseInt(numberOfUsers),
												numberFormat);
										sheet.addCell(numberCell);
									} else {
										numberCell = new jxl.write.Number(column, row, 0, numberFormat);
										sheet.addCell(numberCell);
									}
								}
							}
						}
					} catch (Exception e) {
						System.out.println("=Exception Found for Client====================>" + codeValue + "\n"
								+ stackTraceToString(e));
						e.printStackTrace();
					}
				} else {
					try {
						String countACU1 = "0";
						int countUnigueActiveUser = 0;
						String buildName = "";
						headerFont = new WritableFont(WritableFont.TAHOMA, 10, WritableFont.BOLD);
						headerFont.setColour(Colour.BLACK);
						headerFormat = new WritableCellFormat(headerFont);
						headerFormat.setBackground(Colour.GREY_40_PERCENT);
						headerFormat.setWrap(true);
						headerFormat.setVerticalAlignment(VerticalAlignment.TOP);
						headerFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
						decimalNo = new NumberFormat("#");
						numberFormat = new WritableCellFormat(decimalNo);
						stringFormat = new WritableCellFormat();
						stringFormat.setAlignment(jxl.format.Alignment.RIGHT);
						textFont = new WritableFont(WritableFont.TAHOMA, 10);
						textFormat = new WritableCellFormat(textFont);
						duplicatecellFormat = new WritableCellFormat(textFont);
						textFormat.setWrap(true);
						duplicatecellFormat.setWrap(true);
						duplicatecellFormat.setBackground(jxl.format.Colour.VERY_LIGHT_YELLOW);
						WritableCellFormat cellFormatEmail = new WritableCellFormat();
						cellFormatEmail.setWrap(true);
						cellFormat = new WritableCellFormat(headerFont);
						cellFormat.setBackground(Colour.GREY_40_PERCENT);
						cellFormat.setAlignment(Alignment.CENTRE);
						cellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
						cellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
						cellFormat.setWrap(true);
						if(clientName.equals("Signarama")){
							sheet = workbook.createSheet("franchiseDynamics", sheetCount);
						}else if(clientName.equals("LashLounge")){
							sheet = workbook.createSheet("FranWorth", sheetCount);
						}else{
						sheet = workbook.createSheet(clientName, sheetCount);
						}
						sheetCount++;
						query = "SELECT COUNT(*) AS COUNT FROM (SELECT USER_NAME,USER_ID,CREATION_DATE,EMAIL_ID,BUILD_NAME FROM (SELECT CONCAT(USER_FIRST_NAME,' ',USER_LAST_NAME) AS USER_NAME,USER_ID,CREATION_DATE,EMAIL_ID,BUILD_NAME FROM USERS_THREAD_DATA WHERE USER_LEVEL IN (0) AND EMAIL_ID IS NOT NULL AND EMAIL_ID!='' AND EMAIL_ID NOT LIKE '%franconnect.com%' AND EMAIL_ID NOT LIKE '%franconnect.net%' AND USER_FIRST_NAME NOT LIKE 'Test' AND BUILD_NAME='"
								+ clientName
								+ "' GROUP BY EMAIL_ID UNION ALL  SELECT CONCAT(USER_FIRST_NAME,' ',USER_LAST_NAME) AS USER_NAME ,USER_ID,CREATION_DATE,EMAIL_ID,BUILD_NAME FROM USERS_THREAD_DATA WHERE USER_LEVEL IN (0) AND (EMAIL_ID IS NULL OR EMAIL_ID='') AND EMAIL_ID NOT LIKE '%franconnect.com%' AND EMAIL_ID NOT LIKE '%franconnect.net%' AND USER_FIRST_NAME NOT LIKE 'Test' AND BUILD_NAME='"
								+ clientName + "'";
						for (int j = 0; j < brandClient.size(); j++) {
							query = query + " UNION ALL ";
							query = query
									+ "SELECT CONCAT(USER_FIRST_NAME,' ',USER_LAST_NAME) AS USER_NAME,USER_ID,CREATION_DATE,EMAIL_ID,BUILD_NAME FROM USERS_THREAD_DATA WHERE USER_LEVEL IN (0) AND EMAIL_ID IS NOT NULL AND EMAIL_ID!='' AND EMAIL_ID NOT LIKE '%franconnect.com%' AND EMAIL_ID NOT LIKE '%franconnect.net%' AND BUILD_NAME='"
									+ brandClient.get(j)
									+ "'  GROUP BY EMAIL_ID UNION ALL  SELECT CONCAT(USER_FIRST_NAME,' ',USER_LAST_NAME) AS USER_NAME ,USER_ID,CREATION_DATE,EMAIL_ID,BUILD_NAME FROM USERS_THREAD_DATA WHERE USER_LEVEL IN (0) AND (EMAIL_ID IS NULL OR EMAIL_ID='') AND EMAIL_ID NOT LIKE '%franconnect.com%' AND EMAIL_ID NOT LIKE '%franconnect.net%' AND BUILD_NAME='"
									+ brandClient.get(j)
									+ "' UNION SELECT CONCAT(USER_FIRST_NAME,' ',USER_LAST_NAME) AS USER_NAME,USER_ID,CREATION_DATE,EMAIL_ID,BUILD_NAME FROM USERS_THREAD_DATA WHERE USER_LEVEL IN (0) AND EMAIL_ID IS NOT NULL AND EMAIL_ID!='' AND EMAIL_ID NOT LIKE '%franconnect.com%' AND EMAIL_ID NOT LIKE '%franconnect.net%' AND BUILD_NAME='"
									+ brandClient.get(j) + "' GROUP BY EMAIL_ID";
						}
						query = query + " ) AS TEMP1 ) AS TEMP2 ";
						exchange.getIn().setBody(query.toString());
						Queryout = template.send(endpoint, exchange);
						Queryresult = Queryout.getOut().getBody(List.class);
						i = Queryresult.size();
						userLevel1 = "";
						countACU = "0";
						for (int j = 0; j < i; j++) {
							Map<String, Object> row1 = Queryresult.get(j);
							if (row1.get("COUNT") != null) {
								countACU1 = row1.get("COUNT") + "";
							} else {
								countACU1 = "0";
							}
						}
						query = "SELECT USER_NAME,USER_ID,CREATION_DATE,EMAIL_ID,BUILD_NAME FROM (SELECT CONCAT(USER_FIRST_NAME,' ',USER_LAST_NAME) AS USER_NAME,USER_ID,CREATION_DATE,EMAIL_ID,BUILD_NAME FROM USERS_THREAD_DATA WHERE USER_LEVEL IN (0) AND EMAIL_ID IS NOT NULL AND EMAIL_ID!='' AND EMAIL_ID NOT LIKE '%franconnect.com%' AND EMAIL_ID NOT LIKE '%franconnect.net%' AND USER_FIRST_NAME NOT LIKE 'Test' AND BUILD_NAME='"
								+ clientName
								+ "'  GROUP BY EMAIL_ID UNION ALL  SELECT CONCAT(USER_FIRST_NAME,' ',USER_LAST_NAME) AS USER_NAME ,USER_ID,CREATION_DATE,EMAIL_ID,BUILD_NAME FROM USERS_THREAD_DATA WHERE USER_LEVEL IN (0) AND (EMAIL_ID IS NULL OR EMAIL_ID='') AND EMAIL_ID NOT LIKE '%franconnect.com%' AND EMAIL_ID NOT LIKE '%franconnect.net%' AND USER_FIRST_NAME NOT LIKE 'Test' AND BUILD_NAME='"
								+ clientName + "'";
						for (int j = 0; j < brandClient.size(); j++) {
							query = query + " UNION ALL ";
							query = query
									+ "SELECT CONCAT(USER_FIRST_NAME,' ',USER_LAST_NAME) AS USER_NAME,USER_ID,CREATION_DATE,EMAIL_ID,BUILD_NAME FROM USERS_THREAD_DATA WHERE USER_LEVEL IN (0) AND EMAIL_ID IS NOT NULL AND EMAIL_ID!='' AND EMAIL_ID NOT LIKE '%franconnect.com%' AND EMAIL_ID NOT LIKE '%franconnect.net%' AND BUILD_NAME='"
									+ brandClient.get(j)
									+ "'  GROUP BY EMAIL_ID UNION ALL  SELECT CONCAT(USER_FIRST_NAME,' ',USER_LAST_NAME) AS USER_NAME ,USER_ID,CREATION_DATE,EMAIL_ID,BUILD_NAME FROM USERS_THREAD_DATA WHERE USER_LEVEL IN (0) AND (EMAIL_ID IS NULL OR EMAIL_ID='') AND EMAIL_ID NOT LIKE '%franconnect.com%' AND EMAIL_ID NOT LIKE '%franconnect.net%' AND BUILD_NAME='"
									+ brandClient.get(j) + "'";
						}
						query = query + " ) AS TEMP1 GROUP BY USER_NAME";
						exchange.getIn().setBody(query.toString());
						Queryout = template.send(endpoint, exchange);
						Queryresult = Queryout.getOut().getBody(List.class);
						i = Queryresult.size();
						userLevel1 = "";
						for (int j = 0; j < i; j++) {
							Map<String, Object> row1 = Queryresult.get(j);
							if (row1.get("USER_NAME") != null) {
								countUnigueActiveUser++;
							}
						}
						row = 0;
						column = 0;
						for (int j = 0; j < 10; j++) {
							sheet.setRowView(j, 500);
						}
						sheet.setColumnView(column, 30);
						label = new Label(column, row, "Customer Name", headerFormat);
						sheet.addCell(label);
						column++;
						label = new Label(column, row, clientName, textFormat);
						sheet.addCell(label);
						sheet.setColumnView(column, 30);
						column = column + 3;
						sheet.setColumnView(column, 30);
						label = new Label(column, row, "Brand Name", headerFormat);
						sheet.addCell(label);
						column++;
						for (int j = 0; j < brandClient.size(); j++) {
							label = new Label(column, row, brandClient.get(j), textFormat);
							sheet.addCell(label);
							sheet.setColumnView(column, 30);
							sheet.setRowView(row, 500);
							row++;
						}
						row++;
						row++;
						column = 0;
						label = new Label(column, row, "Total Active Corporate Users In All Brands", headerFormat);
						sheet.addCell(label);
						column++;
						if (isValidNew(countACU)) {
							numberCell = new jxl.write.Number(column, row, Integer.parseInt(countACU1), numberFormat);
							sheet.addCell(numberCell);
						} else {
							numberCell = new jxl.write.Number(column, row, 0, numberFormat);
							sheet.addCell(numberCell);
						}
						column++;
						column = column + 2;
						sheet.setColumnView(column, 30);
						label = new Label(column, row, "Unique Active Corporate Users in All Brands", headerFormat);
						sheet.addCell(label);
						column++;
						numberCell = new jxl.write.Number(column, row, countUnigueActiveUser, numberFormat);
						sheet.addCell(numberCell);
						row++;
						row++;
						column = 0;
						nextColumn = column + 2;
						sheet.mergeCells(column, row, nextColumn, row);
						label = new Label(column, row, "ALL Corporate Users (" + countACU1 + ")", cellFormat);
						sheet.addCell(label);
						nextColumn++;// 3
						column = nextColumn + 1;// 4
						preColumn = column;// 4
						nextColumn = column + 2;// 7
						sheet.mergeCells(column, row, nextColumn, row);
						label = new Label(column, row, "Unique Corporate Users (" + countUnigueActiveUser + ")",
								cellFormat);
						sheet.addCell(label);
						row++;// 19
						column = 0;
						label = new Label(column, row, "User Name", headerFormat);
						sheet.addCell(label);
						column++;
						sheet.setColumnView(column, 35);
						label = new Label(column, row, "Email ID", headerFormat);
						sheet.addCell(label);
						column++;
						sheet.setColumnView(column, 35);
						label = new Label(column, row, "Brand", headerFormat);
						sheet.addCell(label);
						column++;
						column++;
						label = new Label(column, row, "User Name", headerFormat);
						sheet.addCell(label);
						column++;
						sheet.setColumnView(column, 35);
						label = new Label(column, row, "Email ID", headerFormat);
						sheet.addCell(label);
						column++;
						sheet.setColumnView(column, 35);
						label = new Label(column, row, "Brand", headerFormat);
						sheet.addCell(label);
						previousRow = row;
						query = "SELECT USER_NAME,USER_ID,CREATION_DATE,EMAIL_ID,BUILD_NAME FROM (SELECT CONCAT(USER_FIRST_NAME,' ',USER_LAST_NAME) AS USER_NAME,USER_ID,CREATION_DATE,EMAIL_ID,BUILD_NAME FROM USERS_THREAD_DATA WHERE USER_LEVEL IN (0) AND EMAIL_ID IS NOT NULL AND EMAIL_ID!='' AND EMAIL_ID NOT LIKE '%franconnect.com%' AND EMAIL_ID NOT LIKE '%franconnect.net%' AND USER_FIRST_NAME NOT LIKE 'Test' AND BUILD_NAME='"
								+ clientName
								+ "'  GROUP BY EMAIL_ID UNION ALL  SELECT CONCAT(USER_FIRST_NAME,' ',USER_LAST_NAME) AS USER_NAME ,USER_ID,CREATION_DATE,EMAIL_ID,BUILD_NAME FROM USERS_THREAD_DATA WHERE USER_LEVEL IN (0) AND (EMAIL_ID IS NULL OR EMAIL_ID='') AND EMAIL_ID NOT LIKE '%franconnect.com%' AND EMAIL_ID NOT LIKE '%franconnect.net%' AND USER_FIRST_NAME NOT LIKE 'Test' AND BUILD_NAME='"
								+ clientName + "'";
						for (int j = 0; j < brandClient.size(); j++) {
							query = query + " UNION ALL ";
							query = query
									+ "SELECT CONCAT(USER_FIRST_NAME,' ',USER_LAST_NAME) AS USER_NAME,USER_ID,CREATION_DATE,EMAIL_ID,BUILD_NAME FROM USERS_THREAD_DATA WHERE USER_LEVEL IN (0) AND EMAIL_ID IS NOT NULL AND EMAIL_ID!='' AND EMAIL_ID NOT LIKE '%franconnect.com%' AND EMAIL_ID NOT LIKE '%franconnect.net%' AND BUILD_NAME='"
									+ brandClient.get(j)
									+ "'  GROUP BY EMAIL_ID UNION ALL  SELECT CONCAT(USER_FIRST_NAME,' ',USER_LAST_NAME) AS USER_NAME ,USER_ID,CREATION_DATE,EMAIL_ID,BUILD_NAME FROM USERS_THREAD_DATA WHERE USER_LEVEL IN (0) AND (EMAIL_ID IS NULL OR EMAIL_ID='') AND EMAIL_ID NOT LIKE '%franconnect.com%' AND EMAIL_ID NOT LIKE '%franconnect.net%' AND BUILD_NAME='"
									+ brandClient.get(j) + "'";
						}
						query = query + " )AS TEMP1 ORDER BY USER_NAME";
						exchange.getIn().setBody(query.toString());
						Queryout = template.send(endpoint, exchange);
						Queryresult = Queryout.getOut().getBody(List.class);
						i = Queryresult.size();
						userFirstName = "";
						userID = "";
						emailID = "";
						creationDate = "";
						for (int j = 0; j < i; j++) {
							Map<String, Object> row1 = Queryresult.get(j);
							int userCount = 1;
							if (row1.get("USER_NAME") != null) {
								userFirstName = reverseFilterValue(row1.get("USER_NAME") + "");
							} else {
								userFirstName = "";
							}
							if (row1.get("EMAIL_ID") != null) {
								emailID = row1.get("EMAIL_ID") + "";
								if(emailID.contains("@")) {
									emailID = "****"+emailID.substring(emailID.indexOf("@"), emailID.length());
							} else {
									emailID = "****";
								}
							} else {
								emailID = "";
							}
							if (row1.get("BUILD_NAME") != null) {
								buildName = row1.get("BUILD_NAME") + "";
							} else {
								buildName = "";
							}
							row++;
							preColumn = 0;
							column = preColumn;
							label = new Label(column, row, userFirstName, textFormat);
							sheet.setColumnView(column, 35);
							sheet.addCell(label);
							column++;
							label = new Label(column, row, emailID, textFormat);
							sheet.setColumnView(column, 35);
							sheet.addCell(label);
							column++;
							label = new Label(column, row, buildName, textFormat);
							sheet.setColumnView(column, 35);
							sheet.addCell(label);
						}
						column++;
						column++;
						row = previousRow;
						preColumn = column;
						query = "SELECT USER_NAME,USER_ID,CREATION_DATE,EMAIL_ID,BUILD_NAME FROM (SELECT CONCAT(USER_FIRST_NAME,' ',USER_LAST_NAME) AS USER_NAME,USER_ID,CREATION_DATE,EMAIL_ID,BUILD_NAME FROM USERS_THREAD_DATA WHERE USER_LEVEL IN (0) AND EMAIL_ID IS NOT NULL AND EMAIL_ID!='' AND EMAIL_ID NOT LIKE '%franconnect.com%' AND EMAIL_ID NOT LIKE '%franconnect.net%' AND USER_FIRST_NAME NOT LIKE 'Test' AND BUILD_NAME='"
								+ clientName
								+ "'  GROUP BY EMAIL_ID UNION ALL  SELECT CONCAT(USER_FIRST_NAME,' ',USER_LAST_NAME) AS USER_NAME ,USER_ID,CREATION_DATE,EMAIL_ID,BUILD_NAME FROM USERS_THREAD_DATA WHERE USER_LEVEL IN (0) AND (EMAIL_ID IS NULL OR EMAIL_ID='') AND EMAIL_ID NOT LIKE '%franconnect.com%' AND EMAIL_ID NOT LIKE '%franconnect.net%' AND USER_FIRST_NAME NOT LIKE 'Test' AND BUILD_NAME='"
								+ clientName + "'";
						for (int j = 0; j < brandClient.size(); j++) {
							query = query + " UNION ALL ";
							query = query
									+ "SELECT CONCAT(USER_FIRST_NAME,' ',USER_LAST_NAME) AS USER_NAME,USER_ID,CREATION_DATE,EMAIL_ID,BUILD_NAME FROM USERS_THREAD_DATA WHERE USER_LEVEL IN (0) AND EMAIL_ID IS NOT NULL AND EMAIL_ID!='' AND EMAIL_ID NOT LIKE '%franconnect.com%' AND EMAIL_ID NOT LIKE '%franconnect.net%' AND BUILD_NAME='"
									+ brandClient.get(j)
									+ "'  GROUP BY EMAIL_ID UNION ALL  SELECT CONCAT(USER_FIRST_NAME,' ',USER_LAST_NAME) AS USER_NAME ,USER_ID,CREATION_DATE,EMAIL_ID,BUILD_NAME FROM USERS_THREAD_DATA WHERE USER_LEVEL IN (0) AND (EMAIL_ID IS NULL OR EMAIL_ID='') AND EMAIL_ID NOT LIKE '%franconnect.com%' AND EMAIL_ID NOT LIKE '%franconnect.net%' AND BUILD_NAME='"
									+ brandClient.get(j) + "'";
						}
						query = query + " )AS TEMP1 GROUP BY USER_NAME ORDER BY USER_NAME";
						exchange.getIn().setBody(query.toString());
						Queryout = template.send(endpoint, exchange);
						Queryresult = Queryout.getOut().getBody(List.class);
						i = Queryresult.size();
						userFirstName = "";
						userID = "";
						emailID = "";
						creationDate = "";
						for (int j = 0; j < i; j++) {
							Map<String, Object> row1 = Queryresult.get(j);
							int userCount = 1;
							if (row1.get("USER_NAME") != null) {
								userFirstName = reverseFilterValue(row1.get("USER_NAME") + "");
							} else {
								userFirstName = "";
							}
							if (row1.get("EMAIL_ID") != null) {
								emailID = row1.get("EMAIL_ID") + "";
								if(emailID.contains("@")) {
									emailID = "****"+emailID.substring(emailID.indexOf("@"), emailID.length());
								}else {
									emailID = "****";
								}
							} else {
								emailID = "";
							}
							if (row1.get("BUILD_NAME") != null) {
								buildName = row1.get("BUILD_NAME") + "";
							} else {
								buildName = "";
							}
							row++;
							column = preColumn;
							label = new Label(column, row, userFirstName, textFormat);
							sheet.setColumnView(column, 35);
							sheet.addCell(label);
							column++;
							label = new Label(column, row, emailID, textFormat);
							sheet.setColumnView(column, 35);
							sheet.addCell(label);
							column++;
							label = new Label(column, row, buildName, textFormat);
							sheet.setColumnView(column, 35);
							sheet.addCell(label);
						}
					} catch (Exception e) {
						System.out.println("=Exception Found for Client====================>" + codeValue + "\n"
								+ stackTraceToString(e));
						e.printStackTrace();
					}
				}
			}
		}
		try {
			workbook.write();
			workbook.close();
		} catch (Exception e) {
			System.out.println(
					"=Exception Found for Client====================>" + codeValue + "\n" + stackTraceToString(e));
			e.printStackTrace();
		}
	}

	public static void genrateSUExportFile(LinkedHashMap<String, ClientInfo> clients, String exportFilepath) {
		Endpoint connection = DBConnectionProvider.getInstance().getDBConnection("InHouse");
		Exchange exchange = connection.createExchange();
		String query = "";
		CamelContext context = exchange.getContext();
		Endpoint endpoint = context.getEndpoint("direct:InHouse");
		ProducerTemplate template = context.createProducerTemplate();
		exchange = endpoint.createExchange();
		Exchange Queryout = null;
		List<Map<String, Object>> Queryresult = null;
		int i = 0;
		// System.out.println("********************clients+clients........." +
		Iterator<String> clientIt = clients.keySet().iterator();
		String clientName = "";
		String fileName = exportFilepath;

		int row = 0;
		int column = 0;

		int rowMonthCal = 0;
		int columnMonthCal = 0;

		int previousRow = row;
		int nextColumn = column;

		int preColumn = 0;

		WritableSheet sheet = null;
		WritableSheet sheetMonthCal = null;
		WritableFont headerFont = null;
		WritableCellFormat headerFormat = null;
		WritableFont textFont = null;
		WritableCellFormat textFormat = null;
		WritableCellFormat cellFormat = null;
		WritableCellFormat numberFormat = null;
		WritableCellFormat stringFormat = null;
		SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat format2 = new SimpleDateFormat("MM-dd-yyyy");
		String DBdate = null;

		NumberFormat decimalNo = null;
		Label label = null;
		jxl.write.Number numberCell = null;
		String codeValue = "";

		WritableWorkbook workbook = null;
		ClientInfo dbProps = null;
		try {

			File file = new File(fileName);
			if (file.exists()) {
				file.delete();
			}
			WorkbookSettings wbSettings = new WorkbookSettings();
			wbSettings.setRationalization(false);// Indexing Issue
			workbook = Workbook.createWorkbook(file, wbSettings);
			sheetMonthCal = workbook.createSheet("Monthly_Billing", 0);
		} catch (Exception e) {
			System.out.println(
					"=Exception Found for Client====================>" + codeValue + "\n" + stackTraceToString(e));
			e.printStackTrace();
		}

		String atleastOne = "0", moreThanFive = "0", extCal = "0", cal = "0", activeLocation = "0",activeIndevelopmentLocation="0",
				withoutUserLoc = "0", fdd = "0";
		String userFirstName = "", userID = "", emailID = "", usertype = "", areaName = "", franchiseID = "",
				creationDate = "", systemTime = "", admsystemTime = "", clientSystemTime = "";
		String countACU = "0", countARU = "0", countADU = "0", userLevel1 = "", countSpreengreenVenderUser9 = "0",
				countDeconetSupplierUser4 = "0";
		String countAceVendorUser10 = "0", countATU = "0", countAFU = "0";
		String numberOfUsers = "0", openingDate = "";
		String count = "0", moduleQuery="",modulelist="",activeOpnerLoc="0";
		String moduleCount="0";

		String insight1 = "0", insight2 = "0";
		int sheetCount = 1;
		HashMap<String,String> moduleMap = new HashMap<>() ;
		rowMonthCal = 0;
		while (clientIt.hasNext()) {
			clientName = clientIt.next();
			dbProps = clients.get(clientName);
			codeValue = dbProps.getCodeValue();
			try {

				headerFont = new WritableFont(WritableFont.TAHOMA, 10, WritableFont.BOLD);

				headerFont.setColour(Colour.BLACK);
				headerFormat = new WritableCellFormat(headerFont);
				headerFormat.setBackground(Colour.GREY_40_PERCENT);
				headerFormat.setWrap(true);
				headerFormat.setVerticalAlignment(VerticalAlignment.TOP);
				headerFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
				decimalNo = new NumberFormat("#");
				numberFormat = new WritableCellFormat(decimalNo);
				stringFormat = new WritableCellFormat();
				stringFormat.setAlignment(jxl.format.Alignment.RIGHT);
				// write to datasheet

				textFont = new WritableFont(WritableFont.TAHOMA, 10);
				textFormat = new WritableCellFormat(textFont);
				textFormat.setWrap(true);

				WritableCellFormat cellFormatEmail = new WritableCellFormat();
				cellFormatEmail.setWrap(true);
				cellFormat = new WritableCellFormat(headerFont);
				cellFormat.setBackground(Colour.GREY_40_PERCENT);
				cellFormat.setAlignment(Alignment.CENTRE);
				cellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
				cellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
				cellFormat.setWrap(true);

				sheet = workbook.createSheet(clientName, sheetCount);
				sheetCount++;

				/*
				 * 
				 * Month cal Start
				 */
				int heightInPoints = 26 * 30;
				for (int j = 0; j < 20; j++) {
					sheetMonthCal.setColumnView(j, 29);
				}

				if (rowMonthCal == 0) {
					sheetMonthCal.setRowView(rowMonthCal, heightInPoints);
					columnMonthCal = 0;
					label = new Label(columnMonthCal, rowMonthCal, "Customer Name", headerFormat);
					sheetMonthCal.addCell(label);

					columnMonthCal++;
					label = new Label(columnMonthCal, rowMonthCal, "Build Name", headerFormat);
					sheetMonthCal.addCell(label);

					columnMonthCal++;
					label = new Label(columnMonthCal, rowMonthCal, "Active Corporate Users", headerFormat);
					sheetMonthCal.addCell(label);

					columnMonthCal++;
					label = new Label(columnMonthCal, rowMonthCal, "Active Regional Users", headerFormat);
					sheetMonthCal.addCell(label);

					columnMonthCal++;
					label = new Label(columnMonthCal, rowMonthCal, "Active Other Users", headerFormat);
					sheetMonthCal.addCell(label);

					columnMonthCal++;
					label = new Label(columnMonthCal, rowMonthCal, "Total Active Unique Users", headerFormat);
					sheetMonthCal.addCell(label);

					columnMonthCal++;
					label = new Label(columnMonthCal, rowMonthCal, "Active Franchise Users", headerFormat);
					sheetMonthCal.addCell(label);

					columnMonthCal++;
					label = new Label(columnMonthCal, rowMonthCal, "Active Locations", headerFormat);
					sheetMonthCal.addCell(label);
					
					columnMonthCal++;
					label = new Label(columnMonthCal, rowMonthCal, "Active InDevelopment Locations", headerFormat);
					sheetMonthCal.addCell(label);

					columnMonthCal++;
					label = new Label(columnMonthCal, rowMonthCal, "Unique CAL's (Locations having atleast one User)",
							headerFormat);
					sheetMonthCal.addCell(label);

					columnMonthCal++;
					label = new Label(columnMonthCal, rowMonthCal, "Extra CAL's", headerFormat);
					sheetMonthCal.addCell(label);

					columnMonthCal++;
					label = new Label(columnMonthCal, rowMonthCal, "Total CAL's (Unique CAL's + Extra CAL's)",
							headerFormat);
					sheetMonthCal.addCell(label);
					columnMonthCal++;
					label = new Label(columnMonthCal, rowMonthCal, "Locations without any User", headerFormat);
					sheetMonthCal.addCell(label);
					columnMonthCal++;
					label = new Label(columnMonthCal, rowMonthCal, "Locations having more than 5 Users", headerFormat);
					sheetMonthCal.addCell(label);
					columnMonthCal++;
					label = new Label(columnMonthCal, rowMonthCal, "FDD Sent", headerFormat);
					sheetMonthCal.addCell(label);
					columnMonthCal++;
					label = new Label(columnMonthCal, rowMonthCal, "Total System Time", headerFormat);
					sheetMonthCal.addCell(label);
					columnMonthCal++;
					label = new Label(columnMonthCal, rowMonthCal, "Admin System Time", headerFormat);
					sheetMonthCal.addCell(label);
					columnMonthCal++;
					label = new Label(columnMonthCal, rowMonthCal, "Total System Time - Admin System Time",
							headerFormat);
					sheetMonthCal.addCell(label);
					moduleQuery="SELECT MODULE_NAME,KEYVAL_NAME,MODULE_DISPLAY_NAME FROM MODULE_LIST ORDER BY MODULE_DISPLAY_NAME";
					Map<String, Object> modulerow = null;
					String moduleDisplayName="";
					String moduleName="";
					List data=QueryUtil.executeQuerySelect(endpoint,moduleQuery.toString());
					if (data != null) {
						int size = data.size();
						for (int j = 0; j < size; j++) {
							modulerow = (Map<String, Object>) data.get(j);
							columnMonthCal++;
							moduleDisplayName=modulerow.get("MODULE_DISPLAY_NAME")+"";
							moduleName=modulerow.get("MODULE_NAME")+"";
							if(moduleDisplayName.equals(moduleName)){
								label = new Label(columnMonthCal, rowMonthCal, moduleDisplayName,
										headerFormat);
								moduleMap.put(modulerow.get("KEYVAL_NAME")+"", moduleDisplayName);
							}else{
							label = new Label(columnMonthCal, rowMonthCal, moduleDisplayName+"/"+moduleName,
									headerFormat);
							moduleMap.put(modulerow.get("KEYVAL_NAME")+"", moduleDisplayName+"/"+moduleName);
							}
							sheetMonthCal.addCell(label);
						}
					}
					//System.out.println(moduleMap);
					//System.out.println(sheetMonthCal.findCell("Ad Builder").getColumn());
					//System.out.println(sheetMonthCal.findCell("Analytics").getColumn());
					if (Constants.version.equals("SaaS")) {
						columnMonthCal++;
						label = new Label(columnMonthCal, rowMonthCal, "Logged Insights Users", headerFormat);
						sheetMonthCal.addCell(label);
					}
				}

				rowMonthCal++;
				columnMonthCal = 0;
				label = new Label(columnMonthCal, rowMonthCal, clientName, textFormat);
				sheetMonthCal.addCell(label);

				WritableHyperlink hl = new WritableHyperlink(columnMonthCal, rowMonthCal, clientName, sheet, 0, 0);
				sheetMonthCal.addHyperlink(hl);

				columnMonthCal++;
				label = new Label(columnMonthCal, rowMonthCal, codeValue, textFormat);
				sheetMonthCal.addCell(label);

				query = "SELECT COUNT(*) AS COUNT ,USER_LEVEL FROM (SELECT USER_NO,USER_LEVEL FROM USERS_THREAD_DATA WHERE USER_LEVEL IN (0,2,6,4,9,10) AND EMAIL_ID IS NOT NULL AND EMAIL_ID!='' AND EMAIL_ID NOT LIKE '%franconnect.com%' AND EMAIL_ID NOT LIKE '%franconnect.net%' AND BUILD_NAME='"
						+ clientName
						+ "' GROUP BY EMAIL_ID,USER_LEVEL UNION ALL  SELECT USER_NO,USER_LEVEL FROM USERS_THREAD_DATA WHERE USER_LEVEL IN (0,2,6,4,9,10) AND (EMAIL_ID IS NULL OR EMAIL_ID='') AND EMAIL_ID NOT LIKE '%franconnect.com%' AND EMAIL_ID NOT LIKE '%franconnect.net%' AND BUILD_NAME='"
						+ clientName + "') AS TEMP GROUP BY USER_LEVEL";
				exchange.getIn().setBody(query.toString());
				// System.out.println("=====Count=query=Active
				// Corporate/Regional/Other========>"+query.toString());
				Queryout = template.send(endpoint, exchange);
				Queryresult = Queryout.getOut().getBody(List.class);

				i = Queryresult.size();

				userLevel1 = "";
				count = "0";
				countACU = "0";
				countARU = "0";
				countADU = "0";
				countSpreengreenVenderUser9 = "0";
				countDeconetSupplierUser4 = "0";
				countAceVendorUser10 = "0";
				for (int j = 0; j < i; j++) {
					Map<String, Object> row1 = Queryresult.get(j);
					if (row1.get("USER_LEVEL") != null) {
						userLevel1 = row1.get("USER_LEVEL") + "";
					} else {
						userLevel1 = "";
					}
					if (row1.get("COUNT") != null) {
						count = row1.get("COUNT") + "";
					} else {
						count = "0";
					}

					if ("0".equals(userLevel1)) {
						countACU = count;
					} else if ("2".equals(userLevel1)) {
						countARU = count;
					} else if ("6".equals(userLevel1)) {
						countADU = count;
					} else if ("9".equals(userLevel1)) {
						countSpreengreenVenderUser9 = count;
						countADU = count;
					} else if ("4".equals(userLevel1)) {
						countDeconetSupplierUser4 = count;
						countADU = count;
					} else if ("10".equals(userLevel1)) {
						countAceVendorUser10 = count;
						countADU = count;
					}

				}

				columnMonthCal++;

				// label = new
				// Label(columnMonthCal,rowMonthCal,countACU,textFormat);
				// sheetMonthCal.addCell(label);

				if (isValidNew(countACU)) {
					numberCell = new jxl.write.Number(columnMonthCal, rowMonthCal, Integer.parseInt(countACU),
							numberFormat);
					sheetMonthCal.addCell(numberCell);
				} else {
					numberCell = new jxl.write.Number(columnMonthCal, rowMonthCal, 0, numberFormat);
					sheetMonthCal.addCell(numberCell);
				}

				columnMonthCal++;

				// label = new
				// Label(columnMonthCal,rowMonthCal,countARU,textFormat);
				// sheetMonthCal.addCell(label);

				if (isValidNew(countARU)) {
					numberCell = new jxl.write.Number(columnMonthCal, rowMonthCal, Integer.parseInt(countARU),
							numberFormat);
					sheetMonthCal.addCell(numberCell);
				} else {
					numberCell = new jxl.write.Number(columnMonthCal, rowMonthCal, 0, numberFormat);
					sheetMonthCal.addCell(numberCell);
				}

				columnMonthCal++;

				// label = new
				// Label(columnMonthCal,rowMonthCal,countADU,textFormat);
				// sheetMonthCal.addCell(label);

				if (isValidNew(countADU)) {
					numberCell = new jxl.write.Number(columnMonthCal, rowMonthCal, Integer.parseInt(countADU),
							numberFormat);
					sheetMonthCal.addCell(numberCell);
				} else {
					numberCell = new jxl.write.Number(columnMonthCal, rowMonthCal, 0, numberFormat);
					sheetMonthCal.addCell(numberCell);
				}

				query = "SELECT COUNT(*) AS COUNT  FROM (SELECT USER_NO,USER_LEVEL FROM USERS_THREAD_DATA WHERE USER_LEVEL IN (0,2,6,4,9,10) AND EMAIL_ID IS NOT NULL AND EMAIL_ID!='' AND EMAIL_ID NOT LIKE '%franconnect.com%' AND EMAIL_ID NOT LIKE '%franconnect.net%' AND BUILD_NAME='"
						+ clientName
						+ "' GROUP BY EMAIL_ID UNION ALL  SELECT USER_NO,USER_LEVEL FROM USERS_THREAD_DATA WHERE USER_LEVEL IN (0,2,6,4,9,10) AND (EMAIL_ID IS NULL OR EMAIL_ID='') AND EMAIL_ID NOT LIKE '%franconnect.com%' AND EMAIL_ID NOT LIKE '%franconnect.net%' AND BUILD_NAME='"
						+ clientName + "' ) AS TEMP ";
				exchange.getIn().setBody(query.toString());
				// System.out.println("=====Count=query=Total==Active
				// Corporate/Regional/Other========>"+query.toString());
				Queryout = template.send(endpoint, exchange);
				Queryresult = Queryout.getOut().getBody(List.class);

				i = Queryresult.size();
				countATU = "0";

				for (int j = 0; j < i; j++) {
					Map<String, Object> row1 = Queryresult.get(j);
					if (row1.get("COUNT") != null) {
						countATU = row1.get("COUNT") + "";
					} else {
						countATU = "0";
					}
				}

				columnMonthCal++;
				// label = new
				// Label(columnMonthCal,rowMonthCal,countATU,textFormat);
				// sheetMonthCal.addCell(label);

				if (isValidNew(countATU)) {
					numberCell = new jxl.write.Number(columnMonthCal, rowMonthCal, Integer.parseInt(countATU),
							numberFormat);
					sheetMonthCal.addCell(numberCell);
				} else {
					numberCell = new jxl.write.Number(columnMonthCal, rowMonthCal, 0, numberFormat);
					sheetMonthCal.addCell(numberCell);
				}

				query = "SELECT COUNT(*) AS COUNT  FROM (SELECT USER_NO,USER_LEVEL FROM USERS_THREAD_DATA WHERE USER_LEVEL IN (1) AND EMAIL_ID IS NOT NULL AND EMAIL_ID!='' AND EMAIL_ID NOT LIKE '%franconnect.com%' AND EMAIL_ID NOT LIKE '%franconnect.net%' AND BUILD_NAME='"
						+ clientName
						+ "' GROUP BY EMAIL_ID,FRANCHISEE_NAME UNION ALL  SELECT USER_NO,USER_LEVEL FROM USERS_THREAD_DATA WHERE USER_LEVEL IN (1) AND (EMAIL_ID IS NULL OR EMAIL_ID='') AND EMAIL_ID NOT LIKE '%franconnect.com%' AND EMAIL_ID NOT LIKE '%franconnect.net%' AND BUILD_NAME='"
						+ clientName + "') AS TEMP";
				exchange.getIn().setBody(query.toString());
				// System.out.println("=====Count=query=Total==Active
				// Franchisee========>"+query.toString());
				Queryout = template.send(endpoint, exchange);
				Queryresult = Queryout.getOut().getBody(List.class);

				i = Queryresult.size();
				countAFU = "0";
				for (int j = 0; j < i; j++) {
					Map<String, Object> row1 = Queryresult.get(j);
					if (row1.get("COUNT") != null) {
						countAFU = row1.get("COUNT") + "";
					} else {
						countAFU = "0";
					}
				}

				columnMonthCal++;
				// label = new
				// Label(columnMonthCal,rowMonthCal,countAFU,textFormat);
				// sheetMonthCal.addCell(label);
				if (isValidNew(countAFU)) {
					numberCell = new jxl.write.Number(columnMonthCal, rowMonthCal, Integer.parseInt(countAFU),
							numberFormat);
					sheetMonthCal.addCell(numberCell);
				} else {
					numberCell = new jxl.write.Number(columnMonthCal, rowMonthCal, 0, numberFormat);
					sheetMonthCal.addCell(numberCell);
				}

				query = "SELECT AT_LEAST_ONE, MORE_THEN_FIVE, EXT_CAL, CAL, ACTIVE_LOCATIONS, NO_USER_LOCATIONS,FDD_COUNT,TOTAL_SYSTEM_TIME,ADM_SYSTEM_TIME,CLIENT_SYSTEM_TIME,MODULE_NAME,MODULE_COUNT,IN_DEVELOPMENT_LOCATION FROM USER_BILLING_THREAD_DATA WHERE BUILD_NAME='"
						+ clientName + "'";
				exchange.getIn().setBody(query.toString());
				Queryout = template.send(endpoint, exchange);
				Queryresult = Queryout.getOut().getBody(List.class);
				i = Queryresult.size();

				atleastOne = "0";
				moreThanFive = "0";
				extCal = "0";
				cal = "0";
				activeLocation = "0";
				withoutUserLoc = "0";
				fdd = "0";
				modulelist="";
				activeOpnerLoc="0";

				for (int j = 0; j < i; j++) {
					Map<String, Object> row1 = Queryresult.get(j);
					if (row1.get("AT_LEAST_ONE") != null) {

						atleastOne = row1.get("AT_LEAST_ONE") + "";
					} else {
						atleastOne = "0";
					}
					if (row1.get("MORE_THEN_FIVE") != null) {
						moreThanFive = row1.get("MORE_THEN_FIVE") + "";
					} else {
						moreThanFive = "0";
					}

					if (row1.get("EXT_CAL") != null) {
						extCal = row1.get("EXT_CAL") + "";
					} else {
						extCal = "0";
					}
					if (row1.get("CAL") != null) {
						cal = row1.get("CAL") + "";
					} else {
						cal = "0";
					}

					if (row1.get("ACTIVE_LOCATIONS") != null) {
						activeLocation = row1.get("ACTIVE_LOCATIONS") + "";
					} else {
						activeLocation = "0";
					}
					if (row1.get("NO_USER_LOCATIONS") != null) {
						withoutUserLoc = row1.get("NO_USER_LOCATIONS") + "";
					} else {
						withoutUserLoc = "0";
					}

					if (row1.get("FDD_COUNT") != null) {
						fdd = row1.get("FDD_COUNT") + "";
					} else {
						fdd = "0";
					}
					if (row1.get("TOTAL_SYSTEM_TIME") != null) {
						systemTime = row1.get("TOTAL_SYSTEM_TIME") + "";
					} else {
						systemTime = "00:00:00";
					}
					if (row1.get("ADM_SYSTEM_TIME") != null) {
						admsystemTime = row1.get("ADM_SYSTEM_TIME") + "";
					} else {
						admsystemTime = "00:00:00";
					}
					if (row1.get("CLIENT_SYSTEM_TIME") != null) {
						clientSystemTime = row1.get("CLIENT_SYSTEM_TIME") + "";
					} else {
						clientSystemTime = "00:00:00";
					}
					if (row1.get("MODULE_COUNT") != null) {
						moduleCount = row1.get("MODULE_COUNT") +"";
					} else {
						moduleCount = "0";
					}
					if (row1.get("MODULE_NAME") != null) {
						modulelist = row1.get("MODULE_NAME") + "";
					} else {
						modulelist = "";
					}
					if (row1.get("IN_DEVELOPMENT_LOCATION") != null) {
						activeOpnerLoc = row1.get("IN_DEVELOPMENT_LOCATION") + "";
						} else {
							activeOpnerLoc = "";	
				}

				}
				System.out.println(moduleCount+"**********************8"+modulelist);
				columnMonthCal++;
				// label = new
				// Label(columnMonthCal,rowMonthCal,activeLocation,textFormat);
				// sheetMonthCal.addCell(label);
				if (isValidNew(activeLocation)) {
					numberCell = new jxl.write.Number(columnMonthCal, rowMonthCal, Integer.parseInt(activeLocation),
							numberFormat);
					sheetMonthCal.addCell(numberCell);
				} else {
					numberCell = new jxl.write.Number(columnMonthCal, rowMonthCal, 0, numberFormat);
					sheetMonthCal.addCell(numberCell);
				}

				columnMonthCal++;
				
				if (isValidNew(activeOpnerLoc)) {
					numberCell = new jxl.write.Number(columnMonthCal, rowMonthCal, Integer.parseInt(activeOpnerLoc),
							numberFormat);
					sheetMonthCal.addCell(numberCell);
				} else {
					numberCell = new jxl.write.Number(columnMonthCal, rowMonthCal, 0, numberFormat);
					sheetMonthCal.addCell(numberCell);
				}

				columnMonthCal++;
				
				// label = new
				// Label(columnMonthCal,rowMonthCal,atleastOne,textFormat);
				// sheetMonthCal.addCell(label);
				if (isValidNew(atleastOne)) {
					numberCell = new jxl.write.Number(columnMonthCal, rowMonthCal, Integer.parseInt(atleastOne),
							numberFormat);
					sheetMonthCal.addCell(numberCell);
				} else {
					numberCell = new jxl.write.Number(columnMonthCal, rowMonthCal, 0, numberFormat);
					sheetMonthCal.addCell(numberCell);
				}

				columnMonthCal++;
				// label = new
				// Label(columnMonthCal,rowMonthCal,extCal,textFormat);
				// sheetMonthCal.addCell(label);

				if (isValidNew(extCal)) {
					numberCell = new jxl.write.Number(columnMonthCal, rowMonthCal, Integer.parseInt(extCal),
							numberFormat);
					sheetMonthCal.addCell(numberCell);
				} else {
					numberCell = new jxl.write.Number(columnMonthCal, rowMonthCal, 0, numberFormat);
					sheetMonthCal.addCell(numberCell);
				}

				columnMonthCal++;
				// label = new Label(columnMonthCal,rowMonthCal,cal,textFormat);
				// sheetMonthCal.addCell(label);
				if (isValidNew(cal)) {
					numberCell = new jxl.write.Number(columnMonthCal, rowMonthCal, Integer.parseInt(cal), numberFormat);
					sheetMonthCal.addCell(numberCell);
				} else {
					numberCell = new jxl.write.Number(columnMonthCal, rowMonthCal, 0, numberFormat);
					sheetMonthCal.addCell(numberCell);
				}

				columnMonthCal++;
				// label = new
				// Label(columnMonthCal,rowMonthCal,withoutUserLoc,textFormat);
				// sheetMonthCal.addCell(label);

				if (isValidNew(withoutUserLoc)) {
					numberCell = new jxl.write.Number(columnMonthCal, rowMonthCal, Integer.parseInt(withoutUserLoc),
							numberFormat);
					sheetMonthCal.addCell(numberCell);
				} else {
					numberCell = new jxl.write.Number(columnMonthCal, rowMonthCal, 0, numberFormat);
					sheetMonthCal.addCell(numberCell);
				}

				columnMonthCal++;
				// label = new
				// Label(columnMonthCal,rowMonthCal,moreThanFive,textFormat);
				// sheetMonthCal.addCell(label);
				if (isValidNew(moreThanFive)) {
					numberCell = new jxl.write.Number(columnMonthCal, rowMonthCal, Integer.parseInt(moreThanFive),
							numberFormat);
					sheetMonthCal.addCell(numberCell);
				} else {
					numberCell = new jxl.write.Number(columnMonthCal, rowMonthCal, 0, numberFormat);
					sheetMonthCal.addCell(numberCell);
				}

				columnMonthCal++;
				// label = new Label(columnMonthCal,rowMonthCal,fdd,textFormat);
				// sheetMonthCal.addCell(label);
				if (isValidNew(fdd)) {
					numberCell = new jxl.write.Number(columnMonthCal, rowMonthCal, Integer.parseInt(fdd), numberFormat);
					sheetMonthCal.addCell(numberCell);
				} else {
					numberCell = new jxl.write.Number(columnMonthCal, rowMonthCal, 0, numberFormat);
					sheetMonthCal.addCell(numberCell);
				}

				columnMonthCal++;
				if (isValidNew(systemTime)) {
					sheetMonthCal.addCell(new Label(columnMonthCal, rowMonthCal, systemTime, stringFormat));
				} else {
					sheetMonthCal.addCell(new Label(columnMonthCal, rowMonthCal, "00:00:00", stringFormat));
				}
				columnMonthCal++;
				if (isValidNew(admsystemTime)) {
					sheetMonthCal.addCell(new Label(columnMonthCal, rowMonthCal, admsystemTime, stringFormat));
				} else {
					sheetMonthCal.addCell(new Label(columnMonthCal, rowMonthCal, "00:00:00", stringFormat));
				}
				columnMonthCal++;
				if (isValidNew(clientSystemTime)) {
					sheetMonthCal.addCell(new Label(columnMonthCal, rowMonthCal, clientSystemTime, stringFormat));
				} else {
					sheetMonthCal.addCell(new Label(columnMonthCal, rowMonthCal, "00:00:00", stringFormat));
				}

				columnMonthCal++;
				int modulecolumn=0;
				if(!moduleCount.equals("0")){
					String moduleArray[]=modulelist.split(",");
					for(int count1=0; count1<moduleArray.length; count1++){
						if(moduleMap.containsKey(moduleArray[count1])){
							modulecolumn=sheetMonthCal.findCell(moduleMap.get(moduleArray[count1])).getColumn();
							sheetMonthCal.addCell(new Label(modulecolumn, rowMonthCal, "Y", stringFormat));
						}
					}
				}
				if (Constants.version.equals("SaaS")) {
					query = " SELECT INSIGHT_USER FROM USER_BILLING_THREAD_DATA WHERE BUILD_NAME='" + clientName + "'";
					exchange.getIn().setBody(query.toString());
					Queryout = template.send(endpoint, exchange);
					Queryresult = Queryout.getOut().getBody(List.class);
					insight1 = "0";
					insight2 = "0";
					i = Queryresult.size();
					for (int j = 0; j < i; j++) {
						Map<String, Object> row1 = Queryresult.get(j);
						if (row1.get("INSIGHT_USER") != null) {
							insight1 = row1.get("INSIGHT_USER") + "";
						} else {
							insight1 = "0";
						}
					}
					columnMonthCal++;
					if (isValidNew(insight1)) {
						numberCell = new jxl.write.Number(columnMonthCal, rowMonthCal, Integer.parseInt(insight1),
								numberFormat);
						sheetMonthCal.addCell(numberCell);
					} else {
						numberCell = new jxl.write.Number(columnMonthCal, rowMonthCal, 0, numberFormat);
						sheetMonthCal.addCell(numberCell);
					}
				}
				/*
				 * Month cal end
				 * 
				 */

				jxl.SheetSettings setSetting = sheetMonthCal.getSettings();
				setSetting.setHorizontalFreeze(1);
				setSetting.setVerticalFreeze(1); // FCSKYS-18246 Need to freeze
				row = 0;
				column = 0;
				for (int j = 0; j < 23; j++) {
					sheet.setRowView(j, 500);

				}
				sheet.setColumnView(column, 30);
				label = new Label(column, row, "Customer Name", headerFormat);
				sheet.addCell(label);
				column++;
				label = new Label(column, row, clientName, textFormat);
				sheet.addCell(label);
				sheet.setColumnView(column, 30);

				row++;
				column = 0;
				label = new Label(column, row, "Build Name", headerFormat);
				sheet.addCell(label);
				column++;
				label = new Label(column, row, codeValue, textFormat);
				sheet.addCell(label);
				sheet.setColumnView(column, 40);

				row++;
				row++;

				row++;
				column = 0;
				label = new Label(column, row, "Active Corporate Users", headerFormat);
				sheet.addCell(label);

				column++;
				// label = new Label(column,row,countACU,textFormat);
				// sheet.addCell(label);
				if (isValidNew(countACU)) {
					numberCell = new jxl.write.Number(column, row, Integer.parseInt(countACU), numberFormat);
					sheet.addCell(numberCell);
				} else {
					numberCell = new jxl.write.Number(column, row, 0, numberFormat);
					sheet.addCell(numberCell);
				}

				row++;
				column = 0;
				label = new Label(column, row, "Active Regional Users", headerFormat);
				sheet.addCell(label);

				column++;
				// label = new Label(column,row,countARU,textFormat);
				// sheet.addCell(label);
				if (isValidNew(countARU)) {
					numberCell = new jxl.write.Number(column, row, Integer.parseInt(countARU), numberFormat);
					sheet.addCell(numberCell);
				} else {
					numberCell = new jxl.write.Number(column, row, 0, numberFormat);
					sheet.addCell(numberCell);
				}

				row++;
				column = 0;
				label = new Label(column, row, "Active Other Users", headerFormat);
				sheet.addCell(label);

				column++;
				// label = new Label(column,row,countADU,textFormat);
				// sheet.addCell(label);
				if (isValidNew(countADU)) {
					numberCell = new jxl.write.Number(column, row, Integer.parseInt(countADU), numberFormat);
					sheet.addCell(numberCell);
				} else {
					numberCell = new jxl.write.Number(column, row, 0, numberFormat);
					sheet.addCell(numberCell);
				}

				row++;
				column = 0;
				label = new Label(column, row, "Active Franchise Users", headerFormat);
				sheet.addCell(label);

				column++;
				// label = new Label(column,row,countAFU,textFormat);
				// sheet.addCell(label);
				if (isValidNew(countAFU)) {
					numberCell = new jxl.write.Number(column, row, Integer.parseInt(countAFU), numberFormat);
					sheet.addCell(numberCell);
				} else {
					numberCell = new jxl.write.Number(column, row, 0, numberFormat);
					sheet.addCell(numberCell);
				}

				row++;
				column = 0;
				label = new Label(column, row, "Total Active Unique Users", headerFormat);
				sheet.addCell(label);
				column++;
				// label = new Label(column,row,countATU,textFormat);
				// sheet.addCell(label);
				if (isValidNew(countATU)) {
					numberCell = new jxl.write.Number(column, row, Integer.parseInt(countATU), numberFormat);
					sheet.addCell(numberCell);
				} else {
					numberCell = new jxl.write.Number(column, row, 0, numberFormat);
					sheet.addCell(numberCell);
				}

				row++;
				column = 0;
				label = new Label(column, row, "Active Location", headerFormat);
				sheet.addCell(label);
				column++;
				// label = new Label(column,row,activeLocation,textFormat);
				// sheet.addCell(label);
				if (isValidNew(activeLocation)) {
					numberCell = new jxl.write.Number(column, row, Integer.parseInt(activeLocation), numberFormat);
					sheet.addCell(numberCell);
				} else {
					numberCell = new jxl.write.Number(column, row, 0, numberFormat);
					sheet.addCell(numberCell);
				}
				row++;
				column = 0;
				label = new Label(column, row, "Active InDevelopment Location", headerFormat);
				sheet.addCell(label);
				column++;
				// label = new Label(column,row,activeLocation,textFormat);
				// sheet.addCell(label);
				if (isValidNew(activeLocation)) {
					numberCell = new jxl.write.Number(column, row, Integer.parseInt(activeOpnerLoc), numberFormat);
					sheet.addCell(numberCell);
				} else {
					numberCell = new jxl.write.Number(column, row, 0, numberFormat);
					sheet.addCell(numberCell);
				}
				
				
				
				

				row++;
				column = 0;
				label = new Label(column, row, "Unique CAL's (Locations having atleast one User)", headerFormat);
				sheet.addCell(label);
				column++;
				// label = new Label(column,row,atleastOne,textFormat);
				// sheet.addCell(label);
				if (isValidNew(atleastOne)) {
					numberCell = new jxl.write.Number(column, row, Integer.parseInt(atleastOne), numberFormat);
					sheet.addCell(numberCell);
				} else {
					numberCell = new jxl.write.Number(column, row, 0, numberFormat);
					sheet.addCell(numberCell);
				}

				row++;
				column = 0;
				label = new Label(column, row, "Extra CAL's", headerFormat);
				sheet.addCell(label);
				column++;
				// label = new Label(column,row,extCal,textFormat);
				// sheet.addCell(label);
				if (isValidNew(extCal)) {
					numberCell = new jxl.write.Number(column, row, Integer.parseInt(extCal), numberFormat);
					sheet.addCell(numberCell);
				} else {
					numberCell = new jxl.write.Number(column, row, 0, numberFormat);
					sheet.addCell(numberCell);
				}

				row++;
				column = 0;
				label = new Label(column, row, "Total CAL's (Unique CAL's + Extra CAL's)", headerFormat);
				sheet.addCell(label);
				column++;
				// label = new Label(column,row,cal,textFormat);
				// sheet.addCell(label);
				if (isValidNew(cal)) {
					numberCell = new jxl.write.Number(column, row, Integer.parseInt(cal), numberFormat);
					sheet.addCell(numberCell);
				} else {
					numberCell = new jxl.write.Number(column, row, 0, numberFormat);
					sheet.addCell(numberCell);
				}

				row++;
				column = 0;
				label = new Label(column, row, "Locations without any User", headerFormat);
				sheet.addCell(label);
				column++;
				// label = new Label(column,row,withoutUserLoc,textFormat);
				// sheet.addCell(label);
				if (isValidNew(withoutUserLoc)) {
					numberCell = new jxl.write.Number(column, row, Integer.parseInt(withoutUserLoc), numberFormat);
					sheet.addCell(numberCell);
				} else {
					numberCell = new jxl.write.Number(column, row, 0, numberFormat);
					sheet.addCell(numberCell);
				}

				row++;
				column = 0;
				label = new Label(column, row, "Locations having more than 5 Users", headerFormat);
				sheet.addCell(label);
				column++;
				// label = new Label(column,row,moreThanFive,textFormat);
				// sheet.addCell(label);
				if (isValidNew(moreThanFive)) {
					numberCell = new jxl.write.Number(column, row, Integer.parseInt(moreThanFive), numberFormat);
					sheet.addCell(numberCell);
				} else {
					numberCell = new jxl.write.Number(column, row, 0, numberFormat);
					sheet.addCell(numberCell);
				}

				row++;
				column = 0;
				label = new Label(column, row, "FDD Sent", headerFormat);
				sheet.addCell(label);
				column++;
				// label = new Label(column,row,fdd,textFormat);
				// sheet.addCell(label);
				if (isValidNew(fdd)) {
					numberCell = new jxl.write.Number(column, row, Integer.parseInt(fdd), numberFormat);
					sheet.addCell(numberCell);
				} else {
					numberCell = new jxl.write.Number(column, row, 0, numberFormat);
					sheet.addCell(numberCell);
				}

				row++;
				column = 0;
				label = new Label(column, row, "Total System Time", headerFormat);
				sheet.addCell(label);
				column++;
				if (isValidNew(systemTime)) {
					sheet.addCell(new Label(column, row, systemTime, stringFormat));
				} else {
					sheet.addCell(new Label(column, row, "00:00:00", stringFormat));
				}
				row++;
				column = 0;
				label = new Label(column, row, "Admin System Time", headerFormat);
				sheet.addCell(label);
				column++;
				if (isValidNew(admsystemTime)) {
					sheet.addCell(new Label(column, row, admsystemTime, stringFormat));
				} else {
					sheet.addCell(new Label(column, row, "00:00:00", stringFormat));
				}
				row++;
				column = 0;
				label = new Label(column, row, "Total System Time - Admin System Time", headerFormat);
				sheet.addCell(label);
				column++;
				if (isValidNew(clientSystemTime)) {
					sheet.addCell(new Label(column, row, clientSystemTime, stringFormat));
				} else {
					sheet.addCell(new Label(column, row, "00:00:00", stringFormat));
				}

				row++;
				row++;

				/*
				 * Corp Users
				 */

				row++;// 18
				previousRow = row;// 18
				column = 0;
				nextColumn = column + 3;// 2
				// Merge col[0-2] and row[18]
				sheet.mergeCells(column, row, nextColumn, row);

				label = new Label(column, row, "Active Corporate Users (" + countACU + ")", cellFormat);
				sheet.addCell(label);

				row++;// 19
				column = 0;
				label = new Label(column, row, "User Name", headerFormat);
				sheet.addCell(label);

				column++;
				label = new Label(column, row, "User ID", headerFormat);
				sheet.addCell(label);

				column++;
				sheet.setColumnView(column, 18);
				label = new Label(column, row, "Creation Date", headerFormat);
				sheet.addCell(label);
				column++;
				sheet.setColumnView(column, 35);
				label = new Label(column, row, "Email ID", headerFormat);
				sheet.addCell(label);

				// query="SELECT USER_FIRST_NAME,USER_LAST_NAME,EMAIL_ID FROM
				// USERS_THREAD_DATA WHERE USER_LEVEL=0 AND
				// CODE_VALUE='"+codeValue+"'";

				query = "SELECT CONCAT(USER_FIRST_NAME,' ',USER_LAST_NAME) AS USER_NAME,USER_ID,CREATION_DATE,EMAIL_ID FROM USERS_THREAD_DATA WHERE USER_LEVEL IN (0) AND EMAIL_ID IS NOT NULL AND EMAIL_ID!='' AND EMAIL_ID NOT LIKE '%franconnect.com%' AND EMAIL_ID NOT LIKE '%franconnect.net%' AND BUILD_NAME='"
						+ clientName
						+ "'  GROUP BY EMAIL_ID UNION ALL  SELECT CONCAT(USER_FIRST_NAME,' ',USER_LAST_NAME) AS USER_NAME ,USER_ID,CREATION_DATE,EMAIL_ID FROM USERS_THREAD_DATA WHERE USER_LEVEL IN (0) AND (EMAIL_ID IS NULL OR EMAIL_ID='') AND EMAIL_ID NOT LIKE '%franconnect.com%' AND EMAIL_ID NOT LIKE '%franconnect.net%' AND BUILD_NAME='"
						+ clientName + "' ORDER BY USER_NAME ";

				exchange.getIn().setBody(query.toString());
				// System.out.println("=====Count=query=Total==Active
				// Corporate========>"+query.toString());
				Queryout = template.send(endpoint, exchange);
				Queryresult = Queryout.getOut().getBody(List.class);
				i = Queryresult.size();

				userFirstName = "";
				userID = "";
				emailID = "";
				creationDate = "";
				for (int j = 0; j < i; j++) {
					Map<String, Object> row1 = Queryresult.get(j);
					if (row1.get("USER_NAME") != null) {
						userFirstName = reverseFilterValue(row1.get("USER_NAME") + "");
					} else {
						userFirstName = "";
					}
					if (row1.get("USER_ID") != null) {
						userID = reverseFilterValue(row1.get("USER_ID") + "");
					} else {
						userID = "";
					}
					if (row1.get("CREATION_DATE") != null) {
						creationDate = reverseFilterValue(row1.get("CREATION_DATE") + "");
						creationDate = format2.format((format1.parse(creationDate)));
					} else {
						creationDate = "";
					}

					if (row1.get("EMAIL_ID") != null) {
						emailID = row1.get("EMAIL_ID") + "";
						if(emailID.contains("@")) {
							emailID = "****"+emailID.substring(emailID.indexOf("@"), emailID.length());
						}else {
							emailID = "****";
						}
					} else {
						emailID = "";
					}
					row++;
					column = 0;

					label = new Label(column, row, userFirstName, textFormat);
					sheet.addCell(label);

					column++;
					label = new Label(column, row, userID, textFormat);
					sheet.addCell(label);
					column++;
					label = new Label(column, row, creationDate, textFormat);
					sheet.addCell(label);

					column++;
					label = new Label(column, row, emailID, cellFormatEmail);
					sheet.setColumnView(column, 35);
					sheet.addCell(label);

				}

				/*
				 * Reg Users
				 * 
				 */

				row = previousRow;// 18
				nextColumn++;// 3

				column = nextColumn + 1;// 4

				preColumn = column;// 4

				nextColumn = column + 4;// 7

				// Merge col[3-6] and row[18]
				sheet.mergeCells(column, row, nextColumn, row);
				label = new Label(column, row, "Active Regional Users (" + countARU + ")", cellFormat);
				sheet.addCell(label);

				row++;// 19
				label = new Label(column, row, "User Name", headerFormat);
				sheet.setColumnView(column, 18);
				sheet.addCell(label);

				column++;// 5
				label = new Label(column, row, "User ID", headerFormat);
				sheet.setColumnView(column, 18);
				sheet.addCell(label);
				column++;// 5
				label = new Label(column, row, "Creation Date", headerFormat);
				sheet.setColumnView(column, 18);
				sheet.addCell(label);

				column++;// 6
				label = new Label(column, row, "Email ID", headerFormat);
				sheet.setColumnView(column, 25);
				sheet.addCell(label);

				column++;// 7
				label = new Label(column, row, "Area Name", headerFormat);
				sheet.setColumnView(column, 18);
				sheet.addCell(label);

				// query="SELECT
				// USER_FIRST_NAME,USER_LAST_NAME,EMAIL_ID,AREA_NAME FROM
				// USERS_THREAD_DATA WHERE USER_LEVEL=2 AND
				// CODE_VALUE='"+codeValue+"'";
				query = "SELECT CONCAT(USER_FIRST_NAME,' ',USER_LAST_NAME) AS USER_NAME,USER_ID,CREATION_DATE,EMAIL_ID,AREA_NAME FROM USERS_THREAD_DATA WHERE USER_LEVEL IN (2) AND EMAIL_ID IS NOT NULL AND EMAIL_ID!='' AND EMAIL_ID NOT LIKE '%franconnect.com%' AND EMAIL_ID NOT LIKE '%franconnect.net%' AND BUILD_NAME='"
						+ clientName
						+ "' GROUP BY EMAIL_ID UNION ALL  SELECT CONCAT(USER_FIRST_NAME,' ',USER_LAST_NAME) AS USER_NAME,USER_ID,CREATION_DATE,EMAIL_ID,AREA_NAME FROM USERS_THREAD_DATA WHERE USER_LEVEL IN (2) AND (EMAIL_ID IS NULL OR EMAIL_ID='') AND EMAIL_ID NOT LIKE '%franconnect.com%' AND EMAIL_ID NOT LIKE '%franconnect.net%' AND BUILD_NAME='"
						+ clientName + "' ORDER BY USER_NAME";
				exchange.getIn().setBody(query.toString());
				// System.out.println("=====Count=query=Total==Active
				// Regional========>"+query.toString());
				Queryout = template.send(endpoint, exchange);
				Queryresult = Queryout.getOut().getBody(List.class);
				i = Queryresult.size();

				userFirstName = "";
				userID = "";
				emailID = "";
				areaName = "";
				creationDate = "";
				for (int j = 0; j < i; j++) {
					Map<String, Object> row1 = Queryresult.get(j);
					if (row1.get("USER_NAME") != null) {
						userFirstName = reverseFilterValue(row1.get("USER_NAME") + "");
					} else {
						userFirstName = "";
					}
					if (row1.get("USER_ID") != null) {
						userID = reverseFilterValue(row1.get("USER_ID") + "");
					} else {
						userID = "";
					}
					if (row1.get("CREATION_DATE") != null) {
						creationDate = reverseFilterValue(row1.get("CREATION_DATE") + "");
						creationDate = format2.format((format1.parse(creationDate)));
					} else {
						creationDate = "";
					}

					if (row1.get("EMAIL_ID") != null) {
						emailID = row1.get("EMAIL_ID") + "";
						if(emailID.contains("@")) {
							emailID = "****"+emailID.substring(emailID.indexOf("@"), emailID.length());
						}else {
							emailID = "****";
						}
					} else {
						emailID = "";
					}

					if (row1.get("AREA_NAME") != null) {
						areaName = row1.get("AREA_NAME") + "";
					} else {
						areaName = "";
					}
					row++;// 20
					column = preColumn;// 4

					label = new Label(column, row, userFirstName, textFormat);
					sheet.addCell(label);

					column++;// 5
					label = new Label(column, row, userID, textFormat);
					sheet.addCell(label);
					column++;// 5
					label = new Label(column, row, creationDate, textFormat);
					sheet.addCell(label);

					column++;// 6
					label = new Label(column, row, emailID, cellFormatEmail);
					sheet.setColumnView(column, 35);
					sheet.addCell(label);

					column++;// 7
					label = new Label(column, row, areaName, cellFormatEmail);
					sheet.addCell(label);

				}

				/*
				 * other Users
				 * 
				 */

				row = previousRow;// 18
				nextColumn++;// 8

				column = nextColumn + 1;// 9

				preColumn = column;// 9

				nextColumn = column + 4;// 12

				// Merge col[9-12] and row[18]
				sheet.mergeCells(column, row, nextColumn, row);

				label = new Label(column, row, "Active Other Users (" + countADU + ")", cellFormat);
				sheet.addCell(label);

				row++;// 19

				label = new Label(column, row, "User Name", headerFormat);
				sheet.setColumnView(column, 18);
				sheet.addCell(label);

				column++;// 10
				label = new Label(column, row, "User ID", headerFormat);
				sheet.setColumnView(column, 18);
				sheet.addCell(label);
				column++;// 10
				label = new Label(column, row, "Creation Date", headerFormat);
				sheet.setColumnView(column, 18);
				sheet.addCell(label);

				column++;// 11
				label = new Label(column, row, "Email ID", headerFormat);
				sheet.setColumnView(column, 35);
				sheet.addCell(label);

				column++;// 12
				label = new Label(column, row, "User Type", headerFormat);
				sheet.setColumnView(column, 18);
				sheet.addCell(label);

				// query="SELECT
				// USER_FIRST_NAME,USER_LAST_NAME,EMAIL_ID,USER_TYPE FROM
				// USERS_THREAD_DATA WHERE USER_LEVEL IN (6,4,9,10) AND
				// CODE_VALUE='"+codeValue+"'";
				query = "SELECT CONCAT(USER_FIRST_NAME,' ',USER_LAST_NAME) AS USER_NAME,USER_ID,CREATION_DATE,EMAIL_ID,USER_TYPE FROM USERS_THREAD_DATA WHERE USER_LEVEL IN (6,4,9,10) AND EMAIL_ID IS NOT NULL AND EMAIL_ID!='' AND EMAIL_ID NOT LIKE '%franconnect.com%' AND EMAIL_ID NOT LIKE '%franconnect.net%' AND BUILD_NAME='"
						+ clientName
						+ "' GROUP BY EMAIL_ID UNION ALL  SELECT CONCAT(USER_FIRST_NAME,' ',USER_LAST_NAME) AS USER_NAME,USER_ID,CREATION_DATE,EMAIL_ID,USER_TYPE FROM USERS_THREAD_DATA WHERE USER_LEVEL IN (6,4,9,10) AND (EMAIL_ID IS NULL OR EMAIL_ID='') AND EMAIL_ID NOT LIKE '%franconnect.com%' AND EMAIL_ID NOT LIKE '%franconnect.net%' AND BUILD_NAME='"
						+ clientName + "' ORDER BY USER_NAME";
				exchange.getIn().setBody(query.toString());
				// System.out.println("=====Count=query=Total==Active
				// other========>"+query.toString());
				Queryout = template.send(endpoint, exchange);
				Queryresult = Queryout.getOut().getBody(List.class);
				i = Queryresult.size();

				userFirstName = "";
				userID = "";
				emailID = "";
				areaName = "";
				usertype = "";
				creationDate = "";

				for (int j = 0; j < i; j++) {
					Map<String, Object> row1 = Queryresult.get(j);
					if (row1.get("USER_NAME") != null) {
						userFirstName = reverseFilterValue(row1.get("USER_NAME") + "");
					} else {
						userFirstName = "";
					}
					if (row1.get("USER_ID") != null) {
						userID = reverseFilterValue(row1.get("USER_ID") + "");
					} else {
						userID = "";
					}
					if (row1.get("CREATION_DATE") != null) {
						creationDate = reverseFilterValue(row1.get("CREATION_DATE") + "");
						creationDate = format2.format((format1.parse(creationDate)));
					} else {
						creationDate = "";
					}

					if (row1.get("EMAIL_ID") != null) {
						emailID = row1.get("EMAIL_ID") + "";
						if(emailID.contains("@")) {
							emailID = "****"+emailID.substring(emailID.indexOf("@"), emailID.length());
						}else {
							emailID = "****";
						}
					} else {
						emailID = "";
					}

					if (row1.get("USER_TYPE") != null) {
						usertype = row1.get("USER_TYPE") + "";
					} else {
						usertype = "";
					}
					row++;// 20
					column = preColumn;// 9

					label = new Label(column, row, userFirstName, textFormat);
					sheet.addCell(label);

					column++;// 10
					label = new Label(column, row, userID, textFormat);
					sheet.addCell(label);
					column++;// 10
					label = new Label(column, row, creationDate, textFormat);
					sheet.addCell(label);

					column++;// 11
					label = new Label(column, row, emailID, textFormat);
					sheet.setColumnView(column, 35);
					sheet.addCell(label);

					column++;// 12
					label = new Label(column, row, usertype, textFormat);
					sheet.addCell(label);

				}

				/*
				 * Fran Users
				 * 
				 */

				row = previousRow;// 18
				nextColumn++;// 13

				column = nextColumn + 1;// 14

				preColumn = column;// 14

				nextColumn = column + 4;// 17

				// Merge col[9-12] and row[18]
				sheet.mergeCells(column, row, nextColumn, row);

				label = new Label(column, row, "Active Franchise Users (" + countAFU + ")", cellFormat);
				sheet.addCell(label);

				row++;// 19

				label = new Label(column, row, "User Name", headerFormat);
				sheet.addCell(label);
				sheet.setColumnView(column, 18);

				column++;// 15
				label = new Label(column, row, "User ID", headerFormat);
				sheet.setColumnView(column, 18);
				sheet.addCell(label);

				column++;// 15
				label = new Label(column, row, "Creation Date", headerFormat);
				sheet.setColumnView(column, 18);
				sheet.addCell(label);
				column++;// 16
				label = new Label(column, row, "Email ID", headerFormat);
				sheet.setColumnView(column, 35);
				sheet.addCell(label);

				column++;// 17
				label = new Label(column, row, "Franchise ID", headerFormat);
				sheet.setColumnView(column, 18);
				sheet.addCell(label);

				// query="SELECT
				// USER_FIRST_NAME,USER_LAST_NAME,EMAIL_ID,FRANCHISEE_NAME FROM
				// USERS_THREAD_DATA WHERE USER_LEVEL IN (1) AND
				// CODE_VALUE='"+codeValue+"'";
				query = "SELECT CONCAT(USER_FIRST_NAME,' ',USER_LAST_NAME) AS USER_NAME,USER_ID,CREATION_DATE,EMAIL_ID,FRANCHISEE_NAME FROM USERS_THREAD_DATA WHERE USER_LEVEL IN (1) AND EMAIL_ID IS NOT NULL AND EMAIL_ID!=''  AND EMAIL_ID NOT LIKE '%franconnect.com%' AND EMAIL_ID NOT LIKE '%franconnect.net%' AND BUILD_NAME='"
						+ clientName
						+ "' GROUP BY EMAIL_ID,FRANCHISEE_NAME UNION ALL  SELECT CONCAT(USER_FIRST_NAME,' ',USER_LAST_NAME) AS USER_NAME,USER_ID,CREATION_DATE,EMAIL_ID,FRANCHISEE_NAME FROM USERS_THREAD_DATA WHERE USER_LEVEL IN (1) AND (EMAIL_ID IS NULL OR EMAIL_ID='')  AND EMAIL_ID NOT LIKE '%franconnect.com%' AND EMAIL_ID NOT LIKE '%franconnect.net%' AND BUILD_NAME='"
						+ clientName + "' ORDER BY USER_NAME";
				exchange.getIn().setBody(query.toString());
				// System.out.println("=====Count=query=Total==Franchisee
				// ========>"+query.toString());
				Queryout = template.send(endpoint, exchange);
				Queryresult = Queryout.getOut().getBody(List.class);
				i = Queryresult.size();

				userFirstName = "";
				userID = "";
				emailID = "";
				areaName = "";
				usertype = "";
				franchiseID = "";
				creationDate = "";

				for (int j = 0; j < i; j++) {
					Map<String, Object> row1 = Queryresult.get(j);
					if (row1.get("USER_NAME") != null) {
						userFirstName = reverseFilterValue(row1.get("USER_NAME") + "");
					} else {
						userFirstName = "";
					}
					if (row1.get("CREATION_DATE") != null) {
						creationDate = reverseFilterValue(row1.get("CREATION_DATE") + "");
						creationDate = format2.format((format1.parse(creationDate)));
					} else {
						creationDate = "";
					}
					if (row1.get("USER_ID") != null) {
						userID = reverseFilterValue(row1.get("USER_ID") + "");
					} else {
						userID = "";
					}

					if (row1.get("EMAIL_ID") != null) {
						emailID = row1.get("EMAIL_ID") + "";
						if(emailID.contains("@")) {
							emailID = "****"+emailID.substring(emailID.indexOf("@"), emailID.length());
						}else {
							emailID = "****";
						}
					} else {
						emailID = "";
					}

					if (row1.get("FRANCHISEE_NAME") != null) {
						franchiseID = row1.get("FRANCHISEE_NAME") + "";
					} else {
						franchiseID = "";
					}
					row++;// 20
					column = preColumn;// 14

					label = new Label(column, row, userFirstName, textFormat);
					sheet.addCell(label);

					column++;// 15
					label = new Label(column, row, userID, textFormat);
					sheet.addCell(label);
					column++;// 15
					label = new Label(column, row, creationDate, textFormat);
					sheet.addCell(label);

					column++;// 16
					label = new Label(column, row, emailID, textFormat);
					sheet.setColumnView(column, 25);
					sheet.addCell(label);

					column++;// 17
					label = new Label(column, row, franchiseID, textFormat);
					sheet.addCell(label);

				}

				/*
				 * Total Active Unique Users
				 * 
				 */

				row = previousRow;// 18
				nextColumn++;// 18

				column = nextColumn + 1;// 19

				preColumn = column;// 19

				nextColumn = column + 4;// 22

				// Merge col[9-12] and row[18]
				sheet.mergeCells(column, row, nextColumn, row);

				label = new Label(column, row, "Total Active Unique Users (" + countATU + ")", cellFormat);
				sheet.addCell(label);

				row++;// 19

				label = new Label(column, row, "User Name", headerFormat);
				sheet.setColumnView(column, 18);
				sheet.addCell(label);

				column++;// 20
				label = new Label(column, row, "User ID", headerFormat);
				sheet.setColumnView(column, 18);
				sheet.addCell(label);
				column++;// 20
				label = new Label(column, row, "Creation Date", headerFormat);
				sheet.setColumnView(column, 18);
				sheet.addCell(label);

				column++;// 21
				label = new Label(column, row, "Email ID", headerFormat);
				sheet.setColumnView(column, 35);
				sheet.addCell(label);

				column++;// 22
				label = new Label(column, row, "User Type", headerFormat);
				sheet.setColumnView(column, 18);
				sheet.addCell(label);

				// query="SELECT
				// USER_FIRST_NAME,USER_LAST_NAME,EMAIL_ID,USER_TYPE FROM
				// USERS_THREAD_DATA WHERE USER_LEVEL IN (0,2,6,4,9,10) AND
				// CODE_VALUE='"+codeValue+"'";
				query = "SELECT CONCAT(USER_FIRST_NAME,' ',USER_LAST_NAME) AS USER_NAME,USER_ID,CREATION_DATE,EMAIL_ID,USER_TYPE FROM USERS_THREAD_DATA WHERE USER_LEVEL IN (0,2,6,4,9,10) AND EMAIL_ID IS NOT NULL AND EMAIL_ID!=''  AND EMAIL_ID NOT LIKE '%franconnect.com%' AND EMAIL_ID NOT LIKE '%franconnect.net%' AND BUILD_NAME='"
						+ clientName
						+ "' GROUP BY EMAIL_ID UNION ALL  SELECT CONCAT(USER_FIRST_NAME,' ',USER_LAST_NAME) AS USER_NAME,USER_ID,CREATION_DATE,EMAIL_ID,USER_TYPE FROM USERS_THREAD_DATA WHERE USER_LEVEL IN (0,2,6,4,9,10) AND (EMAIL_ID IS NULL OR EMAIL_ID='') AND EMAIL_ID NOT LIKE '%franconnect.com%' AND EMAIL_ID NOT LIKE '%franconnect.net%' AND BUILD_NAME='"
						+ clientName + "' ORDER BY USER_NAME";
				exchange.getIn().setBody(query.toString());
				// System.out.println("=====Count=query=Total==USers
				// ========>"+query.toString());
				Queryout = template.send(endpoint, exchange);
				Queryresult = Queryout.getOut().getBody(List.class);
				i = Queryresult.size();

				userFirstName = "";
				userID = "";
				emailID = "";
				areaName = "";
				usertype = "";
				franchiseID = "";
				creationDate = "";

				for (int j = 0; j < i; j++) {
					Map<String, Object> row1 = Queryresult.get(j);
					if (row1.get("USER_NAME") != null) {
						userFirstName = reverseFilterValue(row1.get("USER_NAME") + "");
					} else {
						userFirstName = "";
					}
					if (row1.get("USER_ID") != null) {
						userID = reverseFilterValue(row1.get("USER_ID") + "");
					} else {
						userID = "";
					}
					if (row1.get("CREATION_DATE") != null) {
						creationDate = reverseFilterValue(row1.get("CREATION_DATE") + "");
						creationDate = format2.format((format1.parse(creationDate)));
					} else {
						creationDate = "";
					}
					if (row1.get("EMAIL_ID") != null) {
						emailID = row1.get("EMAIL_ID") + "";
						if(emailID.contains("@")) {
							emailID = "****"+emailID.substring(emailID.indexOf("@"), emailID.length());
						}else {
							emailID = "****";
						}
					} else {
						emailID = "";
					}

					if (row1.get("USER_TYPE") != null) {
						usertype = row1.get("USER_TYPE") + "";
					} else {
						usertype = "";
					}
					row++;// 20
					column = preColumn;// 19

					label = new Label(column, row, userFirstName, textFormat);
					sheet.addCell(label);

					column++;// 20
					label = new Label(column, row, userID, textFormat);
					sheet.addCell(label);
					column++;// 20
					label = new Label(column, row, creationDate, textFormat);
					sheet.addCell(label);

					column++;// 21
					label = new Label(column, row, emailID, textFormat);
					sheet.addCell(label);

					column++;// 22
					label = new Label(column, row, usertype, textFormat);
					sheet.addCell(label);

				}

				/*
				 * Active location
				 * 
				 */

				row = previousRow;// 18
				nextColumn++;// 23

				column = nextColumn + 1;// 24

				preColumn = column;// 24

				nextColumn = column + 2;// 25

				// Merge col[9-12] and row[18]
				sheet.mergeCells(column, row, nextColumn, row);

				label = new Label(column, row, "Active Locations (" + activeLocation + ")", cellFormat);
				sheet.addCell(label);

				row++;// 19

				label = new Label(column, row, "Franchise ID", headerFormat);
				sheet.setColumnView(column, 18);
				sheet.addCell(label);
				column++;
				label = new Label(column, row, "Openig Date", headerFormat);
				sheet.setColumnView(column, 18);
				sheet.addCell(label);

				column++;// 25
				label = new Label(column, row, "No. of Users Associated", headerFormat);
				sheet.setColumnView(column, 18);
				sheet.addCell(label);

				query = "SELECT FRANCHISEE_NO,FRANCHISEE_NAME,OPENING_DATE AS OPENING_DATE,NO_USERS FROM ACTIVE_LOCATION_THREAD_DATA WHERE  BUILD_NAME='"
						+ clientName + "' ORDER BY FRANCHISEE_NAME";
				exchange.getIn().setBody(query.toString());
				// System.out.println("======query=========>"+query.toString());
				Queryout = template.send(endpoint, exchange);
				Queryresult = Queryout.getOut().getBody(List.class);
				i = Queryresult.size();

				franchiseID = "";
				numberOfUsers = "0";
				openingDate = "";
				for (int j = 0; j < i; j++) {
					Map<String, Object> row1 = Queryresult.get(j);
					if (row1.get("FRANCHISEE_NAME") != null) {
						franchiseID = row1.get("FRANCHISEE_NAME") + "";
					} else {
						franchiseID = "";
					}
					if (row1.get("NO_USERS") != null) {
						numberOfUsers = row1.get("NO_USERS") + "";
					} else {
						numberOfUsers = "0";
					}
					openingDate = row1.get("OPENING_DATE") + "";
					if (row1.get("OPENING_DATE") != null && openingDate.length() > 2) {
						openingDate = reverseFilterValue(row1.get("OPENING_DATE") + "");
						openingDate = format2.format((format1.parse(openingDate)));
					} else {
						openingDate = "";
					}

					row++;// 20
					column = preColumn;// 24

					label = new Label(column, row, franchiseID, textFormat);
					sheet.addCell(label);
					column++;
					label = new Label(column, row, openingDate, textFormat);
					sheet.addCell(label);

					column++;// 25
					// label = new Label(column,row,numberOfUsers,textFormat);
					// sheet.addCell(label);
					if (isValidNew(numberOfUsers)) {
						numberCell = new jxl.write.Number(column, row, Integer.parseInt(numberOfUsers), numberFormat);
						sheet.addCell(numberCell);
					} else {
						numberCell = new jxl.write.Number(column, row, 0, numberFormat);
						sheet.addCell(numberCell);
					}
				}

				/*
				 * 
				 * In Development Location
				 * 
				 */
				
				row = previousRow;// 18
				nextColumn++;// 23

				column = nextColumn + 1;// 24

				preColumn = column;// 24

				nextColumn = column + 2;// 25

				// Merge col[9-12] and row[18]
				sheet.mergeCells(column, row, nextColumn, row);

				label = new Label(column, row, "InDevelopment  Locations (" + activeOpnerLoc + ")", cellFormat);
				sheet.addCell(label);

				row++;// 19

				label = new Label(column, row, "Franchise ID", headerFormat);
				sheet.setColumnView(column, 18);
				sheet.addCell(label);
				column++;
				label = new Label(column, row, "Openig Date", headerFormat);
				sheet.setColumnView(column, 18);
				sheet.addCell(label);

				column++;// 25
				label = new Label(column, row, "No. of Users Associated", headerFormat);
				sheet.setColumnView(column, 18);
				sheet.addCell(label);

				query = "SELECT FRANCHISEE_NO,FRANCHISEE_NAME,OPENING_DATE AS OPENING_DATE,NO_USERS FROM IN_DEVELOPMENT_LOCATION_THREAD_DATA WHERE  BUILD_NAME='"
						+ clientName + "' ORDER BY FRANCHISEE_NAME";
				exchange.getIn().setBody(query.toString());
				// System.out.println("======query=========>"+query.toString());
				Queryout = template.send(endpoint, exchange);
				Queryresult = Queryout.getOut().getBody(List.class);
				i = Queryresult.size();

				franchiseID = "";
				numberOfUsers = "0";
				openingDate = "";
				for (int j = 0; j < i; j++) {
					Map<String, Object> row1 = Queryresult.get(j);
					if (row1.get("FRANCHISEE_NAME") != null) {
						franchiseID = row1.get("FRANCHISEE_NAME") + "";
					} else {
						franchiseID = "";
					}
					if (row1.get("NO_USERS") != null) {
						numberOfUsers = row1.get("NO_USERS") + "";
					} else {
						numberOfUsers = "0";
					}
					openingDate = row1.get("OPENING_DATE") + "";
					if (row1.get("OPENING_DATE") != null && openingDate.length() > 2) {
						openingDate = reverseFilterValue(row1.get("OPENING_DATE") + "");
						openingDate = format2.format((format1.parse(openingDate)));
					} else {
						openingDate = "";
					}

					row++;// 20
					column = preColumn;// 24

					label = new Label(column, row, franchiseID, textFormat);
					sheet.addCell(label);
					column++;
					label = new Label(column, row, openingDate, textFormat);
					sheet.addCell(label);

					column++;// 25
					// label = new Label(column,row,numberOfUsers,textFormat);
					// sheet.addCell(label);
					if (isValidNew(numberOfUsers)) {
						numberCell = new jxl.write.Number(column, row, Integer.parseInt(numberOfUsers), numberFormat);
						sheet.addCell(numberCell);
					} else {
						numberCell = new jxl.write.Number(column, row, 0, numberFormat);
						sheet.addCell(numberCell);
					}
				}
				
				/*
				 * Unique CAL's (Locations having atleast one User)
				 * 
				 */

				row = previousRow;// 18
				nextColumn++;// 23

				column = nextColumn + 1;// 24

				preColumn = column;// 24

				nextColumn = column + 1;// 25

				// Merge col[9-12] and row[18]
				sheet.mergeCells(column, row, nextColumn, row);

				label = new Label(column, row, "Unique CAL's (Locations having atleast one User) (" + atleastOne + ")",
						cellFormat);
				sheet.addCell(label);

				row++;// 19

				label = new Label(column, row, "Franchise ID", headerFormat);
				sheet.setColumnView(column, 18);
				sheet.addCell(label);

				column++;// 25
				label = new Label(column, row, "No. of Users Associated", headerFormat);
				sheet.setColumnView(column, 18);
				sheet.addCell(label);

				query = "SELECT FRANCHISEE_NO,FRANCHISEE_NAME,NO_USERS FROM UNIQUE_LOCATION_THREAD_DATA WHERE  BUILD_NAME='"
						+ clientName + "' ORDER BY FRANCHISEE_NAME";
				exchange.getIn().setBody(query.toString());
				// System.out.println("======query=========>"+query.toString());
				Queryout = template.send(endpoint, exchange);
				Queryresult = Queryout.getOut().getBody(List.class);
				i = Queryresult.size();
				franchiseID = "";
				numberOfUsers = "0";
				for (int j = 0; j < i; j++) {
					Map<String, Object> row1 = Queryresult.get(j);
					if (row1.get("FRANCHISEE_NAME") != null) {
						franchiseID = row1.get("FRANCHISEE_NAME") + "";
					} else {
						franchiseID = "";
					}
					if (row1.get("NO_USERS") != null) {
						numberOfUsers = row1.get("NO_USERS") + "";
					} else {
						numberOfUsers = "0";
					}

					row++;// 20
					column = preColumn;// 24

					label = new Label(column, row, franchiseID, textFormat);
					sheet.addCell(label);

					column++;// 25
					// label = new Label(column,row,numberOfUsers,textFormat);
					// sheet.addCell(label);
					if (isValidNew(numberOfUsers)) {
						numberCell = new jxl.write.Number(column, row, Integer.parseInt(numberOfUsers), numberFormat);
						sheet.addCell(numberCell);
					} else {
						numberCell = new jxl.write.Number(column, row, 0, numberFormat);
						sheet.addCell(numberCell);
					}
				}

				/*
				 * Locations without any User
				 * 
				 */

				row = previousRow;// 18
				nextColumn++;// 23

				column = nextColumn + 1;// 24

				preColumn = column;// 24

				nextColumn = column;// 25

				// Merge col[9-12] and row[18]
				sheet.mergeCells(column, row, nextColumn, row);

				label = new Label(column, row, "Locations without any User (" + withoutUserLoc + ")", cellFormat);
				sheet.addCell(label);

				row++;// 19

				label = new Label(column, row, "Franchise ID", headerFormat);
				sheet.setColumnView(column, 20);
				sheet.addCell(label);

				/*
				 * column++;//25 label = new
				 * Label(column,row,"No. of Users Associated",headerFormat);
				 * sheet.addCell(label);
				 */

				query = "SELECT FRANCHISEE_NO,FRANCHISEE_NAME,NO_USERS FROM NOUSER_LOCATION_THREAD_DATA WHERE  BUILD_NAME='"
						+ clientName + "' ORDER BY FRANCHISEE_NAME";
				exchange.getIn().setBody(query.toString());
				// System.out.println("======query=========>"+query.toString());
				Queryout = template.send(endpoint, exchange);
				Queryresult = Queryout.getOut().getBody(List.class);
				i = Queryresult.size();
				franchiseID = "";
				numberOfUsers = "0";
				for (int j = 0; j < i; j++) {
					Map<String, Object> row1 = Queryresult.get(j);
					if (row1.get("FRANCHISEE_NAME") != null) {
						franchiseID = row1.get("FRANCHISEE_NAME") + "";
					} else {
						franchiseID = "";
					}
					if (row1.get("NO_USERS") != null) {
						numberOfUsers = row1.get("NO_USERS") + "";
					} else {
						numberOfUsers = "0";
					}

					row++;// 20
					column = preColumn;// 24

					label = new Label(column, row, franchiseID, textFormat);
					sheet.addCell(label);

					/*
					 * column++;//25 label = new
					 * Label(column,row,numberOfUsers,textFormat);
					 * sheet.addCell(label);
					 */
				}

				/*
				 * Locations having more than 5 Users
				 * 
				 */

				row = previousRow;// 18
				nextColumn++;// 23

				column = nextColumn + 1;// 24

				preColumn = column;// 24

				nextColumn = column + 1;// 25

				// Merge col[9-12] and row[18]
				sheet.mergeCells(column, row, nextColumn, row);

				label = new Label(column, row, "Locations having more than 5 Users(" + moreThanFive + ")", cellFormat);
				sheet.addCell(label);

				row++;// 19

				label = new Label(column, row, "Franchise ID", headerFormat);
				sheet.addCell(label);
				sheet.setColumnView(column, 18);
				column++;// 25
				label = new Label(column, row, "No. of Users Associated", headerFormat);
				sheet.addCell(label);
				sheet.setColumnView(column, 18);

				query = "SELECT FRANCHISEE_NO,FRANCHISEE_NAME,NO_USERS FROM MORE_THEN_FIVE_LOCATION_THREAD_DATA WHERE  BUILD_NAME='"
						+ clientName + "'  ORDER BY FRANCHISEE_NAME";
				exchange.getIn().setBody(query.toString());
				// System.out.println("======query=========>"+query.toString());
				Queryout = template.send(endpoint, exchange);
				Queryresult = Queryout.getOut().getBody(List.class);
				i = Queryresult.size();
				franchiseID = "";
				numberOfUsers = "0";
				for (int j = 0; j < i; j++) {
					Map<String, Object> row1 = Queryresult.get(j);
					if (row1.get("FRANCHISEE_NAME") != null) {
						franchiseID = row1.get("FRANCHISEE_NAME") + "";
					} else {
						franchiseID = "";
					}
					if (row1.get("NO_USERS") != null) {
						numberOfUsers = row1.get("NO_USERS") + "";
					} else {
						numberOfUsers = "0";
					}

					row++;// 20
					column = preColumn;// 24

					label = new Label(column, row, franchiseID, textFormat);
					sheet.addCell(label);

					column++;// 25
					// label = new Label(column,row,numberOfUsers,textFormat);
					// sheet.addCell(label);
					if (isValidNew(numberOfUsers)) {
						numberCell = new jxl.write.Number(column, row, Integer.parseInt(numberOfUsers), numberFormat);
						sheet.addCell(numberCell);
					} else {
						numberCell = new jxl.write.Number(column, row, 0, numberFormat);
						sheet.addCell(numberCell);
					}
				}

				/*
				 * Extra CAL's
				 * 
				 */

				row = previousRow;// 18
				nextColumn++;// 23

				column = nextColumn + 1;// 24

				preColumn = column;// 24

				nextColumn = column + 1;// 25

				// Merge col[9-12] and row[18]
				sheet.mergeCells(column, row, nextColumn, row);

				label = new Label(column, row, "Extra CAL's (" + extCal + ")", cellFormat);
				sheet.addCell(label);

				row++;// 19

				label = new Label(column, row, "Franchise ID", headerFormat);
				sheet.addCell(label);
				sheet.setColumnView(column, 18);

				column++;// 25
				label = new Label(column, row, "No. of Users Associated", headerFormat);
				sheet.addCell(label);
				sheet.setColumnView(column, 18);

				query = "SELECT FRANCHISEE_NO,FRANCHISEE_NAME,NO_USERS FROM EXTRA_CAL_LOCATION_THREAD_DATA WHERE  BUILD_NAME='"
						+ clientName + "' ORDER BY FRANCHISEE_NAME";
				exchange.getIn().setBody(query.toString());
				// System.out.println("======query=========>"+query.toString());
				Queryout = template.send(endpoint, exchange);
				Queryresult = Queryout.getOut().getBody(List.class);
				i = Queryresult.size();
				franchiseID = "";
				numberOfUsers = "0";
				for (int j = 0; j < i; j++) {
					Map<String, Object> row1 = Queryresult.get(j);
					if (row1.get("FRANCHISEE_NAME") != null) {
						franchiseID = row1.get("FRANCHISEE_NAME") + "";
					} else {
						franchiseID = "";
					}
					if (row1.get("NO_USERS") != null) {
						numberOfUsers = row1.get("NO_USERS") + "";
					} else {
						numberOfUsers = "0";
					}

					row++;// 20
					column = preColumn;// 24

					label = new Label(column, row, franchiseID, textFormat);
					sheet.addCell(label);

					column++;// 25
					// label = new Label(column,row,numberOfUsers,textFormat);
					// sheet.addCell(label);
					if (isValidNew(numberOfUsers)) {
						numberCell = new jxl.write.Number(column, row, Integer.parseInt(numberOfUsers), numberFormat);
						sheet.addCell(numberCell);
					} else {
						numberCell = new jxl.write.Number(column, row, 0, numberFormat);
						sheet.addCell(numberCell);
					}
				}

				/*
				 * Total CAL's (Unique CAL's + Extra CAL's)
				 * 
				 */

				row = previousRow;// 18
				nextColumn++;// 23

				column = nextColumn + 1;// 24

				preColumn = column;// 24

				nextColumn = column + 1;// 25

				// Merge col[9-12] and row[18]
				sheet.mergeCells(column, row, nextColumn, row);

				label = new Label(column, row, "Total CAL's (Unique CAL's + Extra CAL's) (" + cal + ")", cellFormat);
				sheet.addCell(label);

				row++;// 19

				label = new Label(column, row, "Franchise ID", headerFormat);
				sheet.addCell(label);
				sheet.setColumnView(column, 18);

				column++;// 25
				label = new Label(column, row, "No. of Users Associated", headerFormat);
				sheet.addCell(label);
				sheet.setColumnView(column, 18);

				query = "SELECT FRANCHISEE_NO,FRANCHISEE_NAME,NO_USERS FROM TOTAL_CAL_LOCATION_THREAD_DATA WHERE  BUILD_NAME='"
						+ clientName + "' ORDER BY FRANCHISEE_NAME";
				exchange.getIn().setBody(query.toString());
				// System.out.println("======query=========>"+query.toString());
				Queryout = template.send(endpoint, exchange);
				Queryresult = Queryout.getOut().getBody(List.class);
				i = Queryresult.size();
				franchiseID = "";
				numberOfUsers = "0";
				for (int j = 0; j < i; j++) {
					Map<String, Object> row1 = Queryresult.get(j);
					if (row1.get("FRANCHISEE_NAME") != null) {
						franchiseID = row1.get("FRANCHISEE_NAME") + "";
					} else {
						franchiseID = "";
					}
					if (row1.get("NO_USERS") != null) {
						numberOfUsers = row1.get("NO_USERS") + "";
					} else {
						numberOfUsers = "0";
					}

					row++;// 20
					column = preColumn;// 24

					label = new Label(column, row, franchiseID, textFormat);
					sheet.addCell(label);

					column++;// 25
					// label = new Label(column,row,numberOfUsers,textFormat);
					// sheet.addCell(label);
					if (isValidNew(numberOfUsers)) {
						numberCell = new jxl.write.Number(column, row, Integer.parseInt(numberOfUsers), numberFormat);
						sheet.addCell(numberCell);
					} else {
						numberCell = new jxl.write.Number(column, row, 0, numberFormat);
						sheet.addCell(numberCell);
					}
				}

			} catch (Exception e) {
				// Debug.println("Exception in excel generation:" + e);
				System.out.println(
						"=Exception Found for Client====================>" + codeValue + "\n" + stackTraceToString(e));
				e.printStackTrace();
			}
		}

		try {
			workbook.write();
			workbook.close();

			String dbFileName = "";
			if (fileName != null) {
				String[] temp = fileName.split("UserBillingExport/");

				if (temp != null && temp.length > 1) {
					dbFileName = temp[1];
				} else {
					dbFileName = "";
				}

			}

			if (!"".equals(dbFileName)) {
				String flag = "0";
				String hostName = "";
				String userName = "";
				String password = "";
				String ftpFoldername = "./SkyBoxUserBillingExport/";

				query = "SELECT FTP_SERVER_ID, HOST_NAME, USER_NAME, PASSWORD, FLAG, HOST_URL, IS_DEFAULT,DIRECTORY FROM FILE_UPLOAD_FTP_SERVER";
				exchange.getIn().setBody(query.toString());

				Queryout = template.send(endpoint, exchange);
				Queryresult = Queryout.getOut().getBody(List.class);
				i = Queryresult.size();

				for (int j = 0; j < i; j++) {
					Map<String, Object> row1 = Queryresult.get(j);
					if (row1.get("HOST_NAME") != null) {
						hostName = row1.get("HOST_NAME") + "";
					} else {
						hostName = "";
					}
					if (row1.get("USER_NAME") != null) {
						userName = row1.get("USER_NAME") + "";
					} else {
						userName = "";
					}

					if (row1.get("PASSWORD") != null) {
						password = row1.get("PASSWORD") + "";
					} else {
						password = "";
					}

					if (row1.get("FLAG") != null) {
						flag = row1.get("FLAG") + "";
					} else {
						flag = "0";
					}
					if (row1.get("DIRECTORY") != null) {
						ftpFoldername = row1.get("DIRECTORY") + "";
					} else {
						ftpFoldername = "./SkyBoxUserBillingExport/";
					}
				}
				boolean isFileUpload=false;
				// isFileUpload = uploadFilesToFtpServer(fileName, dbFileName, flag, hostName, userName, password,ftpFoldername);
						
				// boolean isFileUpload=true;
				if (isFileUpload) {

					DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
					Date date = new Date();
					String datee = dateFormat.format(date);

					StringBuilder insertQueryActiveLocation = new StringBuilder(
							"INSERT INTO LOGI_USAGE_REPORTS_THREAD (CAL_REPORT,DATE_FROM,DATE_TO,DATE_ON,BILLING_REPORT) VALUES ");

					insertQueryActiveLocation.append("(\"" + dbFileName + "\" , ");
					insertQueryActiveLocation.append("\"" + Constants.dateFrom + "\" , ");
					insertQueryActiveLocation.append("\"" + Constants.dateTo + "\" , ");
					insertQueryActiveLocation.append("\"" + datee + "\" , ");
					insertQueryActiveLocation.append("\"" + dbFileName + "\" )");

					// System.out.println(" query for insert
					// :"+insertQueryActiveLocation.toString());
					exchange.getIn().setBody(insertQueryActiveLocation.toString());
					Exception excpetion = template.send(endpoint, exchange).getException();
					if (excpetion != null) {
						System.out.println(" query for insert isFailed :" + insertQueryActiveLocation.toString() + ""
								+ excpetion.getMessage());
						excpetion.printStackTrace();
					}

				}

			}

		} catch (Exception e) {
			System.out.println(
					"=Exception Found for Client====================>" + codeValue + "\n" + stackTraceToString(e));
			e.printStackTrace();
		}

	}

	public static String reverseFilterValue(String pValue) {

		if (pValue == null || pValue.length() == 0)
			return (pValue);

		pValue = replaceAll(pValue, "&lt;", "<");
		pValue = replaceAll(pValue, "&gt;", ">");
		pValue = replaceAll(pValue, "&amp;", "&");
		pValue = replaceAll(pValue, "&quot;", "\"");
		pValue = replaceAll(pValue, "&#35;", "#");
		pValue = replaceAll(pValue, "<br>", "\n");
		pValue = replaceAll(pValue, "&#63;", "?");
		pValue = replaceAll(pValue, "&#39;", "'");
		pValue = replaceAll(pValue, "&#92;", "\\");
		pValue = replaceAll(pValue, "&#37;", "%");
		pValue = replaceAll(pValue, "&#43;", "+");
		pValue = replaceAll(pValue, "&#61;", "=");
		pValue = replaceAll(pValue, "&apos;", "'");
		pValue = replaceAll(pValue, "&nbsp;", " ");
		pValue = replaceAll(pValue, "&#91;", "[");
		pValue = replaceAll(pValue, "&#93;", "] ");
		pValue = replaceAll(pValue, "&#47;", "/");
		pValue = replaceAll(pValue, "&#146;", "'");
		pValue = replaceAll(pValue, "&#148;", "\"");
		pValue = replaceAll(pValue, "&#8217;", "’");// P_CM_B_43424
		pValue = replaceAll(pValue, "&#8211;", "–");
		return pValue;
	}

	public static String replaceAll(String _string, String _old, String _new) {
		StringBuilder buffer = new StringBuilder(_string);
		int pos = _string.indexOf(_old);
		if (!isValidNew(_new) && !"&nbsp;".equals(_old)) {
			_new = "";
		}
		int newLen = _new.length();
		while (pos != -1) {
			// System.out.println(pos);
			buffer.replace(pos, pos + _old.length(), _new);
			pos = buffer.indexOf(_old, pos + newLen);
			// pos = buffer.toString().indexOf(_old);
		}
		// System.out.println(buffer);
		return buffer.toString();
	}

	public static boolean isValid(String s) {
		return (s != null && s.trim().length() != 0 && !s.trim().equalsIgnoreCase("null"));
	}

	public static boolean isValidNew(String s) {
		return (s != null && s.trim().length() != 0 && !s.trim().equalsIgnoreCase("null") && !s.trim().equals("-1"));
	}

	public static boolean uploadFilesToFtpServer(String basePath, String UserFileName, String flag, String hostName,
			String userName, String password, String ftpFoldername) {
		boolean isCopied = false;

		FileTransferClient ftp = null;
		if (!"1".equals(flag)) {
			return false;
		}

		String UserInformationFile = basePath;

		System.out.println("=======UserInformationFile=========" + UserInformationFile);
		System.out.println("=======UserFileName=========" + UserFileName);
		try {
			ftp = new FileTransferClient();

			// set FTP server settings
			ftp.setRemoteHost(hostName);
			ftp.setUserName(userName);
			ftp.setPassword(password);

			// connect to the server
			ftp.connect();

			// Check valid files put on FTP server for the
			try {
				ftp.uploadFile(UserInformationFile, ftpFoldername + UserFileName);
				isCopied = true;
			} catch (Exception e) {
				System.out.println("=uploadFilesToFtpServer====================>" + stackTraceToString(e));
				e.printStackTrace();
			}

		} catch (Exception e) {
			System.out.println("=uploadFilesToFtpServer====================>" + stackTraceToString(e));
			e.printStackTrace();
			isCopied = false;

			try {
				ftp.disconnect();
			} catch (Exception e2) {
				System.out.println("=uploadFilesToFtpServer====================>" + stackTraceToString(e2));
				e2.printStackTrace();
			}
		}

		try {
			// boolean isDeletedDir = new File(tempDirName).delete();
		} catch (Exception e) {
			System.out.println("=uploadFilesToFtpServer====================>" + stackTraceToString(e));
			e.printStackTrace();
		}

		return isCopied;
	}

	public static String stackTraceToString(Throwable e) {
		StringBuilder sb = new StringBuilder();
		for (StackTraceElement element : e.getStackTrace()) {
			sb.append(element.toString());
			sb.append("\n");
		}
		return sb.toString();
	}

}// end of class
