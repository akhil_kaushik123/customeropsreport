package com.app.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.camel.Endpoint;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.beanutils.DynaClass;

import com.app.base.Constants;
import com.app.base.ModuleInfo;
import com.app.dataProcessor.DataProcessor;
import com.app.helper.DBConnectionProvider;

public class DataBeanProducer implements Processor 
{
	private String clientName;
	private String codeValue;
	private String modules;
	private HashMap<String,ModuleInfo> moduleQueriesMap;


	
	public DataBeanProducer(String clientName,String codeValue,String modules,HashMap<String,ModuleInfo> moduleQueriesMap) 
	{
		System.out.println("moduleQueriesMap:::::::::::"+moduleQueriesMap);
		this.clientName = clientName;
		this.codeValue =codeValue;
		this.modules = modules;
		this.moduleQueriesMap = moduleQueriesMap;
	}
	
	@Override
	public void process(Exchange exchange) throws Exception 
	{	
		Set<String> moduleIds = new HashSet<String>();
		ArrayList<String> columns = new ArrayList<String>();
		columns.add("CLIENT_NAME");
		columns.add("CODE_VALUE");
		columns.add("DATE_FROM");
		columns.add("DATE_TO");
		String tableName = "";
		if("MU".equals(Constants.exportType))
		{
			if(modules.contains(","))
			{
				String[] mods = modules.split(",");
				for(String m:mods)
				{	
					if(moduleQueriesMap.containsKey(m))
					{
						moduleIds.add(m);
						columns.addAll(moduleQueriesMap.get(m).getColumns());
						tableName = moduleQueriesMap.get(m).getTableName();
					}
				}
			}
			else
			{		
				moduleIds.add(modules);
				columns.addAll(moduleQueriesMap.get(modules).getColumns());
				tableName = moduleQueriesMap.get(modules).getTableName();
			}
		}
		else
		{
			moduleIds = moduleQueriesMap.keySet();
			Iterator<String> mods = moduleIds.iterator();
			while(mods.hasNext())
			{
				String modId = mods.next();
				columns.addAll(moduleQueriesMap.get(modId).getColumns());
				tableName = moduleQueriesMap.get(modId).getTableName();
				System.out.println("Doing for table name:::::"+tableName);
			}
		}
		
		ArrayList<DynaBean> data = new ArrayList<DynaBean> ();
		DynaClass dClass = BeanUtil.createBeanGenericProperty(columns, Object.class);
		DynaBean clientData = null;
		
		if(!Constants.isMultiRowData)
		{
			clientData = dClass.newInstance();
			clientData.set("CLIENT_NAME", clientName);
			clientData.set("CODE_VALUE", codeValue);
			clientData.set("DATE_FROM",Constants.dateFrom);
			clientData.set("DATE_TO",Constants.dateTo);
		}
		
		Iterator<String> modulesIt = moduleIds.iterator();
		while(modulesIt.hasNext())
		{
			String moduleId = modulesIt.next();
			Map<String,Map<String,String>> queriesMap = moduleQueriesMap.get(moduleId).getQueriesMap();
			System.out.println("queriesMap::::"+queriesMap);
			Iterator<String> attributesIt = queriesMap.keySet().iterator();
			
			while(attributesIt.hasNext())
			{
				String attributeId = attributesIt.next();
				System.out.println("attributeId::::"+attributeId);
				
				Map<String,String> qMap = queriesMap.get(attributeId);
				String query = qMap.get("QUERY");
				if (query.contains("@@dateFrom")) 
				{
					query = query.replace("@@dateFrom",Constants.dateFrom);
				}
				if(query.contains("@@dateTo"))
				{
					query = query.replace("@@dateTo", Constants.dateTo);
				}
				
				Endpoint dbConnection = DBConnectionProvider.getInstance().getDBConnection(clientName);
				try
				{	if("CD".equals(qMap.get("TYPE")))
				{
					HashMap<String,Object> results = null;
					results = QueryUtil.executeQuery(dbConnection, query);
					boolean isFailed = (boolean)results.get("IS_FAILED");
					if(isFailed)
					{
						if(qMap.containsKey("SECONDARY_QUERY") && qMap.get("SECONDARY_QUERY") != null && !qMap.get("SECONDARY_QUERY").equals(""))
				    	{
							
				    		String secondaryQuery = qMap.get("SECONDARY_QUERY");	
				    		
				    		
				    		if (secondaryQuery.contains("@@dateFrom")) 
							{
								secondaryQuery = secondaryQuery.replace("@@dateFrom",Constants.dateFrom);
							}
							if(secondaryQuery.contains("@@dateTo"))
							{
								secondaryQuery = secondaryQuery.replace("@@dateTo", Constants.dateTo);
							}
							
							System.out.println(" executing secondary query :  "+ secondaryQuery);
							//results = QueryUtil.executeQuery(dbConnection, secondaryQuery);
							
				    	}
				    }
					
					System.out.println("Is Failed value for low level data "+results.get("IS_FAILED")+", for column name "+qMap.get("COLUMN_NAMES"));
					if(results!= null && !(boolean)results.get("IS_FAILED"))
					{
						Set<String> cols = (Set<String>)results.get("COLUMN_NAMES");
						ArrayList<DynaBean> dataBeans = (ArrayList<DynaBean>) results.get("DATA");
				    	if(dataBeans != null && dataBeans.size() > 0 && cols !=null)
				    	{	
				    		Iterator<DynaBean> it = dataBeans.iterator();
				    		while(it.hasNext())
				    		{
					    		DynaBean bean = it.next();
					    		if(Constants.isMultiRowData)
					    		{
					    			clientData = dClass.newInstance();
					    			clientData.set("CLIENT_NAME", clientName);
					    			clientData.set("CODE_VALUE", codeValue);
					    			clientData.set("DATE_FROM",Constants.dateFrom);
					    			clientData.set("DATE_TO",Constants.dateTo);
					    		}
					    		
					    		Iterator<String> colsIt = cols.iterator();
					    		while(colsIt.hasNext())
					    		{
					    			String col = colsIt.next();
					    			Object val = bean.get(col);
					    			try
					    			{
						    			if(qMap.get("ROUTINE_CLASS_NAME")!=null && !qMap.get("ROUTINE_CLASS_NAME").equals(""))
						    			{
						    			Class<DataProcessor> processingClass=(Class<DataProcessor>) Class.forName(qMap.get("ROUTINE_CLASS_NAME"));
						    			DataProcessor processor=(DataProcessor)processingClass.getConstructor().newInstance();
						    			val=processor.postProcessing(val,dbConnection);
						    			}
					    			}
					    			catch(Exception e)
					    			{
					    				e.printStackTrace();
					    			}
					    			if(val==null)
					    				{
					    				val=bean.get(col);
					    				}
					    			clientData.set(col, val);
					    		}
					    		
					    		if(Constants.isMultiRowData)
					    		{
					    			data.add(clientData);
					    		}
					    		
				    		}
				    	}
				    	
					}
					
				}
				else
				{
					HashMap<String,Object> results = null;
					System.out.println("Is Failed value for high level data "+", for column name "+qMap.get("COLUMN_NAMES"));
					results = QueryUtil.executeQuery(dbConnection, query);
					boolean isFailed = (boolean)results.get("IS_FAILED");
			          if (isFailed)
			              if (qMap.containsKey("SECONDARY_QUERY") && qMap.get("SECONDARY_QUERY") != null && !qMap.get("SECONDARY_QUERY").equals("")) {
			                String secondaryQuery = qMap.get("SECONDARY_QUERY");
			                //secondaryQuery = prepareQuery(secondaryQuery, this.dbName);
			                System.out.println(" executing secondary query :  " + secondaryQuery);
			                results = QueryUtil.executeQuery(dbConnection, secondaryQuery);
			              }  
					
					{
						System.out.println("Is Failed for high level data "+results.get("IS_FAILED")+", for column name "+qMap.get("COLUMN_NAMES"));
						
						if(results!= null && !(boolean)results.get("IS_FAILED"))
						{
							Set<String> cols = (Set<String>)results.get("COLUMN_NAMES");
							ArrayList<DynaBean> dataBeans = (ArrayList<DynaBean>) results.get("DATA");
					    	if(dataBeans != null && dataBeans.size() > 0 && cols !=null)
					    	{	

						    		//DynaBean bean = it.next();

						    		
						    		//Iterator<String> colsIt = cols.iterator();

						    			//String col = colsIt.next();
						    			ArrayList<HashMap<String,Object>> beanHM = beanToHashMapConverter(dataBeans,cols);
						    			try
						    			{
							    			if(qMap.get("ROUTINE_CLASS_NAME")!=null && !qMap.get("ROUTINE_CLASS_NAME").equals(""))
							    			{
							    			Class<DataProcessor> processingClass=(Class<DataProcessor>) Class.forName(qMap.get("ROUTINE_CLASS_NAME"));
							    			DataProcessor processor=(DataProcessor)processingClass.getConstructor().newInstance();
							    			beanHM=processor.postProcessing(beanHM,dbConnection);
							    			}
						    			}
						    			catch(Exception e)
						    			{
						    				e.printStackTrace();
						    			}
						    			//ObjectOutputStream oos=new ObjectOutputStream();
//						    			bos = new ByteArrayOutputStream();
//						    	        oos = new ObjectOutputStream(bos);
//						    	        oos.writeObject(beanHM);
//						    	        byteArray = bos.toByteArray();
//						    			System.out.println("column name ::::::::"+qMap);

						    			clientData.set(qMap.get("COLUMN_NAMES"), beanHM);

						    		
						    		
						    		if(Constants.isMultiRowData)
						    		{
						    			data.add(clientData);
						    		}
						    		
					    		
					    	}
					    	
						}
						
					}
					
				}
				}catch(Exception ex)
				{
					ex.printStackTrace();
					System.out.println(" ex occured for "+query + " client :"+clientName);
				}
			}
			
		}
		System.out.println(" does it arrived here after flow lost? ");
		Message msg = exchange.getIn();
		if(Constants.isMultiRowData)
		{
			msg.setBody(data);
		}
		else
		{
			msg.setBody(clientData);
		}
		msg.setHeader("COLUMNS",columns);
		msg.setHeader("TABLE_NAME",tableName);
		exchange.setOut(msg);
	}

	private ArrayList<HashMap<String, Object>> beanToHashMapConverter(ArrayList dynaBeans, Set<String> cols)  {
		ArrayList<HashMap<String, Object>> list = new ArrayList();
		HashMap<String, Object> hm;
		Iterator<DynaBean> it= dynaBeans.iterator();
		while (it.hasNext())
		{
			hm=new HashMap<String, Object>();
			DynaBean bean= it.next();
			for(String col:cols)
			{
				hm.put(col, bean.get(col));
				
			}
			
			list.add( hm);
			
		}
		// TODO Auto-generated method stub
		return list;
	}
}