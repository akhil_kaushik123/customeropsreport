package com.app.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

import org.apache.commons.beanutils.BasicDynaClass;
import org.apache.commons.beanutils.DynaClass;
import org.apache.commons.beanutils.DynaProperty;
 

public class BeanUtil {
    
    public static DynaClass createBean(Set<String> colNames) throws IllegalAccessException, InstantiationException
    {
    	Iterator<String> colItr = colNames.iterator();
    	int size = colNames.size();
    	
    	DynaProperty[] properties = new DynaProperty[size];
    	int i = 0;
    	while(colItr.hasNext())
    	{ 
    		String colName = colItr.next();
    		properties[i] = new DynaProperty(colName,String.class);
    		i++;
    	}
    	return new BasicDynaClass("DemoBean",null,properties);
    }
    
    public static DynaClass createBean(ArrayList<String> colNames) throws IllegalAccessException, InstantiationException
    {
    	Iterator<String> colItr = colNames.iterator();
    	int size = colNames.size();
    	
    	DynaProperty[] properties = new DynaProperty[size];
    	int i = 0;
    	while(colItr.hasNext())
    	{ 
    		String colName = colItr.next();
    		properties[i] = new DynaProperty(colName,String.class);
    		i++;
    	}
    	return new BasicDynaClass("DemoBean",null,properties);
    }
    
    public static DynaClass createBeanGenericProperty(ArrayList<String> colNames, Class<?> cl) throws IllegalAccessException, InstantiationException
    {
    	Iterator<String> colItr = colNames.iterator();
    	int size = colNames.size();
    	
    	DynaProperty[] properties = new DynaProperty[size];
    	int i = 0;
    	while(colItr.hasNext())
    	{ 
    		String colName = colItr.next();
    		properties[i] = new DynaProperty(colName,cl);
    		i++;
    	}
    	return new BasicDynaClass("DemoBean",null,properties);
    }
    
    
 }
 