package com.app.util;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.camel.CamelContext;
import org.apache.camel.Endpoint;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.camel.ProducerTemplate;

import com.app.base.BillingReportBean;
import com.app.base.ClientInfo;


public class SystemUsageDataProducer implements Processor
{
	private String clientName ;
	//private String codeValue ;
	//private String billableLevel;
	private ClientInfo dbProps;
	
	public SystemUsageDataProducer(String clientName,ClientInfo dbProps) 
	{
		this.clientName = clientName;
		this.dbProps= dbProps;
	}
	
	@Override
	public void process(Exchange exchange) throws Exception 
	{	
		String codeValue = dbProps.getCodeValue();
		String billableLevel = dbProps.getBillableLevel();
		
		ArrayList<String> userCount=	null;
		ArrayList<String> testUserCount = null;
		ArrayList<String> systemUsageTime = null;
		HashMap<String, String> data = null;
		ArrayList<String> loggedUserCount = null;
		ArrayList<String> loggedUserHrs = null;
		try
		{	
			CamelContext context = exchange.getContext();
			Endpoint endpoint = context.getEndpoint("direct:"+clientName);
			ProducerTemplate template = context.createProducerTemplate();
			userCount=SystemUsageUtility.getActiveBW(template, endpoint, exchange,billableLevel);
			testUserCount = SystemUsageUtility.getTestActiveBW(template, endpoint, exchange,billableLevel);
			systemUsageTime = SystemUsageUtility.getTotalSystemTime(template, endpoint, exchange);
			data = SystemUsageUtility.getData(template, endpoint, exchange);
			loggedUserCount = SystemUsageUtility.getLoggedInUsers(template, endpoint, exchange);
			loggedUserHrs = SystemUsageUtility.getLoggedUserHrs(template, endpoint, exchange);
			String totalLogins = SystemUsageUtility.getTotalLogins(template, endpoint, exchange);
			
			BillingReportBean report = new BillingReportBean();
			
			report.setDbName(SystemUsageUtility.getDbName(template, endpoint, exchange));
			
			report.setDateFrom(SystemUsageUtility.dateFrom);
			report.setDateTo(SystemUsageUtility.dateTo);
			
			if(userCount!=null && userCount.size()>2)
			{
				report.setClientName(clientName);
				report.setCodeValue(codeValue);
				report.setActiveCU(userCount.get(0));
				report.setActiveFU(userCount.get(1));
				report.setActiveRU(userCount.get(2));
				report.setActiveDU(userCount.get(3));
			}
			if(testUserCount!=null && testUserCount.size()>2)
			{
				report.setClientName(clientName);
				report.setCodeValue(codeValue);
				report.setTestCU(testUserCount.get(0));
				report.setTestFU(testUserCount.get(1));
				report.setTestRU(testUserCount.get(2));
				report.setTestDU(testUserCount.get(3));
			}
			if(systemUsageTime !=null)
			{
				report.setTotalSystemTime(systemUsageTime.get(0));
				report.setAdminSystemTime(systemUsageTime.get(1));
				report.setNetSystemTime(systemUsageTime.get(2));
			}
			if(data!=null)
			{
				report.setActiveCals(data.get("ACTIVE_CALS"));
				report.setUniqueCals(data.get("UNIQUE_CALS"));
				report.setExtraCals(data.get("EXTRA_CALS"));
				report.setTotalCals(data.get("TOTAL_CALS"));
				report.setFddCount(data.get("FDD_COUNT"));
			}
		
			if(loggedUserCount!=null)
			{
				report.setLoggedInCU(loggedUserCount.get(0));
				report.setLoggedInFU(loggedUserCount.get(1));
				report.setLoggedInRU(loggedUserCount.get(2));
				report.setLoggedInDU(loggedUserCount.get(3));
			}
			
			if(loggedUserHrs!=null)
			{
				report.setLoggedInHrsCU(loggedUserHrs.get(0));
				report.setLoggedInHrsFU(loggedUserHrs.get(1));
				report.setLoggedInHrsRU(loggedUserHrs.get(2));
				report.setLoggedInHrsDU(loggedUserHrs.get(3));
			}
			
			Message msg = exchange.getIn();
			msg.setBody(report);
			exchange.setOut(msg);
			System.out.println("report : "+report);
		}
		catch(Exception ex)
		{
			System.out.println(" exception in Report Bean Generator fot endpoint : "+clientName);
			ex.printStackTrace();
		}
		finally
		{
			userCount = null;
			testUserCount = null;
			systemUsageTime = null;
			data = null;
			loggedUserCount = null;
			loggedUserHrs = null;
		}
		
		
	}
}