/*
 *  FCSKYS-18302 | Users Fetching based on their Roles 
 */
package com.app.base;

public class ClientInfo {
	
	private String clientName;
	private String dbName;
	private String dbServerIP;
	private String dbUser;
	private String dbPassword;
	private String billableLevel;
	private String codeValue;
	private String hostURL;
	private String tomcatServer;
	
	
	public String getTomcatServer() {
		return tomcatServer;
	}
	public void setTomcatServer(String tomcatServer) {
		this.tomcatServer = tomcatServer;
	}
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	public String getDbName() {
		return dbName;
	}
	public void setDbName(String dbName) {
		this.dbName = dbName;
	}
	public String getDbServerIP() {
		return dbServerIP;
	}
	public void setDbServerIP(String dbServerIP) {
		this.dbServerIP = dbServerIP;
	}
	public String getDbUser() {
		return dbUser;
	}
	public void setDbUser(String dbUser) {
		this.dbUser = dbUser;
	}
	public String getDbPassword() {
		return dbPassword;
	}
	public void setDbPassword(String dbPassword) {
		this.dbPassword = dbPassword;
	}
	public String getBillableLevel() {
		return billableLevel;
	}
	public void setBillableLevel(String billableLevel) {
		this.billableLevel = billableLevel;
	}
	public String getCodeValue() {
		return codeValue;
	}
	public void setCodeValue(String codeValue) {
		this.codeValue = codeValue;
	}		
	public String getHostURL() {
		return hostURL;
	}
	public void setHostURL(String hostURL) {
		this.hostURL = hostURL;
	}		
}
