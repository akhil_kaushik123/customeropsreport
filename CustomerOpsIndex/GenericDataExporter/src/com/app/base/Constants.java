/*
 * Apphome for AWS and Alaska Server
 * FCSKYS-15522 :Report Name change According to Legacy and SaaS
 *  FCSKYS-15522  | Implemented Billing Changes
 FCSKYS-18245 | Automate MUID Reports
 FCSKYS-18302 | Users Fetching based on their Roles 
 FCSKYS-18301 | Merging of users
 */
package com.app.base;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.app.helper.DBConnectionProvider;
import com.app.helper.ExecutorHelper;
import com.app.util.DataFactory;
import com.app.util.Utility;

public class Constants 
{
	
	public static String filePath = null;
	public static String fileName = null;
	public static StringBuilder clNotConnected = null;
	public static StringBuilder clCVNotMatched = null;
	public static String dateFrom = null;
	public static String dateTo = null;
	public static String tables = null;
	public static String rowType = null;
	public static String clients = null;
	public static String moduleIds = null;
	//public static String dataLimit = null;
	
	
	public static boolean isMultiRowData = false;
	public static String exportType = null;
	public static String tableNames[] = null;
	public static String patterns[] = null;
	public static StringBuilder mailSubject = null; 
	public static StringBuilder mailText = null;
	public static String mailAttachment = null;
	public static String mailerPropsFile = null;
	public static String dbPropsFile = null;
	public static boolean isUpdateThread = false;
	public static String attributeIds = null;
	public static String appHome = null;
	public static String exportTaskName = null;
	public static String version = null;// Report Name change According to Legacy and SaaS
	public static String executeFor = null;
	public static String specialReport=null;
	//User Billing New Changes start
	public static String mailAttachmentExport = null;
	
	
	//User Billing New Changes END
	
	public static void initialize(String args[])
	{
		mailSubject = new StringBuilder();
		mailText = new StringBuilder();
		clNotConnected = new StringBuilder();
		clCVNotMatched = new StringBuilder();
		dateFrom = args[0]; //Date From
		dateTo = args[1];	//Date To
		int typeId = Integer.parseInt(args[2]); // Exporter ID
		tables = args[3]; //Comma sepearated tables list (only if Type is CD else NA )
		rowType = args[4]; // M for multiple rows per client, S for Single row per client
		clients = args[5]; // Comma seperated list of clients or ALL for all clients
		moduleIds = args[6];// Comma seperated list of modules(mainly for CHA_DATA) or NA
		attributeIds = args[7];// Comma seperated list of attributes(mainly for CHA_DATA) or NA
		isUpdateThread = ("u".equals(args[8]) || "U".equals(args[8]) ) ? true:false; // if thread is update thread u else NA(mainly when fetching updating data for some module or attributes)
		
		appHome=args[9]; //Apphome for AWS and Alaska Server
		System.out.println(" is update thread : "+isUpdateThread);
		System.out.println(" appHome: "+appHome);
		//appHome = "/home/bhawna/builds/Automation/";
		
		filePath = appHome+"logs/";
		mailerPropsFile = appHome + "jar/config/mailer.properties";
		dbPropsFile = appHome+"jar/config/db.properties";
		
		if("M".equals(rowType) || "m".equals(rowType))
		{
			isMultiRowData =true;
		}
		
		//User Billing New Changes start
		if(typeId==7 || typeId==8)
		{
			
		}
		else
		{
		
		if(!Utility.isValidDate(dateFrom))
		{
			System.out.println("From Date not valid");
			System.exit(0);
		}
		if(!Utility.isValidDate(dateTo))
		{
			System.out.println("To Date not valid");
			System.exit(0);
		}
		}
		//User Billing New Changes end
		switch(typeId)
		{
			case 1 : 
				exportType = "SU";
				exportTaskName = "System Usage";
				break;
			case 2 : 
				exportType = "MU";
				exportTaskName = "Module Usage";
				break;
			case 3 : 
				exportType = "AL";
				exportTaskName= "Average Logins Data";
				break;
			case 4 : 
				exportType = "'CD','HD'";
				exportTaskName = "CUSTOM DATA";
				break;
			case 5 : 
				exportType = "MSU";
				exportTaskName = "Merge System Usage";
				break;
			case 6 : 
				exportType = "MMU";
				exportTaskName = " Merge Module Usage";
				break;
				//User Billing New Changes start
				
			case 7 : 
				exportType = "SUBILLING";
				exportTaskName = " System Usage Billing";
				break;	
				//User Billing New Changes end
			case 8 : 
				exportType = "SUEXPORT";
				exportTaskName = " System Usage Export";
				break;	
			case 9 : 
				exportType = "EUGenerate";
				exportTaskName = "EU Uses Data Generate";
				break;	
			case 10 : 
				exportType = "EUExport";
				exportTaskName = "EU Uses Data Export";
				break;		
			case 11:	
				exportType = "SpecialReport";
				specialReport="Y";
				exportTaskName = "Special Report";
				break;	
			case 12:	
				exportType = "SpecialReportExport";
				specialReport="Y";
				exportTaskName = "Special Report Export";
				break;	
				//User Billing New Changes end
            case 13:
                Constants.exportType = "MUEXPORT";
                Constants.exportTaskName = "Customer Usage Export";
                break;
            case 14:
                Constants.exportType = "WUEXPORT";
                Constants.exportTaskName = "Customer Usage Export";
                break;
            case 15:
                exportType = "EAVDATA";
                exportTaskName = "Postgres Data Generate";
                break;
            case 44:
                exportType = "LUMINEXPORT";
                exportTaskName = "Lumin Leads data Export";
                break;
            case 22:
                exportType = "GENERICDATA";
                exportTaskName = "Lumin Leads data Export";
                break;
			default : 
				System.out.println(" Wrong input ");
		}
		
		System.out.println(" Execution Started : "+System.currentTimeMillis()+" working .................");
		if("CD".equals(exportType) || "'CD','HD'" .equals(exportType) || "EAVDATA" .equals(exportType))
		{
				System.out.println("Table names to populate data for :(names should be comma-seperated if more than one table) " +tables);
				if(tables!=null && !"".equals(tables))
				{
					if(tables.contains(","))
					{
						tableNames = tables.split(",");
					}
					else
					{	
						tableNames = new String[] {tables};
					}
				}
		}
		
		if("GENERICDATA" .equals(exportType))
		{
			System.out.println("*********"+args.length);
			
			
				System.out.println("Patterns for Generic Table search :(names should be comma-seperated if more than one pattern) " +moduleIds);
				if(moduleIds!=null && !"".equals(moduleIds))
				{
					if(moduleIds.contains(","))
					{
						patterns = moduleIds.split(",");
					}
					else
					{	
						patterns = new String[] {moduleIds};
					}
				}
		}
		
		else if("AL".equals(exportType))
		{
			tableNames = new String[] {"AVG_LOGINS_DATA"};
		}
		else if("MU".equals(exportType))
		{
			tableNames = new String[]{"CHA_DATA"};
		}

		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		String datee = dateFormat.format(date);
		//User Billing New Changes start
		if("SUBILLING".equals(exportType) || "SUEXPORT".equals(exportType) || "SpecialReportExport".equals(Constants.exportType) || "SpecialReport".equals(Constants.exportType) || "MUEXPORT".equals(Constants.exportType) || "WUEXPORT".equals(Constants.exportType) ||"LUMINEXPORT".equals(Constants.exportType) )
		{
			fileName = "DataExporter"+exportType+"_"+datee+".txt";	
		}
		else
		{
			fileName = "DataExporter"+exportType+"_"+datee+"_"+dateFrom+"_"+dateTo+".txt";	
		}
		//User Billing New Changes end
		//prepare mailSubject
		mailSubject.append("Data InHouse Exporter Thread Logs for Thread : "+exportTaskName);
		
		//prepare mailText
		mailText.append("Thread Executed : "+exportTaskName + "<br>");
		if(tableNames != null && tableNames.length>0)
		{
			mailText.append("Tables Populated : ");
			for(String tab : tableNames)
			{
				mailText.append(tab+" , ");
			}
		}
		mailText.append("<br>Data fetched for dates : "+dateFrom + " TO "+dateTo);
		dateFormat = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
		mailText.append(" Thread Start time : "+dateFormat.format(date));
		//prepare mailAttachment
		mailAttachment = filePath + fileName;
		
		mailAttachmentExport=appHome+"UserBillingExport/userMonthly_user_cal_"+dateFrom+"_"+dateTo+".xls";//User Billing New Changes start
		
	}
	
	
	
	
}
