/*
 * FCSKYS-17240 : To make the user data available in a table 
 * (FCSKYS-18304) Special Report in the BrightStar for Cars Report
 */
package com.app.base;

import java.util.HashMap;
import java.util.Map;

public class BillingReportBean {

	private String clientName;
	private String codeValue;
	
	private String dbName;
	
	private String dateFrom;
	private String dateTo;
	
	private String activeCU;
	private String activeRU;
	private String activeFU;
	private String activeDU;
	private String testCU;
	private String testRU;
	private String testFU;
	private String testDU;
	
	private String activeCals;
	private String uniqueCals;
	private String extraCals;
	private String totalCals;
	private String fddCount;
	
	private String totalSystemTime;
	private String adminSystemTime;
	private String netSystemTime;
	
	private String loggedInCU;    
	private String loggedInRU;    
	private String loggedInFU;
	private String loggedInDU;
	
	private String loggedInHrsCU;
	private String loggedInHrsRU;
	private String loggedInHrsFU;
	private String loggedInHrsDU;
	private String hostURL;
	
	private HashMap<String,HashMap<String,HashMap<String,String>>> EUMap;
    public HashMap<String, HashMap<String, HashMap<String,String>>> getEUMap() {
		return EUMap;
	}
	public void setEUMap(HashMap<String, HashMap<String, HashMap<String,String>>> eUMap) {
		EUMap = eUMap;
	}
    private HashMap<String,HashMap<String,HashMap<String,String>>> locationMap;
	
	private Map<String,Object> brightStarCarReportMap;
	public Map<String, Object> getBrightStarCarReportMap() {
		return brightStarCarReportMap;
	}
	public void setBrightStarCarReportMap(Map<String, Object> brightStarCarReportMap) {
		this.brightStarCarReportMap = brightStarCarReportMap;
	}
	public HashMap<String,HashMap<String,HashMap<String,String>>> getLocationMap() {
		return locationMap;
	}
	
	public void setLocationMap( HashMap<String,HashMap<String,HashMap<String,String>>> locationMap) {
		this.locationMap=locationMap;
	}
	
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	
	public String getCodeValue() {
		return codeValue;
	}
	
	public void setCodeValue(String codeValue) {
		this.codeValue = codeValue;
	}
	
	public String getDbName() {
		return dbName;
	}
	
	public void setDbName(String dbName) {
		this.dbName = dbName;
	}
	
	public String getDateFrom() {
		return dateFrom;
	}
	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}
	public String getDateTo() {
		return dateTo;
	}
	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}
	
	public String getActiveCU() {
		return activeCU;
	}
	public void setActiveCU(String activeCU) {
		this.activeCU = activeCU;
	}
	public String getActiveRU() {
		return activeRU;
	}
	public void setActiveRU(String activeRU) {
		this.activeRU = activeRU;
	}
	public String getActiveFU() {
		return activeFU;
	}
	public void setActiveFU(String activeFU) {
		this.activeFU = activeFU;
	}
	public String getActiveDU() {
		return activeDU;
	}
	public void setActiveDU(String activeDU) {
		this.activeDU = activeDU;
	}
	public String getTestCU() {
		return testCU;
	}
	public void setTestCU(String testCU) {
		this.testCU = testCU;
	}
	public String getTestRU() {
		return testRU;
	}
	public void setTestRU(String testRU) {
		this.testRU = testRU;
	}
	public String getTestFU() {
		return testFU;
	}
	public void setTestFU(String testFU) {
		this.testFU = testFU;
	}
	public String getTestDU() {
		return testDU;
	}
	public void setTestDU(String testDU) {
		this.testDU = testDU;
	}
	
	
	public String getActiveCals() {
		return activeCals;
	}
	public void setActiveCals(String activeCals) {
		this.activeCals = activeCals;
	}
	public String getUniqueCals() {
		return uniqueCals;
	}
	public void setUniqueCals(String uniqueCals) {
		this.uniqueCals = uniqueCals;
	}
	public String getExtraCals() {
		return extraCals;
	}
	public void setExtraCals(String extraCals) {
		this.extraCals = extraCals;
	}
	public String getTotalCals() {
		return totalCals;
	}
	public void setTotalCals(String totalCals) {
		this.totalCals = totalCals;
	}
	public String getFddCount() {
		return fddCount;
	}
	public void setFddCount(String fddCount) {
		this.fddCount = fddCount;
	}
	
	public String getTotalSystemTime() {
		return totalSystemTime;
	}
	public void setTotalSystemTime(String totalSystemTime) {
		this.totalSystemTime = totalSystemTime;
	}
	public String getAdminSystemTime() {
		return adminSystemTime;
	}
	public void setAdminSystemTime(String adminSystemTime) {
		this.adminSystemTime = adminSystemTime;
	}
	public String getNetSystemTime() {
		return netSystemTime;
	}
	public void setNetSystemTime(String netSystemTime) {
		this.netSystemTime = netSystemTime;
	}
	


	
	
	public String getLoggedInCU() {
		return loggedInCU;
	}
	public void setLoggedInCU(String loggedInCU) {
		this.loggedInCU = loggedInCU;
	}
	public String getLoggedInRU() {
		return loggedInRU;
	}
	public void setLoggedInRU(String loggedInRU) {
		this.loggedInRU = loggedInRU;
	}
	public String getLoggedInFU() {
		return loggedInFU;
	}
	public void setLoggedInFU(String loggedInFU) {
		this.loggedInFU = loggedInFU;
	}
	public String getLoggedInDU() {
		return loggedInDU;
	}
	public void setLoggedInDU(String loggedInDU) {
		this.loggedInDU = loggedInDU;
	}
	public String getLoggedInHrsCU() {
		return loggedInHrsCU;
	}
	public void setLoggedInHrsCU(String loggedInHrsCU) {
		this.loggedInHrsCU = loggedInHrsCU;
	}
	public String getLoggedInHrsRU() {
		return loggedInHrsRU;
	}
	public void setLoggedInHrsRU(String loggedInHrsRU) {
		this.loggedInHrsRU = loggedInHrsRU;
	}
	public String getLoggedInHrsFU() {
		return loggedInHrsFU;
	}
	public void setLoggedInHrsFU(String loggedInHrsFU) {
		this.loggedInHrsFU = loggedInHrsFU;
	}
	public String getLoggedInHrsDU() {
		return loggedInHrsDU;
	}
	public void setLoggedInHrsDU(String loggedInHrsDU) {
		this.loggedInHrsDU = loggedInHrsDU;
	}
	public String getHostURL() {
		return hostURL;
	}
	public void setHostURL(String hostURL) {
		this.hostURL = hostURL;
	}
	
	
}
