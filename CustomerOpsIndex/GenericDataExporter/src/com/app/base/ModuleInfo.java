package com.app.base;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ModuleInfo {

	private String moduleId;
	private ArrayList<String> attributeIds;
	private ArrayList<String> columnsList;
	private String tableName;
	
	private Map<String,Map<String,String>> queriesMap;
	
	public ModuleInfo()
	{
		init();
	}
	
	private void init()
	{
		attributeIds = new ArrayList<String>();
		columnsList = new ArrayList<String>();
		queriesMap = new HashMap<String,Map<String,String>>();
	}
	
	public Map<String,Map<String,String>> getQueriesMap()
	{
		return queriesMap;
	}
	
	public Map<String,String> getQueriesForAttribute(String attributeId)
	{
		if(queriesMap.containsKey(attributeId))
		{
			return queriesMap.get(attributeId);
		}
		else
		{
			return null;
		}
	}
	
	public String getModuleId()
	{
		return moduleId;
	}
	public ArrayList<String> getAttributes()
	{
		return attributeIds;
	}
	
	public ArrayList<String> getColumns()
	{
		return columnsList;	
	}
	
	public void setModuleId(String moduleId)
	{
		this.moduleId = moduleId;
	}
	
	public void addAttribute(String attributeId)
	{
		attributeIds.add(attributeId);
	}
	
	public void addColumns(String columns)
	{
		if(columns.contains(","))
		{
			String[] cols = columns.split(",");
		
			for(String c : cols)
			{
				columnsList.add(c);
			}
		}
		else
		{
			columnsList.add(columns);
		}
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
}
