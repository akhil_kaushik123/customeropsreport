package com.app.helper;

import org.apache.camel.builder.RouteBuilder;

public class RouteProvidor extends RouteBuilder
{	
	private String clientName;
	private static RouteProvidor _instance = null;
	
	private RouteProvidor(String clientName) 
	{
		this.clientName = clientName;
	}
	
	public static RouteBuilder getNewRoute(String clientName)
	{
		_instance = new RouteProvidor(clientName);
		return _instance;
	}
	
	@Override
	public void configure() throws Exception 
	{
		from("direct://"+clientName).to("jdbc://"+clientName);
	}

}






