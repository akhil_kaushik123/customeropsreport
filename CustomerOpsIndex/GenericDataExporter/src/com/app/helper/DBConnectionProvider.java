package com.app.helper;

import java.util.HashMap;

import org.apache.camel.Endpoint;
import org.apache.camel.RoutesBuilder;
import org.apache.camel.component.jdbc.JdbcEndpoint;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.impl.SimpleRegistry;

import com.app.base.ClientInfo;
import com.app.util.QueryUtil;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import org.postgresql.ds.PGSimpleDataSource;

public class DBConnectionProvider 
{
	private DefaultCamelContext _context = null; 
	private static DBConnectionProvider _instance = null;
	private SimpleRegistry _registry = null;
	
	private DBConnectionProvider()
	{
		init();
	}
	
	private void init()
	{	
		_registry = new SimpleRegistry();
		//_registry.
		_context = new DefaultCamelContext(_registry);
		//_context
	}
	
	public static DBConnectionProvider getInstance()
	{
		if(_instance == null)
		{
			_instance = new DBConnectionProvider();
		}
		return _instance;
	}
	
	public void initialize()
	{
		try 
		{
			_context.start();
			_context.getShutdownStrategy().setTimeout(1000);
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	public boolean isComplete()
	{
		
		return _context.isStopped();
	}

	public void cleanUpAllResources()
	{
		try 
		{	
			System.out.println("called ...........");
			_context.stop();
			while(!_context.isStopped())
			{
				System.out.println("sleeping ....");
				Thread.sleep(1000*10*2);
			}
			System.out.println(" context stopped");
			_instance = null;
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	public void setupDB(ClientInfo dbProps)
	{		
		
		try 
		{	
			registerSource(dbProps.getClientName(), configureDataSource(dbProps));
			_context.addRoutes(RouteProvidor.getNewRoute(dbProps.getClientName()));
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	public boolean isDBConnected(String clientName)
	{
		boolean dbConnected = false;
		String query = "SELECT 1";
		Endpoint dbConnection = DBConnectionProvider.getInstance().getDBConnection(clientName);
		HashMap<String,Object> result = QueryUtil.executeQuery(dbConnection, query);
		dbConnected = (boolean)result.get("IS_FAILED");
		return dbConnected;
	}
	
	public Endpoint getDBConnection(String clientName)
	{
		return _context.getEndpoint("jdbc://"+clientName);
	}
	
	

	private MysqlDataSource configureDataSource(ClientInfo dbProps)
	{
		MysqlDataSource dataSource = new MysqlDataSource();
		dataSource.setURL("jdbc:mysql://"+dbProps.getDbServerIP()+"/"+dbProps.getDbName());
		dataSource.setUser(dbProps.getDbUser());
		dataSource.setPassword(dbProps.getDbPassword());
		return dataSource;
	}
	
	  public void setupPGDB(ClientInfo dbProps) {
		    try {
		      registerSource(dbProps.getClientName(), configurePGDataSource(dbProps));
		      this._context.addRoutes((RoutesBuilder)RouteProvidor.getNewRoute(dbProps.getClientName()));
		    } catch (Exception e) {
		      e.printStackTrace();
		    } 
		  }
	  
	  private PGSimpleDataSource configurePGDataSource(ClientInfo dbProps) {
		    PGSimpleDataSource source = new PGSimpleDataSource();
		    source.setServerName(dbProps.getDbServerIP());
		    source.setPortNumber(5432);
		    source.setDatabaseName(dbProps.getDbName());
		    source.setUser(dbProps.getDbUser());
		    source.setPassword(dbProps.getDbPassword());
		    return source;
		  }
	
	public void registerSource(String name,Object ds)
	{
		_registry.put(name, ds);
	}
}
