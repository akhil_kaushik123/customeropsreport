package com.app.helper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.camel.Endpoint;
import org.apache.camel.Exchange;
import org.apache.commons.beanutils.DynaBean;

import com.app.base.ClientInfo;
import com.app.base.Constants;
import com.app.base.ModuleInfo;
import com.app.util.QueryUtil;
import com.app.util.Utility;

/*
 * import package for new changes Start
 */
import org.apache.camel.CamelContext;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.camel.ProducerTemplate;
import java.util.List;
/*
 * import package for new changes End
 * (FCSKYS-15522) : Bhavna Chugh : Inhouse Billing : FileName Change
 * (FCSKYS-15522) : Bhavna Chugh : Inhouse Billing : Report According to the Clients Name in ASC Order
 * (FCSKYS-15522) : Bhavna Chugh : Inhouse Billing : Check For SaaS and Legacy 
 *  FCSKYS-15522 :Report Name change According to Legacy and SaaS
 *  FCSKYS-15522 : Check For Sync Clients
 *   FCSKYS-15522  | Implemented Billing Changes
 FCSKYS-18245 | Automate MUID Reports
 FCSKYS-18302 | Users Fetching based on their Roles 
 FCSKYS-18301 | Merging of users
 */

public class ExecutorHelper {

	public static LinkedHashMap<String, ClientInfo> setupResources() {
		setupFSSDb();
		setupInHouseDB();
	    if ("EAVDATA".equals(Constants.exportType))
	        {setupPostGresDB(); 
	    setupPostGresDBApac();}
		HashMap<String, Object> clientsMap = getClientsData();
		LinkedHashMap<String, ClientInfo> clients = new LinkedHashMap<String, ClientInfo>();
		System.out.println("not failed clients::::"+clientsMap);
		if (clientsMap != null) {
			boolean isFailed = (boolean) clientsMap.get("IS_FAILED");
			if (!isFailed) {

				System.out.println("not failed clients");
				clients = setupClientResources(clientsMap);
			} else {
				System.exit(0);
			}
		} else {
			System.exit(0);
		}
		System.out.println("not failed clients::::"+clients);
		return clients;
	}

	public static void setupFSSDb() {
		ClientInfo dbProps = new ClientInfo();
		String file = Constants.dbPropsFile;
		Properties props = Utility.readProperties(file);
		dbProps.setClientName("fss");
		dbProps.setDbName(props.getProperty("fss.dbName"));// fss //fss08Nov2016
		dbProps.setDbServerIP(props.getProperty("fss.dbServer"));// 209.249.130.154
																	// //192.168.8.12
		dbProps.setDbUser(props.getProperty("fss.dbUser"));// fsslogin
		dbProps.setDbPassword(props.getProperty("fss.dbPassword")); // fss@l0g1n
		DBConnectionProvider.getInstance().setupDB(dbProps);
	}

	public static void setupInHouseDB() {
		ClientInfo dbProps = new ClientInfo();
		Properties props = Utility.readProperties(Constants.dbPropsFile);
		dbProps.setClientName("InHouse");
		dbProps.setDbName(props.getProperty("inhouse.dbName"));// InHouse90
																// //InHouse9029Nov2016
		dbProps.setDbServerIP(props.getProperty("inhouse.dbServer"));// 192.168.1.127/InHouse90
																		// //192.168.8.12
		dbProps.setDbUser(props.getProperty("inhouse.dbUser"));// clientlogin
		dbProps.setDbPassword(props.getProperty("inhouse.dbpassword"));// cl13ntl0g1n
		System.out.println("INHOUSE DB DEtail "+props.getProperty("inhouse.dbName")+props.getProperty("inhouse.dbServer")+props.getProperty("inhouse.dbUser")+props.getProperty("inhouse.dbpassword"));
		DBConnectionProvider.getInstance().setupDB(dbProps);
	}
	
	  public static void setupPostGresDB() {
		    ClientInfo dbProps = new ClientInfo();
		    Properties props = Utility.readProperties(Constants.dbPropsFile);
		    dbProps.setClientName("PostGres");
		    dbProps.setDbName(props.getProperty("postgres.dbName"));
		    dbProps.setDbServerIP(props.getProperty("postgres.dbServer"));
		    dbProps.setDbUser(props.getProperty("postgres.dbUser"));
		    dbProps.setDbPassword(props.getProperty("postgres.dbpassword"));
		    System.out.println("PostGres DB DEtail " + props.getProperty("postgres.dbName") + props.getProperty("postgres.dbServer") + props.getProperty("postgres.dbUser") + props.getProperty("postgres.dbpassword"));
		    DBConnectionProvider.getInstance().setupPGDB(dbProps);
		  }
	  public static void setupPostGresDBApac() {
		    ClientInfo dbProps = new ClientInfo();
		    Properties props = Utility.readProperties(Constants.dbPropsFile);
		    dbProps.setClientName("PostGresApac");
		    dbProps.setDbName(props.getProperty("postgresApac.dbName"));
		    dbProps.setDbServerIP(props.getProperty("postgresApac.dbServer"));
		    dbProps.setDbUser(props.getProperty("postgresApac.dbUser"));
		    dbProps.setDbPassword(props.getProperty("postgresApac.dbpassword"));
		    System.out.println("PostGres DB DEtail " + props.getProperty("postgresApac.dbName") + props.getProperty("postgresApac.dbServer") + props.getProperty("postgresApac.dbUser") + props.getProperty("postgresApac.dbpassword"));
		    DBConnectionProvider.getInstance().setupPGDB(dbProps);
		  }

	public static HashMap<String, Object> getClientsData() {
		StringBuffer clientsQuery = new StringBuffer();
		StringBuffer saaSClientQuery = new StringBuffer();
		StringBuffer checkVersion = new StringBuffer();
		StringBuffer checkExecuteFor = new StringBuffer();
		StringBuffer specialClientQuery = new StringBuffer();
		Map<String, Object> row = null;
		String version = "";
		String executeFor = "";
		checkVersion.append("SELECT VERSION,EXECUTE_FOR_ALL FROM USER_BILLING_DATE");
		List data = QueryUtil.executeQuerySelect(DBConnectionProvider.getInstance().getDBConnection("InHouse"),
				checkVersion.toString());
		if (data != null) {
			int i = data.size();
			row = (Map<String, Object>) data.get(0);
			version = (String) row.get("VERSION");
			executeFor = (String) row.get("EXECUTE_FOR_ALL");
		}else{
			System.out.println("InHouse Connection Issue");
		}
		Constants.version = version;// FCSKYS-15522 :Report Name change
		Constants.executeFor = executeFor;
		System.out.println("*******************" + version);// Check for SaaS
		System.out.println("*******************" + executeFor);
		// SELECT ENTITY_NAME,CODE_VALUE,DB_SERVER,DB_NAME , BILLABLE_LEVEL FROM
		// ENTITIES WHERE DB_NAME!='null' AND DB_NAME!='' AND DB_SERVER !='null'
		// AND DB_SERVER!='' AND BUILD_STATUS ='52' AND IS_ARCHIVED='No' AND
		// ENTITY_NAME NOT
		// IN('mbeww','mbeItaly','mbeaustralia','mbegermany','flcouncil','mbespain','mbefrance','mbeuk','mbePoland','mbechile','mbeLatam','mbecentralamerica')
		// AND DB_SERVER LIKE '%192.168.8.38%'
		// SELECT ENTITY_NAME,CODE_VALUE,DB_SERVER,DB_NAME , BILLABLE_LEVEL FROM
		// ENTITIES WHERE DB_NAME!='null' AND DB_NAME!='' AND DB_SERVER !='null'
		// AND DB_SERVER!='' AND BUILD_STATUS ='52' AND IS_ARCHIVED='No' AND
		// ENTITY_NAME NOT
		// IN('mbeww','mbeItaly','mbeaustralia','mbegermany','flcouncil','mbespain','mbefrance','mbeuk','mbePoland','mbechile','mbeLatam','mbecentralamerica')

		// orignal query

		// SELECT ENTITY_NAME,CODE_VALUE,DB_SERVER,DB_NAME , BILLABLE_LEVEL FROM
		// ENTITIES WHERE DB_NAME!='null' AND DB_NAME!='' AND DB_SERVER !='null'
		// AND DB_SERVER!='' AND BUILD_STATUS ='52' AND IS_ARCHIVED='No' AND
		// ENTITY_NAME NOT
		// IN('mbeww','mbeItaly','mbeaustralia','mbegermany','flcouncil','mbespain','mbefrance','mbeuk','mbePoland','mbechile','mbeLatam','mbecentralamerica')

		// clientsQuery.append("SELECT ENTITY_NAME,CODE_VALUE,DB_SERVER,DB_NAME
		// , BILLABLE_LEVEL FROM ENTITIES WHERE DB_NAME!='null' AND DB_NAME!=''
		// AND DB_SERVER !='null' AND DB_SERVER!='' AND BUILD_STATUS ='52' AND
		// IS_ARCHIVED='No' AND ENTITY_NAME NOT
		// IN('mbeww','mbeItaly','mbeaustralia','mbegermany','flcouncil','mbespain','mbefrance','mbeuk','mbePoland','mbechile','mbeLatam','mbecentralamerica')
		// ");
		StringBuffer clientsWhereClause = new StringBuffer("");
		if (!"ALL".equals(Constants.clients)) {
			StringBuilder str = null;
			
			if (Constants.clients.contains(",")) {
				String c[] = Constants.clients.split(",");
				for (String s : c) {
					if (str == null) {
						str = new StringBuilder("'" + s.trim() + "'");
					} else {
						str.append(",'" + s.trim() + "'");
					}
				}
			} else {
				str = new StringBuilder("'" + Constants.clients.trim() + "'");
			}
			
			clientsWhereClause.append(" AND ENTITY_NAME IN (" + str.toString() + ")");
		}
		if (version.equals("Legacy")) {
			if (executeFor.equals("N")) {
			clientsQuery.append(
						"SELECT ENTITY_NAME,CODE_VALUE,DB_SERVER,DB_NAME , BILLABLE_LEVEL,BUILDS_URL,TOMCAT_SERVER FROM ENTITIES WHERE DB_NAME!='null' AND DB_NAME!='' AND DB_SERVER !='null' AND DB_SERVER!=''  AND BUILD_STATUS ='52' AND IS_ARCHIVED='No' AND ENTITY_NAME NOT IN('mbeww','mbeItaly','mbeaustralia','mbegermany','flcouncil','mbespain','mbefrance','mbeuk','mbePoland','mbechile','mbeLatam','mbecentralamerica','FacebookConnect725_30','FacebookConnect725','mbeskyfinlandFC','mbeskyfinlandZC','mbeskyromaniaFC','mbeskyromaniaZC') AND CODE_VALUE NOT IN ('mbeGlobal') AND VERSION_NO!='14' AND ENTITY_NAME NOT IN (SELECT ENTITY_NAME FROM UNBILLED_CLIENT WHERE VERSION='Legacy') AND ENTITY_NAME IN (SELECT CLIENT_NAME FROM BILLING_CLIENTS) "+((!"ALL".equals(Constants.clients))?clientsWhereClause.toString():"")+" ORDER BY UPPER(CODE_VALUE),UPPER(ENTITY_NAME) ASC");
			} else {
				clientsQuery.append(
						"SELECT ENTITY_NAME,CODE_VALUE,DB_SERVER,DB_NAME , BILLABLE_LEVEL,BUILDS_URL, TOMCAT_SERVER FROM ENTITIES WHERE DB_NAME!='null' AND DB_NAME!='' AND DB_SERVER !='null' AND DB_SERVER!=''  AND BUILD_STATUS ='52' AND IS_ARCHIVED='No' AND ENTITY_NAME NOT IN('mbeww','mbeItaly','mbeaustralia','mbegermany','flcouncil','mbespain','mbefrance','mbeuk','mbePoland','mbechile','mbeLatam','mbecentralamerica','FacebookConnect725_30','FacebookConnect725','mbeskyfinlandFC','mbeskyfinlandZC','mbeskyromaniaFC','mbeskyromaniaZC')  AND CODE_VALUE NOT IN ('mbeGlobal') AND VERSION_NO!='14' AND ENTITY_NAME NOT IN (SELECT ENTITY_NAME FROM UNBILLED_CLIENT WHERE VERSION='Legacy') "+((!"ALL".equals(Constants.clients))?clientsWhereClause.toString():"")+"  ORDER BY UPPER(CODE_VALUE),UPPER(ENTITY_NAME) ASC");
			}
		} else {
			if (executeFor.equals("N")) {
			clientsQuery.append(
						"SELECT ENTITY_NAME,CODE_VALUE,DB_SERVER,DB_NAME , BILLABLE_LEVEL,BUILDS_URL,TOMCAT_SERVER FROM ENTITIES WHERE DB_NAME!='null' AND DB_NAME!='' AND DB_SERVER !='null' AND DB_SERVER!=''  AND BUILD_STATUS ='52' AND IS_ARCHIVED='No' AND ENTITY_NAME NOT IN('mbeww','mbeItaly','mbeaustralia','mbegermany','flcouncil','mbespain','mbefrance','mbeuk','mbePoland','mbechile','mbeLatam','mbecentralamerica','FacebookConnect725_30','FacebookConnect725','mbeskyfinlandFC','mbeskyfinlandZC','mbeskyromaniaFC','mbeskyromaniaZC') AND VERSION_NO='14' AND ENTITY_NAME NOT IN (SELECT ENTITY_NAME FROM UNBILLED_CLIENT WHERE VERSION='SaaS') AND ENTITY_NAME IN (SELECT CLIENT_NAME FROM BILLING_CLIENTS) "+((!"ALL".equals(Constants.clients))?clientsWhereClause.toString():"")+" ORDER BY UPPER(CODE_VALUE),UPPER(ENTITY_NAME) ASC");
			} else {
				clientsQuery.append(
						"SELECT ENTITY_NAME,CODE_VALUE,DB_SERVER,DB_NAME , BILLABLE_LEVEL,BUILDS_URL,TOMCAT_SERVER FROM ENTITIES WHERE DB_NAME!='null' AND DB_NAME!='' AND DB_SERVER !='null' AND DB_SERVER!=''  AND BUILD_STATUS ='52' AND IS_ARCHIVED='No' AND ENTITY_NAME NOT IN('mbeww','mbeItaly','mbeaustralia','mbegermany','flcouncil','mbespain','mbefrance','mbeuk','mbePoland','mbechile','mbeLatam','mbecentralamerica','FacebookConnect725_30','FacebookConnect725','mbeskyfinlandFC','mbeskyfinlandZC','mbeskyromaniaFC','mbeskyromaniaZC') AND VERSION_NO='14' AND ENTITY_NAME NOT IN (SELECT ENTITY_NAME FROM UNBILLED_CLIENT WHERE VERSION='SaaS') "+((!"ALL".equals(Constants.clients))?clientsWhereClause.toString():"")+" ORDER BY UPPER(CODE_VALUE),UPPER(ENTITY_NAME) ASC");
			}
		}
		if ("Y".equals(Constants.specialReport)) {
			if (version.equals("Legacy"))
				specialClientQuery.append(
						"SELECT ENTITY_NAME,CODE_VALUE,DB_SERVER,DB_NAME , BILLABLE_LEVEL,BUILDS_URL,TOMCAT_SERVER FROM ENTITIES WHERE DB_NAME!='null' AND DB_NAME!='' AND DB_SERVER !='null' AND DB_SERVER!=''  AND BUILD_STATUS ='52' AND IS_ARCHIVED='No' AND ENTITY_NAME NOT IN('mbeww','mbeItaly','mbeaustralia','mbegermany','flcouncil','mbespain','mbefrance','mbeuk','mbePoland','mbechile','mbeLatam','mbecentralamerica','FacebookConnect725_30','FacebookConnect725') AND VERSION_NO!='14' AND ENTITY_NAME NOT IN (SELECT ENTITY_NAME FROM UNBILLED_CLIENT WHERE VERSION='SaaS') AND ENTITY_NAME IN (SELECT BUILD_NAME FROM SPECIAL_CLIENT UNION SELECT MAP_CLIENT FROM SPECIAL_CLIENT_MAPPING) ORDER BY UPPER(CODE_VALUE),UPPER(ENTITY_NAME)  ASC");
			else
				specialClientQuery.append(
						"SELECT ENTITY_NAME,CODE_VALUE,DB_SERVER,DB_NAME , BILLABLE_LEVEL,BUILDS_URL,TOMCAT_SERVER FROM ENTITIES WHERE DB_NAME!='null' AND DB_NAME!='' AND DB_SERVER !='null' AND DB_SERVER!=''  AND BUILD_STATUS ='52' AND IS_ARCHIVED='No' AND ENTITY_NAME NOT IN('mbeww','mbeItaly','mbeaustralia','mbegermany','flcouncil','mbespain','mbefrance','mbeuk','mbePoland','mbechile','mbeLatam','mbecentralamerica','FacebookConnect725_30','FacebookConnect725') AND VERSION_NO='14' AND ENTITY_NAME NOT IN (SELECT ENTITY_NAME FROM UNBILLED_CLIENT WHERE VERSION='SaaS') AND ENTITY_NAME IN (SELECT BUILD_NAME FROM SPECIAL_CLIENT UNION SELECT MAP_CLIENT FROM SPECIAL_CLIENT_MAPPING) ORDER BY UPPER(CODE_VALUE),UPPER(ENTITY_NAME)  ASC");
			clientsQuery = specialClientQuery;
		}
		System.out.println("++++++++++++clientsQuery" + clientsQuery);
		return QueryUtil.executeQuery(DBConnectionProvider.getInstance().getDBConnection("fss"),
				clientsQuery.toString(), version);
	}

	public static LinkedHashMap<String, ClientInfo> setupClientResources(HashMap<String, Object> clientsMap) {
		LinkedHashMap<String, ClientInfo> clients = new LinkedHashMap<String, ClientInfo>();
		ArrayList<String> cl = new ArrayList<String>();
		try {

			Set<String> cols = (Set<String>) clientsMap.get("COLUMN_NAMES");
			ArrayList<DynaBean> dataBeans = (ArrayList<DynaBean>) clientsMap.get("DATA");
			if (dataBeans != null && cols != null) {

				Iterator<DynaBean> dataItr = dataBeans.iterator();
				while (dataItr.hasNext()) {
					DynaBean bean = dataItr.next();
					ClientInfo dbProps = new ClientInfo();
					String clientName = (String) bean.get("ENTITY_NAME");
					dbProps.setClientName(clientName);
					dbProps.setDbName((String) bean.get("DB_NAME"));
					dbProps.setDbServerIP((String) bean.get("DB_SERVER"));
					dbProps.setBillableLevel((String) bean.get("BILLABLE_LEVEL"));
					dbProps.setCodeValue((String) bean.get("CODE_VALUE"));
					dbProps.setDbUser("fsslogin");
					dbProps.setDbPassword("fss@l0g1n");
					dbProps.setHostURL((String) bean.get("BUILDS_URL"));
					dbProps.setTomcatServer((String)bean.get("TOMCAT_SERVER"));
					DBConnectionProvider.getInstance().setupDB(dbProps);
					if (!DBConnectionProvider.getInstance().isDBConnected(clientName)) {
						clients.put(clientName, dbProps);
					} else {
						cl.add(clientName);
						Constants.clNotConnected.append(clientName + " , ");
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return clients;
	}

	public static HashMap<String, ModuleInfo> getModulesInfo(String tableName) {
		Endpoint dbConnection = null;
		HashMap<String, ModuleInfo> modulesMap = new HashMap<String, ModuleInfo>();
		try {
			dbConnection = DBConnectionProvider.getInstance().getDBConnection("InHouse");
			StringBuffer query = new StringBuffer();
			query.append(
					"SELECT MODULE_ID,MUI.ATTRIBUTE_ID,QUERY,COLUMN_NAMES,TABLE_NAME,SECONDARY_QUERY,TYPE, ROUTINE_CLASS_NAME FROM MODULE_USAGE_INFO MUI LEFT JOIN DATA_PROCESSING_ROUTINE DPR ON MUI.ATTRIBUTE_ID=DPR.ATTRIBUTE_ID WHERE 1=1 ");

			if(Constants.exportType.contains(","))
			query.append(" AND TYPE  IN (" + Constants.exportType + ")");
			else
			query.append(" AND TYPE  = '" + Constants.exportType + "'");
			System.out.println("query:::::::"+query.toString());

			if (tableName != null && !"NA".equals(tableName)) {
				query.append(" AND TABLE_NAME='" + tableName + "'");
			}

			if (Constants.moduleIds != null && !"NA".equals(Constants.moduleIds)
					&& !"100".equals(Constants.moduleIds)) {
				query.append(" AND MODULE_ID IN (" + Constants.moduleIds + ")");
			}

			if (Constants.attributeIds != null && !"NA".equals(Constants.attributeIds)) {
				query.append(" AND ATTRIBUTE_ID IN (" + Constants.attributeIds + ")");
			}

			HashMap<String, Object> results = QueryUtil.executeQuery(dbConnection, query.toString());
			boolean isFailed = (boolean) results.get("IS_FAILED");
			if (!isFailed) {
				Set<String> cols = (Set) results.get("COLUMN_NAMES");
				ArrayList<DynaBean> dataBeans = (ArrayList<DynaBean>) results.get("DATA");
				if (dataBeans != null && cols != null) {
					Iterator<DynaBean> dataItr = dataBeans.iterator();
					while (dataItr.hasNext()) {
						DynaBean bean = dataItr.next();
						String moduleId = (String) bean.get("MODULE_ID");
						ModuleInfo modInfo = null;

						if (modulesMap.containsKey(moduleId)) {
							modInfo = modulesMap.get(moduleId);
						} else {
							modInfo = new ModuleInfo();
							modulesMap.put(moduleId, modInfo);
						}
						modInfo.setModuleId(moduleId);
						String attributeId = (String) bean.get("ATTRIBUTE_ID");
						String primaryQuery = (String) bean.get("QUERY");
						String secondaryQuery = (String) bean.get("SECONDARY_QUERY");
						System.out.println("preparing q map : " + secondaryQuery);
						Map<String, String> qMap = new HashMap<String, String>();
						qMap.put("QUERY", primaryQuery);
						qMap.put("SECONDARY_QUERY", secondaryQuery);
						qMap.put("TYPE",(String)bean.get("TYPE") );
						qMap.put("COLUMN_NAMES",(String)bean.get("COLUMN_NAMES") );
						qMap.put("ROUTINE_CLASS_NAME", (String)bean.get("ROUTINE_CLASS_NAME"));
						modInfo.getQueriesMap().put(attributeId, qMap);
						System.out.println(primaryQuery);
						modInfo.addColumns((String) bean.get("COLUMN_NAMES"));
						modInfo.setTableName((String) bean.get("TABLE_NAME"));
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return modulesMap;
	}

	public static HashMap<String, String> getClientModulesMap() {
		Endpoint connection = DBConnectionProvider.getInstance().getDBConnection("InHouse");
		HashMap<String, String> cmMap = new HashMap<String, String>();
		StringBuffer cmQuery = new StringBuffer();
		cmQuery.append(
				"SELECT CUSTOMER_CODE_VALUE, GROUP_CONCAT(DISTINCT MODULE_ID)AS MODULE_IDS  FROM FRANCHISEE F ,FIM_CLIENT_MODULE_LIST FCM WHERE  F.FRANCHISEE_NO = FCM.CLIENT_ID ");
		if (Constants.moduleIds != null && !"NA".equals(Constants.moduleIds) && !"100".equals(Constants.moduleIds)) {
			cmQuery.append(" AND MODULE_ID IN (" + Constants.moduleIds + ") ");
		}
		cmQuery.append("GROUP BY FRANCHISEE_NO");
		HashMap<String, Object> results = QueryUtil.executeQuery(connection, cmQuery.toString());
		boolean isFailed = (boolean) results.get("IS_FAILED");
		if (!isFailed) {
			Set<String> cols = (Set) results.get("COLUMN_NAMES");
			ArrayList<DynaBean> dataBeans = (ArrayList<DynaBean>) results.get("DATA");
			if (dataBeans != null && cols != null) {
				Iterator<DynaBean> dataItr = dataBeans.iterator();
				while (dataItr.hasNext()) {
					DynaBean bean = dataItr.next();
					cmMap.put((String) bean.get("CUSTOMER_CODE_VALUE"), (String) bean.get("MODULE_IDS"));
				}
			}
		}
		return cmMap;
	}

	public static void removeOldData(String date) {

		Endpoint dbConnection = DBConnectionProvider.getInstance().getDBConnection("InHouse");
		String query = "DELETE FROM USER_BILLING_REPORT WHERE DATE_FROM ='" + date + "'";

		Exchange exchange = QueryUtil.executeDelete(dbConnection, query);
		System.out
				.println("DELETE FROM USER_BILLING_REPORT for date : " + date + " is failed : " + exchange.isFailed());

		query = "DELETE FROM CLIENT_SPECIAL_BILLING_DATA WHERE DATE_FROM ='" + date + "'";

		exchange = QueryUtil.executeDelete(dbConnection, query);
		System.out.println(
				"delete from CLIENT_SPECIAL_BILLING_DATA for date : " + date + " is failed : " + exchange.isFailed());

		query = "DELETE FROM TBC_LIFECYCLE_REPORT_DATA WHERE DATE_FROM ='" + date + "'";

		exchange = QueryUtil.executeDelete(dbConnection, query);
		System.out.println(
				"delete from TBC_LIFECYCLE_REPORT_DATA for date : " + date + " is failed : " + exchange.isFailed());

	}

	public static void removeOldData() {
		Endpoint dbConnection = DBConnectionProvider.getInstance().getDBConnection("InHouse");
		String date = Constants.dateFrom;
		if ("SU".equals(Constants.exportType)) {
			String query = "DELETE FROM USER_BILLING_REPORT WHERE DATE_FROM ='" + date + "'";

			Exchange exchange = QueryUtil.executeUpdate(dbConnection, query);
			System.out.println(
					"DELETE FROM USER_BILLING_REPORT for date : " + date + " is failed : " + exchange.isFailed());

			query = "DELETE FROM CLIENT_SPECIAL_BILLING_DATA WHERE DATE_FROM ='" + date + "'";

			exchange = QueryUtil.executeUpdate(dbConnection, query);
			System.out.println("delete from CLIENT_SPECIAL_BILLING_DATA for date : " + date + " is failed : "
					+ exchange.isFailed());

			query = "DELETE FROM TBC_LIFECYCLE_REPORT_DATA WHERE DATE_FROM ='" + date + "'";

			exchange = QueryUtil.executeUpdate(dbConnection, query);
			System.out.println(
					"delete from TBC_LIFECYCLE_REPORT_DATA for date : " + date + " is failed : " + exchange.isFailed());
		} else if ("MU".equals(Constants.exportType)) {
			String query = "DELETE FROM CHA_DATA WHERE DATE_FROM ='" + date + "'";

			Exchange exchange = QueryUtil.executeUpdate(dbConnection, query);
			System.out
					.println("DELETE FROM CHA_DATA WHERE DATE_FROM ='" + date + "' is failed : " + exchange.isFailed());
		}
	}

	public static void mergeData() {
		System.out.println(" called merged");
		String query1 = null;
		String query2 = null;
		String query3 = null;
		String query4 = null;
		if ("MSU".equals(Constants.exportType)) {
			query1 = "CREATE TABLE IF NOT EXISTS CODE_VALUE AS SELECT CODE_VALUE FROM USER_BILLING_REPORT WHERE CODE_VALUE IN (SELECT DISTINCT CODE_VALUE FROM USER_BILLING_REPORT WHERE BUILD_NAME LIKE '%zc%') GROUP BY CODE_VALUE HAVING COUNT(DISTINCT BUILD_NAME)=2";
			query2 = "UPDATE USER_BILLING_REPORT A LEFT JOIN ( SELECT CODE_VALUE,DATE_TO, SUM(SUBSTRING_INDEX(LOGGED_IN_HRS_FU,':',1)*3600+(SUBSTRING_INDEX(SUBSTRING_INDEX(LOGGED_IN_HRS_FU,':',-2),':',1)*60)+SUBSTRING_INDEX(LOGGED_IN_HRS_FU,':',-1)) AS FU,SUM(SUBSTRING_INDEX(LOGGED_IN_HRS_RU,':',1)*3600+(SUBSTRING_INDEX(SUBSTRING_INDEX(LOGGED_IN_HRS_RU,':',-2),':',1)*60)+SUBSTRING_INDEX(LOGGED_IN_HRS_RU,':',-1)) AS RU,SUM(SUBSTRING_INDEX(LOGGED_IN_HRS_CU,':',1)*3600+(SUBSTRING_INDEX(SUBSTRING_INDEX(LOGGED_IN_HRS_CU,':',-2),':',1)*60)+SUBSTRING_INDEX(LOGGED_IN_HRS_CU,':',-1)) AS CU,SUM(SUBSTRING_INDEX(P_TIME,':',1)*3600+(SUBSTRING_INDEX(SUBSTRING_INDEX(P_TIME,':',-2),':',1)*60)+SUBSTRING_INDEX(P_TIME,':',-1)) AS PU,SUM(SUBSTRING_INDEX(TOTAL_SYSTEM_TIME,':',1)*3600+(SUBSTRING_INDEX(SUBSTRING_INDEX(TOTAL_SYSTEM_TIME,':',-2),':',1)*60)+SUBSTRING_INDEX(TOTAL_SYSTEM_TIME,':',-1)) AS SU,SUM(SUBSTRING_INDEX(ADMIN_TIME,':',1)*3600+(SUBSTRING_INDEX(SUBSTRING_INDEX(ADMIN_TIME,':',-2),':',1)*60)+SUBSTRING_INDEX(ADMIN_TIME,':',-1)) AS AU,SUM(SUBSTRING_INDEX(LOGGED_IN_HRS_DU,':',1)*3600+(SUBSTRING_INDEX(SUBSTRING_INDEX(LOGGED_IN_HRS_DU,':',-2),':',1)*60)+SUBSTRING_INDEX(LOGGED_IN_HRS_DU,':',-1)) AS DU FROM USER_BILLING_REPORT WHERE CODE_VALUE IN (SELECT CODE_VALUE FROM CODE_VALUE) GROUP BY  CODE_VALUE,DATE_TO ) B ON A.CODE_VALUE=B.CODE_VALUE  AND  A.DATE_TO=B.DATE_TO SET A.LOGGED_IN_HRS_CU=CONCAT(FLOOR(B.CU/3600),':',LPAD(FLOOR((B.CU%3600)/60),2,0),':',LPAD((B.CU%3600)%60,2,0)), A.LOGGED_IN_HRS_RU=CONCAT(FLOOR(B.RU/3600),':',LPAD(FLOOR((B.RU%3600)/60),2,0),':',LPAD((B.RU%3600)%60,2,0)), A.LOGGED_IN_HRS_FU=CONCAT(FLOOR(B.FU/3600),':',LPAD(FLOOR((B.FU%3600)/60),2,0),':',LPAD((B.FU%3600)%60,2,0)), A.P_TIME=CONCAT(FLOOR(B.PU/3600),':',LPAD(FLOOR((B.PU%3600)/60),2,0),':',LPAD((B.PU%3600)%60,2,0)), A.TOTAL_SYSTEM_TIME=CONCAT(FLOOR(B.SU/3600),':',LPAD(FLOOR((B.SU%3600)/60),2,0),':',LPAD((B.SU%3600)%60,2,0)), A.ADMIN_TIME=CONCAT(FLOOR(B.AU/3600),':',LPAD(FLOOR((B.AU%3600)/60),2,0),':',LPAD((B.AU%3600)%60,2,0)), A.LOGGED_IN_HRS_DU=CONCAT(FLOOR(B.DU/3600),':',LPAD(FLOOR((B.DU%3600)/60),2,0),':',LPAD((B.DU%3600)%60,2,0)) WHERE B.CODE_VALUE IS NOT NULL";
			query3 = "DELETE FROM USER_BILLING_REPORT WHERE CODE_VALUE IN (SELECT CODE_VALUE FROM CODE_VALUE) AND BUILD_NAME LIKE '%zc%'";
			query4 = "DROP TABLE CODE_VALUE";
		}

		else if ("MMU".equals(Constants.exportType)) {
			query1 = "CREATE TABLE IF NOT EXISTS CODE_VALUE AS SELECT CODE_VALUE FROM CHA_DATA WHERE CODE_VALUE IN (SELECT DISTINCT CODE_VALUE FROM CHA_DATA WHERE CLIENT_NAME LIKE '%zc%') GROUP BY CODE_VALUE HAVING COUNT(DISTINCT CLIENT_NAME)=2";
			query2 = "UPDATE CHA_DATA A LEFT JOIN (SELECT CODE_VALUE,DATE_TO,CRM_LEAD_ADDED,OPPORTUNIITY_ADDED,ACCOUNTS_ADDED,CRM_CONTACTS_ADDED,EMAIL_SENT_CM,CALLS_LOGGED_CM,TASKS_CREATED_CM,LEADS_CLOSED_CRM,TABS_CREATED_CRM,SECTIONS_ADDED_CRM,FIELDS_ADDED_CRM,ADS_BY_CORPORATE,ARTWORK_SAVED,SAVED_ARTWORK_CARTS,ADS_EMAILED,AD_MAILED,ADS_DOWNLOADED,SITES_ADDED,CHANGES_PUBLISHED,AVG_UPDATES_ZEES,NEW_ORDERS,ORDER_TOTAL,NO_PURCHASE_ORDERS,NO_PRODUCTS_SOLD,TOP_SELLING_PRODUCT,SUCCESSFUL_ORDER_PERCENTAGE,NO_INCOMPLETE_LISTINGS,LOCATIONS_LISTINGS_ACTIVATED_PERCENTAGE,AVG_REPUTATION_SCORE,MAX_REPUTATION_SCORE,MIN_REPUTATION_SCORE,MEDIAN_REPUTATION_SCORE,LOCATIONS_REPUTATION_ACTIVATED_PERCENTAGE,TEMPLATES_ADDED,CAMPAIGNS_CREATED,CAMPAIGN_EMAILS_SENT,CAMPAIGN_EMAILS_FAILED FROM   CHA_DATA WHERE CLIENT_NAME LIKE '%zc%' AND CODE_VALUE IN (SELECT CODE_VALUE FROM CODE_VALUE) GROUP BY  CODE_VALUE,DATE_TO) B ON A.CODE_VALUE=B.CODE_VALUE  AND  A.DATE_TO=B.DATE_TO SET A.CRM_LEAD_ADDED=B.CRM_LEAD_ADDED, A.OPPORTUNIITY_ADDED=B.OPPORTUNIITY_ADDED,A.ACCOUNTS_ADDED=B.ACCOUNTS_ADDED,A.CRM_CONTACTS_ADDED=B.CRM_CONTACTS_ADDED,A.EMAIL_SENT_CM=B.EMAIL_SENT_CM,A.CALLS_LOGGED_CM=B.CALLS_LOGGED_CM,A.TASKS_CREATED_CM=B.TASKS_CREATED_CM,A.LEADS_CLOSED_CRM=B.LEADS_CLOSED_CRM,A.TABS_CREATED_CRM=B.TABS_CREATED_CRM,A.SECTIONS_ADDED_CRM=B.SECTIONS_ADDED_CRM,A.FIELDS_ADDED_CRM=B.FIELDS_ADDED_CRM,A.ADS_BY_CORPORATE=B.ADS_BY_CORPORATE,A.ARTWORK_SAVED=B.ARTWORK_SAVED,A.SAVED_ARTWORK_CARTS=B.SAVED_ARTWORK_CARTS,A.ADS_EMAILED=B.ADS_EMAILED,A.AD_MAILED=B.AD_MAILED,A.ADS_DOWNLOADED=B.ADS_DOWNLOADED,A.SITES_ADDED=B.SITES_ADDED,A.CHANGES_PUBLISHED=B.CHANGES_PUBLISHED,A.AVG_UPDATES_ZEES=B.AVG_UPDATES_ZEES,A.NEW_ORDERS=B.NEW_ORDERS,A.ORDER_TOTAL=B.ORDER_TOTAL,A.NO_PURCHASE_ORDERS=B.NO_PURCHASE_ORDERS,A.NO_PRODUCTS_SOLD=B.NO_PRODUCTS_SOLD,A.TOP_SELLING_PRODUCT=B.TOP_SELLING_PRODUCT,A.SUCCESSFUL_ORDER_PERCENTAGE=B.SUCCESSFUL_ORDER_PERCENTAGE,A.NO_INCOMPLETE_LISTINGS=B.NO_INCOMPLETE_LISTINGS,A.LOCATIONS_LISTINGS_ACTIVATED_PERCENTAGE=B.LOCATIONS_LISTINGS_ACTIVATED_PERCENTAGE,A.AVG_REPUTATION_SCORE=B.AVG_REPUTATION_SCORE,A.MAX_REPUTATION_SCORE=B.MAX_REPUTATION_SCORE,A.MIN_REPUTATION_SCORE=B.MIN_REPUTATION_SCORE,A.MEDIAN_REPUTATION_SCORE=B.MEDIAN_REPUTATION_SCORE,A.LOCATIONS_REPUTATION_ACTIVATED_PERCENTAGE=B.LOCATIONS_REPUTATION_ACTIVATED_PERCENTAGE,A.TEMPLATES_ADDED=B.TEMPLATES_ADDED,A.CAMPAIGNS_CREATED=B.CAMPAIGNS_CREATED,A.CAMPAIGN_EMAILS_SENT=B.CAMPAIGN_EMAILS_SENT,A.CAMPAIGN_EMAILS_FAILED=B.CAMPAIGN_EMAILS_FAILED WHERE B.CODE_VALUE IS NOT NULL";
			query3 = "DELETE FROM CHA_DATA WHERE CODE_VALUE IN (SELECT CODE_VALUE FROM CODE_VALUE) AND CLIENT_NAME LIKE '%zc%'";
			query4 = "DROP TABLE CODE_VALUE";
		}
		Endpoint dbConnection = DBConnectionProvider.getInstance().getDBConnection("InHouse");
		Exchange exchange1 = QueryUtil.executeUpdate(dbConnection, query1);
		if (!exchange1.isFailed()) {
			Exchange exchange2 = QueryUtil.executeUpdate(dbConnection, query2);
			if (!exchange2.isFailed()) {
				Exchange exchange3 = QueryUtil.executeUpdate(dbConnection, query3);
				if (exchange3.isFailed()) {
					System.out.println(" data merged but ZC data not deleted from : " + Constants.exportTaskName
							+ exchange3.getException());
				}

			} else {
				System.out.println(" merge data failed for " + Constants.exportTaskName + "query is : " + query2);
				Exception exp = exchange2.getException();
				System.out.println(" exption is : " + exp.getMessage());
				exp.getStackTrace();
			}

			Exchange exchange4 = QueryUtil.executeUpdate(dbConnection, query4);
			if (exchange4.isFailed()) {
				System.out.println("query failed : " + query4 + "exception : " + exchange4.getException());
			}
		} else {
			System.out.println(Constants.exportTaskName + " failed because of query " + query1);
		}
	}

	public static void setupDateFromDateTo() {
		String dateFrom = "", dateTo = "";

		Endpoint connection = DBConnectionProvider.getInstance().getDBConnection("InHouse");
		Exchange exchange = connection.createExchange();
		String query = "";
		CamelContext context = exchange.getContext();
		Endpoint endpoint = context.getEndpoint("direct:InHouse");
		ProducerTemplate template = context.createProducerTemplate();
		exchange = endpoint.createExchange();
		Exchange Queryout = null;
		List<Map<String, Object>> Queryresult = null;
		int i = 0;

		query = "SELECT DATE_FROM,DATE_TO FROM USER_BILLING_DATE ORDER BY DATE_FROM DESC,DATE_TO DESC LIMIT 1";
		exchange.getIn().setBody(query.toString());
		// System.out.println("======query=====111111====>"+query.toString());
		Queryout = template.send(endpoint, exchange);
		Queryresult = Queryout.getOut().getBody(List.class);

		i = Queryresult.size();

		for (int j = 0; j < i; j++) {
			Map<String, Object> row = Queryresult.get(j);
			if (row.get("DATE_FROM") != null) {
				dateFrom = row.get("DATE_FROM") + "";
			} else {
				dateFrom = "";
			}
			if (row.get("DATE_TO") != null) {
				dateTo = row.get("DATE_TO") + "";
			} else {
				dateTo = "";
			}
		}

		if ("".equals(dateFrom.trim()) || "".equals(dateTo.trim())) {
			System.out.println("From Date not valid OR To Date not valid");

			query = "SELECT DATE_SUB(CURRENT_DATE, INTERVAL DAYOFMONTH(CURRENT_DATE)-1 DAY) AS DATE_FROM,DATE_ADD(CURRENT_DATE, INTERVAL -1 DAY) AS DATE_TO";
			exchange.getIn().setBody(query.toString());
			// System.out.println("======query=====22====>"+query.toString());
			Queryout = template.send(endpoint, exchange);
			Queryresult = Queryout.getOut().getBody(List.class);

			i = Queryresult.size();

			for (int j = 0; j < i; j++) {
				Map<String, Object> row = Queryresult.get(j);
				if (row.get("DATE_FROM") != null) {
					dateFrom = row.get("DATE_FROM") + "";
				} else {
					dateFrom = "";
				}
				if (row.get("DATE_TO") != null) {
					dateTo = row.get("DATE_TO") + "";
				} else {
					dateTo = "";
				}
			}
		}

		if (!Utility.isValidDate(dateFrom)) {
			System.out.println("From Date not valid");
			System.exit(0);
		}
		if (!Utility.isValidDate(dateTo)) {
			System.out.println("To Date not valid");
			System.exit(0);
		}

		Constants.dateFrom = dateFrom;
		Constants.dateTo = dateTo;

		System.out.println("dateFrom"+dateFrom);
		System.out.println("dateTo"+dateTo);
		// dateFrom=Constants.dateFrom;
		// dateTo=Constants.dateTo;

		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		String datee = dateFormat.format(date);
		Constants.mailText = null;
		Constants.mailSubject.setLength(0);
		Constants.mailText = new StringBuilder();

		Constants.mailText.append("Thread Executed : " + Constants.exportTaskName + "<br>");

		Constants.mailText.append("<br>Data fetched for dates : " + dateFrom + " TO " + dateTo);
		dateFormat = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
		Constants.mailText.append(" Thread Start time : " + dateFormat.format(date));

		// Constants.mailAttachmentExport=Constants.appHome+"UserBillingExport/userMonthly_user_cal_"+dateFrom+"_"+dateTo+".xls";
		Constants.fileName = "Monthly_Billing_Report_" + dateFrom + "_" + dateTo + ".xls"; // Change
																							// in
																							// File
																							// Name
		if (Constants.version.equals("Legacy")) { // FCSKYS-15522 :Report Name
			if ("Y".equals(Constants.specialReport)) {
				Constants.fileName = "Special_Billing_Report_Legacy_" + dateFrom + "_" + java.time.LocalDate.now() + ".xls";
				Constants.mailSubject.append(
						"Special Data InHouse Exporter Thread Logs for Legacy Thread : " + Constants.exportTaskName);
				Constants.mailAttachmentExport = Constants.appHome + "UserBillingExport/Special_Billing_Report_Legacy_"
						+ dateFrom + "_" + java.time.LocalDate.now() + ".xls";
			} else {
				Constants.fileName = "Monthly_Billing_Report_Legacy_" + dateFrom + "_" + java.time.LocalDate.now() + ".xls";
			Constants.mailSubject
					.append("Data InHouse Exporter Thread Logs for Legacy Thread : " + Constants.exportTaskName);
			Constants.mailAttachmentExport = Constants.appHome + "UserBillingExport/Monthly_Billing_Report_Legacy_"
						+ dateFrom + "_" + java.time.LocalDate.now() + ".xls";
			}
		} else {
			if ("Y".equals(Constants.specialReport)) {
				Constants.fileName = "Special_Billing_Report_SaaS_" + dateFrom + "_" + java.time.LocalDate.now() + ".xls";
				Constants.mailSubject.append(
						"Special Data InHouse Exporter Thread Logs for SaaS Thread : " + Constants.exportTaskName);
				Constants.mailAttachmentExport = Constants.appHome + "UserBillingExport/Special_Billing_Report_SaaS_"
						+ dateFrom + "_" + java.time.LocalDate.now() + ".xls";
			} else {
				Constants.fileName = "Monthly_Billing_Report_SaaS_" + dateFrom + "_" + java.time.LocalDate.now() + ".xls";
			Constants.mailSubject
					.append("Data InHouse Exporter Thread Logs for SaaS Thread : " + Constants.exportTaskName);
			Constants.mailAttachmentExport = Constants.appHome + "UserBillingExport/Monthly_Billing_Report_SaaS_"
						+ dateFrom + "_" + java.time.LocalDate.now() + ".xls";
			}
		}

	}

	public static void removeOldDataForSUBILLING() {
		Endpoint dbConnection = DBConnectionProvider.getInstance().getDBConnection("InHouse");
		String query = "";
		if ("SUBILLING".equals(Constants.exportType) || "SpecialReport".equals(Constants.exportType)) {
			if("N".equals(Constants.executeFor)) {
				query = "DELETE FROM USER_BILLING_THREAD_DATA WHERE BUILD_NAME IN (SELECT CLIENT_NAME FROM BILLING_CLIENTS)";
			}else {
				query = "DELETE FROM USER_BILLING_THREAD_DATA";
			}
			Exchange exchange = QueryUtil.executeUpdate(dbConnection, query);
			// System.out.println("DELETE FROM USER_BILLING_THREAD_DATA : is
			// failed : "+exchange.isFailed());

			if("N".equals(Constants.executeFor)) {
				query = "DELETE FROM ACTIVE_LOCATION_THREAD_DATA WHERE BUILD_NAME IN (SELECT CLIENT_NAME FROM BILLING_CLIENTS)";
			}else {
			query = "DELETE FROM ACTIVE_LOCATION_THREAD_DATA";
			}
			exchange = QueryUtil.executeUpdate(dbConnection, query);
			// System.out.println("DELETE FROM ACTIVE_LOCATION_THREAD_DATA : is
			// failed : "+exchange.isFailed());

			if("N".equals(Constants.executeFor)) {
				query = "DELETE FROM UNIQUE_LOCATION_THREAD_DATA WHERE BUILD_NAME IN (SELECT CLIENT_NAME FROM BILLING_CLIENTS)";
			}else {
			query = "DELETE FROM UNIQUE_LOCATION_THREAD_DATA";
			}
			exchange = QueryUtil.executeUpdate(dbConnection, query);
			// System.out.println("DELETE FROM UNIQUE_LOCATION_THREAD_DATA : is
			// failed : "+exchange.isFailed());

			if("N".equals(Constants.executeFor)) {
				query = "DELETE FROM MORE_THEN_FIVE_LOCATION_THREAD_DATA WHERE BUILD_NAME IN (SELECT CLIENT_NAME FROM BILLING_CLIENTS)";
			}else {
			query = "DELETE FROM MORE_THEN_FIVE_LOCATION_THREAD_DATA";
			}
			exchange = QueryUtil.executeUpdate(dbConnection, query);
			// System.out.println("DELETE FROM
			// MORE_THEN_FIVE_LOCATION_THREAD_DATA : is failed :
			// "+exchange.isFailed());

			if("N".equals(Constants.executeFor)) {
				query = "DELETE FROM EXTRA_CAL_LOCATION_THREAD_DATA WHERE BUILD_NAME IN (SELECT CLIENT_NAME FROM BILLING_CLIENTS)";
			}else {
			query = "DELETE FROM EXTRA_CAL_LOCATION_THREAD_DATA";
			}
			exchange = QueryUtil.executeUpdate(dbConnection, query);
			// System.out.println("DELETE FROM EXTRA_CAL_LOCATION_THREAD_DATA :
			// is failed : "+exchange.isFailed());

			if("N".equals(Constants.executeFor)) {
				query = "DELETE FROM TOTAL_CAL_LOCATION_THREAD_DATA WHERE BUILD_NAME IN (SELECT CLIENT_NAME FROM BILLING_CLIENTS)";
			}else {
			query = "DELETE FROM TOTAL_CAL_LOCATION_THREAD_DATA";
			}
			exchange = QueryUtil.executeUpdate(dbConnection, query);
			// System.out.println("DELETE FROM TOTAL_CAL_LOCATION_THREAD_DATA :
			// is failed : "+exchange.isFailed());

			if("N".equals(Constants.executeFor)) {
				query = "DELETE FROM NOUSER_LOCATION_THREAD_DATA WHERE BUILD_NAME IN (SELECT CLIENT_NAME FROM BILLING_CLIENTS)";
			}else {
			query = "DELETE FROM NOUSER_LOCATION_THREAD_DATA";
			}
			exchange = QueryUtil.executeUpdate(dbConnection, query);
			//System.out.println("DELETE FROM NOUSER_LOCATION_THREAD_DATA : is failed : " + exchange.isFailed());

			if("N".equals(Constants.executeFor)) {
				query = "DELETE FROM USERS_THREAD_DATA WHERE BUILD_NAME IN (SELECT CLIENT_NAME FROM BILLING_CLIENTS)";
			}else {
			query = "DELETE FROM USERS_THREAD_DATA";
			}
			exchange = QueryUtil.executeUpdate(dbConnection, query);
			if("N".equals(Constants.executeFor)) {
				query = "DELETE FROM SALES_ROLE WHERE BUILD_NAME IN (SELECT CLIENT_NAME FROM BILLING_CLIENTS)";
			}else {
			query = "DELETE FROM SALES_ROLE";
			}
			exchange = QueryUtil.executeUpdate(dbConnection, query);
			if("N".equals(Constants.executeFor)) {
				query = "DELETE FROM MUID_USERS_THREAD_DATA WHERE BUILD_NAME IN (SELECT CLIENT_NAME FROM BILLING_CLIENTS)";
			}else {
			query = "DELETE FROM MUID_USERS_THREAD_DATA";
			}
			exchange = QueryUtil.executeUpdate(dbConnection, query);
			if("N".equals(Constants.executeFor)) {
				query = "DELETE FROM BRIGHTSTAR_BILLING_USER_DATA WHERE BUILD_NAME IN (SELECT CLIENT_NAME FROM BILLING_CLIENTS)";
			}else {
			query = "DELETE FROM BRIGHTSTAR_BILLING_USER_DATA";
			}
			exchange = QueryUtil.executeUpdate(dbConnection, query);
			if("N".equals(Constants.executeFor)) {
				query = "DELETE FROM BRIGHTSTAR_BILLING_DATA WHERE BUILD_NAME IN (SELECT CLIENT_NAME FROM BILLING_CLIENTS)";
			}else {
			query = "DELETE FROM BRIGHTSTAR_BILLING_DATA";
			}
			exchange = QueryUtil.executeUpdate(dbConnection, query);
			if("N".equals(Constants.executeFor)) {
				query = "DELETE FROM LIGHTBRIDGE_BILLING_DATA WHERE BUILD_NAME IN (SELECT CLIENT_NAME FROM BILLING_CLIENTS)";
			}else {
			query = "DELETE FROM LIGHTBRIDGE_BILLING_DATA";
			}
			exchange = QueryUtil.executeUpdate(dbConnection, query);
			// System.out.println("DELETE FROM USERS_THREAD_DATA : is failed :
			// "+exchange.isFailed());

			/*
			 * User Billing New Changes End
			 */

		}

	}

	public static void removeOldEUData() {
		Endpoint dbConnection = DBConnectionProvider.getInstance().getDBConnection("InHouse");
		if ("EUGenerate".equals(Constants.exportType)) {
			String query = "DELETE FROM EU_DATA";
			Exchange exchange = QueryUtil.executeUpdate(dbConnection, query);
		}
	}
	public static void removeBillingClients() {
		Endpoint dbConnection = DBConnectionProvider.getInstance().getDBConnection("InHouse");
		String query = "DELETE FROM BILLING_CLIENTS";
		Exchange exchange = QueryUtil.executeUpdate(dbConnection, query);
	}
	
	public static void setupMUDateFromDateTo() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        Constants.mailText = null;
        Constants.mailSubject.setLength(0);
        Constants.mailText = new StringBuilder();
        Constants.mailText.append("Thread Executed : " + Constants.exportTaskName + "<br>");
        Constants.mailText.append("<br>Data fetched for dates : " + Constants.dateFrom + " TO " + Constants.dateTo);
        dateFormat = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
        Constants.mailText.append(" Thread Start time : " + dateFormat.format(date));
        Constants.fileName = "Customer_Usage_Report_" + Constants.dateFrom + "_" + Constants.dateTo + ".xls";
        if (Constants.version.equals("Legacy")) {
            Constants.fileName = "Customer_Usage_Report_Legacy_" + Constants.dateFrom + "_" + Constants.dateTo + ".xlsx";
            Constants.mailSubject.append("Customer Usage InHouse Exporter Thread Logs for Legacy Thread : " + Constants.exportTaskName);
            Constants.mailAttachmentExport = Constants.appHome + "Ops_Index_Report_Legacy_" + Constants.dateFrom + "_" + Constants.dateTo + ".xlsx";
        }
        else {
            Constants.fileName = "Customer_Usage_Report_SaaS_" + Constants.dateFrom + "_" + Constants.dateTo + ".xlsx";
            Constants.mailSubject.append("Customer Usage InHouse Exporter Thread Logs for SaaS Thread : " + Constants.exportTaskName);
            Constants.mailAttachmentExport = Constants.appHome + "Ops_Index_Report_SaaS_" + Constants.dateFrom + "_" + Constants.dateTo + ".xlsx";
        }
    }
	
	
	
	
	public static void setupGenericTableDataDate() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        Constants.mailText = null;
        Constants.mailSubject.setLength(0);
        Constants.mailText = new StringBuilder();
        Constants.mailText.append("Thread Executed : " + Constants.exportTaskName + "<br>");
        Constants.mailText.append("<br>Data fetched for dates : " + Constants.dateFrom + " TO " + Constants.dateTo);
        dateFormat = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
        Constants.mailText.append(" Thread Start time : " + dateFormat.format(date));
        Constants.fileName = "Generic_Table_record_" + Constants.dateFrom + "_" + Constants.dateTo + ".xls";
        if (Constants.version.equals("Legacy")) {
            Constants.fileName = "Generic_Table_record_" + Constants.dateFrom + "_" + Constants.dateTo + ".xlsx";
            Constants.mailSubject.append("Generic_Table_record InHouse Exporter Thread Logs for Legacy Thread : " + Constants.exportTaskName);
            Constants.mailAttachmentExport = Constants.appHome + "Generic_Table_record_Legacy_" + Constants.dateFrom + "_" + Constants.dateTo + ".xlsx";
        }
        else {
            Constants.fileName = "Generic_Table_record_SaaS_" + Constants.dateFrom + "_" + Constants.dateTo + ".xlsx";
            Constants.mailSubject.append("Lumin Leads Report Exporter Thread Logs for SaaS Thread : " + Constants.exportTaskName);
            Constants.mailAttachmentExport = Constants.appHome + "Generic_Table_record_SaaS_" + Constants.dateFrom + "_" + Constants.dateTo + ".xlsx";
        }
    }
	
	public static void setupLuminDateFromDateTo() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        Constants.mailText = null;
        Constants.mailSubject.setLength(0);
        Constants.mailText = new StringBuilder();
        Constants.mailText.append("Thread Executed : " + Constants.exportTaskName + "<br>");
        Constants.mailText.append("<br>Data fetched for dates : " + Constants.dateFrom + " TO " + Constants.dateTo);
        dateFormat = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
        Constants.mailText.append(" Thread Start time : " + dateFormat.format(date));
        Constants.fileName = "Lumin_Leads_Report_" + Constants.dateFrom + "_" + Constants.dateTo + ".xls";
        if (Constants.version.equals("Legacy")) {
            Constants.fileName = "Lumin_Leads_Report_Legacy_" + Constants.dateFrom + "_" + Constants.dateTo + ".xlsx";
            Constants.mailSubject.append("Lumin Leads Report InHouse Exporter Thread Logs for Legacy Thread : " + Constants.exportTaskName);
            Constants.mailAttachmentExport = Constants.appHome + "Lumin_Leads_Report_Legacy_" + Constants.dateFrom + "_" + Constants.dateTo + ".xlsx";
        }
        else {
            Constants.fileName = "Lumin_Leads_Report_SaaS_" + Constants.dateFrom + "_" + Constants.dateTo + ".xlsx";
            Constants.mailSubject.append("Lumin Leads Report Exporter Thread Logs for SaaS Thread : " + Constants.exportTaskName);
            Constants.mailAttachmentExport = Constants.appHome + "Lumin_Leads_Report_SaaS_" + Constants.dateFrom + "_" + Constants.dateTo + ".xlsx";
        }
    }
	
	  public static HashMap<String, ModuleInfo> getPGAttributesInfo(String tableName) {
		    Endpoint dbConnection = null;
		    HashMap<String, ModuleInfo> modulesMap = new HashMap<>();
		    try {
		      dbConnection = DBConnectionProvider.getInstance().getDBConnection("InHouse");
		      StringBuffer query = new StringBuffer();
		      query.append("SELECT MODULE_ID,ATTRIBUTE_ID,QUERY,COLUMN_NAMES,TABLE_NAME,SECONDARY_QUERY,TYPE FROM POSTGRES_ATTRIBUTES WHERE 1=1 ");
		      //query.append(" AND TYPE ='" + Constants.exportType + "'");
		      if (tableName != null && !"NA".equals(tableName))
		        query.append(" AND TABLE_NAME='" + tableName + "'"); 
		      if (Constants.moduleIds != null && !"NA".equals(Constants.moduleIds) && 
		        !"100".equals(Constants.moduleIds))
		        query.append(" AND MODULE_ID IN (" + Constants.moduleIds + ")"); 
		      if (Constants.attributeIds != null && !"NA".equals(Constants.attributeIds))
		        query.append(" AND ATTRIBUTE_ID IN (" + Constants.attributeIds + ")"); 
		      HashMap<String, Object> results = QueryUtil.executeQuery(dbConnection, query.toString());
		      boolean isFailed = ((Boolean)results.get("IS_FAILED")).booleanValue();
		      if (!isFailed) {
		        Set<String> cols = (Set<String>)results.get("COLUMN_NAMES");
		        ArrayList<DynaBean> dataBeans = (ArrayList<DynaBean>)results.get("DATA");
		        if (dataBeans != null && cols != null) {
		          Iterator<DynaBean> dataItr = dataBeans.iterator();
		          while (dataItr.hasNext()) {
		            DynaBean bean = dataItr.next();
		            String moduleId = (String)bean.get("MODULE_ID");
		            ModuleInfo modInfo = null;
		            if (modulesMap.containsKey(moduleId)) {
		              modInfo = modulesMap.get(moduleId);
		            } else {
		              modInfo = new ModuleInfo();
		              modulesMap.put(moduleId, modInfo);
		            } 
		            modInfo.setModuleId(moduleId);
		            String attributeId = (String)bean.get("ATTRIBUTE_ID");
		            String primaryQuery = (String)bean.get("QUERY");
		            String secondaryQuery = (String)bean.get("SECONDARY_QUERY");
		            Map<String, String> qMap = new HashMap<>();
		            qMap.put("QUERY", primaryQuery);
		            qMap.put("SECONDARY_QUERY", secondaryQuery);
		            modInfo.getQueriesMap().put(attributeId, qMap);
		            System.out.println("Primary Query : " + primaryQuery);
		            System.out.println("Secondary Query : " + secondaryQuery);
		            qMap.put("TYPE", (String)bean.get("TYPE"));
		            qMap.put("COLUMN_NAMES", (String)bean.get("COLUMN_NAMES"));
		            modInfo.addColumns((String)bean.get("COLUMN_NAMES"));
		            modInfo.setTableName((String)bean.get("TABLE_NAME"));
		          } 
		        } 
		      } 
		    } catch (Exception ex) {
		      ex.printStackTrace();
		    } 
		    return modulesMap;
		  }
}
