package com.app.dataProcessor;

import java.util.List;

import org.apache.camel.Endpoint;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;

public class FinDataProcessor implements DataProcessor {

	@Override
	public ArrayList<HashMap<String, Object>> postProcessing(ArrayList<HashMap<String, Object>> data) {
		// TODO Auto-generated method stub
		List<HashMap<String, Object>> list=data;
		Iterator<HashMap<String,Object>> it=list.iterator();
		Iterator<String> itCategory =null;
		HashMap<String,List<HashMap<String, Object>>> mapToList =new HashMap<>();
		HashSet<Integer> recordToDeleteIndex=new HashSet<>();
		int i=0;
		try {
		while(it.hasNext())
		{
			HashMap<String, Object> item=it.next();
			if(mapToList.get(item.get("CATEGORY"))==null)
			{
				List <HashMap<String, Object>> subCategoryList=new ArrayList<>();
				subCategoryList.add(item);
				mapToList.put((String)item.get("CATEGORY"),subCategoryList);
			}
			else
			{
				List <HashMap<String, Object>> subCategoryList=mapToList.get(item.get("CATEGORY"));
				subCategoryList.add(item);
			}
			
			i++;
		}
	
		System.out.println("####### "+mapToList);
		itCategory=mapToList.keySet().iterator();
		while(itCategory.hasNext())
		{
			String category=itCategory.next();
			//System.out.println("finData------------->"+category);
			if(mapToList.get(category).size()>1) // if more than one rows for a category
			{
				List<HashMap<String, Object>> rowData = mapToList.get(category);
				//System.out.println("finData------------->"+rowData);
				for(HashMap<String, Object> subCategoryData: rowData)
				{
					if(category.equals(subCategoryData.get("SUBCATEGORY")))
					{
						System.out.println("removed>>>>"+subCategoryData);
						data.remove(subCategoryData);
						break;
					}
						
				}
				/*
				 rowData.stream()
				.filter((s)->( (String) s).split("####")[1]==category)
				.map((s)->((String)s).split("####")[0])
				.forEach((s)->data.remove(s));*/
				//String[] categoryData=rowData.
			}
		}
		
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		
		return data;
	}

	@Override
	public ArrayList<HashMap<String, Object>> postProcessing(ArrayList<HashMap<String, Object>> data,
			Endpoint dbConnection) {
		// TODO Auto-generated method stub
		return postProcessing(data);
	}

	@Override
	public Object postProcessing(Object data, Endpoint dbConnection) {
		// TODO Auto-generated method stub
		return null;
	}

}
