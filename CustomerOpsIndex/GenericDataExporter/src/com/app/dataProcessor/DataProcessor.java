package com.app.dataProcessor;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.camel.Endpoint;


public interface DataProcessor {
	
	public ArrayList<HashMap<String,Object>> postProcessing(ArrayList<HashMap<String,Object>> data);
	public ArrayList<HashMap<String,Object>> postProcessing(ArrayList<HashMap<String,Object>> data, Endpoint dbConnection);
	public Object postProcessing(Object data, Endpoint dbConnection);

}
