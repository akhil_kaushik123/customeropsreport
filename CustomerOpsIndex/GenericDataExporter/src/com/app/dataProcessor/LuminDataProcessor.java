package com.app.dataProcessor;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.camel.Endpoint;
import org.apache.commons.beanutils.DynaBean;

import com.app.base.Constants;
import com.app.util.QueryUtil;

public class LuminDataProcessor implements DataProcessor {


	@Override
	public ArrayList<HashMap<String, Object>> postProcessing(ArrayList<HashMap<String, Object>> data,
			Endpoint dbConnection) {
		SimpleDateFormat sdtf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat usDate = new SimpleDateFormat("MM/dd/yyyy");
		SimpleDateFormat usTime = new SimpleDateFormat("hh:mm:ss aa");
		// TODO Auto-generated method stub
			Iterator<DynaBean> iterator = null;
		String UsersQuery = "SELECT USER_NO, CONCAT(USER_FIRST_NAME,' ',USER_LAST_NAME) USER_NAME FROM USERS";
		HashMap<String,Object> usersMap = null;
		HashMap<String,String> clientUsers = new HashMap<>();
		usersMap = QueryUtil.executeQuery(dbConnection, UsersQuery);
		ArrayList<DynaBean> usersList = (ArrayList<DynaBean>) usersMap.get("DATA");
		iterator = usersList.iterator();
		while(iterator.hasNext())
		{
			DynaBean userRecord = iterator.next();
			clientUsers.put((String)userRecord.get("USER_NO"), (String)userRecord.get("USER_NAME"));
		}
		
		String StatusQuery = "SELECT CALL_STATUS_ID, CALL_STATUS_NAME FROM CALL_STATUS";
		HashMap<String,Object> statusMap = null;
		HashMap<String,String> callStatus = new HashMap<>();
		statusMap = QueryUtil.executeQuery(dbConnection, StatusQuery);
		ArrayList<DynaBean> statusList = (ArrayList<DynaBean>) statusMap.get("DATA");
		iterator = statusList.iterator();
		while(iterator.hasNext())
		{
			DynaBean statusRecord = iterator.next();
			callStatus.put((String)statusRecord.get("CALL_STATUS_ID"), (String)statusRecord.get("CALL_STATUS_NAME"));
		}
		
		String callTypeQuery = "SELECT CALL_TYPE_ID, CALL_TYPE_NAME FROM CALL_TYPE";
		HashMap<String,Object> callTypeMap = null;
		HashMap<String,String> callType = new HashMap<>();
		callTypeMap = QueryUtil.executeQuery(dbConnection, callTypeQuery);
		ArrayList<DynaBean> callTypeList = (ArrayList<DynaBean>) callTypeMap.get("DATA");
		iterator = callTypeList.iterator();
		while(iterator.hasNext())
		{
			DynaBean callTypeRecord = iterator.next();
			callType.put((String)callTypeRecord.get("CALL_TYPE_ID"), (String)callTypeRecord.get("CALL_TYPE_NAME"));
		}
		HashMap<String,Object> results = null;
		HashMap<String,HashMap<String,ArrayList<HashMap<String,String>>>> leadRecords = new HashMap<>();
		
		String deletedLeadsQuery = "SELECT TABLE_NAME, COLUMN_NAME, COLUMN_VALUE,RECORD_ID FROM DELETED_RECORDS_LOGS WHERE MODULE_NAME='Franchise Sales' AND DATE(DELETION_DATE)>='"+Constants.dateFrom+"'";
		
		results = QueryUtil.executeQuery(dbConnection, deletedLeadsQuery);
		
		if(!(boolean)results.get("IS_FAILED")!=false)
		{
			Set<String> cols = (Set<String>)results.get("COLUMN_NAMES");
			ArrayList<DynaBean> dataBeans = (ArrayList<DynaBean>) results.get("DATA");
			for(DynaBean databean:dataBeans)
			{
            	String tableName=(String)databean.get("TABLE_NAME");
            	String columnName=(String)databean.get("COLUMN_NAME");
            	String columnValue=(String)databean.get("COLUMN_VALUE");
            	String leadID = (String)databean.get("RECORD_ID");
            	//List<String> tableList=new ArrayList<String>(tableName.split("@@@3@@@"));
            	String[] tableList=tableName.split("@@@3@@@");
            	String[] columnList= columnName.split("@@@3@@@");
            	String[] columnValueList=columnValue.split("@@@3@@@");	
            	HashMap<String,ArrayList<HashMap<String,String>>> tableMap = new HashMap<>();
            	
            	for(int i=0,j=0;i<tableList.length && j<columnValueList.length;i++,j++)
            	{
            		String table=tableList[i];
            		String singleRowColumns = columnList[j];
            		String singleRowData = columnValueList[j];
            		String[] columnSplit = singleRowColumns.split(",");
            		String[] dataSplit = singleRowData.split("######");
            		HashMap<String,String> tableRow = new HashMap<>();
            		for(int k=0;k<dataSplit.length;k++)
            		{
            			tableRow.put(columnSplit[k].trim(), dataSplit[k].replaceAll("^'|'$", ""));
            		
            			
            		}
            		if(tableMap.get(table)==null)
            		{
            			ArrayList<HashMap<String,String>> listSingleTable = new ArrayList<>();
            			listSingleTable.add(tableRow);
            			tableMap.put(table, listSingleTable);
            		}
            		else
            		{
            			ArrayList<HashMap<String,String>> listSingleTable = tableMap.get(table);
            			listSingleTable.add(tableRow);
            		}
            		
            	}
            	leadRecords.put(leadID, tableMap);
            	
            	
            	
			}
			
			Iterator<String> iteratorLeadRecord = null;
			iteratorLeadRecord = leadRecords.keySet().iterator();
			String leadID = null;
			System.out.println("here in processor");
			while(iteratorLeadRecord.hasNext())
			{
				leadID = iteratorLeadRecord.next();
				HashMap<String,ArrayList<HashMap<String,String>>> tableDimension = leadRecords.get(leadID);
				ArrayList<HashMap<String,String>> leadDetails = tableDimension.get("FS_LEAD_DETAILS");
				ArrayList<HashMap<String,String>> callDetails = tableDimension.get("FS_LEAD_CALL");
				HashMap<String,String> leadRecord = leadDetails.get(0);
				try {
				if(leadRecord!=null && callDetails!=null)
				{
					ArrayList<HashMap<String,String>> cancelledCalls = new ArrayList<>();
					for(HashMap<String,String> callRowData : callDetails)
					{
						//System.out.println("callrowdata::::"+callRowData+" "+callRowData.get("CALL_TYPE")+" "+callRowData.get("COMMENTS")+" "+leadRecord.get("EMAIL_ID")+" "+callRowData.get("DATE"));
						//System.out.println(" "+sdtf.parse(callRowData.get("DATE").replace(".0", ""))+"  "+sdtf.parse(Constants.dateFrom+" 00:00:00"));
						//System.out.println();
							if(callRowData.get("SUBJECT").toLowerCase().startsWith("canceled intro call") && callRowData.get("LOGGED_BY_ID")=="2")
							{
								cancelledCalls.add(callRowData);
								
								
							}
						} 
					
					
					for(HashMap<String,String> callRowData : callDetails)
					{
						System.out.println("callrowdata::::"+callRowData+" "+callRowData.get("CALL_TYPE")+" "+callRowData.get("COMMENTS")+" "+leadRecord.get("EMAIL_ID")+" "+callRowData.get("DATE"));
						System.out.println(" "+sdtf.parse(callRowData.get("DATE").replace(".0", ""))+"  "+sdtf.parse(Constants.dateFrom+" 00:00:00"));
						//System.out.println();
							if(sdtf.parse(callRowData.get("DATE").replace(".0", "")).after(sdtf.parse(Constants.dateFrom+" 00:00:00")) && sdtf.parse(callRowData.get("DATE").replace(".0", "")).before(sdtf.parse(Constants.dateTo+" 23:59:59")) 
									&& (callType.get(callRowData.get("CALL_TYPE")) !=null) && leadRecord.get("EMAIL_ID").indexOf("franconnect.com")==-1 && leadRecord.get("EMAIL_ID").indexOf("lumin.ai")==-1 && callRowData.get("COMMENTS").toLowerCase().startsWith("scheduled for "))
							{
								boolean callCancelledAfter24hrs =false, callNotCancelled = true;
								
								Date callDateSystem = sdtf.parse(callRowData.get("DATE").replace(".0", ""));
								long diff = 0l;
								long diffHours=0l;
								for(HashMap<String,String> cancelledCall:cancelledCalls)
								{
									Date cancelledCallDate = sdtf.parse(cancelledCall.get("DATE").replace(".0", ""));
									if(cancelledCallDate.after(callDateSystem))
									{
										callNotCancelled = false;
										 diff = cancelledCallDate.getTime() - callDateSystem.getTime();
										 diffHours = diff / (60 * 60 * 1000);
										
										if(diffHours>=24)
											callCancelledAfter24hrs=true;
										break;
									}
								}
								if(callNotCancelled || callCancelledAfter24hrs)
								{
								HashMap<String,Object> newLeadData=new HashMap<>();
								String callDate= usDate.format(sdtf.parse(callRowData.get("DATE").replace(".0", "")));
								String callTime = usTime.format(sdtf.parse(callRowData.get("DATE").replace(".0", "")));
								newLeadData.put("CALL_DATE",callDate);
								newLeadData.put("TIME", callTime);
								newLeadData.put("USER_NO", callRowData.get("LOGGED_BY_ID"));
								newLeadData.put("FIRST_NAME", leadRecord.get("FIRST_NAME"));
								newLeadData.put("LAST_NAME", leadRecord.get("LAST_NAME"));
								newLeadData.put("COMMENTS", callRowData.get("COMMENTS"));
								newLeadData.put("SUBJECT", callRowData.get("SUBJECT"));
								newLeadData.put("LOGGED_BY", clientUsers.get(callRowData.get("LOGGED_BY_ID")));
								newLeadData.put("CALL_TYPE_NAME",callType.get(callRowData.get("CALL_TYPE")));
								newLeadData.put("CALL_STATUS_NAME",callStatus.get(callRowData.get("CALL_STATUS")));
								newLeadData.put("TYPE", "Deleted");
								if(callCancelledAfter24hrs)
								newLeadData.put("CANCEL_STATUS", diffHours+"");
								else
									newLeadData.put("CANCEL_STATUS", "");	
								System.out.println(newLeadData);
								
							
							data.add(newLeadData);
								}
								}
						} 
					}
				}
				catch (Exception e) {
					
					e.printStackTrace();
				}
				
			}
			
			
			
			
			
		}
		return data;
	}

	@Override
	public ArrayList<HashMap<String, Object>> postProcessing(ArrayList<HashMap<String, Object>> data) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object postProcessing(Object data, Endpoint dbConnection) {
		// TODO Auto-generated method stub
		return null;
	}

}
