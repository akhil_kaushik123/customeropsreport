/*
 * (FCSKYS-15522) : Bhavna Chugh : Inhouse Billing : FileName Change
 *  FCSKYS-15522  | Implemented Billing Changes
 FCSKYS-18245 | Automate MUID Reports
 FCSKYS-18302 | Users Fetching based on their Roles 
 FCSKYS-18301 | Merging of users
 */
package com.app.main;

import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;

import com.app.base.ClientInfo;
import com.app.base.Constants;
import com.app.base.ModuleInfo;
import com.app.helper.DBConnectionProvider;
import com.app.helper.ExecutorHelper;
import com.app.util.DataFactory;
import com.app.util.MailSender;
import com.app.util.Utility;

public class MainExecutor 
{
	static DBConnectionProvider connectionProvider = null;
	static LinkedHashMap<String,ClientInfo> clients = null;
	static HashMap<String,String> clientModulesMap  =  null;
	
	public static void main(String args[])
	{	
		try 
		{	
			Constants.initialize(args);
			PrintStream sOut = System.out;
		    PrintStream out = Utility.setOutputToLogFile();
			System.setOut(out);
			
			
			System.out.println("Data Exporter Menu\n1. System and CAL Billing Data\n2. Module Usage Data\n3. AVG_Logins Data\n4. Custom Queries \n5. Merge System Usage \n6. Merge Module Usage");
			DBConnectionProvider.getInstance().initialize();
			clients = ExecutorHelper.setupResources();
			System.out.println("+++++++++++++++++"+clients);
			if(!"GENERICDATA".equals(Constants.exportType))
			clientModulesMap = ExecutorHelper.getClientModulesMap(); //preparing client modules map (customer code vales , Module id's)
			//User Billing New Changes start
			if("SUEXPORT".equals(Constants.exportType) || "SUBILLING".equals(Constants.exportType) || "SpecialReportExport".equals(Constants.exportType) || "SpecialReport".equals(Constants.exportType))
			{
				ExecutorHelper.setupDateFromDateTo();
				
		    }
            if ("MUEXPORT".equals(Constants.exportType) || "WUEXPORT".equals(Constants.exportType)) {
                ExecutorHelper.setupMUDateFromDateTo();
            }
            if ("LUMINEXPORT".equals(Constants.exportType)) {
                ExecutorHelper.setupLuminDateFromDateTo();
            }
            if ("GENERICDATA".equals(Constants.exportType)) {
                ExecutorHelper.setupGenericTableDataDate();
            }
            
            
			if("SUBILLING".equals(Constants.exportType))
			{
			ExecutorHelper.removeOldDataForSUBILLING();
			
			}
			//User Billing New Changes end
			
			//remove earlier existing data in case of weekly scheduled threads....
			if(!Constants.isUpdateThread)
			{
				ExecutorHelper.removeOldData();
			}
			
			//User Billing New Changes start
			if("SUEXPORT".equals(Constants.exportType))
			{
				com.app.util.SystemUsageUtilityExport.genrateSUExportFile(clients,Constants.mailAttachmentExport);
			}
			//User Billing New Changes end
            if ("MUEXPORT".equals(Constants.exportType) || "WUEXPORT".equals(Constants.exportType)) {
            	com.app.util.ModuleUsageUtilityExport.genrateMUExportFile(Constants.mailAttachmentExport);
            }
            if ( "LUMINEXPORT".equals(Constants.exportType)) {
            	com.app.util.ModuleUsageUtilityExport.genrateLuminExportFile(Constants.mailAttachmentExport);
            }
            
            if ( "GENERICDATA".equals(Constants.exportType)) {
            	com.app.util.ModuleUsageUtilityExport.genrateGenericTableDataRecordFile(Constants.mailAttachmentExport,clients);
            }
            
            
			DataFactory dataFactory = new DataFactory();
			//User Billing New Changes start
			if("SUBILLING".equals(Constants.exportType))
			{
				dataFactory.prepareDataUserBilling(clients);	
			}
			if("EUGenerate".equals(Constants.exportType))
			{
				ExecutorHelper.removeOldEUData();
				dataFactory.prepareDataEUUser(clients);	
			}
			if("SpecialReport".equals(Constants.exportType))
			{
				ExecutorHelper.removeOldDataForSUBILLING();
				dataFactory.prepareDataUserBilling(clients);
			}
			if("SpecialReportExport".equals(Constants.exportType))
			{
				com.app.util.SystemUsageUtilityExport.genrateSUSpecialExportFile(clients,Constants.mailAttachmentExport);
			}
			//User Billing New Changes end
			//prepare report
			else if("SU".equals(Constants.exportType))
			{
				dataFactory.prepareData(clients);
			}
			else if("MU".equals(Constants.exportType) || "AU".equals(Constants.exportType) || "CD".equals(Constants.exportType) || "'CD','HD'".equals(Constants.exportType))
			{	
				System.out.println("got here");
				if(Constants.tableNames!=null && Constants.tableNames.length>0)
				{
					for(String tableName : Constants.tableNames)
					{
						HashMap<String,ModuleInfo> modulesQueryMap = ExecutorHelper.getModulesInfo(tableName);
						dataFactory.prepareData(clients,clientModulesMap,modulesQueryMap);
					}
				}
			}else if ("EAVDATA".equals(Constants.exportType)) {
				Arrays.stream(Constants.tableNames).forEach(System.out::println);
		        if (Constants.tableNames != null && Constants.tableNames.length > 0)
		            for (String tableName : Constants.tableNames) {
		              HashMap<String, ModuleInfo> modulesQueryMap = ExecutorHelper.getPGAttributesInfo(tableName);
		              dataFactory.preparePGData(clients, clientModulesMap, modulesQueryMap);
		            }  
		        }
			//merge FC and ZC data in case of CAL and Module Usage data
			else if("MSU".equals(Constants.exportType) || "MMU".equals(Constants.exportType) && !Constants.isUpdateThread)
			{
				ExecutorHelper.mergeData();
			}
			Constants.mailText.append("<br> Database Not connected for following clients : "+Constants.clNotConnected);
			Constants.mailText.append("<br> Either codevalue din't matched or there is no entry for modules for following builds in InHouse : "+Constants.clCVNotMatched);
			DBConnectionProvider.getInstance().cleanUpAllResources();
			System.setOut(sOut);
			Constants.mailText.append("Thread End Time : "+ (new SimpleDateFormat("dd/MM/yy HH:mm:ss")).format(new Date()));
			MailSender.getInstance().sendMail(Constants.mailSubject.toString(), Constants.mailText.toString(), Constants.mailAttachment);
			System.out.println(" Execution completed : "+System.currentTimeMillis());
			//User Billing New Changes start
			if("SUEXPORT".equals(Constants.exportType) || "SpecialReportExport".equals(Constants.exportType) || "MUEXPORT".equals(Constants.exportType) || "WUEXPORT".equals(Constants.exportType) || "LUMINEXPORT".equals(Constants.exportType) || "GENERICDATA".equals(Constants.exportType))
			{
				Constants.mailSubject.setLength(0);
				 String[] monthName = {"January", "February",
			                "March", "April", "May", "June", "July",
			                "August", "September", "October", "November",
			                "December"};
				    Calendar cal = Calendar.getInstance();
			        String month = monthName[cal.get(Calendar.MONTH)];
				if(Constants.version.equals("Legacy")){
					if("SpecialReportExport".equals(Constants.exportType)){
						Constants.mailSubject.append("Special Billing Report of Legacy Clients of "+month+" "+java.time.LocalDateTime.now().getYear() );
					}else if ("MUEXPORT".equals(Constants.exportType)) {
                        Constants.mailSubject.append("Customer Usage Report of Legacy Clients of " + month + " "+java.time.LocalDateTime.now().getYear() );
                    }else if ("WUEXPORT".equals(Constants.exportType)) {
                        Constants.mailSubject.append("Customer Usage Report of Legacy Clients for period " + Constants.dateFrom + " to "+Constants.dateTo);
                    }
                    else if ("LUMINEXPORT".equals(Constants.exportType)) {
                        Constants.mailSubject.append("Lumin Leads Report of Legacy Clients for period " + Constants.dateFrom + " to "+Constants.dateTo);
                    }
                    else if ("GENERICDATA".equals(Constants.exportType)) {
                        Constants.mailSubject.append("Generic Table Records of Legacy Clients for period " + Constants.dateFrom + " to "+Constants.dateTo);
                    }
                    else{
						Constants.mailSubject.append("Billing Report of Legacy Clients of "+month+" "+java.time.LocalDateTime.now().getYear() );
					}
				}else{
					if("SpecialReportExport".equals(Constants.exportType)){
					Constants.mailSubject.append("Special Billing Report of SaaS Clients of "+month+" "+java.time.LocalDateTime.now().getYear() );
					}else if ("MUEXPORT".equals(Constants.exportType)) {
	                    Constants.mailSubject.append("Customer Usage Report of SaaS Clients of " + month + " "+java.time.LocalDateTime.now().getYear() );
	                }else if ("WUEXPORT".equals(Constants.exportType)) {
                        Constants.mailSubject.append("Customer Usage Report of SaaS Clients for period " + Constants.dateFrom + " to "+Constants.dateTo);
                    }
	                else if ("LUMINEXPORT".equals(Constants.exportType)) {
                        Constants.mailSubject.append("Lumin Leads Report of SaaS Clients for period " + Constants.dateFrom + " to "+Constants.dateTo);
                    }
	                else if ("GENERICDATA".equals(Constants.exportType)) {
                        Constants.mailSubject.append("Generic Table Records of SaaS Clients for period " + Constants.dateFrom + " to "+Constants.dateTo);
                    }
	                else{
						Constants.mailSubject.append("Billing Report of SaaS Clients of "+month+" "+java.time.LocalDateTime.now().getYear());
					}
				}
				String exportMailContent="Hi team,<br><br>Please find the attached file containing automatic billing sheet of  "+Constants.version+" customers of "+month+" "+java.time.LocalDateTime.now().getYear() +".<br><br>Regards,<br>FranConnect";
                if ("MUEXPORT".equals(Constants.exportType)) {
                    exportMailContent = "Hi team,<br><br>Please find the attached file containing automatic Customer Usage sheet of  " + Constants.version + " customers of " + month + " " + java.time.LocalDateTime.now().getYear() + ".<br><br>Regards,<br>FranConnect";
                    MailSender.getInstance().sendCustomerUsageMail(Constants.mailSubject.toString(), exportMailContent, Constants.mailAttachmentExport, Constants.fileName);
                }else if ("WUEXPORT".equals(Constants.exportType)) {
                    exportMailContent = "Hi team,<br><br>Please find the attached file containing automatic Customer Usage sheet of  " + Constants.version + " customers for period " + Constants.dateFrom + " to "+Constants.dateTo + ".<br><br>Regards,<br>FranConnect";
                    MailSender.getInstance().sendCustomerUsageMail(Constants.mailSubject.toString(), exportMailContent, Constants.mailAttachmentExport, Constants.fileName);
                }
                else if ("LUMINEXPORT".equals(Constants.exportType)) {
                    exportMailContent = "Hi team,<br><br>Please find the attached file containing automatic Lumin Leads Report sheet of  " + Constants.version + " customers for period " + Constants.dateFrom + " to "+Constants.dateTo + ".<br><br>Regards,<br>FranConnect";
                    MailSender.getInstance().sendCustomerUsageMail(Constants.mailSubject.toString(), exportMailContent, Constants.mailAttachmentExport, Constants.fileName);
                }
                else if ("GENERICDATA".equals(Constants.exportType)) {
                    exportMailContent = "Hi team,<br><br>Please find the attached file containing automatic Generic Table Records sheet of  " + Constants.version + " customers for period " + Constants.dateFrom + " to "+Constants.dateTo + ".<br><br>Regards,<br>FranConnect";
                    MailSender.getInstance().sendCustomerUsageMail(Constants.mailSubject.toString(), exportMailContent, Constants.mailAttachmentExport, Constants.fileName);
                }
                else {
                	MailSender.getInstance().sendMailNew(Constants.mailSubject.toString(),exportMailContent, Constants.mailAttachmentExport,Constants.fileName);	//Change in File Name
                }
			}
			if("N".equals(Constants.executeFor))
			{
				ExecutorHelper.removeBillingClients();
			}
			//User Billing New Changes end
		}
		catch(Exception ex)
		{
			System.err.println("Uncaught exception - " + ex.getMessage());         ex.printStackTrace(System.err); 
			ex.printStackTrace();
		}
		finally{
		}
	}
	
}